function makeZoomOfic() {
    $('.to_zoom').jqzoom({
        zoomType: 'innerzoom',
        zoomWidth: 300,
        zoomHeight: 250,
        title: false
    });
}


function setAnchor() {
    // hide #back-top first
    $("#back-top").hide();
    // fade in #back-top
    $(function () {
        $(window).scroll(function () {
            if ($(this).scrollTop() > 100) {
                $('#back-top').fadeIn();
            } else {
                $('#back-top').fadeOut();
            }
        });

        // scroll body to 0px on click
        $('#back-top, #back-top a').click(function () {
            $('body,html').animate({
                scrollTop: 0
            }, 1500);
            return false;
        });
    });
}

function center_image() {
    $('.center_img').each(function () {
        var obj = $(this);
        var bg_ratio = obj.attr('data-width-img') / obj.attr('data-height-img');
        var wrapper_ratio = obj.parent().width() / obj.parent().height();
        if (bg_ratio < wrapper_ratio) {
            var center = (obj.parent().width() / bg_ratio - obj.parent().height()) * (-0.5);
            obj.css({'left': '0px', 'top': center, 'width': '100%', 'height': 'auto', 'max-width': 'none'});
        }
        else {
            var center_hor = (bg_ratio * obj.parent().height() - obj.parent().width()) * (-0.5);
            obj.css({'left': center_hor, 'top': '0px', 'height': '100%', 'width': 'auto', 'min-width': '980px'});
        }
    });
}

function toggleMenu() {
    var menu = $('.js-toggle-menu'),
        arrow = $('.js-toggle-arrow');

    arrow.on('click', function () {
        menu.toggleClass('hidden_menu');
        if (menu.hasClass('hidden_menu')) {
            menu.animate({left: "-93px"}, 400);
        } else {
            menu.animate({left: "19px"}, 400)
        }
    });
}

;
(function () {

    $(window).load(function () {
        center_image();
        toggleMenu();
    });

    $(window).resize(function () {
        center_image();
    });
})();

$(document).on("click", ".js_register_modal_show", function () {
    $(this).closest(".modal").modal("hide");
    $("#register").modal("show");
    return false;
});