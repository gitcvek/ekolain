<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <!--[if lt IE 9]>
  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
  <meta name="viewport" content="width=device-width">
  <meta http-equiv="content-language" content="ru"> <?php // TODO - в будущем генетить автоматом ?>
<!--<link href='http://fonts.googleapis.com/css?family=Open+Sans&subset=latin,cyrillic-ext,latin-ext,cyrillic' rel='stylesheet' type='text/css'>-->
<!--<link href='http://fonts.googleapis.com/css?family=Didact+Gothic&subset=cyrillic-ext,latin,cyrillic,latin-ext' rel='stylesheet' type='text/css'>-->


  <?php
    //Регистрируем файлы скриптов в <head>
    if (YII_DEBUG) {
      Yii::app()->assetManager->publish(YII_PATH . '/web/js/source', false, -1, true);
    }

    Yii::app()->clientScript->registerCoreScript('jquery');
    Yii::app()->clientScript->registerCoreScript('bootstrap');
    $bootstrapFont = Yii::getPathOfAlias('application.assets.bootstrap.fonts') . DIRECTORY_SEPARATOR;
    Yii::app()->clientScript->addDependResource('bootstrap.min.css', array(
      $bootstrapFont . 'glyphicons-halflings-regular.eot' => '../fonts/',
      $bootstrapFont . 'glyphicons-halflings-regular.svg' => '../fonts/',
	  $bootstrapFont . 'glyphicons-halflings-regular.ttf' => '../fonts/',
      $bootstrapFont . 'glyphicons-halflings-regular.woff' => '../fonts/',

    ));

    Yii::app()->clientScript->registerScriptFile('/themes/business/js/js.js', CClientScript::POS_HEAD);

    Yii::app()->clientScript->registerScript('setScroll', "setAnchor();", CClientScript::POS_READY);
    Yii::app()->clientScript->registerScript('menu.init', "$('.dropdown-toggle').dropdown();", CClientScript::POS_READY);

	
	Yii::app()->clientScript->registerCssFile('/themes/business/css/didactgothic.css');
    Yii::app()->clientScript->registerCssFile('/themes/business/css/content.css');
    Yii::app()->clientScript->registerCssFile('/themes/business/css/page.css');
    Yii::app()->clientScript->registerCssFile('/themes/business/css/bootstrap-overrides.css');
  ?>
  <title><?php echo CHtml::encode($this->getPageTitle()); ?></title>
</head>
<body>
<?php if (Yii::app()->request->getUrl() == '/'): ?>
  <?php $this->widget('ygin.widgets.vitrine.VitrineWidget'); ?>
<?php endif; ?>
<?php
if (Yii::app()->user->hasFlash('paymentError')) {
  $this->widget('AlertWidget', array(
      'title' => 'Оплата',
      'message' => Yii::app()->user->getFlash('paymentError'),
  ));
}
?>
<div id="wrap" class="container-fluid">
  <div id="head" class="row">
    <?php if (Yii::app()->request->url == "/") { ?>
      <div class="logo pull-right"><img border="0" alt="Название компании - На главную"
                                        src="/themes/business/gfx/logo-white.png"></div>
    <?php } else { ?>
      <a href="/"  data-toggle="tooltip" data-placement="bottom" title="Вернуться на главную страницу" class="logo pull-right"><img src="/themes/business/gfx/logo-white.png"></a>
    <?php } ?>
    <div class="cname">
      <img src="/themes/business/gfx/ecoline_n1.png" alt="">
    </div>
    <div class="tright pull-right">
      <div class="numbers">
        <p>телефон службы доставки 202-802</p>

        <p class="mail"><a href="mailto:info@ecoline-komi.ru">info@ecoline-komi.ru</a></p>
		<p class="mail"><a href="http://vk.com/krasnozatonskaya">vk.com/krasnozatonskaya</a></p>
      </div>
    </div>
  </div>

  <?php $this->widget('BlockWidget', array("place" => SiteModule::PLACE_TOP)); ?>

  <?php // + Главный блок ?>
  <div id="main" class="container">

    <div id="container" class="row">
      <?php

        $column1 = 0;
        $column2 = 9;
        $column3 = 0;

        if (Yii::app()->menu->current != null) {
          $column1 = 3;
          $column2 = 6;
          $column3 = 3;

          if (Yii::app()->menu->current->getCountModule(SiteModule::PLACE_LEFT) == 0) {
            $column1 = 0;
            $column3 = 4;
          }
          if (Yii::app()->menu->current->getCountModule(SiteModule::PLACE_RIGHT) == 0) {
            $column3 = 0;
            $column1 = $column1 * 4 / 3;
          }
          $column2 = 12 - $column1 - $column3;
          //if ($column2 == 12) $column2 = 9;
        }

      ?>
      <?php if ($column1 > 0): // левая колонка ?>
        <div id="sidebarLeft" class="col-sm-<?php echo $column1; ?>">
          <?php $this->widget('BlockWidget', array("place" => SiteModule::PLACE_LEFT)); ?>
        </div>
      <?php endif ?>

      <div id="content" class="col-sm-<?php echo $column2; ?>">
        <!--<div class="page-header">
            <h1><?php //echo $this->caption; ?></h1>
          </div>
          
          --><?php if ($this->useBreadcrumbs && isset($this->breadcrumbs)): // Цепочка навигации ?>
          <?php $this->widget('BreadcrumbsWidget', array(
            'homeLink' => array('Главная' => Yii::app()->homeUrl),
            'links' => $this->breadcrumbs,
          )); ?>
        <?php endif ?>

        <div class="cContent">
          <?php $this->widget('BlockWidget', array("place" => SiteModule::PLACE_CONTENT)); ?>
          <?php
            $menuWithCarouselIds = array(
              131,
              115, //
              121, //компания
              122, //продукция
              123, //производство
              132, //акции
              133, //новости
              117, //кулеры и помпы
              131, //новичкам
            );
            if (($current = Yii::app()->menu->getCurrent()) && !in_array($current->id_parent, $menuWithCarouselIds)): ?>
              <?php echo $content; ?>
            <?php endif; ?>
        </div>
        <?php $this->widget('BlockWidget', array("place" => SiteModule::PLACE_BOTTOM)); ?>
        <?php $this->widget('application.widgets.leftMenu.LeftMenuWidget'); ?>
      </div>

      <?php if ($column3 > 0): // левая колонка ?>
        <div id="sidebarRight" class="col-sm-<?php echo $column3; ?>">
          <?php $this->widget('BlockWidget', array("place" => SiteModule::PLACE_RIGHT)); ?>
        </div>
      <?php endif ?>

    </div>
    <?php //Тут возможно какие-нить модули снизу ?>
    <div class="clr"></div>
  </div>
  <?php // - Главный блок ?>

  <div id="back-top"><span>↑</span></div>
</div>
<div id="footer" class="container-fluid">
  <div class="footer_inner row">
    <div class="b-menu-bottom navbar navbar-default" role="navigation">
      <div class="collapse navbar-collapse b-menu-bottom_inner">
        <?php

          if (Yii::app()->hasModule('search')) {
            $this->widget('SearchWidget');
          }
          $this->widget('MenuWidget', array(
            'rootItem' => Yii::app()->menu->all,
            'htmlOptions' => array('class' => 'nav navbar-nav b-menu-bottom_navigation'),
            // корневой ul
            'submenuHtmlOptions' => array('class' => 'dropdown-menu'),
            // все ul кроме корневого
            'activeCssClass' => 'active',
            // активный li
            'activateParents' => 'true',
            // добавлять активность не только для конечного раздела, но и для всех родителей
            //'labelTemplate' => '{label}', // шаблон для подписи
            'labelDropDownTemplate' => '{label} <b class="caret"></b>',
            // шаблон для подписи разделов, которых есть потомки
            //'linkOptions' => array(), // атрибуты для ссылок
            'linkDropDownOptions' => array(
              'data-target' => '#',
              'class' => 'dropdown-toggle',
              'data-toggle' => 'dropdown'
            ),
            // атрибуты для ссылок для разделов, у которых есть потомки
            'linkDropDownOptionsSecondLevel' => array(
              'data-target' => '#',
              'data-toggle' => 'dropdown'
            ),
            // атрибуты для ссылок для разделов, у которых есть потомки
            //'itemOptions' => array(), // атрибуты для li
            'itemDropDownOptions' => array('class' => 'dropdown'),
            // атрибуты для li разделов, у которых есть потомки
            'itemDropDownOptionsSecondLevel' => array('class' => 'dropdown-submenu'),
            //  'itemDropDownOptionsThirdLevel' => array('class' => ''),
            'maxChildLevel' => 0,
            'encodeLabel' => false,
          ));

        ?>
      </div>
    </div>
    <?php $this->widget('application.widgets.cabinetButton.CabinetbuttonWidget'); ?>
    <!--<div class="col-sm-4 logo">
      <img alt="Логотип компании" src="/themes/business/gfx/l0000000.gif">
    </div>-->
    <div class="col-sm-1">
      <?php $this->widget('BlockWidget', array("place" => SiteModule::PLACE_FOOTER)); ?>
    </div>
  </div>
</div>

<?php
  $this->widget('application.modules.order.widgets.OrderWidget');
?>

<?php if (Yii::app()->user->isGuest) {
  $this->widget('application.modules.user.widgets.RegisterWidget');
}?>


<script>
/**** make modal button to first item of left menu ****/
$(document).ready( function(){
	    <?php if (Yii::app()->user->isGuest): ?>
	$('.item.icon-1 a').attr('href','#');
	$('.item.icon-1 a').attr('data-toggle','modal');
	$('.item.icon-1 a').attr('data-target','#BuyWater');
    <?php else: ?>
	$('.item.icon-1 a').attr('href','<?= Yii::app()->createUrl('order/default/index') ?>');
    <?php endif; ?>
});

$(function () {
	  $('[data-toggle="tooltip"]').tooltip()
	})
</script>

</body>
</html>