<? $this->registerJsFile('news.js');?>
<div id = "carousel-example-generic" class = "carousel slide b_actions-carousel" data-ride = "carousel">
    <?php $count = count ($news); ?>
    <div class = "b-action-news carousel-inner" role = "listbox">
        <div class = "item active">
            <?php foreach ($news as $key => $child): ?>
            <?php if( $image = $child->image ): ?>
                <?php if( $preview = $image->getPreview (270,0,'_menu1') ): ?>
                    <a rel="<?=$child->primaryKey;?>" href = "<?=Yii::app()->createUrl('news/news/modalView');?>" class = "text-center">
                        <img alt = "<?=$child->title?>" src = "<?=$preview->getUrlPath ()?>">
                    </a>
                <?php endif; ?>
            <?php endif; ?>
            <?php if ($key!=0 && ($key+1)%3==0 &&  ($key+1)!=$count): ?>
        </div>
        <div class = "item">
            <?php endif; ?>
            <?php endforeach; ?>
        </div>
    </div>
    <?php if( $count>3 ): ?>
        <a class = "left carousel-control" href = "#carousel-example-generic" role = "button" data-slide = "prev">
            <span class = "glyphicon glyphicon-chevron-left" aria-hidden = "true"></span>
            <span class = "sr-only">Previous</span>
        </a>
        <a class = "right carousel-control" href = "#carousel-example-generic" role = "button" data-slide = "next">
            <span class = "glyphicon glyphicon-chevron-right" aria-hidden = "true"></span>
            <span class = "sr-only">Next</span>
        </a>
    <?php endif; ?>
    <?php if( $count>3 ): ?>
        <ol class = "carousel-indicators">
            <?php for( $i = 0;$i<$count/3;$i++ ): ?>
                <li data-target = "#carousel-example-generic" data-slide-to = "<?=$i?>" <?php echo $i==0?'class="active"':''; ?>></li>
            <?php endfor; ?>
        </ol>
    <?php endif; ?>
</div>
