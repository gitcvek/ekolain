<?php $this->breadcrumbs[$current->title] = ''; ?>
<div class = "b-about">
	<?php $count = count ($news) ?>
	<?php if( $count>1 ): ?>
		<?php if( $prev ): ?>
			<div class = "b-about_arrow left">
				<a href = "<?=$this->createUrl ('index',array(
					'idCategory' => $category->primaryKey,
					'idNews' => $prev->primaryKey,
				))?>"><span class = "glyphicon glyphicon-chevron-left"></span></a>
			</div>
		<?php endif; ?>
		<?php if( $next ): ?>
			<div class = "b-about_arrow right">
				<a href = "<?=$this->createUrl ('index',array(
					'idCategory' => $category->primaryKey,
					'idNews' => $next->primaryKey,
				))?>"><span class = "glyphicon glyphicon-chevron-right"></span></a>
			</div>
		<?php endif; ?>
	<?php endif; ?>
	<?php if( $image = $current->image ): ?>
		<?php if( $preview = $image->getPreview (600,0,'_menu1') ): ?>
			<div class = "b-about_inner text-center">
				<img src = "<?=$preview->getUrlPath ()?>" alt = "<?=$current->title?>"/>
			</div>
		<?php endif; ?>
	<?php endif; ?>

	<?php if( $count>1 ): ?>
		<div class = "b-about_dots">
			<?php foreach( array_reverse($news) as $sibling ): ?>
				<a class = "dot <?php echo $sibling->primaryKey==$current->primaryKey?'active':''; ?>"
				   href = "<?=$this->createUrl ('index',array(
					   'idCategory' => $category->primaryKey,
					   'idNews' => $sibling->primaryKey,
				   ))?>"></a>
			<?php endforeach; ?>
		</div>
	<?php endif; ?>
	<div class = "b-about_text">
		<p>
			<?=$current->content?>
		</p>
		<?php if($category->primaryKey != 3) { ?>
		<a class="text-left" href="/news/category/3/">Архив новостей</a>
		<?php } ?>
	</div>

</div>
