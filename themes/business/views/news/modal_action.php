<div class="modal fade b-single-action">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="e-action-close close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><?=$model->title;?></h4>
      </div>
      <div class="modal-body"><?=$model->content;?></div>
      <div class="modal-footer">
        <button type="button" class="e-action-close btn btn-default" data-dismiss="modal">Закрыть</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->