function cNewsCategoryBind(){
  $('.b-news-category').buttonset()
    .find('a.active').addClass('ui-button-disabled ui-state-disabled').click(function(){return false});
}

$(document).ready(function(){
    $('.b-action-news .item a').bind('click', function(){
        $this = $(this);
        var id = $this.attr('rel');
        $.get($this.attr('href'), {'id':id}, function(result){
            $('body').append(result);
            $('.b-single-action').modal({backdrop:'static'});
        });
        return false;
    });
    $('body').on('click', '.e-action-close', function(){
        $('.b-single-action,.modal-backdrop').remove();
    });

})