<?php
  /**
   * Created by PhpStorm.
   * User: cranky4
   * Date: 03.12.14
   * Time: 12:08
   *
   * @var PhizRegForm $phizForm
   * @var YurRegForm $yurForm
   */

  $this->pageTitle = "Регистрация";
  $this->caption = "Регистрация";
?>

<div class="b-registration">
  <div>
    <h3>Регистрация</h3>
    <!-- Nav tabs -->
<!--    <ul class="nav nav-tabs" role="tablist">-->
<!--      <li role="presentation" class="active">-->
<!--        <a href="#phiz_reg" aria-controls="home" role="tab" data-toggle="tab">Физическое лицо</a>-->
<!--      </li>-->
<!--      <li role="presentation">-->
<!--        <a href="#yur_reg" aria-controls="profile" role="tab" data-toggle="tab">Юридическое лицо</a>-->
<!--      </li>-->
<!--    </ul>-->

    <!-- Tab panes -->
    <div class="tab-content">
      <div role="tabpanel" class="tab-pane active" id="phiz_reg">
        <?php $this->renderPartial('application.modules.user.widgets.views._phizForm', array(
          //'convinientTimes' => $convinientTimes,
          'model' => $phizForm
        ));?>
      </div>
<!--      <div role="tabpanel" class="tab-pane" id="yur_reg">-->
<!--        --><?php //$this->renderPartial('application.modules.user.widgets.views._yurForm', array(
//          'convinientTimes' => $convinientTimes,
//          'model' => $yurForm
//        ));?>
<!--      </div>-->
    </div>

  </div>
</div>