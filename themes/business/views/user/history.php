<?php
  /**
   * Created by PhpStorm.
   * User: cranky4
   * Date: 28.01.15
   * Time: 16:43
   *
   * @var PCabinetController $this
   * @var FOrders $order
   * @var Water $water
   * @var FClients $client
   */

  $this->caption = 'История заказов';
  $this->pageTitle = 'История заказов';
?>

<div class="b-profile-order-history">
  <div class="client-info">
    <h3>Данные о клиенте</h3>
    <dl class="dl-horizontal">
      <dt>
        Абонентский
        номер
      </dt>
      <dd>
        <?php echo $client->ab_number?>
      </dd>
      <dt>
        ФИО
      </dt>
      <dd>
        <?php echo $client->ab_number?>
      </dd>
      <dt>
        Адрес
      </dt>
      <dd>
        <?php echo $client->address?>
      </dd>
      <dt>
        Телефон
      </dt>
      <dd>
        <?php echo $client->phone?>
      </dd>
      <dt>
        Email
      </dt>
      <dd>
        <?php echo $client->mail?>
      </dd>
      <dd>
        <?php echo $client->phone?>
      </dd>
      <dt>
        Ваши баллы
      </dt>
      <dd>
        <?php echo $client->points?>
      </dd>

    </dl>

  </div>
  <table class="table table-condensed">
    <thead>
    <tr>
      <th>
       Номер заказа
      </th>
      <th>
        Дата
      </th>
      <th>
        Наименование
        товара
      </th>
      <th>
        Кол-во
      </th>
      <th>
        Цена
      </th>
      <th>
        Сумма
      </th>
      <th>
        Баллы
      </th>
      <th>
        Статус
      </th>
    </tr>
    </thead>
    <?php foreach ($orders as $order): ?>
      <tr class="<?php switch($order->status) {
        case 1 : echo 'active';
              break;
        case 2 : echo 'info';
          break;
        case 3 : echo 'success';
          break;
        case 4 : echo 'warning';
          break;
      }
       ?>">
        <td>
          <?php echo $order->id_orders?>
          <br>
          <a href="<?=Yii::app()->createUrl('user/cabinet/view/',array('order_id' => $order->id_orders));?>">Страница заказа</a>
        </td>
        <td>
          <?php echo $order->date_order?>
        </td>
        <td>
          <?php echo $order->product?>
        </td>
        <td>
          <?php echo $order->count?>
        </td>
        <td>
          <?php echo $order->price?>
        </td>
        <td>
          <?php echo (int)$order->count*(int)$order->price?>
        </td>
        <td>
          <?php echo (int)$order->points?>
        </td>
        <td>
         <!-- <a href="<?/*=Yii::app()->createUrl('pCabinet/orderView',array('order_id' => $order->id_orders));*/?>">Страница заказа</a>-->
          <?php if($order->status==2): ?>
          <a class="btn btn-primary" href="<?=Yii::app()->createUrl('payment/register/',array('order_id' => $order->id_orders));?>">Оплатить</a>
          <?php else : ?>
            <?php
            switch($order->status) {
              case 1 :
                echo 'Новый';
                break;
              case 3 :
                echo 'Оплачен';
                break;
              case 4 :
                echo 'Выполнен';
                break;
              case 5 :
                echo 'Отменён';
                break;
            }
              ?>
          <?php endif;?>
        </td>
      </tr>
    <?php endforeach; ?>
  </table>

  <?php $this->widget('CLinkPager', array(
      'pages' => $pages,
  )) ?>
</div>