<?php
  /**
   * Created by PhpStorm.
   * User: cranky4
   * Date: 28.01.15
   * Time: 16:43
   *
   * @var PCabinetController $this
   * @var Order $order
   * @var Water $water
   */

  $this->caption = 'История заказов';
  $this->pageTitle = 'История заказов';
?>

<div class="b-profile-order-history">
  <table class="table table-condensed">
    <thead>
    <tr>
      <th>
        № заказа
      </th>
      <th>
        Заказ
      </th>
      <th>
        Дата заказа
      </th>
      <th>
        Кол-во
      </th>
      <th>
        Цена
      </th>
      <th>
        Статус заказа
      </th>
    </tr>
    </thead>
    <?php foreach ($orders as $order): ?>
      <tr>
        <td>
          <?php echo $order->primaryKey?>
        </td>
        <td>
          <?php echo $order->product?>
        </td>
        <td>
          <?php echo $order->date_order?>
        </td>
        <td>
          <?php echo $order->count?>
        </td>
        <td>
          <?php echo $order->price?>
        </td>
        <td>
          <?php echo $order->status?>
        </td>
      </tr>
    <?php endforeach; ?>
  </table>

  <?php $this->widget('CLinkPager', array(
      'pages' => $pages,
  )) ?>
</div>