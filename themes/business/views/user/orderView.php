<?php

$this->caption = 'Заказ';
$this->pageTitle = 'Заказ';
?>

<dl class="dl-horizontal">
    <dt>
            Номер заказа
        </dt>
    <dd>
        <?php echo $order[0]->id_orders?>
    </dd>
        <dt>
            Дата
        </dt>
    <dd>
        <?php echo $order[0]->date_order?>
        <?php
        if ($order[0]->date_order==null) echo '0';?>
    </dd>
        <dt>
            Наименование
            товара
        </dt>
    <dd>
        <?php echo $order[0]->product?>
    </dd>
        <dt>
            Кол-во
        </dt>
    <dd>
        <?php echo $order[0]->count?>
    </dd>
        <dt>
            Цена
        </dt>
    <dd>
        <?php echo $order[0]->price?>
    </dd>
        <dt>
            Сумма
        </dt>
    <dd>
        <?php echo (int)$order[0]->count*(int)$order[0]->price?>
    </dd>
        <dt>
            Баллы
        </dt>
    <dd>
        <?php echo (int)$order[0]->points; ?>
    </dd>
        <dt>
            Статус
        </dt>

    <dd>
            <?php
            switch($order[0]->status) {
                case 1 :
                    echo 'Новый';
                    break;
                case 2 :
                    echo 'Согласован';
                    break;
                case 3 :
                    echo 'Оплачен';
                    break;
                case 4 :
                    echo 'Выполнен';
                    break;
                case 5 :
                    echo 'Отменён';
                    break;
            }
            ?>

    </dd>
    </dl>
<?php if($order[0]->status==2): ?>
    <a class="btn btn-success" href="<?=Yii::app()->createUrl('payment/register',array('order_id' => $order[0]->id_orders));?>">Оплатить</a>
<?php endif;?>






