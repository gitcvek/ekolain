<?php
  /**
   * @var UserController $this
   * @var FClients $client
   */
  $cs = Yii::app()->clientScript;
  $this->registerJsFile('user.js');
  $cs->registerScript("profileForm", "User.showPassBind('showPass','profile-form','" . CHtml::activeName($model, "user_password") . "');", CClientScript::POS_READY);

  if (Yii::app()->user->hasFlash('profileSuccess')) {
    $this->widget('AlertWidget', array(
      'title' => $this->caption,
      'message' => Yii::app()->user->getFlash('profileSuccess'),
    ));
  }

  if (Yii::app()->user->hasFlash('registerSuccess')) {
    $this->widget('AlertWidget', array(
      'title' => 'Успешная регистрация',
      'message' => Yii::app()->user->getFlash('registerSuccess'),
    ));
  }
?>
<div class="b-profile-edit">

  <?php
    /**
     * @var $form CActiveForm
     */
    $form = $this->beginWidget('CActiveForm', array(
      'id' => 'profile-form',
      'enableAjaxValidation' => true,
      'enableClientValidation' => true,
      'focus' => array(
        $model,
        'mail'
      ),
      'htmlOptions' => array(
        'class' => '',
      ),
      'clientOptions' => array(
        'validateOnSubmit' => true,
        'validateOnChange' => false,
      ),
      'errorMessageCssClass' => 'label label-important',
    ));
  ?>
  <?php echo $form->errorSummary($model, false); ?>
  <fieldset>
    <div class="form-group">
      <?php echo $form->labelEx($model, 'name', array('class' => 'control-label')); ?>
      <?php echo $model->getEncodedName(); ?>
    </div>
    <!-- +not-encode-mail -->
    <!--    <div class="form-group">-->
    <!--      --><?php //echo $form->labelEx($model, 'mail', array('class' => 'control-label')); ?>
    <!--      --><?php //echo $form->textField($model, 'mail', array(
      //        'class' => 'form-control',
      //        'type' => 'email'
      //      )); ?>
    <!--      --><?php //echo $form->error($model, 'mail'); ?>
    <!--    </div>-->
    <!-- -not-encode-mail -->
    <div class="form-group">
      <button type="button" class="btn btn-default btn-sm" onclick="$(this).remove(); $('#passRow').show();">Изменить
        пароль
      </button>
      <div id="passRow" style="display:none">
        <?php echo $form->labelEx($model, 'user_password', array('class' => 'control-label')); ?>
        <?php echo $form->passwordField($model, 'user_password', array('class' => 'form-control')); ?>
        <?php echo $form->error($model, 'user_password'); ?>

        <div class="checkbox">
          <label for="showPass">
            <input type="checkbox" id="showPass">
            Показать пароль
          </label>
        </div>
        <?php echo CHtml::submitButton('Сохранить', array('class' => 'btn btn-primary')); ?>
        <?php $this->endWidget(); ?>
      </div>
    </div>

    <hr>
    <?php if ($client): ?>

      <dl class="dl-horizontal">
        <dt>
          Абонентский
          номер
        </dt>
        <dd>
          <?php echo $client->ab_number?>
        </dd>
        <dt>
          ФИО
        </dt>
        <dd>
          <?php echo $client->ab_number?>
        </dd>
        <dt>
          Адрес
        </dt>
        <dd>
          <?php echo $client->address?>
        </dd>
        <dt>
          Телефон
        </dt>
        <dd>
          <?php echo $client->phone?>
        </dd>
        <dt>
          Email
        </dt>
        <dd>
          <?php echo $client->mail?>
        </dd>
        <dd>
          <?php echo $client->phone?>
        </dd>
        <dt>
          Ваши баллы
        </dt>
        <dd>
          <?php echo $client->points?>
        </dd>

      </dl>

     <!-- <div class="form-group">
        <label class="control-label" for="">Абонентский номер</label>

        <div class="form-control">
          <?/*= $client->ab_number; */?>
        </div>
      </div>

      <div class="form-group">
        <label class="control-label" for="">Адрес</label>

        <div class="form-control">
          <?/*= $client->address */?>
        </div>
      </div>

      <div class="form-group">
        <label class="control-label" for="">Ф.И.О</label>

        <div class="form-control">
          <?/*= $client->fio */?>
        </div>
      </div>

      <div class="form-group">
        <label class="control-label" for="">Телефон</label>

        <div class="form-control">
          <?/*= $client->phone */?>
        </div>
      </div>

      <div class="form-group">
        <label class="control-label" for="">E-mail</label>

        <div class="form-control">
          <?/*= $client->mail */?>
        </div>
      </div>

      <div class="form-group">
        <label class="control-label" for="">Баллы</label>

        <div class="form-control">
          <?/*= $client->points */?>
        </div>
      </div>-->
      <?php else: ?>
      <div class="alert alert-info">
        К сожалению, ваша учетная запись еще не активирована. Ждать осталось недолго - обычно активация происходит в течение суток.
      </div>
    <?php endif; ?>

    <!--    <div class="form-group">-->
    <!--      --><?php //echo $form->labelEx($model, 'subscriber_number', array('class' => 'control-label')); ?>
    <!--      <div class="form-control">-->
    <!--        --><? //=$model->subscriber_number?>
    <!--      </div>-->
    <!--      --><?php //
      //      // echo $form->textField($model, 'subscriber_number', array(
      //      //   'class' => 'form-control',
      //      //   'rows' => '8'
      //      // )); ?>
    <!--      --><?php //echo $form->error($model, 'subscriber_number'); ?>
    <!--    </div>-->
    <!---->
    <!--    <div class="form-group">-->
    <!--      --><?php //echo $form->labelEx($model, 'full_name', array('class' => 'control-label')); ?>
    <!--      --><?php //echo $form->textField($model, 'full_name', array(
      //        'class' => 'form-control',
      //        'rows' => '8'
      //      )); ?>
    <!--      --><?php //echo $form->error($model, 'full_name'); ?>
    <!--    </div>-->
    <!---->
    <!--    <div class="form-group">-->
    <!--      --><?php //echo $form->labelEx($model, 'address', array('class' => 'control-label')); ?>
    <!--      --><?php //echo $form->textField($model, 'address', array(
      //        'class' => 'form-control',
      //        'type' => 'email'
      //      )); ?>
    <!--      --><?php //echo $form->error($model, 'address'); ?>
    <!--    </div>-->
    <!---->
    <!--    <div class="form-group">-->
    <!--      --><?php //echo $form->labelEx($model, 'phone', array('class' => 'control-label')); ?>
    <!--      --><?php //echo $form->textField($model, 'phone', array(
      //        'class' => 'form-control',
      //        'type' => 'email'
      //      )); ?>
    <!--      --><?php //echo $form->error($model, 'phone'); ?>
    <!--    </div>-->
    <!---->
    <!--    <div class="form-group">-->
    <!--      --><?php //echo $form->labelEx($model, 'convenient_time', array('class' => 'control-label')); ?>
    <!--      --><?php //echo $form->dropDownList($model, 'convenient_time', $convinientTimes, array(
      //        'class' => 'form-control',
      //        'type' => 'email'
      //      )); ?>
    <!--      --><?php //echo $form->error($model, 'convenient_time'); ?>
    <!--    </div>-->
    <!---->
    <!--    --><?php //if ($model->yur): ?>
    <!--      <div class="form-group">-->
    <!--        --><?php //echo $form->labelEx($model, 'company', array('class' => 'control-label')); ?>
    <!--        --><?php //echo $form->textField($model, 'company', array(
      //          'class' => 'form-control',
      //          'type' => 'email'
      //        )); ?>
    <!--        --><?php //echo $form->error($model, 'company'); ?>
    <!--      </div>-->
    <!--    --><?php //endif; ?>

<!--    <?php /*echo CHtml::submitButton('Сохранить', array('class' => 'btn btn-primary')); */?>
  </fieldset>
  --><?php /*$this->endWidget(); */?>
  <hr>
  <?php echo CHtml::link('Мои заказы', Yii::app()->createUrl('user/cabinet/history/'), array(
    'class' => 'btn btn-primary'
  )) ?>
</div>