<?php $this->registerCssFile('vitrine.css'); ?>

<div id="ygin-vitrine-carousel" class="b-vitrine carousel slide" data-ride="carousel">
  <div class="carousel-inner">
    <?php
      $i = 0;
      foreach ($models AS $model) {
        $id = $model->id_vitrine;
        $class = ($i == 0) ? 'item active' : 'item';
        $link = $model->link;
        $imgHTML = '';
          $img = new ImageUtils();
          $a = $img->info($model->file->getFilePath());
        if ($model->file) {
          $imgHTML = '<img class="center_img" src="' . $model->file->getUrlPath() . '" alt="' . $model->title . '" data-width-img="'.$a['width'].'" data-height-img="'.$a['height'].'">';
        } else {
          $imgHTML = '<div class="img"></div>';
        }
        ?>
        <div class="<? echo $class ?>">
          <?php echo $imgHTML ?>
          <div class="carousel-caption">
            <h3 class="title"><a href="<?php echo $link; ?>"><?php echo $model->title; ?></a></h3>

            <div class="text"><?php echo $model->text; ?></div>
          </div>
        </div>
        <?php
        $i++;
      }
    ?>
  </div>
</div>