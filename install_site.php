<?php
// скрипт запускается на хостинге

// набор вспомогательных функций
mb_internal_encoding('UTF-8');
mb_regex_encoding('UTF-8');
function addSlashPath($s) {
  $rest = substr($s, -1);
  if (($rest == "/") || ($rest == "\\")) return $s;
  $s = $s."/";
  return $s;
}
function log_da($str, $fn='install_log.txt') {
  global $logStart, $step;
  $rn = false;
  if ($logStart == null) {$logStart = 1; $rn = true;}
  if ($step == null) $step = 0;
  if ($fn == 'install_log.txt') {
    $step++;
    $str = $step.'.'.$str;
  }
  
  $fileName = dirname(__FILE__).'/'.$fn;
  $exists = true;
  if (!file_exists($fileName)) {
    $exists = false;
  }
  if ($fp=fopen($fileName, "a")) {
    //OK. Work with file
    $s = "$ ".date("Y.m.d H:i:s")." ".$str."\r\n";
    if ($fn == 'install_log.txt') echo $s;
    if ($rn) $s = "\r\n".$s;
    fwrite($fp, $s);
    fclose($fp);
  }
  if (!$exists) {
    chmod($fileName, 0777);
  }
}
function stripslashesRecursive($array) {
  if (is_array($array)) {
    return array_map('stripslashesRecursive', $array);
  }
  return stripslashes($array);
}
function GET($name, $default=null) {
  if (array_key_exists($name, $_GET)) {
    $res = $_GET[$name];
    $a = ini_get("magic_quotes_gpc");
    if (is_numeric($a) && intval($a) == 1) {
      $res = stripslashesRecursive($res);
    }
    return $res;
  } 
  return $default;
}
function getSizeStr($filesize) {
  $result = null;
  if ($filesize == null) return $result;
  if ($filesize < 1024) {
    // в байтах
    $result = $filesize." byte";
  } else if ($filesize < 1048576) {
    // в килобайтах
    $filesize = round($filesize / 1024, 2);
    $result = $filesize." kb";
  } else if ($filesize < 1073741824) {
    // в мегабайтах
    $filesize = round($filesize / 1048576, 2);
    $result = $filesize." mb";
  } else {
    // в гигабайтах
    $filesize = round($filesize / 1073741824, 2);
    $result = $filesize." gb";
  }
  return $result;
}
function execShell($command, $escape=true) {
  log_da($command, 'install_shell.txt');
  if ($escape) $command = escapeshellcmd($command);
  return trim(shell_exec($command));
  ob_start();
  passthru($command);
  $data = ob_get_contents();
  ob_end_clean();
  return $data; 
}

//////////// начало ///////////////
log_da('start: file='.@$_GET['file']);
if (@$_GET['file'] == null) {
  log_da('file empty, exit');
  return;
}

$file = $_GET['file'];
$path = addSlashPath(str_replace("\\", "/", $_SERVER['DOCUMENT_ROOT']));
//$path = $path.'temp/';
$path2bzip = $path.basename($file);

$installStatus = 0;
if (file_exists($path.'install_status')) $installStatus = intval(trim(file_get_contents($path.'install_status')));

if ($installStatus == -1) {
  log_da('file is uploading, please wait (size='.getSizeStr(filesize($path2bzip)).'), exit');
  return;
}

// проверяем существование директории
//$output = execShell('if [ ! -d "'.($path).'" ]; then echo bad; fi');
//if (trim($output) == "bad") {
//  log_da('2. folder for site not exists ('.$path.'), exit');
//  return;
//}

//log_da('2. folder for site exists');

//$file = 'cveknew.baitek.org/temp/robots222.txt';

if ($installStatus == 0) {
  file_put_contents($path.'install_status', '-1');
  $link = 'http://'.str_replace('http://', '', $file);
  log_da('start download');
  execShell('wget -c -q -P '.$path.' --limit-rate=80k '.$link);
  
  if (!file_exists($path2bzip)) {
    log_da('file download with error, exit');
    return;
  } else if (filesize($path2bzip) < 50) {
    log_da('file size small, exit');
    return;
  }
  log_da('file download finish');
  file_put_contents($path.'install_status', '1');
  $installStatus = 1;
} else {
  log_da('download file skip, as it already download');
}

if ($installStatus == 1) {
  execShell('bunzip2 '.$path2bzip);
  log_da('file archive decompress');
  file_put_contents($path.'install_status', '2');
  $installStatus = 2;
}

$path2tar = str_replace('.bz2', '', $path2bzip);
if ($installStatus == 2) {
  execShell('tar xf '.$path2tar);
  log_da('tar file unpack');
  
  execShell('chmod -R 777 '.$path);
  log_da('chmod end');
  file_put_contents($path.'install_status', '3');
  $installStatus = 3; 
}

$path2sql = str_replace('.tar', '.sql', $path2tar);
if ($installStatus == 3) {
  // разбираем файл base_accounts
  $siteName = $path;
  if (substr($siteName, -1) == '/') $siteName = substr($siteName, 0, strlen($siteName)-1);
  $siteName = substr($siteName, strrpos($siteName, '/')+1);
//$siteName = 'cvek.ru';
  $db = 'base_'.str_replace('.', '_', $siteName);
  $user = substr($siteName, 0, strpos($siteName, '.'));
  $pwd = '';
  $output = execShell('cat '.$path.'../base_accounts | grep account='.$user.':', false);
  $output = explode("\n", trim($output));
  if (count($output) == 1 && trim($output[0]) != '') {
    $output = $output[0];
    $pwd = trim(str_replace('account='.$user.':', '', $output));
  } else {
    log_da('password for db not found: '.print_r($output, true));
    return;
  }
  $host = 'localhost';
  
  log_da('db settings: user='.$user.'; pwd='.$pwd.'; db='.$db);
  
  function processConent($content, $user, $pwd, $db, $host) {
    $content = str_replace('__USER__', $user, $content);
    $content = str_replace('__PASS__', $pwd, $content);
    $content = str_replace('__NAME__', $db, $content);
    $content = str_replace('__HOST__', $host, $content);
    return $content;
  }
  
  // заменяем настройки к базе:
  if (file_exists($path.'project/global_project.da.php')) {
    $content = file_get_contents($path.'project/global_project.da.php');
    $content = preg_replace('~(define.*?\((?:\\\'|\")PR_DB_USER_NAME(?:\\\'|\").*?\,\s*?(?:\\\'|\"))(.+?)((?:\\\'|\")\))~', '\\1__USER__\\3', $content);
    $content = preg_replace('~(define.*?\((?:\\\'|\")PR_DB_PASSWORD(?:\\\'|\").*?\,\s*?(?:\\\'|\"))(.+?)((?:\\\'|\")\))~', '\\1__PASS__\\3', $content);
    $content = preg_replace('~(define.*?\((?:\\\'|\")PR_DB_NAME(?:\\\'|\").*?\,\s*?(?:\\\'|\"))(.+?)((?:\\\'|\")\))~', '\\1__NAME__\\3', $content);
    $content = preg_replace('~(define.*?\((?:\\\'|\")PR_DB_HOST(?:\\\'|\").*?\,\s*?(?:\\\'|\"))(.+?)((?:\\\'|\")\))~', '\\1__HOST__\\3', $content);
    file_put_contents($path.'project/global_project.da.php', processConent($content, $user, $pwd, $db, $host));
  }
  if (file_exists($path.'protected/config/project.php')) {
    $content = file_get_contents($path.'protected/config/project.php');
    $content = preg_replace('~(\$user\s*?\=\s*?(?:\\\'|\"))(.+?)((?:\\\'|\")\;)~', '\\1__USER__\\3', $content);
    $content = preg_replace('~(\$password\s*?\=\s*?(?:\\\'|\"))(.+?)((?:\\\'|\")\;)~', '\\1__PASS__\\3', $content);
    $content = preg_replace('~(\$dbName\s*?\=\s*?(?:\\\'|\"))(.+?)((?:\\\'|\")\;)~', '\\1__NAME__\\3', $content);
    $content = preg_replace('~(\$host\s*?\=\s*?(?:\\\'|\"))(.+?)((?:\\\'|\")\;)~', '\\1__HOST__\\3', $content);
    file_put_contents($path.'protected/config/project.php', processConent($content, $user, $pwd, $db, $host));
  }
  if (file_exists($path.'protected/config/local.php')) {
    $content = file_get_contents($path.'protected/config/local.php');
    $content = preg_replace('~(\$user\s*?\=\s*?(?:\\\'|\"))(.+?)((?:\\\'|\")\;)~', '\\1__USER__\\3', $content);
    $content = preg_replace('~(\$password\s*?\=\s*?(?:\\\'|\"))(.+?)((?:\\\'|\")\;)~', '\\1__PASS__\\3', $content);
    $content = preg_replace('~(\$dbName\s*?\=\s*?(?:\\\'|\"))(.+?)((?:\\\'|\")\;)~', '\\1__NAME__\\3', $content);
    $content = preg_replace('~(\$host\s*?\=\s*?(?:\\\'|\"))(.+?)((?:\\\'|\")\;)~', '\\1__HOST__\\3', $content);
    file_put_contents($path.'protected/config/local.php', processConent($content, $user, $pwd, $db, $host));
  }

  execShell('mysql -h localhost -u '.$user.' --password='.$pwd.' '.$db.' < '.$path2sql, false);

  // меняем почтовые настройки
  $addSql = "UPDATE da_mail_account SET email_from='robot@".$siteName."', from_name='".$siteName."';";
  file_put_contents($path2sql.'_2', $addSql);
  execShell('mysql -h localhost -u '.$user.' --password='.$pwd.' '.$db.' < '.$path2sql.'_2', false);
  @unlink($path2sql.'_2');
  
  file_put_contents($path.'install_status', '4');
  $installStatus = 4; 
}

if ($installStatus == 4) {
//  @unlink($path2tar);
//  @unlink($path2sql);
  // удаляем также инсталляционные файлы
  @unlink(__FILE__);
  if (GET('install_folder') != null && file_exists($path.GET('install_folder')) ) {
    execShell('rm -R '.$path.GET('install_folder'));
  }
  @unlink($path.'install_status');
}










