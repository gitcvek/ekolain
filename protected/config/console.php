<?php 

return array(
'components' => array(
    'db' => array(
      'connectionString' => 'mysql:host=localhost;dbname=ekolain',
      'username' =>  'www',
      'password' => 'mpwd123',
      'emulatePrepare' => true,
      'charset' => 'utf8',
      'schemaCachingDuration'=>3600,
      'enableProfiling' => true,
      'enableParamLogging' => true,
    ),  
  ),
);
?>