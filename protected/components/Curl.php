<?php

/**
 * Created by PhpStorm.
 * User: Cranky4
 * Date: 02.09.14
 * Time: 17:12
 */
class Curl {

    private $curl_result = '';

    public function __construct($url, $data = null) {
        $this->curl_result =  self::get($url, $data);
    }

    public static function get($url, $data = null) {
        // 1. �������������
        $ch = curl_init();
        // 2. ��������� ���������, ������� url
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        if ($data) curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

        // 3. �������� HTML � �������� ����������
        $output = curl_exec($ch);

        if ($output === false) {
            Yii::log(curl_error($ch));
            $return = false;
        } else {
            $return = $output;
        }

        // 4. ��������� ����������
        curl_close($ch);
        return $return;
    }

    public function getResult() {
        return $this->curl_result;
    }
}