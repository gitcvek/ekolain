<?php

class PaymentController extends Controller {

    const RUB = 643; //RUB – 643 (ISO 4217)
    const USD = 840;
    const EUR = 978;

    const API_USER = 'ecoline_komi-api';
    const API_PASSWORD = 'ecolinekomi';


    public function actionRegister($order_id) {
        if (isset($order_id)) {
            $criteria = new CDbCriteria;
            $criteria->addCondition('id_orders = :id');
            $criteria->params = array(':id' => $order_id);
            $order = FOrders::model()->findAll($criteria);
            if (!empty($order)) {
                $currency = self::RUB;
                $sum = $order[0]->price*100;
                $id = $order[0]->id_orders;
                //$id = rand();
                //сюда вернёмся с платёжной формы
                $back_url = 'http://ekolain.baitek.org'.Yii::app()->createUrl('payment/redirect');
                //$back_url .= "/id/".$order_id;
                $redirect_url = 'https://3dsec.sberbank.ru/payment/rest/register.do?';
                $redirect_url .= 'userName='.self::API_USER;
                $redirect_url .= '&password='.self::API_PASSWORD;
                $redirect_url .= '&orderNumber='.$id;
                $redirect_url .= '&amount='.$sum;
                $redirect_url .= '&currency='.$currency;
                $redirect_url .= '&description="'.urlencode($order[0]->product).'"';
                $redirect_url .= '&returnUrl='.$back_url;
                $redirect_url .= '&language=ru';
                $response = new Curl($redirect_url);
                //обработка первого ответа
                $responseObj = json_decode($response->getResult());
                if (!empty($responseObj->errorCode)) {
                    Yii::log("Получена ошибка шлюза: ".$responseObj->errorCode." ".$responseObj->errorMessage.". Номер заказа ".$id , CLogger::LEVEL_INFO, 'application.payment.error');
                    Yii::app()->user->setFlash('paymentError', $responseObj->errorMessage);
                    $this->redirect(Yii::app()->user->returnUrl);
                }
                else {
                    if (!empty($responseObj->formUrl)) {
                        Yii::log("Заказ №".$id." зарегистрирован в платёжной системе под номером ".$responseObj->orderId , CLogger::LEVEL_INFO, 'application.payment.error');
                        $order[0]->payment_id = $responseObj->orderId;
                        if($order[0]->save()) {
                            //уходим на платёжную форму
                            $this->redirect($responseObj->formUrl);
                        }
                        else {
                            Yii::app()->user->setFlash('paymentError', 'Ошибка при обработке запроса, попробуйте ещё раз');
                            Yii::log("Заказ №".$id.". Ошибка при записи ответа в базу" , CLogger::LEVEL_INFO, 'application.payment.error');
                        }
                    }
                    else {
                        Yii::log("Шлюз не вернул адрес платёжной формы. Номер заказа ".$id , CLogger::LEVEL_INFO, 'application.payment.error');
                    }
                }
            }
            else {
                Yii::log("Заказ не найден ".$order_id, CLogger::LEVEL_INFO, 'application.payment.error');
                $this->redirect(Yii::app()->user->returnUrl);
            }

        }
        else {
            Yii::log("Отсутствует номер заказа ".$order_id, CLogger::LEVEL_INFO, 'application.payment.error');
            $this->redirect(Yii::app()->user->returnUrl);
        }
    }

    public function actionRedirect($orderId) {
        $status_url = 'https://3dsec.sberbank.ru/payment/rest/getOrderStatus.do?';
        $status_url .= 'userName='.self::API_USER;
        $status_url .= '&password='.self::API_PASSWORD;
        $criteria = new CDbCriteria;
        $criteria->addCondition('payment_id = :id');
        $criteria->params = array(':id' => $orderId);
        $order = FOrders::model()->findAll($criteria);
        $status_url .= '&orderId='.$order[0]->payment_id;
        $status_url .= '&language=ru';
        $response = new Curl($status_url);
        //обработка второго ответа
        $responseObj = json_decode($response->getResult());
        //print_r($responseObj);exit;
        if (!empty($responseObj->ErrorCode) && $responseObj->ErrorCode != 0) {
            Yii::log("Получена ошибка шлюза: ".$responseObj->ErrorCode." ".$responseObj->ErrorMessage.". Номер заказа ".$id , CLogger::LEVEL_INFO, 'application.payment.error');
        }
        else {
            if ($responseObj->ErrorCode == 0 && $responseObj->OrderStatus == 2) {
                Yii::log("Заказ №".$orderId." успешно оплачен <".$responseObj->ErrorCode.":".$responseObj->ErrorMessage.">" , CLogger::LEVEL_INFO, 'application.payment.error');
                $order[0]->status = 3;
                if($order[0]->save()) {
                //возвращаемся на страницу заказа
                Yii::app()->user->setFlash('paymentError', 'Оплата прошла успешно! Спасибо!');
                $this->redirect(Yii::app()->createUrl('user/cabinet/view/',array('order_id' => $order[0]->id_orders)));
                }
                else {
                    Yii::app()->user->setFlash('paymentError', 'Оплата прошла успешно, но не удалось изменить статус заказа на сайте. Пожалуйста обратитесь к менеджеру.');
                    $this->redirect(Yii::app()->createUrl('user/cabinet/view/',array('order_id' => $order[0]->id_orders)));
                    Yii::log("Не удалось изменить статус заказа на сайте! Заказ №".$orderId." успешно оплачен <".$responseObj->ErrorCode.":".$responseObj->ErrorMessage.">" , CLogger::LEVEL_INFO, 'application.payment.error');
                }
            }
            else {
                Yii::log("Шлюз не вернул статус заказа. Номер заказа ".$orderId , CLogger::LEVEL_INFO, 'application.payment.error');
            }
        }
    }
}

?>