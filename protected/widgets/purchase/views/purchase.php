<div class="b-purchase">
  <div class="b-purchase__buy-btn">
    <?php if (Yii::app()->user->isGuest): ?>
      <a href="#" class="btn" title="Купить воду" data-toggle="modal" data-target="#BuyWater"></a>
    <?php else: ?>
      <a href="<?= Yii::app()->createUrl('order/default/index') ?>" class="btn"></a>
    <?php endif; ?>
  </div>
  <div class="b-purchase__text">
    <span>С пользой для здоровья,</span>
    <span>с заботой о близких!</span>
  </div>
</div>