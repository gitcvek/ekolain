<?php

  class CabinetbuttonWidget extends DaWidget {

    public function run() {
      if(Yii::app()->user->isGuest) {
        $model = BaseFormModel::newModel('LoginForm');
        $this->render('buttonAutorization', array(
          'model' => $model
        ));
      } else {
        $this->render('buttonProfile');
      }
    }

  }