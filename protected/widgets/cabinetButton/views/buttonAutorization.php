<div class="b-cabinet-btn">
  <a href="#" class="btn btn-sign-in" title="Личный кабинет" data-toggle="modal" data-target="#AuthModal"></a>
</div>

<!-- Modal -->
<div class="b-water-modal modal fade" id="AuthModal" tabindex="-1" role="dialog" aria-labelledby="AuthModalLabel"
     aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
            class="sr-only">Close</span></button>
        <h4 class="modal-title" id="AuthModalLabel">Войти</h4>
      </div>
      <div class="modal-body">
        <?php
          $form = $this->beginWidget('CActiveForm', array(
            'id' => 'login-form',
            'action' => Yii::app()->createUrl('user/cabinet/login/'),
            'enableAjaxValidation' => true,
            'enableClientValidation' => true,
            'focus' => array(
              $model,
              'username'
            ),
            'htmlOptions' => array(
              'class' => '',
              'role' => 'form',
            ),
            'clientOptions' => array(
              'validateOnSubmit' => true,
              'validateOnChange' => false,
            ),
            'errorMessageCssClass' => 'label label-important',
          ));
        ?>
        <?php echo $form->errorSummary($model, false); ?>
        <fieldset>
          <!-- +not-encode-mail -->
          <div class="form-group">
            <!--            --><?php //echo $form->labelEx($model, 'username', array('class'=>'control-label')); ?>
            <?php echo $form->textField($model, 'username', array(
              'class' => 'form-control',
              'placeholder' => "Логин"
            )); ?>
            <?php echo $form->error($model, 'username'); ?>
          </div>
          <!-- -not-encode-mail -->
          <div class="form-group">
            <!--            --><?php //echo $form->labelEx($model, 'password', array('class'=>'control-label')); ?>
            <?php echo $form->passwordField($model, 'password', array(
              'class' => 'form-control',
              'placeholder' => "Пароль"
            )); ?>
            <?php echo $form->error($model, 'password'); ?>
          </div>
          <div class="checkbox">
            <div class="clearfix">
              <div class="remember-check">
                <?php echo $form->checkBox($model, 'rememberMe'); ?>
                <label for="LoginForm_rememberMe">Запомнить меня</label>
              </div>
            </div>
            <?php echo $form->error($model, 'rememberMe'); ?>
          </div>
          <button type="submit" class="btn sign-in">Войти</button>
        </fieldset>
        <?php $this->endWidget(); ?>


      </div>
      <div class="modal-footer">
        <a href="<?php echo Yii::app()->createUrl('user/cabinet/register/')?>" class="register">Зарегистрироваться</a>
        &nbsp;&nbsp;
        <a href="<?php echo Yii::app()->createUrl('user/cabinet/recover/')?>" class="register">Забыли пароль?</a>
      </div>
    </div>
  </div>
</div>