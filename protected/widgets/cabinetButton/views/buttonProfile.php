<div class="b-cabinet-btn">
  <a href="<?php echo Yii::app()->createUrl('/user/cabinet/profile/')?>" class="btn btn-private" title="Личный кабинет" ></a>
  <?php echo CHtml::link('<i class="glyphicon glyphicon-exit"></i>', Yii::app()->createUrl('/user/cabinet/logout/'), array(
    'class' => 'btn btn-sm btn-enter',
  ))?>
</div>