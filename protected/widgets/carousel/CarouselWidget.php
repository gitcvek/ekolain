<?php
/**
 * Created by PhpStorm.
 * User: vanya
 * Date: 13.11.2014
 * Time: 17:58
 */
class CarouselWidget extends DaWidget {
	const ID_LEFT_MENU = 129;

	public function run () {
		$current = Yii::app ()->menu->getCurrent ();
		if( $current->alias=='aktsii' || $current->getParent()->alias == 'aktsii') {
			$childs = $current->getChild ();
			if( !$childs ) {
				return $current->content;
			}
			$this->render ('carousel_action',array(
				'childs' => $childs,
			));
		}else {
			$parent = $current->getParent ();
			//предки парента
			$parenSiblings = array();
			if( ($parent != null && $parentParent = $parent->getParent ()) && (count ($parenSiblings)<5) && ($parentParent->primaryKey!=self::ID_LEFT_MENU) ) {
				$parenSiblings = $parentParent->getChild ();
			}
			$siblings = $parent->getChild ();
			foreach( $siblings as $key => $subling ) {
				if( $subling->alias==$current->alias ) {
					$index = $key;
				}
			}
			$prevIndex = $index-1;
			$nextIndex = $index+1;
			if( isset($siblings[ $prevIndex ]) ) {
				$prev = $siblings[ $prevIndex ];
			}else {
				$prev = $siblings[ count ($siblings)-1 ];
			}
			if( isset($siblings[ $nextIndex ]) ) {
				$next = $siblings[ $nextIndex ];
			}else {
				$next = $siblings[ 0 ];
			}
			$this->render ('carousel',array(
				'prev' => $prev,
				'next' => $next,
				'current' => $current,
				'siblings' => $siblings,
				'parentSiblings' => $parenSiblings
			));
		}
	}
}