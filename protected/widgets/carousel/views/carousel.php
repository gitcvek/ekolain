<?php if ($count = count($parentSiblings)): ?>
  <?php // $col = 12 / $count; ?>
  <div class="b-carousel-top-menu row">
    <?php foreach ($parentSiblings as $parentSibling): ?>
      <?php if ($parentSibling->alias == Yii::app()->menu->current->getParent()->alias) {
        $active = 'btn-primary';
      } else {
        $active = 'btn-default';
      }
      ?>
      <!--  <div class="col-lg-<?//= $col ?>">-->
      <div class="b-carousel-top-menu__item">
        <?php echo CHtml::link($parentSibling->name, $parentSibling->getUrl(), array(
          'class' => 'btn ' . $active
        ));?>
      </div>
    <?php endforeach; ?>
  </div>
<?php endif; ?>
<div class="b-about">
  <?php $count = count($siblings) ?>
  <?php if ($count > 1): ?>
    <div class="b-about_arrow left">
      <a href="<?= $prev->getUrl() ?>"><span class="glyphicon glyphicon-chevron-left"></span></a>
    </div>
    <div class="b-about_arrow right">
      <a href="<?= $next->getUrl() ?>"><span class="glyphicon glyphicon-chevron-right"></span></a>
    </div>
  <?php endif; ?>
  <?php if ($image = $current->imageFile): ?>
    <?php if ($preview = $image->getPreview(600, 0, '_menu1')): ?>
      <div class="b-about_inner text-center">
        <img src="<?= $preview->getUrlPath() ?>" alt="<?= $current->getName() ?>"/>
      </div>
    <?php endif; ?>
  <?php endif; ?>

  <?php if ($count > 1): ?>
    <div class="b-about_dots">
      <?php foreach ($siblings as $sibling): ?>
        <a class="dot <?php echo $sibling->alias == $current->alias ? 'active' : ''; ?>"
           href="<?= $sibling->getUrl() ?>"></a>
      <?php endforeach; ?>
    </div>
  <?php endif; ?>
  <div class="b-about_text">
    <p>
      <?= $current->content ?>
    </p>
  </div>
</div>