<div class="b-customer-menu js-toggle-menu">
    <?php foreach ($menu->getChild() as $key => $child): ?>
        <div class="item icon-<?= ++$key ?>">
            <?php if ($child->content || $child->external_link): ?>
                <a href="<?= $child->getUrl() ?>"></a>
            <?php elseif ($childs = $child->getChild()): ?>
                <?php $child = current($childs); ?>
                <a href="<?= $child->getUrl() ?>"></a>
            <?php endif; ?>
        </div>
    <?php endforeach; ?>
    <div class="toggle js-toggle-arrow">
        <span class="arrow"></span>
    </div>
</div>