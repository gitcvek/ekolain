<?php
class LeftMenuWidget extends DaWidget {
	const ID_LEFT_MENU = 129;

	public function run () {
		$menu = Menu::model()->getTree()->getById (self::ID_LEFT_MENU);
		$this->render ('leftMenu',array(
			'menu' => $menu,
		));
	}
}