<?php

  class DefaultController extends Controller {

    public function actionIndex() {
      //регаем датепикер
      Yii::app()->clientScript->registerScriptFile(
        Yii::app()->assetManager->publish(
          Yii::getPathOfAlias('ygin.assets.bootstrap').'/js/bootstrap.min.js'
        ),
        CClientScript::POS_END
      );
      Yii::app()->clientScript->registerScriptFile(
        Yii::app()->assetManager->publish(
          Yii::getPathOfAlias('ygin.assets.bootstrap').'/js/bootstrap-datepicker.js'
        ),
        CClientScript::POS_END
      );
      Yii::app()->clientScript->registerCssFile(
        Yii::app()->assetManager->publish(
          Yii::getPathOfAlias('ygin.assets.bootstrap').'/css/bootstrap.min.css'
        )
      );
      Yii::app()->clientScript->registerCssFile(
        Yii::app()->assetManager->publish(
          Yii::getPathOfAlias('ygin.assets.bootstrap').'/css/datepicker.css'
        )
      );


      $type = Yii::app()->request->getPost('orderType');
      if ($type == 'phiz') {
        $scenario = 'orderPhiz';
        $form_id = 'order-form-phiz';
      } elseif ($type == 'yur') {
        $scenario = 'orderYur';
        $form_id = 'order-form-yur';
      } else {
        $scenario = null;
        $form_id = '';
      }

      $model = new OrderForm($scenario);
      $class = get_class($model);

      //валидация и сохранение заказа
      $this->performAjaxValidation($model, $form_id);
      if (($data = Yii::app()->request->getPost($class)) && ($counts = Yii::app()->request->getPost("count"))) {
        $order = new Order();
        $order->attributes = $data;
        $order->date = time();
        $order->delivery_date = strtotime($order->delivery_date);

        if ($order->save()) {
          foreach ($counts as $id_water => $count) {
            $waterCount = new WaterCount();
            $waterCount->id_water = $id_water;
            $waterCount->id_order = $order->primaryKey;
            $waterCount->count = $count;
            $waterCount->save();
          }

          //письмо о заказе после сохранения
          //$order->onAfterSave($this,'sendAdminMail');
          if (Yii::app()->user->isGuest) {
            $user = false;
            $client = false;
          } else {
            $user = Yii::app()->user->getModel();
            $client = $user->getClient();
          }


          Yii::app()->notifier->addNewEvent(
            Order::ID_EVENT_NEW_ORDER,
            $this->renderPartial('mail/_newOrder', array(
              'model' => $order,
              'user' => $user,
              'client' => $client,
            ), true)
          );
          
          if ( $order->email) {
           Yii::app()->notifier->addNewEvent(
            Order::ID_EVENT_NEW_ORDER,
            $this->renderPartial('mail/_newOrderUser', array(
              'model' => $order,
              'user' => $user,
              'client' => $client,
            ), true),
            ORDER_SUBSCRIBER_ID,
            $order->email    
          );
          }
          
          Yii::app()->getUser()->setFlash('order-status', 'Заказ успешно оформлен');
          $this->redirect(Yii::app()->createUrl('order/default/index/'));
        }
      }


      //удобное время доставки
      $_convinientTimesReference = ReferenceElement::model()->findAllByAttributes(array(
        'id_reference' => 'project-reference-vremya-dostavki'
      ));
      $convinientTimes = CHtml::listData($_convinientTimesReference,'id_reference_element','value');

      //если юзер авторизован, то рисуем заполненную форму
      if (!Yii::app()->user->isGuest) {
        $model = new OrderForm();
        $user = Yii::app()->user->model;

          $model->subscriber_number = $user->subscriber_number;
          $model->fio = $user->full_name;
          $model->address = $user->address;
          $model->company = $user->company;
          $model->email = $user->mail;
          $model->phone = $user->phone;
		  $model->convenient_time = $user->convenient_time;

          if($client = $user->getClient()) {
            $model->subscriber_number = $client->ab_number;
            $model->fio = $client->fio;
            $model->address = $client->address;
            $model->email = $client->mail;
            $model->phone = $client->phone;
          }

        $this->render('authIndex', array(
          'model' => $model,
          'convinientTimes' => $convinientTimes
        ));
      } else {
        $modelPhiz = new OrderForm('orderPhiz');
        $modelYur = new OrderForm('orderYur');



        $this->render('index', array(
          'modelPhiz' => $modelPhiz,
          'modelYur' => $modelYur,
          'convinientTimes' => $convinientTimes
        ));
      }
    }

    public function performAjaxValidation($model, $ajaxFormId) {
      if (isset($_POST['ajax']) && $_POST['ajax'] === $ajaxFormId) {
        echo CActiveForm::validate($model);
        Yii::app()->end();
      }
    }

    public function sendAdminMail(CEvent $event) {
      $model = $event->sender;
      if (!$user = Yii::app()->user->isGuest) {
        $user = false;
      }

      Yii::app()->notifier->addNewEvent(Order::ID_EVENT_NEW_ORDER, $this->renderPartial('mail/_newOrder', array(
          'model' => $model,
          'user' => $user
        ), true), Order::ID_EVENT_NEW_ORDER_SUBSCRIBER);
    }
  }