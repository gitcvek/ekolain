<?php
  /**
   * Created by PhpStorm.
   * User: cranky4
   * Date: 05.12.14
   * Time: 17:52
   * @var Order $model
   * @var WaterCount $count
   * @var PUser $user
   * @var FClients $client
   */
  $sum = 0;

?>
<?php $times = Order::model()->getTimes(); ?>
<h1>Новый заказ от <?php echo Yii::app()->dateFormatter->format('dd MMMM yyyy HH:mm:ss',time())?></h1>
<p>Данные заказа</p>
Абонентский номер: <b><?= $model->subscriber_number ? $model->subscriber_number : "не установлен"; ?></b> <br><br>
ИД пользователя на сайте: <?= $user ? $user->primaryKey : " - " ?> <br>
Фио: <?= $model->fio; ?><br>
Телефон: <?= $model->phone; ?> <br>
Адрес: <?= $model->address; ?> <br>
email: <?= $model->email; ?> <br>
Дата доставки: <?=Yii::app()->dateFormatter->format('dd MMMM yyyy',$model->delivery_date);?> <br>
Удобное время доставки: <?= $times[$model->convenient_time - 1]->value ?> <br>
Примечание: <?= $model->description; ?> <br>
<br>
Заказ:
<table>
  <tr>
    <td>
      Наименование
    </td>
 <!--   <td>
      Цена
    </td>-->
    <td>
      Кол-во
    </td>
 <!--   <td>
      Стоимость
    </td>-->
  </tr>
  <?php foreach ($model->waterCounts as $count): ?>
    <tr>
     <td>
        <?php echo $count->water->title ?>
      </td> 
  <!--     <td>
        <?php echo $count->water->price ?>
      </td>-->
      <td>
        <?php echo $count->count; ?>
      </td>
<!--      <td>
        <?php
          $sum += $count->count * $count->water->price;
          echo $count->count * $count->water->price;
        ?>
      </td> -->
    </tr>
  <?php endforeach; ?>
 <!-- <tr>
    <td colspan="3">
      ИТОГО:
    </td>
    <td>
      <?php echo $sum ?>
    </td>
  </tr>-->
</table>
