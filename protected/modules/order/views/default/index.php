<?php
/**
 * @var $this DefaultController
 * @var OrderForm $modelPhiz
 * @var OrderForm $modelYur
 */
$this->pageTitle = 'Заказ воды';
$this->caption = "Заказ воды";

if (Yii::app()->user->hasFlash('order-status')) {
    $this->widget('ygin.widgets.alert.AlertWidget', array(
        'title' => 'Статус заказа',
        'message' => Yii::app()->user->getFlash('order-status')
    ));
}

Yii::app()->getClientScript()->registerScript('water-selector-form', '
    $(document).on("click", ".js_water_select_form_submit", function() {
      var $form1 = $(".js_water_select_form"),
          $btn = $(this),
          $form2 = $btn.closest("form"),
          $holder = $form2.find(".js_water_selector_holder"),

          data1 = $form1.serializeArray();

      $holder.html("");

      $added = 0;
      $.each(data1, function(i, item) {
        if(item.value != 0) {
          $added++;
          $holder.append(\'<input type="hidden" name="\'+item.name+\'" value="\'+item.value+\'">\');
        }
      });
       if($added == 0) {
          $form2.find("#OrderForm_count_em_").show().text("Выберите хотя бы одну бутыль воды для заказа");
        } else {
          $form2.find("#OrderForm_count_em_").hide().text("");
          $form2.submit();
        }
    });

    $(document).ready(function(){
        $(".datepicker_init").datepicker({
            "format": "dd.mm.yyyy",
            "weekStart": 1,
        });
    });
  ');
?>

<div class="b-water-order">
    <h2>Выберите воду</h2>
    <hr>
    <?php $this->widget('application.modules.order.widgets.WaterSelectorWidget'); ?>

    <h2 id="water-order">Заполните форму заказа</h2>
    <hr>
    <div class="water-form">
        <div role="tabpanel">

            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <!--        <li role="presentation" class="active">-->
                <!--          <a href="#formPhiz" aria-controls="formPhiz" role="tab" data-toggle="tab">Физическое лицо</a>-->
                <!--        </li>-->
                <!--        <li role="presentation">-->
                <!--          <a href="#formYur" aria-controls="formYur" role="tab" data-toggle="tab">Юридическое лицо</a>-->
                <!--        </li>-->
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="formPhiz">
                    <h3></h3>
                    <?php echo $this->renderPartial('_orderPhizForm', array(
                        'model' => $modelPhiz
                    )) ?>
                </div>
                <!--        <div role="tabpanel" class="tab-pane" id="formYur">-->
                <!--          <h3></h3>-->
                <!--          --><?php //echo $this->renderPartial('_orderYurForm', array(
                //            'model' => $modelYur
                //          ))?>
                <!--        </div>-->
            </div>

        </div>
    </div>
</div>