<?php
  /**
   * @var $form  CActiveForm
   * @var $model OrderForm
   */
  $form = $this->beginWidget('CActiveForm', array(
    'id' => 'order-form-phiz',
    'enableAjaxValidation' => true,
    'enableClientValidation' => true,
    //    'focus' => array(
    //      $model,
    //      'subscriber_number'
    //    ),
    'htmlOptions' => array(
      'class' => 'form-horizontal',
    ),
    'clientOptions' => array(
      'validateOnSubmit' => true,
      'validateOnChange' => false,
    ),
    'errorMessageCssClass' => 'label label-danger',
  ));
?>

  <div class="form-group">
    <?php echo $form->labelEx($model, 'subscriber_number', array("class" => "col-md-4 control-label")); ?>
    <div class="col-md-4">
      <?php echo $form->textField($model, 'subscriber_number', array(
        'class' => 'form-control',
        'placeholder' => '1234'
      )); ?>
      <?php echo $form->error($model, 'subscriber_number', array('class' => 'label label-danger')); ?>
    </div>
  </div>

  <div class="form-group">
    <?php echo $form->labelEx($model, 'fio', array("class" => "col-md-4 control-label")); ?>
    <div class="col-md-4">
      <?php echo $form->textField($model, 'fio', array(
        'class' => 'form-control',
        'placeholder' => 'Иванов Иван Иванович'
      )); ?>
      <?php echo $form->error($model, 'fio', array('class' => 'label label-danger')); ?>
    </div>
  </div>

  <div class="form-group">
    <?php echo $form->labelEx($model, 'phone', array("class" => "col-md-4 control-label")); ?>
    <div class="col-md-4">
      <?php echo $form->textField($model, 'phone', array(
        'class' => 'form-control',
        'placeholder' => '89431234567'
      )); ?>
      <?php echo $form->error($model, 'phone', array('class' => 'label label-danger')); ?>
    </div>
  </div>

  <div class="form-group">
    <?php echo $form->labelEx($model, 'address', array("class" => "col-md-4 control-label")); ?>
    <div class="col-md-4">
      <?php echo $form->textField($model, 'address', array(
        'class' => 'form-control',
        'placeholder' => 'Кутузова, 19 - 2'
      )); ?>
      <?php echo $form->error($model, 'address', array('class' => 'label label-danger')); ?>
    </div>
  </div>

  <div class="form-group">
    <?php echo $form->labelEx($model, 'email', array("class" => "col-md-4 control-label")); ?>
    <div class="col-md-4">
      <?php echo $form->textField($model, 'email', array(
        'class' => 'form-control',
        'placeholder' => 'name@domain.ru'
      )); ?>
      <?php echo $form->error($model, 'email', array('class' => 'label label-danger')); ?>
    </div>
  </div>

  <div class="form-group">
    <?php echo $form->labelEx($model, 'delivery_date', array("class" => "col-md-4 control-label")); ?>
    <div class="col-md-4">
      <?php echo $form->textField($model, 'delivery_date', array(
        'class' => 'form-control datepicker_init',
        'autocomplete' => 'off'
      )); ?>
      <?php echo $form->error($model, 'delivery_date', array('class' => 'label label-danger')); ?>
    </div>
  </div>

  <div class="form-group">
    <?php echo $form->labelEx($model, 'convenient_time', array("class" => "col-md-4 control-label")); ?>
    <div class="col-md-4">
      <?php $times = CHtml::listData(Order::model()->getTimes(), 'id_reference_element', 'value');?>
      <?php echo $form->dropDownList($model, 'convenient_time', array_merge(array(0 => ''),$times), array(
        'class' => 'form-control',
      )); ?>
      <?php echo $form->error($model, 'convenient_time', array('class' => 'label label-danger')); ?>
    </div>
  </div>

  <div class="form-group">
    <?php echo $form->labelEx($model, 'description', array("class" => "col-md-4 control-label")); ?>
    <div class="col-md-4">
      <?php echo $form->textArea($model, 'description', array(
        'class' => 'form-control',
        'rows' => 4,
        'placeholder' => ''
      )); ?>
      <?php echo $form->error($model, 'description', array('label label-danger')); ?>
    </div>
  </div>


<?php if (Yii::app()->user->isGuest): ?>
<div class="form-group">
    <?php echo $form->label($model, 'new', array("class" => "col-md-4 control-label")); ?>
    <div class="col-md-4">
      <?php echo $form->checkBox($model, 'new', array(
        'class' => 'form-control',
      )); ?>
      <?php echo $form->error($model, 'new', array('class' => 'label label-danger')); ?>
    </div>
  </div>


  <div class="form-group">
    <?php echo $form->labelEx($model, 'personal_data_agreement', array("class" => "col-md-4 control-label")); ?>
    <div class="col-md-4">
      <?php echo $form->checkBox($model, 'personal_data_agreement', array(
        'class' => 'form-control'
      )); ?>
      <?php echo $form->error($model, 'personal_data_agreement', array('class' => 'label label-danger')); ?>
    </div>
  </div>
<?php endif; ?>

  <div class="form-group">
    <?php echo $form->error($model, 'count', array('class' => 'label label-danger')); ?>
    <div class="js_water_selector_holder">
    </div>
  </div>

<?php if (!Yii::app()->user->isGuest): ?>
  <?php echo $form->hiddenField($model, 'id_user', array(
    'value' => Yii::app()->user->id,
  )); ?>
<?php endif; ?>

  <div class="form-group">
    <div class="col-md-4 col-md-offset-4">
      <?php echo CHtml::hiddenField('orderType', 'phiz'); ?>
      <?php echo CHtml::htmlButton('Отправить', array(
        'class' => 'btn btn-primary js_water_select_form_submit'
      )) ?>
    </div>
  </div>

<?php
  $this->endWidget();
?>