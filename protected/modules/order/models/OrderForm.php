<?php
/**
 * Created by PhpStorm.
 * User: cranky4
 * Date: 03.12.14
 * Time: 11:55
 */
class OrderForm extends BaseFormModel {
	public $fio;
	public $phone;
	public $address;
	public $email;
	public $convenient_time;
	public $personal_data_agreement;
	public $subscriber_number;
	public $description;
	public $company;
	public $count;
//	public $new;
	public $delivery_date;

	public function attributeLabels () {
		$labels = array(
			'subscriber_number' => 'Абонентский номер',
			'fio' => 'ФИО (название организации)',
			'phone' => 'Телефон',
			'address' => 'Адрес',
			'email' => 'email',
			'convenient_time' => 'Удобное время доставки',
			'login' => 'Логин',
			'password' => 'Пароль',
			'confirm_password' => 'Повторите пароль',
			'personal_data_agreement' => 'Согласие на обработку персональных данных',
			'description' => 'Примечание',
			'company' => 'Компания',
//			'new' => 'Новый клиент',
			'delivery_date' => 'Дата доставки',
		);
		if( $this->scenario=="orderYur" ) {
			$labels[ 'fio' ] = 'ФИО ответственного лица';
		}
		return $labels;
	}

	public function rules () {
		return array(
			array(
				'fio, phone, address, convenient_time, delivery_date',
				'required',
			),
			array(
				'company',
				'required',
				'on' => 'orderYur'
			),
			array(
				'email',
				'email'
			),
			array(
			//	'personal_data_agreement, new',
				'personal_data_agreement',
				'boolean',
			),
			array(
				'personal_data_agreement',
				'required',
				'message' => 'Нужно дать согласие на обработку персональных данных'
			),
		);
	}

	public function uniqueLogin ($attribute,$params) {
		$row = Yii::app ()->db->createCommand ()
			->select ("COUNT(name) as c")
			->from ('da_users')
			->where ("name = :NAME",array(
				':NAME' => $this->$attribute
			))
			->queryRow ();
		if( HArray::val ($row,'c') ) {
			$this->addError ($attribute,'Этот логин занят');
		}
	}
}