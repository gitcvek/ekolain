<?php

/**
 * Модель для таблицы "pr_water_count".
 *
 * The followings are the available columns in table 'pr_water_count':
 * @property integer $id_water_count
 * @property integer $id_order
 * @property integer $count
 * @property integer $id_water
 *
 * The followings are the available model relations:
 * @property Order $order
 * @property Water $water
 */
class WaterCount extends DaActiveRecord {

  const ID_OBJECT = 'project-kolichestvo-vody';

  protected $idObject = self::ID_OBJECT;

  /**
   * Returns the static model of the specified AR class.
   * @param string $className active record class name.
   * @return WaterCount the static model class
   */
  public static function model($className = __CLASS__) {
    return parent::model($className);
  }

  /**
   * @return string the associated database table name
   */
  public function tableName() {
    return 'pr_water_count';
  }

  /**
   * @return array validation rules for model attributes.
   */
  public function rules() {
    return array(
      array('id_order, id_water', 'required'),
      array('id_order, count, id_water', 'numerical', 'integerOnly'=>true),
    );
  }

  /**
   * @return array relational rules.
   */
  public function relations() {
    return array(
      'order' => array(self::BELONGS_TO, 'Order', 'id_order'),
      'water' => array(self::BELONGS_TO, 'Water', 'id_water'),
    );
  }

  /**
   * @return array customized attribute labels (name=>label)
   */
  public function attributeLabels() {
    return array(
      'id_water_count' => 'ID',
      'id_order' => 'Заказ',
      'count' => 'Количество',
      'id_water' => 'Вода',
    );
  }

}