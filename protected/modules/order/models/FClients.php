<?php

/**
 * Модель для таблицы "f_clients".
 *
 * The followings are the available columns in table 'f_clients':
 * @property integer $ab_number
 * @property string $fio
 * @property string $address
 * @property string $phone
 * @property string $mail
 * @property integer $points
 * @property string $time_stamp
 * @property string $passw
 *
 * The followings are the available model relations:
 * @property FOrders[] $fOrders
 */
class FClients extends DaActiveRecord {

  const ID_OBJECT = 'project-klienty-vneschnaya-baza';

  protected $idObject = self::ID_OBJECT;

  /**
   * Returns the static model of the specified AR class.
   * @param string $className active record class name.
   * @return FClients the static model class
   */
  public static function model($className = __CLASS__) {
    return parent::model($className);
  }

  /**
   * @return string the associated database table name
   */
  public function tableName() {
    return 'f_clients';
  }

  /**
   * @return array validation rules for model attributes.
   */
  public function rules() {
    return array(
      array('points', 'numerical', 'integerOnly'=>true),
      array('fio, address, phone, mail, time_stamp, passw', 'length', 'max'=>255),
    );
  }

  /**
   * @return array relational rules.
   */
  public function relations() {
    return array(
      'fOrders' => array(self::HAS_MANY, 'FOrders', 'id_client'),
    );
  }

  /**
   * @return array customized attribute labels (name=>label)
   */
  public function attributeLabels() {
    return array(
      'id_client' => 'ID',
      'fio' => 'ФИО',
      'address' => 'Адрес',
      'phone' => 'Телефон',
      'mail' => 'mail',
      'points' => 'Баллы',
      'time_stamp' => 'time_stamp',
      'passw' => 'passw',
    );
  }

}