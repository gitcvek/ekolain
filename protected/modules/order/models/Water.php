<?php

/**
 * Модель для таблицы "pr_water".
 *
 * The followings are the available columns in table 'pr_water':
 * @property integer $id_water
 * @property string $title
 * @property string $short
 * @property integer $photo
 * @property string $price
 * @property integer $sequence
 * @property integer $visible
 *
 * The followings are the available model relations:
 * @property File $photoFile
 */
class Water extends DaActiveRecord {

  const ID_OBJECT = 'project-vid-vody';

  protected $idObject = self::ID_OBJECT;

  /**
   * Returns the static model of the specified AR class.
   * @param string $className active record class name.
   * @return Water the static model class
   */
  public static function model($className = __CLASS__) {
    return parent::model($className);
  }

  /**
   * @return string the associated database table name
   */
  public function tableName() {
    return 'pr_water';
  }

  /**
   * @return array validation rules for model attributes.
   */
  public function rules() {
    return array(
      array('title', 'required'),
      array('photo, sequence, visible', 'numerical', 'integerOnly'=>true),
      array('title, price', 'length', 'max'=>255),
      array('short', 'safe'),
    );
  }

  /**
   * @return array relational rules.
   */
  public function relations() {
    return array(
      'photoFile' => array(self::BELONGS_TO, 'File', 'photo'),
    );
  }

  /**
   * @return array customized attribute labels (name=>label)
   */
  public function attributeLabels() {
    return array(
      'id_water' => 'ID',
      'title' => 'Название',
      'short' => 'Краткое описание',
      'photo' => 'Фото',
      'price' => 'Цена',
      'sequence' => 'п/п',
      'visible' => 'Видимость',
    );
  }

  public function defaultScope() {
    $t = $this->getTableAlias(false,false);
    return array(
      'condition' => $t.'.visible = 1'
    );
  }

  public function behaviors() {
    return array(
      'ImagePreviewBehavior' => array(
        'class' => 'ImagePreviewBehavior',
        'imageProperty' => 'photoFile',
        'formats' => array(
          '_list' => array(
            //'width' => 290,
            'height' => 406,
          ),
        ),
      ),
    );
  }

  public function getUrl() {
    return Yii::app()->createUrl('order/default/view/', array(
      'id' => $this->primaryKey
    ));
  }

}