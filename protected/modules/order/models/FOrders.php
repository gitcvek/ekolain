<?php

/**
 * Модель для таблицы "f_orders".
 *
 * The followings are the available columns in table 'f_orders':
 * @property integer $id_orders
 * @property integer $id_client
 * @property string $date_order
 * @property string $product
 * @property integer $count
 * @property integer $price
 * @property string $status
 *
 * The followings are the available model relations:
 * @property FClients $client
 */
class FOrders extends DaActiveRecord {

  const ID_OBJECT = 'project-zakazy-vneschnie';

  protected $idObject = self::ID_OBJECT;

  /**
   * Returns the static model of the specified AR class.
   * @param string $className active record class name.
   * @return FOrders the static model class
   */
  public static function model($className = __CLASS__) {
    return parent::model($className);
  }

  /**
   * @return string the associated database table name
   */
  public function tableName() {
    return 'f_orders';
  }

  /**
   * @return array validation rules for model attributes.
   */
  public function rules() {
    return array(
      array('id_client, count, price', 'numerical', 'integerOnly'=>true),
      array('date_order, product, status', 'length', 'max'=>255),
    );
  }

  /**
   * @return array relational rules.
   */
  public function relations() {
    return array(
      'client' => array(self::BELONGS_TO, 'FClients', 'id_client'),
    );
  }

  /**
   * @return array customized attribute labels (name=>label)
   */
  public function attributeLabels() {
    return array(
      'id_orders' => 'ID',
      'id_client' => 'Клиент',
      'date_order' => 'Дата заказа',
      'product' => 'Наименование продукции',
      'count' => 'Кол-во',
      'price' => 'Стоимость',
      'status' => 'Статус заказа',
    );
  }

}