<?php

  /**
   * Модель для таблицы "pr_order".
   *
   * The followings are the available columns in table 'pr_order':
   * @property integer $id_order
   * @property string $fio
   * @property string $phone
   * @property string $address
   * @property string $email
   * @property string $convenient_time
   * @property string $subscriber_number
   * @property string $description
   * @property string $company
   * @property string $date
   *
   * The followings are the available model relations:
   * @property WaterCount[] $waterCounts
   * @property PUser $user
   */
  class Order extends DaActiveRecord {

    const ID_OBJECT = 'project-zakaz';
    const ID_EVENT_NEW_ORDER = 107;
    const ID_EVENT_NEW_ORDER_SUBSCRIBER = 3;
    const ID_REFERENCE_TIMES = 'project-reference-vremya-dostavki';

    protected $idObject = self::ID_OBJECT;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Order the static model class
     */
    public static function model($className = __CLASS__) {
      return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
      return 'pr_order';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
      return array(
        array(
          'fio, phone, address, email, convenient_time, subscriber_number, company',
          'length',
          'max' => 255
        ),
        array(
     //   'date, new, delivery_date',
          'date, delivery_date',
          'length',
          'max' => 10
        ),
        array(
          'description,id_user',
          'safe'
        ),
      );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
      return array(
        'waterCounts' => array(
          self::HAS_MANY,
          'WaterCount',
          'id_order'
        ),
        'user' => array(
          self::BELONGS_TO,
          'PUser',
          'id_user'
        )
      );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
      return array(
        'id_order' => 'ID',
        'fio' => 'ФИО (название организации)',
        'phone' => 'Телефон',
        'address' => 'Адрес',
        'email' => 'email',
        'convenient_time' => 'Удобное время доставки',
        'subscriber_number' => 'Абонентский номер',
        'description' => 'Примечание',
        'company' => 'Компания',
        'date' => 'Дата',
        'delivery_date' => 'Дата доставки',
        'id_user' => 'Зарегистрированный пользователь'
      );
    }

    public function getTimes() {
      return $times = ReferenceElement::model()->findAll('id_reference = "' . self::ID_REFERENCE_TIMES . '"');
    }

    public function getDateCreate() {
      if($this->date) {
        return Yii::app()->dateFormatter->format('dd MMMM yyyy',$this->date);
      }
      return "";
    }
    public function getDateDelivery() {
      if($this->delivery_date) {
        return Yii::app()->dateFormatter->format('dd MMMM yyyy',$this->delivery_date);
      }
      return "";
    }
  }