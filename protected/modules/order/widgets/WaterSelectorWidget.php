<?php
/**
 * Created by PhpStorm.
 * User: cranky4
 * Date: 05.12.14
 * Time: 11:44
 */

class WaterSelectorWidget extends DaWidget {

  public function run() {
    $models = Water::model()->with('photoFile')->findAll();
    $numbers = array();
    for($i=0;$i<21;$i++){
      $numbers[] = $i;
    }

    $this->render('waterSelector', array(
      'models' => $models,
      'numbers' => $numbers
    ));
  }

}