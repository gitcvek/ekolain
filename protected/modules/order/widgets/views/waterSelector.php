<?php
  /**
   * Created by PhpStorm.
   * User: cranky4
   * Date: 05.12.14
   * Time: 11:58
   *
   * @var Water $model
   */
?>

<div class="water-selector">
  <form class="js_water_select_form" onsubmit="return false;">
    <div class="items row">
      <?php foreach ($models as $model): ?>
        <div class="item col-md-6">
		          <div class="title">
            <h3><?php echo $model->title; ?></h3>
          </div>
          <div class="photo img-thumbnail" title="x1" id="<?php echo 'item_'.$model->primaryKey;?>">
            <?php if ($model->photoFile && file_exists($model->photoFile->getFilePath(true)) && ($file = $model->getImagePreview('_list')->getFilePath())) {
              $img = CHtml::image("/" . $file, $model->title, array(
                'class' => 'img',
              ));
            } else {
              $img = CHtml::image("http://placehold.it/290x217", $model->title, array(
                'class' => 'img img-thumbnail',
              ));
            }
            	echo $img;
             /* echo CHtml::link($img, $model->getUrl(), array(
                'class' => ''
              )); */
            ?>
            <?php ?>
         </div>
         <div class="item-descr">


          <div class="price">
            Цена за штуку:
           <b> <?php echo $model->price; ?>*</b>
            x
            <?php 
           echo CHtml::numberField('count[' . $model->primaryKey . ']', 0, array(
              'class' => 'js_water_count',
           		'min' => '0'
            ))
            
            /*echo CHtml::dropDownList('count[' . $model->primaryKey . ']', 0, $numbers, array(
              'class' => 'js_water_count'
            ))*/ ?>
            шт.
          </div>

          <div class="short">
            <?php echo $model->short; ?>
          </div>
        </div>
        </div>
      <?php endforeach; ?>
    </div>
    <div class="alert text-muted"><b>Внимание!!! Заказ воды через сайт пока доступен только для г. Сыктывкара</b><br>
	<b>* Цена 19 л воды с доставкой может различаться в зависимости от формы оплаты и действующих акций</b>
	</div>

  </form>

</div>

<script type="text/javascript">
//при выборе кол-ва воды скролл к форме заказа
$('.js_water_count').change(function() {
	$("html, body").animate({ scrollTop: $('.water-form').offset().top }, 1000);
});

$('.photo').click(function() {
	var id = '#count'+$(this).attr('id').replace(/item/, '');
	$(id).val(parseInt($(id).val(),10)+1);
});
</script>