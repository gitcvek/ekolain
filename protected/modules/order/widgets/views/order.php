<?php
  /**
   * Created by PhpStorm.
   * User: cranky4
   * Date: 05.12.14
   * Time: 11:29
   */
?>

<div class="b-water-modal modal fade" id="BuyWater" tabindex="-1" role="dialog" aria-labelledby="BuyWaterLabel"
     aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
            class="sr-only">Close</span></button>
        <h4 class="modal-title" id="BuyWaterlLabel">Купить воду</h4>
      </div>
      <div class="modal-body">
        <a href="#" id="order-delivery" class="" data-dismiss="modal" data-toggle="modal"
           data-target="#OrderDelivery"><img src="/themes/business/gfx/btn_order.png"></a>
        <a href="/page/stores/" id="firm-shop" class=""><img src="/themes/business/gfx/btn_shop.png"></a>
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>

<div class="b-water-modal modal fade" id="OrderDelivery" tabindex="-1" role="dialog"
     aria-labelledby="OrderDeliveryLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
            class="sr-only">Close</span></button>
        <h4 class="modal-title" id="OrderDeliveryLabel">Заказать воду с доставкой</h4>
      </div>
      <div class="modal-body">
        <?php if (Yii::app()->getUser()->isGuest): ?>
          <a href="#" data-dismiss="modal" data-target="#AuthModal" data-toggle="modal"  id="order-delivery" class="b-water-modal__nav-link">Заказать воду (зарегистрированным
            пользователям)</a>
        <?php else: ?>
          <a href="#" id="order-delivery" class="b-water-modal__nav-link">Заказать воду (зарегистрированным
            пользователям)</a>
        <?php endif; ?>
        <a href="<?php echo Yii::app()->createUrl('order/default/index/') ?>" id="order-delivery" class="b-water-modal__nav-link">Заказать воду без регистрации</a>
      </div>
      <div class="modal-footer">
        <a href="<?php echo Yii::app()->createUrl('user/cabinet/register/') ?>" class="register">зарегистрироваться для
          быстрого заказа воды с сайта</a>
      </div>
    </div>
  </div>
</div>
