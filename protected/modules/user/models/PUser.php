<?php
  /**
   * Created by PhpStorm.
   * User: cranky4
   * Date: 03.12.14
   * Time: 13:38
   */

  Yii::import('ygin.modules.user.models.User');


  /**
   * Модель для таблицы "da_users".
   *
   * The followings are the available columns in table 'da_users':
   * @property integer $id_user
   * @property string $name
   * @property string $user_password
   * @property string $mail
   * @property string $full_name
   * @property string $rid
   * @property integer $create_date
   * @property integer $count_post
   * @property integer $active
   * @property integer $requires_new_password
   * @property string $salt
   * @property string $password_strategy
   * @property string $subscriber_number
   * @property string $phone
   * @property string $address
   * @property string $convenient_time
   * @property integer $yur
   * @property string $company
   *
   * The followings are the available model relations:
   * @property Comment[] $comments
   * @property EventSubscriber[] $eventSubscribers
   */
  class PUser extends User {

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
      return array(
        'id_user' => 'ID',
        'name' => 'Логин',
        'user_password' => 'Пароль',
        'mail' => 'E-mail',
        'full_name' => 'Имя пользователя',
        'rid' => 'Регистрационный ИД',
        'create_date' => 'Дата регистрации пользователя',
        'count_post' => 'count_post',
        'active' => 'Активен',
        'requires_new_password' => 'Необходимо сменить пароль',
        'salt' => 'Salt',
        'password_strategy' => 'Password Strategy',
        'subscriber_number' => 'Абонентский номер',
        'phone' => 'Телефон',
        'address' => 'Адрес',
//        'convenient_time' => 'Предпочитаемое время доставки',
        'yur' => 'Юр. лицо',
        'company' => 'Компания',
        'personal_data_agreement' => 'Обработка персональных данных'
      );
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
      return CMap::mergeArray(parent::rules(), array(
        array('yur', 'numerical', 'integerOnly'=>true),
     //   array('subscriber_number, phone, address, convenient_time, company', 'length', 'max'=>255),
        array('subscriber_number, phone, address, company', 'length', 'max'=>255),
      ));
    }

    public function getSubscriberNumber() {
      if ($this->subscriber_number) {
        return $this->subscriber_number;
      }
      return null;
    }

    public function getClient() {
      if($cl = FClients::model()->findAllByAttributes(array(
        'id_user' => $this->id_user,
      ))) {
        if(count($cl) == 1) {
          return current($cl);
        }
      }
      return false;
    }
    
  }