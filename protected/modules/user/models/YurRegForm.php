<?php
/**
 * Created by PhpStorm.
 * User: cranky4
 * Date: 03.12.14
 * Time: 11:55
 */

class YurRegForm extends BaseFormModel {
  public $login;
  public $company;
  public $fio;
  public $name;
  public $phone;
  public $address;
  public $email;
  public $convenient_time;
  public $password;
  public $confirm_password;
  public $personal_data_agreement;

  public function attributeLabels() {
    return array(
      'fio' => 'ФИО ответственного за доставку воды',
      'company'=> 'Название компании',
      'phone' => 'Телефон',
      'address' => 'Адрес',
      'email' => 'email',
      'convenient_time' => 'Удобное время доставки',
      'login' => 'Логин',
      'password' => 'Пароль',
      'confirm_password' => 'Повторите пароль',
      'personal_data_agreement' => 'Согласие на обработку персональных данных'
    );
  }

  public function rules() {
    return array(
      array(
        'login, password, confirm_password, fio, phone, address, email, convenient_time, company',
        'required',
        'on' => 'register'
      ),
      array(
        'login',
        'uniqueLogin',
        'on' => 'register'
      ),
//      array(
//        'company',
//        'uniqueCompany',
//        'on' => 'register'
//      ),
      array(
        'email',
        'email'
      ),
      array(
        'personal_data_agreement',
        'boolean',
      ),
      array(
        'personal_data_agreement',
        'required',
        'message' => 'Нужно дать согласие на обработку персональных данных'
      ),
      array(
        'confirm_password',
        'compare',
        'compareAttribute' => 'password',
        'message' => 'Пароли не совпадают'
      ),
      array(
        'password',
        'length',
        'min' => 6,
      ),
    );
  }

  public function uniqueLogin($attribute, $params) {
    $row = Yii::app()->db->createCommand()
          ->select("COUNT(name) as c")
          ->from('da_users')
          ->where("name = :NAME", array(
            ':NAME' => $this->$attribute
          ))->queryRow();
    if(HArray::val($row,'c'))
      $this->addError($attribute,'Этот логин занят');
  }

  public function uniqueCompany($attribute, $params) {
    $row = Yii::app()->db->createCommand()
      ->select("COUNT(name) as c")
      ->from('da_users')
      ->where("company = :NAME", array(
        ':NAME' => $this->$attribute
      ))->querRow();
    if(HArray::val($row,'c'))
      $this->addError($attribute,'Этот логин занят');
  }

} 