<?php
/**
 * Created by PhpStorm.
 * User: cranky4
 * Date: 03.12.14
 * Time: 11:55
 */

class PhizRegForm extends BaseFormModel {
  public $login;
  public $fio;
  public $phone;
  public $address;
  public $email;
//  public $convenient_time;
  public $password;
  public $confirm_password;
  public $personal_data_agreement;
//  public $new_client;
//  public $subscriber_number;

  public function attributeLabels() {
    return array(
      'fio' => 'ФИО (название организации)',
      'phone' => 'Телефон',
      'address' => 'Адрес',
      'email' => 'email',
//      'convenient_time' => 'Удобное время доставки',
      'login' => 'Логин',
      'password' => 'Пароль',
      'confirm_password' => 'Повторите пароль',
      'personal_data_agreement' => 'Согласие на обработку персональных данных',
//      'new_client' => 'Новый клиент',
      'subscriber_number' => 'Абонентский номер'
    );
  }

  public function rules() {
    return array(
      array(
      //  'login, password, confirm_password, fio, phone, address, email, convenient_time',
      	'login, password, confirm_password, fio, phone, address, email',
        'required',
        'on' => 'register'
      ),
      array(
        'login',
        'uniqueLogin',
        'on' => 'register'
      ),
      // array(
      //   'subscriber_number',
      //   'checkAbNumber'
      // ),
      array(
        'email',
        'email'
      ),
      array(
        'personal_data_agreement',
        'boolean',
      ),
      array(
        'personal_data_agreement',
        'required',
        'message' => 'Нужно дать согласие на обработку персональных данных'
      ),
      array(
        'confirm_password',
        'compare',
        'compareAttribute' => 'password',
        'message' => 'Пароли не совпадают'
      ),
      array(
        'password',
        'length',
        'min' => 6,
      ),
  //    array(
  //      'new_client,subscriber_number',
  //      'safe'
  //   )
    );
  }

  public function uniqueLogin($attribute, $params) {
    $row = Yii::app()->db->createCommand()
      ->select("COUNT(name) as c")
      ->from('da_users')
      ->where("name = :NAME", array(
        ':NAME' => $this->$attribute
      ))->queryRow();
    if(HArray::val($row,'c'))
      $this->addError($attribute,'Этот логин занят');
  }

  public function checkAbNumber($attr,$params){
    if($this->$attr && is_int($this->$attr*1)) {
      $client = FClients::model()->findByPk($this->$attr);
      if(null === $client)
        $this->addError($attr,'Введите правильный абонентский номер. Если у вас его нет, или вы его забыли, оставьте поле пустым');
    }

  }

} 