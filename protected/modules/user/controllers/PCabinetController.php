<?php
  /**
   * Created by PhpStorm.
   * User: cranky4
   * Date: 03.12.14
   * Time: 12:41
   */
  Yii::import('ygin.modules.user.controllers.CabinetController');
  Yii::import('application.modules.user.models.PhizRegForm');
  Yii::import('application.modules.user.models.YurRegForm');

  class PCabinetController extends CabinetController {

    public function accessRules() {
      return CMap::mergeArray(array(
        array('allow',
          'actions'=>array('history', 'view'),
          'users'=>array('@'),
        ),
      ), parent::accessRules());
    }

    public function actionProfile() {
      $model = Yii::app()->user->getModel();
      $model->scenario = 'profile';
      $modelClass = get_class($model);
      $this->performAjaxValidation($model, 'profile-form');

      if (isset($_POST[$modelClass])) {
        $model->attributes = $_POST[$modelClass];
        if ($model->save()) {
          Yii::app()->user->setFlash('profileSuccess', 'Профиль успешно изменен.');
          $this->refresh();
        }
      }

      if($client = FClients::model()->findAllByAttributes(array(
        'id_user' => $model->primaryKey,
      ))) {
        if(count($client) > 1) {
          throw new CHttpException(500,'Ошибка данных клиента. Сообщите о ней оператору. Спасибо!');
        } else if(count($client) == 1){
          $client = current($client);
        } else {
          $client = false;
        }
      }

      //удобное время доставки
/*      $_convinientTimesReference = ReferenceElement::model()->findAllByAttributes(array(
        'id_reference' => 'project-reference-vremya-dostavki'
      ));
      $convinientTimes = CHtml::listData($_convinientTimesReference, 'id_reference_element', 'value'); */

      //Затираем пароль, чтобы он не отображался в инпуте
      $model->user_password = '';
      $this->render('/profile', array(
        'model' => $model,
        'client' => $client,
       // 'convinientTimes' => $convinientTimes
      ));
    }


    public function actionRegister($yur = null) {
      if (!Yii::app()->request->getParam('yur')) {
        $model = new PhizRegForm('register');
        $modelClass = get_class($model);
        $formId = 'phiz-reg-form';
        $yur = 0;
      } else {
        $model = new YurRegForm('register');
        $modelClass = get_class($model);
        $formId = 'yur-reg-form';
        $yur = 1;
      }
      $this->performAjaxValidation($model, $formId);

      if ($data = HArray::val($_POST, $modelClass)) {


        $model = BaseActiveRecord::newModel('User');
        $model->attributes = $data;
        $model->name = HArray::val($data, 'login');
        $model->mail = HArray::val($data, 'email');
        $model->full_name = HArray::val($data, 'fio');
        $model->company = HArray::val($data, 'company');
        $model->phone = HArray::val($data, 'phone');
        $model->address = HArray::val($data, 'address');
//       $model->convenient_time = HArray::val($data, 'convenient_time');
        $model->user_password = HArray::val($data, 'password');
//        $model->subscriber_number = HArray::val($data, 'subscriber_number');
        $model->yur = $yur;

        $identity = new UserIdentity($model->name, $model->user_password);
        // $model->onAfterSave = array(
        //   $this,
        //   'sendRegisterMessage'
        // );

        if ($model->save()) {
          //если разрешено сразу авторизовать пользователя
          if (Yii::app()->getModule('user')->immediatelyAuthorization) {
            //загружаем модель пользователя
            $identity->authenticate();
            //Сразу авторизуем пользователя
            Yii::app()->user->login($identity);
            Yii::app()->user->setFlash('registerSuccess', 'Благодарим за регистрацию! Через 24 часа Вы сможете пользоваться личным кабинетом и удобно заказывать воду как зарегистрированный пользователь.');
          } else {
            Yii::app()->user->setFlash('registerSuccess', 'Благодарим за регистрацию! Через 24 часа Вы сможете пользоваться личным кабинетом и удобно заказывать воду как зарегистрированный пользователь.');
          }

          $this->redirect(Yii::app()->createUrl($this->getRedirectRouteAfterRegister()));
        }
      }

      //удобное время доставки
/*      $_convinientTimesReference = ReferenceElement::model()->findAllByAttributes(array(
        'id_reference' => 'project-reference-vremya-dostavki'
      ));
      $convinientTimes = CHtml::listData($_convinientTimesReference, 'id_reference_element', 'value');
*/
      $this->render('/register', array(
//        'convinientTimes' => $convinientTimes,
        'phizForm' => new PhizRegForm('register'),
        'yurForm' => new YurRegForm('register'),
      ));
    }

    public function actionLogout() {
      if (Yii::app()->isBackend) Yii::app()->user->returnUrl = Yii::app()->homeUrl;
      $returnUrl = Yii::app()->user->returnUrl;
      Yii::app()->user->logout();
      $this->redirect('/');
    }


//старая история =)
//    public function actionHistory($id_user = null) {
//      if (null === $id_user) {
//        $id_user = Yii::app()->user->id;
//      }
//
//      $criteria=new CDbCriteria();
//      $criteria->compare('id_user',$id_user);
//      $criteria->with = array(
//        'waterCounts.water'
//      );
//      //заказы
//      $count = Order::model()->count($criteria);
//      $pages=new CPagination($count);
//
//      // results per page
//      $pages->pageSize=40;
//      $pages->applyLimit($criteria);
//      $orders=Order::model()->findAll($criteria);
//
//      $this->render('/history', array(
//        'orders' => $orders,
//        'pages' => $pages
//      ));
//    }

  public function actionHistory() {
    $user = Yii::app()->user->getModel();

    $client = FClients::model()->findAllByAttributes(array(
      'id_user' => $user->primaryKey
	));

    if(count($client) == 0) {
      $this->render('/historyEmpty');
      Yii::app()->end();
    }

    if(count($client) > 1) {
      $this->render('/historyEmpty', array(
        'msg' => 'Ошибка получения истории заказов. Попробуйте позднее. Если ошибка повторяется, сообщите оператору.'
      ));
      Yii::app()->end();	
    }

    //только 1 клиент у юзера
    $client = current($client);

    $criteria=new CDbCriteria();
    $criteria->compare('id_client',$client->primaryKey);
    $criteria->order = "status ASC";

    //заказы
    $count = FOrders::model()->count($criteria);
    $pages=new CPagination($count);

    // results per page
    $pages->pageSize=40;
    $pages->applyLimit($criteria);
    $orders = FOrders::model()->findAll($criteria);

    if(count($orders) == 0) {
      $this->render('/historyEmpty');
      Yii::app()->end();
    }

    $this->render('/history', array(
      'orders' => $orders,
      'pages' => $pages,
      'client' => $client
    ));

  }

  public function actionView($order_id) {
    $criteria=new CDbCriteria();
    $criteria->addCondition('id_orders = :id');
    $criteria->params = array(':id' => $order_id);
    $order = FOrders::model()->findAll($criteria);
    if(count($order) == 0) {
      $this->render('/historyEmpty');
      Yii::app()->end();
    }
    //print_r($order);
    $this->render('/orderView', array(
        'order' => $order
    ));
  }

    public function actionLogin() {
      $model = BaseFormModel::newModel('LoginForm');
      $modelClass = get_class($model);
      $ajaxId = isset($_POST['login-widget-form']) ?'login-widget-form' : 'login-form';
      if (isset($_POST['ajax']) && $_POST['ajax']===$ajaxId) {
        echo CActiveForm::validate($model);
        Yii::app()->end();
      }
      if (isset($_POST[$modelClass])) {
        $model->attributes = $_POST[$modelClass];
        if ($model->validate() && $model->login()) {
          Yii::log('Успешная авторизация под логином '.$model->username, CLogger::LEVEL_INFO, 'application.login.success');
          unset(Yii::app()->request->cookies['login_attempt']);
          $this->redirect(Yii::app()->createUrl('order/default/index/'));
        } else {
          Yii::log('Ошибка авторизации (логин='.$model->username.')', CLogger::LEVEL_INFO, 'application.login.error');
          Yii::app()->user->setFlash('user_login_error', CHtml::errorSummary($model, ' '));
          if (isset(Yii::app ()->request->cookies[ 'login_attempt' ])) {
            $cookie = Yii::app ()->request->cookies[ 'login_attempt'];
            $cookie->value += 1;
          } else {
            $cookie = new CHttpCookie('login_attempt', 1);
            $cookie->expire = time()*60;
          }
          Yii::app ()->request->cookies[ 'login_attempt' ] = $cookie;
        }
      }

      $view = '/login';
      if (Yii::app()->isBackend) $view = 'backend.views.auth';
      if ($view != null) {
        $this->render($view, array('model'=>$model));
      } else {
        $this->redirect(Yii::app()->user->returnUrl);
      }
    }
  }