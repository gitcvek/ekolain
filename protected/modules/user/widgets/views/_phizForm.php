<?php
  /**
   * @var $form CActiveForm
   * @var PhizRegForm $model
   */
  $form = $this->beginWidget('CActiveForm', array(
    'id' => 'phiz-reg-form',
    'action' => Yii::app()->createUrl('user/cabinet/register/'),
    'enableAjaxValidation' => true,
    'enableClientValidation' => true,
    'focus' => array(
      $model,
      'fio'
    ),
    'htmlOptions' => array(
      'class' => 'form-horizontal',
    ),
    'clientOptions' => array(
      'validateOnSubmit' => true,
      'validateOnChange' => false,
    ),
    'errorMessageCssClass' => 'label label-danger',
  ));
?>
<?php /*
  <div class="form-group">
    <?php echo $form->labelEx($model, 'subscriber_number', array("class" => "col-md-4 control-label")); ?>
    <div class="col-md-4">
      <?php echo $form->textField($model, 'subscriber_number', array('class' => 'form-control')); ?>
      <?php echo $form->error($model, 'subscriber_number', array('class' => 'label label-danger')); ?>
    </div>
  </div>
*/?>
  <div class="form-group">
    <?php echo $form->labelEx($model, 'fio', array("class" => "col-md-4 control-label")); ?>
    <div class="col-md-4">
      <?php echo $form->textField($model, 'fio', array('class' => 'form-control')); ?>
      <?php echo $form->error($model, 'fio', array('class' => 'label label-danger')); ?>
    </div>
  </div>

  <div class="form-group">
    <?php echo $form->labelEx($model, 'phone', array("class" => "col-md-4 control-label")); ?>
    <div class="col-md-4">
      <?php echo $form->textField($model, 'phone', array('class' => 'form-control')); ?>
      <?php echo $form->error($model, 'phone', array('class' => 'label label-danger')); ?>
    </div>
  </div>

  <div class="form-group">
    <?php echo $form->labelEx($model,'address',  array("class" => "col-md-4 control-label")); ?>
    <div class="col-md-4">
      <?php echo $form->textArea( $model,'address', array('class' => 'form-control','rows' => 3));?>
      <?php echo $form->error($model,'address',  array('label label-danger')); ?>
    </div>
  </div>

  <div class="form-group">
    <?php echo $form->labelEx($model, 'email', array("class" => "col-md-4 control-label")); ?>
    <div class="col-md-4">
      <?php echo $form->textField($model, 'email', array('class' => 'form-control')); ?>
      <?php echo $form->error($model, 'email', array('class' => 'label label-danger')); ?>
    </div>
  </div>
<?php /*
  <div class="form-group">
    <?php echo $form->labelEx($model, 'convenient_time', array("class" => "col-md-4 control-label")); ?>
    <div class="col-md-4">
      <?php echo $form->dropDownList($model, 'convenient_time', $convinientTimes, array('class' => 'form-control')); ?>
      <?php echo $form->error($model, 'convenient_time', array('class' => 'label label-danger')); ?>
    </div>
  </div>

  <div class="form-group">
    <?php echo $form->labelEx($model, 'new_client', array("class" => "col-md-4 control-label")); ?>
    <div class="col-md-4">
      <?php echo $form->checkBox($model, 'new_client', array('class' => 'form-control')); ?>
      <?php echo $form->error($model, 'new_client', array('class' => 'label label-danger')); ?>
    </div>
  </div>
*/?>
  <div class="form-group">
    <?php echo $form->labelEx($model, 'personal_data_agreement', array("class" => "col-md-4 control-label")); ?>
    <div class="col-md-4">
      <?php echo $form->checkBox($model, 'personal_data_agreement', array('class' => 'form-control')); ?>
      <?php echo $form->error($model, 'personal_data_agreement', array('class' => 'label label-danger')); ?>
    </div>
  </div>

  <hr>

  <div class="form-group">
    <?php echo $form->labelEx($model, 'login', array("class" => "col-md-4 control-label")); ?>
    <div class="col-md-4">
      <?php echo $form->textField($model, 'login', array('class' => 'form-control')); ?>
      <?php echo $form->error($model, 'login', array('class' => 'label label-danger')); ?>
    </div>
  </div>

  <div class="form-group">
    <?php echo $form->labelEx($model, 'password', array("class" => "col-md-4 control-label")); ?>
    <div class="col-md-4">
      <?php echo $form->passwordField($model, 'password', array('class' => 'form-control')); ?>
      <?php echo $form->error($model, 'password', array('class' => 'label label-danger')); ?>
    </div>
  </div>

  <div class="form-group">
    <?php echo $form->labelEx($model, 'confirm_password', array("class" => "col-md-4 control-label")); ?>
    <div class="col-md-4">
      <?php echo $form->passwordField($model, 'confirm_password', array('class' => 'form-control')); ?>
      <?php echo $form->error($model, 'confirm_password', array('class' => 'label label-danger')); ?>
    </div>
  </div>

  <div class="form-group">
    <div class="col-md-4 col-md-offset-4">
      <?php echo CHtml::submitButton('Отправить', array(
        'class' => 'btn btn-primary'
      ))?>
    </div>
  </div>

<?php
  $this->endWidget();
?>