<?php
/**
 * Created by PhpStorm.
 * User: cranky4
 * Date: 03.12.14
 * Time: 12:08
 */
?>

<!-- Modal -->
<div class="modal fade b-water-modal" id="register" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Регистрация</h4>
      </div>
      <div class="modal-body">
        <div role="tabpanel">

          <!-- Nav tabs -->
          <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active">
              <a href="#phiz_reg" aria-controls="home" role="tab" data-toggle="tab">Физическое лицо</a>
            </li>
            <li role="presentation">
              <a href="#yur_reg" aria-controls="profile" role="tab" data-toggle="tab">Юридическое лицо</a>
            </li>
          </ul>

          <!-- Tab panes -->
          <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="phiz_reg">
              <?php $this->controller->renderPartial('application.modules.user.widgets.views._phizForm', array(
                'model' => $phizForm,
               // 'convinientTimes' => $convinientTimes
              ));?>
            </div>
            <div role="tabpanel" class="tab-pane" id="yur_reg">
              <?php $this->controller->renderPartial('application.modules.user.widgets.views._yurForm', array(
                'model' => $yurForm,
               // 'convinientTimes' => $convinientTimes
              ));?>
            </div>
          </div>

        </div>
      </div>
      <div class="modal-footer">
        <p>Поля отмеченные * обязательны к заполнению</p>
      </div>
    </div>
  </div>
</div>