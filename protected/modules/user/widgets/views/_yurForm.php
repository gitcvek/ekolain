<?php
  /**
   * @var $form CActiveForm
   * @var YurRegForm $model
   */
  $form = $this->beginWidget('CActiveForm', array(
    'id' => 'yur-reg-form',
    'action' => Yii::app()->createUrl('user/cabinet/register/', array(
      'yur' => 1
    )),
    'enableAjaxValidation' => true,
    'enableClientValidation' => true,
    'focus' => array(
      $model,
      'company'
    ),
    'htmlOptions' => array(
      'class' => 'form-horizontal',
    ),
    'clientOptions' => array(
      'validateOnSubmit' => true,
      'validateOnChange' => false,
    ),
    'errorMessageCssClass' => 'label label-danger',
  ));
?>
  <div class="form-group">
    <?php echo $form->labelEx($model, 'company', array("class" => "col-md-4 control-label")); ?>
    <div class="col-md-4">
      <?php echo $form->textField($model, 'company', array('class' => 'form-control')); ?>
      <?php echo $form->error($model, 'company', array('class' => 'label label-danger')); ?>
    </div>
  </div>

  <div class="form-group">
    <?php echo $form->labelEx($model, 'fio', array("class" => "col-md-4 control-label")); ?>
    <div class="col-md-4">
      <?php echo $form->textField($model, 'fio', array('class' => 'form-control')); ?>
      <?php echo $form->error($model, 'fio', array('class' => 'label label-danger')); ?>
    </div>
  </div>

  <div class="form-group">
    <?php echo $form->labelEx($model, 'phone', array("class" => "col-md-4 control-label")); ?>
    <div class="col-md-4">
      <?php echo $form->textField($model, 'phone', array('class' => 'form-control')); ?>
      <?php echo $form->error($model, 'phone', array('class' => 'label label-danger')); ?>
    </div>
  </div>

  <div class="form-group">
    <?php echo $form->labelEx($model,'address',  array("class" => "col-md-4 control-label")); ?>
    <div class="col-md-4">
      <?php echo $form->textArea( $model,'address', array('class' => 'form-control','rows' => 3));?>
      <?php echo $form->error($model,'address',  array('label label-danger')); ?>
    </div>
  </div>

  <div class="form-group">
    <?php echo $form->labelEx($model, 'email', array("class" => "col-md-4 control-label")); ?>
    <div class="col-md-4">
      <?php echo $form->textField($model, 'email', array('class' => 'form-control')); ?>
      <?php echo $form->error($model, 'email', array('class' => 'label label-danger')); ?>
    </div>
  </div>
<?php /*
  <div class="form-group">
    <?php echo $form->labelEx($model, 'convenient_time', array("class" => "col-md-4 control-label")); ?>
    <div class="col-md-4">
      <?php echo $form->dropDownList($model, 'convenient_time', $convinientTimes,  array('class' => 'form-control')); ?>
      <?php echo $form->error($model, 'convenient_time', array('class' => 'label label-danger')); ?>
    </div>
  </div>
*/?>
  <hr>

  <div class="form-group">
    <?php echo $form->labelEx($model, 'login', array("class" => "col-md-4 control-label")); ?>
    <div class="col-md-4">
      <?php echo $form->textField($model, 'login', array('class' => 'form-control')); ?>
      <?php echo $form->error($model, 'login', array('class' => 'label label-danger')); ?>
    </div>
  </div>

  <div class="form-group">
    <?php echo $form->labelEx($model, 'password', array("class" => "col-md-4 control-label")); ?>
    <div class="col-md-4">
      <?php echo $form->passwordField($model, 'password', array('class' => 'form-control')); ?>
      <?php echo $form->error($model, 'password', array('class' => 'label label-danger')); ?>
    </div>
  </div>

  <div class="form-group">
    <?php echo $form->labelEx($model, 'confirm_password', array("class" => "col-md-4 control-label")); ?>
    <div class="col-md-4">
      <?php echo $form->passwordField($model, 'confirm_password', array('class' => 'form-control')); ?>
      <?php echo $form->error($model, 'confirm_password', array('class' => 'label label-danger')); ?>
    </div>
  </div>

  <div class="form-group">
    <div class="col-md-4 col-md-offset-4">
      <?php echo CHtml::submitButton('Оптравить', array(
        'class' => 'btn btn-primary'
      ))?>
    </div>
  </div>

<?php
  $this->endWidget();
?>