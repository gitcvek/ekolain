<?php
/**
 * Created by PhpStorm.
 * User: cranky4
 * Date: 03.12.14
 * Time: 12:07
 */
Yii::import('application.modules.user.models.PhizRegForm');
  Yii::import('application.modules.user.models.YurRegForm');
class RegisterWidget extends DaWidget {

  public function run() {

    //удобное время доставки
/*    $_convinientTimesReference = ReferenceElement::model()->findAllByAttributes(array(
      'id_reference' => 'project-reference-vremya-dostavki'
    ));
    $convinientTimes = CHtml::listData($_convinientTimesReference, 'id_reference_element', 'value');*/

    $this->render('registerWidget', array(
      'phizForm' => new PhizRegForm('register'),
      'yurForm' => new YurRegForm('register'),
     // 'convinientTimes' => $convinientTimes
    ));
  }

} 