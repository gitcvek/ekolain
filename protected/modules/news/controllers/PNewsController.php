<?php
  Yii::import('ygin.modules.news.controllers.NewsController');

  class PNewsController extends NewsController {
    const ID_CATEGORY_ACTION = 2;
    const ID_CATEGORY_NEWS = 1;

    public function actionIndex($idCategory = null, $idNews = null) {
      $criteria = new CDbCriteria();
      //$criteria->scopes = array('last');//неверное
      $criteria->order = 'date DESC';
      $newsModule = $this->getModule();
      $category = null;
      //$categories = array();
      //Если включено отображение категорий
      if ($newsModule->showCategories) {
        if ($idCategory !== null && $category = $this->loadModelOr404('NewsCategory', $idCategory)) {
          $criteria->compare('t.id_news_category', $idCategory);
        }
        //$categories = NewsCategory::model ()->findAll (array('order' => 'seq'));
      }
      //$count = News::model ()->count ($criteria);
      //$pages = new CPagination($count);
      //$pages->pageSize = $newsModule->itemsCountPerPage;
      //$pages->applyLimit ($criteria);
      $news = News::model()->findAll($criteria);
      if ($idCategory && $idCategory != self::ID_CATEGORY_ACTION && $idCategory != self::ID_CATEGORY_NEWS) {
        if ($idNews) {
          $current = News::model()->findByPk($idNews);
        } else if($idCategory) {
          $current = News::model()->find(array(
            'order' => 'date DESC',
            'condition' => 'id_news_category = :ID_NEWS_CATEGORY',
            'params' => array(
              ':ID_NEWS_CATEGORY' => $idCategory,
            ),
          ));
        } else {
          $current = News::model()->find();
        }

        if($current == null) {
          throw new CHttpException(404);          
        }

        $prev = News::model()->find(array(
          'condition' => 'date < :CUR_DATE AND id_news_category = :ID_NEWS_CATEGORY',
          'order' => 'date DESC',
          'params' => array(
            ':CUR_DATE' => $current->date,
            ':ID_NEWS_CATEGORY' => $idCategory,
          ),
        ));
        $next = News::model()->find(array(
          'condition' => 'date > :CUR_DATE AND id_news_category = :ID_NEWS_CATEGORY',
          'order' => 'date ASC',
          'params' => array(
            ':CUR_DATE' => $current->date,
            ':ID_NEWS_CATEGORY' => $idCategory,
          ),
        ));
        $this->render('/index', array(
          'news' => $news,
          // список новостей
          'prev' => $prev,
          'next' => $next,
          'current' => $current,
          //'pages' => $pages,  // пагинатор
          'category' => $category,
          // текущая категория
          //'categories' => $categories,  // все категории
        ));
      } else {
/*          if($idCategory == self::ID_CATEGORY_NEWS) {
            $this->render('/index_news', array(
                'news' => $news,
              // список новостей
                'category' => $category,
              // текущая категория
            ));

          }*/
        $this->render('/index_action', array(
          'news' => $news,
          // список новостей
          'category' => $category,
          // текущая категория
        ));
      }
    }

    public function actionModalView($id) {
      $model = $this->loadModelOr404('News', $id);
      echo $this->renderPartial('/modal_action', array('model' => $model), true);
    }
  }