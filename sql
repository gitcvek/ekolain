-- MySQL dump 10.13  Distrib 5.5.35, for debian-linux-gnu (i686)
--
-- Host: localhost    Database: ekolain
-- ------------------------------------------------------
-- Server version	5.5.35-0ubuntu0.12.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `da_auth_assignment`
--

DROP TABLE IF EXISTS `da_auth_assignment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `da_auth_assignment` (
  `itemname` varchar(64) NOT NULL,
  `userid` varchar(64) NOT NULL,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`itemname`,`userid`),
  CONSTRAINT `da_auth_assignment_ibfk_1` FOREIGN KEY (`itemname`) REFERENCES `da_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `da_auth_assignment_ibfk_2` FOREIGN KEY (`itemname`) REFERENCES `da_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `da_auth_assignment_ibfk_3` FOREIGN KEY (`itemname`) REFERENCES `da_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `da_auth_assignment_ibfk_4` FOREIGN KEY (`itemname`) REFERENCES `da_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `da_auth_assignment_ibfk_5` FOREIGN KEY (`itemname`) REFERENCES `da_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `da_auth_assignment_ibfk_6` FOREIGN KEY (`itemname`) REFERENCES `da_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `da_auth_assignment_ibfk_7` FOREIGN KEY (`itemname`) REFERENCES `da_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `da_auth_assignment_ibfk_8` FOREIGN KEY (`itemname`) REFERENCES `da_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `da_auth_assignment`
--

LOCK TABLES `da_auth_assignment` WRITE;
/*!40000 ALTER TABLE `da_auth_assignment` DISABLE KEYS */;
INSERT INTO `da_auth_assignment` VALUES ('dev','1',NULL,'N;'),('dev','106',NULL,'N;'),('dev','108',NULL,'N;'),('dev','39',NULL,'N;'),('dev','52',NULL,'N;'),('dev','53',NULL,'N;'),('dev','55',NULL,'N;'),('dev','57',NULL,'N;'),('dev','60',NULL,'N;'),('dev','61',NULL,'N;'),('dev','62',NULL,'N;'),('dev','65',NULL,'N;'),('dev','66',NULL,'N;'),('dev','67',NULL,'N;'),('editor','46',NULL,'N;'),('editor','63',NULL,'N;'),('editor','99',NULL,'N;');
/*!40000 ALTER TABLE `da_auth_assignment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `da_auth_item`
--

DROP TABLE IF EXISTS `da_auth_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `da_auth_item` (
  `name` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `da_auth_item`
--

LOCK TABLES `da_auth_item` WRITE;
/*!40000 ALTER TABLE `da_auth_item` DISABLE KEYS */;
INSERT INTO `da_auth_item` VALUES ('create_object_101',0,'Операция создание экземпляра объекта Наборы модулей',NULL,'N;'),('create_object_103',0,'Операция создание экземпляра объекта Модули сайта',NULL,'N;'),('create_object_105',0,'Операция создание экземпляра объекта Голосование',NULL,'N;'),('create_object_106',0,'Операция создание экземпляра объекта Ответы на голосование',NULL,'N;'),('create_object_20',0,'Операция создание экземпляра объекта Объекты',NULL,'N;'),('create_object_21',0,'Операция создание экземпляра объекта Свойства объекта',NULL,'N;'),('create_object_22',0,'Операция создания для объекта Типы данных',NULL,'N;'),('create_object_23',0,'Операция создание экземпляра объекта Группы пользователей',NULL,'N;'),('create_object_24',0,'Операция создание экземпляра объекта Пользователи',NULL,'N;'),('create_object_250',0,'Операция создание экземпляра объекта Комментарии',NULL,'N;'),('create_object_26',0,'Операция создание экземпляра объекта Права доступа',NULL,'N;'),('create_object_260',0,'Операция создание экземпляра объекта Баннеры',NULL,'N;'),('create_object_261',0,'Операция создание экземпляра объекта Баннерные места',NULL,'N;'),('create_object_27',0,'Операция создание экземпляра объекта Справочники',NULL,'N;'),('create_object_28',0,'Операция создание экземпляра объекта Значения справочника',NULL,'N;'),('create_object_30',0,'Операция создание экземпляра объекта Настройки сайта',NULL,'N;'),('create_object_31',0,'Операция создание экземпляра объекта Домены сайта',NULL,'N;'),('create_object_33',0,'Операция создание экземпляра объекта Формат сообщения',NULL,'N;'),('create_object_34',0,'Операция создание экземпляра объекта Подписчики на события',NULL,'N;'),('create_object_35',0,'Операция создание экземпляра объекта Тип события',NULL,'N;'),('create_object_47',0,'Операция создание экземпляра объекта Права пользователей',NULL,'N;'),('create_object_50',0,'Операция создание экземпляра объекта Почтовые аккаунты',NULL,'N;'),('create_object_500',0,'Операция создание экземпляра объекта Фотогалереи',NULL,'N;'),('create_object_501',0,'Операция создание экземпляра объекта Фотографии',NULL,'N;'),('create_object_502',0,'Операция создание экземпляра объекта Новости',NULL,'N;'),('create_object_503',0,'Операция создание экземпляра объекта Категории новостей',NULL,'N;'),('create_object_505',0,'Операция создание экземпляра объекта Вопрос',NULL,'N;'),('create_object_506',0,'Операция создание экземпляра объекта Ответ',NULL,'N;'),('create_object_507',0,'Операция создание экземпляра объекта Отвечающий',NULL,'N;'),('create_object_508',0,'Операция создание экземпляра объекта Специализация отвечающего',NULL,'N;'),('create_object_509',0,'Операция создание экземпляра объекта Категории продукции',NULL,'N;'),('create_object_51',0,'Операция создание экземпляра объекта Планировщик',NULL,'N;'),('create_object_511',0,'Операция создание экземпляра объекта Продукция',NULL,'N;'),('create_object_512',0,'Операция создание экземпляра объекта Вопрос-ответ',NULL,'N;'),('create_object_513',0,'Операция создание экземпляра объекта OpenID провайдер',NULL,'N;'),('create_object_514',0,'Операция создание экземпляра объекта OpenId аккаунты',NULL,'N;'),('create_object_517',0,'Операция создание экземпляра объекта Обратная связь',NULL,'N;'),('create_object_520',0,'Операция создание экземпляра объекта Витрина',NULL,'N;'),('create_object_521',0,'Операция создание экземпляра объекта Викторины',NULL,'N;'),('create_object_522',0,'Операция создание экземпляра объекта Вопросы викторины',NULL,'N;'),('create_object_523',0,'Операция создание экземпляра объекта Варианты ответов',NULL,'N;'),('create_object_524',0,'Операция создание экземпляра объекта Ответ пользователя',NULL,'N;'),('create_object_525',0,'Операция создание экземпляра объекта Брэнды',NULL,'N;'),('create_object_529',0,'Операция создание экземпляра объекта Статусы остатка продукции',NULL,'N;'),('create_object_530',0,'Операция создание экземпляра объекта Отзывы клиентов',NULL,'N;'),('create_object_54',0,'Операция создание экземпляра объекта Доступные локализации',NULL,'N;'),('create_object_61',0,'Операция создание экземпляра объекта Инструкции',NULL,'N;'),('create_object_63',0,'Операция создание экземпляра объекта Представление',NULL,'N;'),('create_object_66',0,'Операция создание экземпляра объекта Колонка представления',NULL,'N;'),('create_object_80',0,'Операция создание экземпляра объекта php-скрипты',NULL,'N;'),('create_object_86',0,'Операция создание экземпляра объекта Интерфейс php-скрипта',NULL,'N;'),('create_object_project-kolichestvo-vody',0,'Операция создания для объекта Количество воды',NULL,'N;'),('create_object_project-vid-vody',0,'Операция создания для объекта Вид воды',NULL,'N;'),('create_object_project-zakaz',0,'Операция создания для объекта Заказ',NULL,'N;'),('create_object_ygin-menu',0,'Операция создания для объекта Меню',NULL,'N;'),('create_object_ygin-views-generator',0,'Операция создания для объекта Генерация вьюхи',NULL,'N;'),('delete_object_101',0,'Операция удаления экземпляра объекта Наборы модулей',NULL,'N;'),('delete_object_103',0,'Операция удаления экземпляра объекта Модули сайта',NULL,'N;'),('delete_object_105',0,'Операция удаления экземпляра объекта Голосование',NULL,'N;'),('delete_object_106',0,'Операция удаления экземпляра объекта Ответы на голосование',NULL,'N;'),('delete_object_20',0,'Операция удаления экземпляра объекта Объекты',NULL,'N;'),('delete_object_21',0,'Операция удаления экземпляра объекта Свойства объекта',NULL,'N;'),('delete_object_22',0,'Операция удаления для объекта Типы данных',NULL,'N;'),('delete_object_23',0,'Операция удаления экземпляра объекта Группы пользователей',NULL,'N;'),('delete_object_24',0,'Операция удаления экземпляра объекта Пользователи',NULL,'N;'),('delete_object_250',0,'Операция удаления экземпляра объекта Комментарии',NULL,'N;'),('delete_object_26',0,'Операция удаления экземпляра объекта Права доступа',NULL,'N;'),('delete_object_260',0,'Операция удаления экземпляра объекта Баннеры',NULL,'N;'),('delete_object_261',0,'Операция удаления экземпляра объекта Баннерные места',NULL,'N;'),('delete_object_27',0,'Операция удаления экземпляра объекта Справочники',NULL,'N;'),('delete_object_28',0,'Операция удаления экземпляра объекта Значения справочника',NULL,'N;'),('delete_object_30',0,'Операция удаления экземпляра объекта Настройки сайта',NULL,'N;'),('delete_object_33',0,'Операция удаления экземпляра объекта Формат сообщения',NULL,'N;'),('delete_object_34',0,'Операция удаления экземпляра объекта Подписчики на события',NULL,'N;'),('delete_object_35',0,'Операция удаления экземпляра объекта Тип события',NULL,'N;'),('delete_object_47',0,'Операция удаления экземпляра объекта Права пользователей',NULL,'N;'),('delete_object_50',0,'Операция удаления экземпляра объекта Почтовые аккаунты',NULL,'N;'),('delete_object_500',0,'Операция удаления экземпляра объекта Фотогалереи',NULL,'N;'),('delete_object_501',0,'Операция удаления экземпляра объекта Фотографии',NULL,'N;'),('delete_object_502',0,'Операция удаления экземпляра объекта Новости',NULL,'N;'),('delete_object_503',0,'Операция удаления экземпляра объекта Категории новостей',NULL,'N;'),('delete_object_505',0,'Операция удаления экземпляра объекта Вопрос',NULL,'N;'),('delete_object_506',0,'Операция удаления экземпляра объекта Ответ',NULL,'N;'),('delete_object_507',0,'Операция удаления экземпляра объекта Отвечающий',NULL,'N;'),('delete_object_508',0,'Операция удаления экземпляра объекта Специализация отвечающего',NULL,'N;'),('delete_object_509',0,'Операция удаления экземпляра объекта Категории продукции',NULL,'N;'),('delete_object_51',0,'Операция удаления экземпляра объекта Планировщик',NULL,'N;'),('delete_object_511',0,'Операция удаления экземпляра объекта Продукция',NULL,'N;'),('delete_object_512',0,'Операция удаления экземпляра объекта Вопрос-ответ',NULL,'N;'),('delete_object_513',0,'Операция удаления экземпляра объекта OpenID провайдер',NULL,'N;'),('delete_object_514',0,'Операция удаления экземпляра объекта OpenId аккаунты',NULL,'N;'),('delete_object_517',0,'Операция удаления экземпляра объекта Обратная связь',NULL,'N;'),('delete_object_519',0,'Операция удаления экземпляра объекта Заказы пользователей',NULL,'N;'),('delete_object_520',0,'Операция удаления экземпляра объекта Витрина',NULL,'N;'),('delete_object_521',0,'Операция удаления экземпляра объекта Викторины',NULL,'N;'),('delete_object_522',0,'Операция удаления экземпляра объекта Вопросы викторины',NULL,'N;'),('delete_object_523',0,'Операция удаления экземпляра объекта Варианты ответов',NULL,'N;'),('delete_object_524',0,'Операция удаления экземпляра объекта Ответ пользователя',NULL,'N;'),('delete_object_525',0,'Операция удаления экземпляра объекта Брэнды',NULL,'N;'),('delete_object_529',0,'Операция удаления экземпляра объекта Статусы остатка продукции',NULL,'N;'),('delete_object_530',0,'Операция удаления экземпляра объекта Отзывы клиентов',NULL,'N;'),('delete_object_531',0,'Операция удаления экземпляра объекта Уведомления',NULL,'N;'),('delete_object_54',0,'Операция удаления экземпляра объекта Доступные локализации',NULL,'N;'),('delete_object_61',0,'Операция удаления экземпляра объекта Инструкции',NULL,'N;'),('delete_object_63',0,'Операция удаления экземпляра объекта Представление',NULL,'N;'),('delete_object_66',0,'Операция удаления экземпляра объекта Колонка представления',NULL,'N;'),('delete_object_80',0,'Операция удаления экземпляра объекта php-скрипты',NULL,'N;'),('delete_object_86',0,'Операция удаления экземпляра объекта Интерфейс php-скрипта',NULL,'N;'),('delete_object_project-kolichestvo-vody',0,'Операция удаления для объекта Количество воды',NULL,'N;'),('delete_object_project-vid-vody',0,'Операция удаления для объекта Вид воды',NULL,'N;'),('delete_object_project-zakaz',0,'Операция удаления для объекта Заказ',NULL,'N;'),('delete_object_ygin-invoice',0,'Операция удаления для объекта Счета',NULL,'N;'),('delete_object_ygin-menu',0,'Операция удаления для объекта Меню',NULL,'N;'),('delete_object_ygin-views-generator',0,'Операция удаления для объекта Генерация вьюхи',NULL,'N;'),('dev',2,'Разработчик',NULL,'N;'),('editor',2,'Редактор',NULL,'N;'),('edit_object_101',0,'Операция изменения экземпляра объекта Наборы модулей',NULL,'N;'),('edit_object_103',0,'Операция изменения экземпляра объекта Модули сайта',NULL,'N;'),('edit_object_105',0,'Операция изменения экземпляра объекта Голосование',NULL,'N;'),('edit_object_106',0,'Операция изменения экземпляра объекта Ответы на голосование',NULL,'N;'),('edit_object_20',0,'Операция изменения экземпляра объекта Объекты',NULL,'N;'),('edit_object_21',0,'Операция изменения экземпляра объекта Свойства объекта',NULL,'N;'),('edit_object_22',0,'Операция изменения для объекта Типы данных',NULL,'N;'),('edit_object_23',0,'Операция изменения экземпляра объекта Группы пользователей',NULL,'N;'),('edit_object_24',0,'Операция изменения экземпляра объекта Пользователи',NULL,'N;'),('edit_object_250',0,'Операция изменения экземпляра объекта Комментарии',NULL,'N;'),('edit_object_26',0,'Операция изменения экземпляра объекта Права доступа',NULL,'N;'),('edit_object_260',0,'Операция изменения экземпляра объекта Баннеры',NULL,'N;'),('edit_object_261',0,'Операция изменения экземпляра объекта Баннерные места',NULL,'N;'),('edit_object_27',0,'Операция изменения экземпляра объекта Справочники',NULL,'N;'),('edit_object_28',0,'Операция изменения экземпляра объекта Значения справочника',NULL,'N;'),('edit_object_29',0,'Операция изменения экземпляра объекта Группы системных параметров',NULL,'N;'),('edit_object_30',0,'Операция изменения экземпляра объекта Настройки сайта',NULL,'N;'),('edit_object_31',0,'Операция изменения экземпляра объекта Домены сайта',NULL,'N;'),('edit_object_33',0,'Операция изменения экземпляра объекта Формат сообщения',NULL,'N;'),('edit_object_34',0,'Операция изменения экземпляра объекта Подписчики на события',NULL,'N;'),('edit_object_35',0,'Операция изменения экземпляра объекта Тип события',NULL,'N;'),('edit_object_47',0,'Операция изменения экземпляра объекта Права пользователей',NULL,'N;'),('edit_object_50',0,'Операция изменения экземпляра объекта Почтовые аккаунты',NULL,'N;'),('edit_object_500',0,'Операция изменения экземпляра объекта Фотогалереи',NULL,'N;'),('edit_object_501',0,'Операция изменения экземпляра объекта Фотографии',NULL,'N;'),('edit_object_502',0,'Операция изменения экземпляра объекта Новости',NULL,'N;'),('edit_object_503',0,'Операция изменения экземпляра объекта Категории новостей',NULL,'N;'),('edit_object_505',0,'Операция изменения экземпляра объекта Вопрос',NULL,'N;'),('edit_object_506',0,'Операция изменения экземпляра объекта Ответ',NULL,'N;'),('edit_object_507',0,'Операция изменения экземпляра объекта Отвечающий',NULL,'N;'),('edit_object_508',0,'Операция изменения экземпляра объекта Специализация отвечающего',NULL,'N;'),('edit_object_509',0,'Операция изменения экземпляра объекта Категории продукции',NULL,'N;'),('edit_object_51',0,'Операция изменения экземпляра объекта Планировщик',NULL,'N;'),('edit_object_511',0,'Операция изменения экземпляра объекта Продукция',NULL,'N;'),('edit_object_512',0,'Операция изменения экземпляра объекта Вопрос-ответ',NULL,'N;'),('edit_object_513',0,'Операция изменения экземпляра объекта OpenID провайдер',NULL,'N;'),('edit_object_514',0,'Операция изменения экземпляра объекта OpenId аккаунты',NULL,'N;'),('edit_object_517',0,'Операция изменения экземпляра объекта Обратная связь',NULL,'N;'),('edit_object_519',0,'Операция изменения экземпляра объекта Заказы пользователей',NULL,'N;'),('edit_object_520',0,'Операция изменения экземпляра объекта Витрина',NULL,'N;'),('edit_object_521',0,'Операция изменения экземпляра объекта Викторины',NULL,'N;'),('edit_object_522',0,'Операция изменения экземпляра объекта Вопросы викторины',NULL,'N;'),('edit_object_523',0,'Операция изменения экземпляра объекта Варианты ответов',NULL,'N;'),('edit_object_524',0,'Операция изменения экземпляра объекта Ответ пользователя',NULL,'N;'),('edit_object_525',0,'Операция изменения экземпляра объекта Брэнды',NULL,'N;'),('edit_object_529',0,'Операция изменения экземпляра объекта Статусы остатка продукции',NULL,'N;'),('edit_object_530',0,'Операция изменения экземпляра объекта Отзывы клиентов',NULL,'N;'),('edit_object_531',0,'Операция изменения экземпляра объекта Уведомления',NULL,'N;'),('edit_object_54',0,'Операция изменения экземпляра объекта Доступные локализации',NULL,'N;'),('edit_object_61',0,'Операция изменения экземпляра объекта Инструкции',NULL,'N;'),('edit_object_63',0,'Операция изменения экземпляра объекта Представление',NULL,'N;'),('edit_object_66',0,'Операция изменения экземпляра объекта Колонка представления',NULL,'N;'),('edit_object_80',0,'Операция изменения экземпляра объекта php-скрипты',NULL,'N;'),('edit_object_86',0,'Операция изменения экземпляра объекта Интерфейс php-скрипта',NULL,'N;'),('edit_object_project-kolichestvo-vody',0,'Операция изменения для объекта Количество воды',NULL,'N;'),('edit_object_project-vid-vody',0,'Операция изменения для объекта Вид воды',NULL,'N;'),('edit_object_project-zakaz',0,'Операция изменения для объекта Заказ',NULL,'N;'),('edit_object_ygin-invoice',0,'Операция изменения для объекта Счета',NULL,'N;'),('edit_object_ygin-menu',0,'Операция изменения для объекта Меню',NULL,'N;'),('edit_object_ygin-views-generator',0,'Операция изменения для объекта Генерация вьюхи',NULL,'N;'),('list_object_101',0,'Просмотр списка данных объекта Наборы модулей',NULL,'N;'),('list_object_103',0,'Просмотр списка данных объекта Модули сайта',NULL,'N;'),('list_object_20',0,'Просмотр списка данных объекта Объекты',NULL,'N;'),('list_object_21',0,'Просмотр списка данных объекта Свойства объекта',NULL,'N;'),('list_object_22',0,'Просмотр списка данных объекта Типы данных',NULL,'N;'),('list_object_23',0,'Просмотр списка данных объекта Группы пользователей',NULL,'N;'),('list_object_24',0,'Просмотр списка данных объекта Пользователи',NULL,'N;'),('list_object_250',0,'Просмотр списка данных объекта Комментарии',NULL,'N;'),('list_object_26',0,'Просмотр списка данных объекта Права доступа',NULL,'N;'),('list_object_27',0,'Просмотр списка данных объекта Справочники',NULL,'N;'),('list_object_28',0,'Просмотр списка данных объекта Значения справочника',NULL,'N;'),('list_object_29',0,'Просмотр списка данных объекта Группы системных параметров',NULL,'N;'),('list_object_30',0,'Просмотр списка данных объекта Настройки сайта',NULL,'N;'),('list_object_31',0,'Просмотр списка данных объекта Домены сайта',NULL,'N;'),('list_object_32',0,'Просмотр списка данных объекта Настройка прав',NULL,'N;'),('list_object_33',0,'Просмотр списка данных объекта Формат сообщения',NULL,'N;'),('list_object_34',0,'Просмотр списка данных объекта Подписчики на события',NULL,'N;'),('list_object_35',0,'Просмотр списка данных объекта Тип события',NULL,'N;'),('list_object_43',0,'Просмотр списка данных объекта SQL',NULL,'N;'),('list_object_47',0,'Просмотр списка данных объекта Права пользователей',NULL,'N;'),('list_object_50',0,'Просмотр списка данных объекта Почтовые аккаунты',NULL,'N;'),('list_object_500',0,'Просмотр списка данных объекта Галерея',NULL,'N;'),('list_object_501',0,'Просмотр списка данных объекта Фотографии',NULL,'N;'),('list_object_502',0,'Просмотр списка данных объекта Новости',NULL,'N;'),('list_object_503',0,'Просмотр списка данных объекта Категории новостей',NULL,'N;'),('list_object_505',0,'Просмотр списка данных объекта Вопрос',NULL,'N;'),('list_object_506',0,'Просмотр списка данных объекта Ответ',NULL,'N;'),('list_object_507',0,'Просмотр списка данных объекта Отвечающий',NULL,'N;'),('list_object_508',0,'Просмотр списка данных объекта Специализация отвечающего',NULL,'N;'),('list_object_51',0,'Просмотр списка данных объекта Планировщик',NULL,'N;'),('list_object_512',0,'Просмотр списка данных объекта Вопрос-ответ',NULL,'N;'),('list_object_513',0,'Просмотр списка данных объекта OpenID провайдер',NULL,'N;'),('list_object_514',0,'Просмотр списка данных объекта OpenId аккаунты',NULL,'N;'),('list_object_520',0,'Просмотр списка данных объекта Витрина',NULL,'N;'),('list_object_521',0,'Просмотр списка данных объекта Викторины',NULL,'N;'),('list_object_522',0,'Просмотр списка данных объекта Вопросы викторины',NULL,'N;'),('list_object_523',0,'Просмотр списка данных объекта Варианты ответов',NULL,'N;'),('list_object_524',0,'Просмотр списка данных объекта Ответ пользователя',NULL,'N;'),('list_object_528',0,'Управление объектом Плагины системы',NULL,'N;'),('list_object_531',0,'Просмотр списка данных объекта ид=531',NULL,'N;'),('list_object_54',0,'Просмотр списка данных объекта Доступные локализации',NULL,'N;'),('list_object_60',0,'Просмотр списка данных объекта Проверка файлов',NULL,'N;'),('list_object_61',0,'Просмотр списка данных объекта Инструкции',NULL,'N;'),('list_object_63',0,'Просмотр списка данных объекта Представление',NULL,'N;'),('list_object_66',0,'Просмотр списка данных объекта Колонка представления',NULL,'N;'),('list_object_80',0,'Просмотр списка данных объекта php-скрипты',NULL,'N;'),('list_object_86',0,'Просмотр списка данных объекта Интерфейс php-скрипта',NULL,'N;'),('list_object_89',0,'Просмотр списка данных объекта Очистить кэш',NULL,'N;'),('list_object_91',0,'Просмотр списка данных объекта Поисковый индекс',NULL,'N;'),('list_object_94',0,'Просмотр списка данных объекта Логи',NULL,'N;'),('list_object_a',0,'Просмотр списка данных объекта Фотографии',NULL,'N;'),('list_object_project-kolichestvo-vody',0,'Просмотр списка данных объекта Количество воды',NULL,'N;'),('list_object_project-vid-vody',0,'Просмотр списка данных объекта Вид воды',NULL,'N;'),('list_object_project-zakaz',0,'Просмотр списка данных объекта Заказ',NULL,'N;'),('list_object_ygin-gii',0,'Просмотр списка данных объекта gii (debug=true)',NULL,'N;'),('list_object_ygin-invoice',0,'Просмотр списка данных объекта Счета',NULL,'N;'),('list_object_ygin-menu',0,'Просмотр списка данных объекта Меню',NULL,'N;'),('list_object_ygin-override',0,'Просмотр списка данных объекта Переопределение представлений (debug=true)',NULL,'N;'),('list_object_ygin-views-generator',0,'Просмотр списка данных объекта Генерация вьюхи',NULL,'N;'),('showAdminPanel',0,'доступ к админке',NULL,'N;'),('view_object_26',0,'Операция просмотра экземпляра объекта Права доступа',NULL,'N;'),('view_object_30',0,'Операция просмотра экземпляра объекта Настройки сайта',NULL,'N;'),('view_object_32',0,'Операция просмотра экземпляра объекта Настройка прав',NULL,'N;'),('view_object_43',0,'Операция просмотра экземпляра объекта SQL',NULL,'N;'),('view_object_529',0,'Операция просмотра экземпляра объекта Статусы остатка продукции',NULL,'N;'),('view_object_531',0,'Операция просмотра экземпляра объекта Уведомления',NULL,'N;'),('view_object_60',0,'Операция просмотра экземпляра объекта Проверка файлов',NULL,'N;'),('view_object_89',0,'Операция просмотра экземпляра объекта Очистить кэш',NULL,'N;'),('view_object_91',0,'Операция просмотра для объекта Поисковый индекс',NULL,'N;'),('view_object_94',0,'Операция просмотра экземпляра объекта Логи',NULL,'N;'),('view_object_project-kolichestvo-vody',0,'Операция просмотра для объекта Количество воды',NULL,'N;'),('view_object_project-vid-vody',0,'Операция просмотра для объекта Вид воды',NULL,'N;'),('view_object_project-zakaz',0,'Операция просмотра для объекта Заказ',NULL,'N;'),('view_object_ygin-gii',0,'Операция просмотра для объекта gii (debug=true)',NULL,'N;'),('view_object_ygin-override',0,'Операция просмотра для объекта Переопределение представлений (debug=true)',NULL,'N;'),('view_object_ygin-views-generator',0,'Операция просмотра для объекта Генерация вьюхи',NULL,'N;');
/*!40000 ALTER TABLE `da_auth_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `da_auth_item_child`
--

DROP TABLE IF EXISTS `da_auth_item_child`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `da_auth_item_child` (
  `parent` varchar(64) NOT NULL,
  `child` varchar(64) NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`),
  CONSTRAINT `da_auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `da_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `da_auth_item_child_ibfk_10` FOREIGN KEY (`child`) REFERENCES `da_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `da_auth_item_child_ibfk_11` FOREIGN KEY (`parent`) REFERENCES `da_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `da_auth_item_child_ibfk_12` FOREIGN KEY (`child`) REFERENCES `da_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `da_auth_item_child_ibfk_13` FOREIGN KEY (`parent`) REFERENCES `da_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `da_auth_item_child_ibfk_14` FOREIGN KEY (`child`) REFERENCES `da_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `da_auth_item_child_ibfk_15` FOREIGN KEY (`parent`) REFERENCES `da_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `da_auth_item_child_ibfk_16` FOREIGN KEY (`child`) REFERENCES `da_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `da_auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `da_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `da_auth_item_child_ibfk_3` FOREIGN KEY (`parent`) REFERENCES `da_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `da_auth_item_child_ibfk_4` FOREIGN KEY (`child`) REFERENCES `da_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `da_auth_item_child_ibfk_5` FOREIGN KEY (`parent`) REFERENCES `da_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `da_auth_item_child_ibfk_6` FOREIGN KEY (`child`) REFERENCES `da_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `da_auth_item_child_ibfk_7` FOREIGN KEY (`parent`) REFERENCES `da_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `da_auth_item_child_ibfk_8` FOREIGN KEY (`child`) REFERENCES `da_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `da_auth_item_child_ibfk_9` FOREIGN KEY (`parent`) REFERENCES `da_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `da_auth_item_child`
--

LOCK TABLES `da_auth_item_child` WRITE;
/*!40000 ALTER TABLE `da_auth_item_child` DISABLE KEYS */;
INSERT INTO `da_auth_item_child` VALUES ('dev','create_object_101'),('editor','create_object_101'),('dev','create_object_103'),('editor','create_object_103'),('dev','create_object_105'),('editor','create_object_105'),('dev','create_object_106'),('dev','create_object_20'),('dev','create_object_21'),('dev','create_object_22'),('dev','create_object_23'),('dev','create_object_24'),('dev','create_object_250'),('editor','create_object_250'),('dev','create_object_26'),('dev','create_object_260'),('editor','create_object_260'),('dev','create_object_261'),('dev','create_object_27'),('dev','create_object_28'),('dev','create_object_30'),('dev','create_object_31'),('dev','create_object_33'),('dev','create_object_34'),('dev','create_object_35'),('dev','create_object_47'),('dev','create_object_50'),('dev','create_object_500'),('editor','create_object_500'),('dev','create_object_501'),('editor','create_object_501'),('dev','create_object_502'),('editor','create_object_502'),('dev','create_object_503'),('editor','create_object_503'),('dev','create_object_505'),('editor','create_object_505'),('dev','create_object_506'),('editor','create_object_506'),('dev','create_object_507'),('editor','create_object_507'),('dev','create_object_508'),('editor','create_object_508'),('dev','create_object_509'),('editor','create_object_509'),('dev','create_object_51'),('dev','create_object_511'),('editor','create_object_511'),('dev','create_object_512'),('editor','create_object_512'),('dev','create_object_513'),('dev','create_object_514'),('editor','create_object_514'),('dev','create_object_517'),('editor','create_object_517'),('dev','create_object_520'),('editor','create_object_520'),('dev','create_object_521'),('dev','create_object_522'),('dev','create_object_523'),('dev','create_object_524'),('dev','create_object_525'),('editor','create_object_525'),('dev','create_object_529'),('editor','create_object_529'),('dev','create_object_530'),('editor','create_object_530'),('dev','create_object_61'),('dev','create_object_63'),('dev','create_object_66'),('dev','create_object_80'),('dev','create_object_86'),('dev','create_object_project-kolichestvo-vody'),('editor','create_object_project-kolichestvo-vody'),('dev','create_object_project-vid-vody'),('editor','create_object_project-vid-vody'),('dev','create_object_project-zakaz'),('editor','create_object_project-zakaz'),('dev','create_object_ygin-menu'),('editor','create_object_ygin-menu'),('dev','create_object_ygin-views-generator'),('editor','create_object_ygin-views-generator'),('dev','delete_object_101'),('editor','delete_object_101'),('dev','delete_object_103'),('editor','delete_object_103'),('dev','delete_object_105'),('editor','delete_object_105'),('dev','delete_object_106'),('dev','delete_object_20'),('dev','delete_object_21'),('dev','delete_object_22'),('dev','delete_object_23'),('dev','delete_object_24'),('dev','delete_object_250'),('editor','delete_object_250'),('dev','delete_object_26'),('dev','delete_object_260'),('editor','delete_object_260'),('dev','delete_object_261'),('dev','delete_object_27'),('dev','delete_object_28'),('dev','delete_object_30'),('dev','delete_object_33'),('dev','delete_object_34'),('dev','delete_object_35'),('dev','delete_object_47'),('dev','delete_object_50'),('dev','delete_object_500'),('editor','delete_object_500'),('dev','delete_object_501'),('editor','delete_object_501'),('dev','delete_object_502'),('editor','delete_object_502'),('dev','delete_object_503'),('editor','delete_object_503'),('dev','delete_object_505'),('editor','delete_object_505'),('dev','delete_object_506'),('editor','delete_object_506'),('dev','delete_object_507'),('editor','delete_object_507'),('dev','delete_object_508'),('editor','delete_object_508'),('dev','delete_object_509'),('editor','delete_object_509'),('dev','delete_object_51'),('dev','delete_object_511'),('editor','delete_object_511'),('dev','delete_object_512'),('editor','delete_object_512'),('dev','delete_object_513'),('dev','delete_object_514'),('editor','delete_object_514'),('dev','delete_object_517'),('editor','delete_object_517'),('dev','delete_object_519'),('dev','delete_object_520'),('editor','delete_object_520'),('dev','delete_object_521'),('dev','delete_object_522'),('dev','delete_object_523'),('dev','delete_object_524'),('dev','delete_object_525'),('editor','delete_object_525'),('dev','delete_object_529'),('editor','delete_object_529'),('dev','delete_object_530'),('editor','delete_object_530'),('dev','delete_object_531'),('dev','delete_object_61'),('dev','delete_object_63'),('dev','delete_object_66'),('dev','delete_object_80'),('dev','delete_object_86'),('dev','delete_object_project-kolichestvo-vody'),('editor','delete_object_project-kolichestvo-vody'),('dev','delete_object_project-vid-vody'),('editor','delete_object_project-vid-vody'),('dev','delete_object_project-zakaz'),('editor','delete_object_project-zakaz'),('dev','delete_object_ygin-invoice'),('editor','delete_object_ygin-invoice'),('dev','delete_object_ygin-menu'),('editor','delete_object_ygin-menu'),('dev','delete_object_ygin-views-generator'),('editor','delete_object_ygin-views-generator'),('dev','editor'),('dev','edit_object_101'),('editor','edit_object_101'),('dev','edit_object_103'),('editor','edit_object_103'),('dev','edit_object_105'),('editor','edit_object_105'),('dev','edit_object_106'),('dev','edit_object_20'),('dev','edit_object_21'),('dev','edit_object_22'),('dev','edit_object_23'),('dev','edit_object_24'),('dev','edit_object_250'),('editor','edit_object_250'),('dev','edit_object_26'),('dev','edit_object_260'),('editor','edit_object_260'),('dev','edit_object_261'),('editor','edit_object_261'),('dev','edit_object_27'),('dev','edit_object_28'),('dev','edit_object_30'),('editor','edit_object_30'),('dev','edit_object_31'),('dev','edit_object_33'),('dev','edit_object_34'),('dev','edit_object_35'),('dev','edit_object_47'),('dev','edit_object_50'),('dev','edit_object_500'),('editor','edit_object_500'),('dev','edit_object_501'),('editor','edit_object_501'),('dev','edit_object_502'),('editor','edit_object_502'),('dev','edit_object_503'),('editor','edit_object_503'),('dev','edit_object_505'),('editor','edit_object_505'),('dev','edit_object_506'),('editor','edit_object_506'),('dev','edit_object_507'),('editor','edit_object_507'),('dev','edit_object_508'),('editor','edit_object_508'),('dev','edit_object_509'),('editor','edit_object_509'),('dev','edit_object_51'),('dev','edit_object_511'),('editor','edit_object_511'),('dev','edit_object_512'),('editor','edit_object_512'),('dev','edit_object_513'),('dev','edit_object_514'),('editor','edit_object_514'),('dev','edit_object_517'),('editor','edit_object_517'),('dev','edit_object_519'),('dev','edit_object_520'),('editor','edit_object_520'),('dev','edit_object_521'),('dev','edit_object_522'),('dev','edit_object_523'),('dev','edit_object_524'),('dev','edit_object_525'),('editor','edit_object_525'),('dev','edit_object_529'),('editor','edit_object_529'),('dev','edit_object_530'),('editor','edit_object_530'),('dev','edit_object_531'),('dev','edit_object_61'),('dev','edit_object_63'),('dev','edit_object_66'),('dev','edit_object_80'),('dev','edit_object_86'),('dev','edit_object_project-kolichestvo-vody'),('editor','edit_object_project-kolichestvo-vody'),('dev','edit_object_project-vid-vody'),('editor','edit_object_project-vid-vody'),('dev','edit_object_project-zakaz'),('editor','edit_object_project-zakaz'),('dev','edit_object_ygin-invoice'),('editor','edit_object_ygin-invoice'),('dev','edit_object_ygin-menu'),('editor','edit_object_ygin-menu'),('dev','edit_object_ygin-views-generator'),('editor','edit_object_ygin-views-generator'),('dev','list_object_101'),('editor','list_object_101'),('dev','list_object_103'),('editor','list_object_103'),('dev','list_object_20'),('dev','list_object_21'),('dev','list_object_22'),('dev','list_object_23'),('dev','list_object_24'),('editor','list_object_250'),('dev','list_object_26'),('dev','list_object_27'),('dev','list_object_28'),('dev','list_object_30'),('editor','list_object_30'),('dev','list_object_31'),('dev','list_object_32'),('dev','list_object_33'),('dev','list_object_34'),('dev','list_object_35'),('dev','list_object_43'),('dev','list_object_47'),('dev','list_object_50'),('editor','list_object_500'),('editor','list_object_501'),('editor','list_object_502'),('editor','list_object_503'),('editor','list_object_505'),('editor','list_object_506'),('editor','list_object_507'),('editor','list_object_508'),('dev','list_object_51'),('editor','list_object_512'),('dev','list_object_513'),('dev','list_object_514'),('editor','list_object_514'),('editor','list_object_520'),('dev','list_object_522'),('dev','list_object_523'),('dev','list_object_524'),('dev','list_object_528'),('dev','list_object_531'),('dev','list_object_60'),('dev','list_object_61'),('dev','list_object_63'),('dev','list_object_66'),('dev','list_object_80'),('dev','list_object_86'),('dev','list_object_89'),('dev','list_object_91'),('editor','list_object_a'),('dev','list_object_project-kolichestvo-vody'),('editor','list_object_project-kolichestvo-vody'),('dev','list_object_project-vid-vody'),('editor','list_object_project-vid-vody'),('dev','list_object_project-zakaz'),('editor','list_object_project-zakaz'),('dev','list_object_ygin-gii'),('dev','list_object_ygin-invoice'),('editor','list_object_ygin-invoice'),('dev','list_object_ygin-menu'),('editor','list_object_ygin-menu'),('dev','list_object_ygin-override'),('dev','list_object_ygin-views-generator'),('editor','list_object_ygin-views-generator'),('editor','showAdminPanel'),('dev','view_object_26'),('editor','view_object_30'),('dev','view_object_32'),('dev','view_object_43'),('dev','view_object_529'),('editor','view_object_529'),('dev','view_object_531'),('dev','view_object_60'),('dev','view_object_89'),('dev','view_object_91'),('dev','view_object_project-kolichestvo-vody'),('editor','view_object_project-kolichestvo-vody'),('dev','view_object_project-vid-vody'),('editor','view_object_project-vid-vody'),('dev','view_object_project-zakaz'),('editor','view_object_project-zakaz'),('dev','view_object_ygin-gii'),('dev','view_object_ygin-override'),('dev','view_object_ygin-views-generator'),('editor','view_object_ygin-views-generator');
/*!40000 ALTER TABLE `da_auth_item_child` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `da_domain`
--

DROP TABLE IF EXISTS `da_domain`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `da_domain` (
  `id_domain` int(8) NOT NULL COMMENT 'id',
  `domain_path` varchar(255) DEFAULT NULL COMMENT 'Путь к содержимому домена',
  `name` varchar(255) NOT NULL COMMENT 'Доменное имя',
  `id_default_page` int(8) NOT NULL COMMENT 'ID страницы по умолчанию',
  `description` varchar(255) DEFAULT NULL COMMENT 'Описание',
  `path2data_http` varchar(255) DEFAULT NULL COMMENT 'Путь к данным по http',
  `settings` text COMMENT 'Настройки',
  `keywords` varchar(255) DEFAULT NULL COMMENT 'Ключевые слова',
  `image_src` int(11) DEFAULT NULL COMMENT 'Картинка для сохранения в закладках',
  `active` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Активен',
  PRIMARY KEY (`id_domain`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Домены сайта';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `da_domain`
--

LOCK TABLES `da_domain` WRITE;
/*!40000 ALTER TABLE `da_domain` DISABLE KEYS */;
INSERT INTO `da_domain` VALUES (1,NULL,'engine_macro.baitek.org',100,'Название организации',NULL,NULL,NULL,500,1);
/*!40000 ALTER TABLE `da_domain` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `da_domain_localization`
--

DROP TABLE IF EXISTS `da_domain_localization`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `da_domain_localization` (
  `id_domain` int(8) NOT NULL,
  `id_localization` int(8) NOT NULL,
  PRIMARY KEY (`id_domain`,`id_localization`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `da_domain_localization`
--

LOCK TABLES `da_domain_localization` WRITE;
/*!40000 ALTER TABLE `da_domain_localization` DISABLE KEYS */;
INSERT INTO `da_domain_localization` VALUES (1,1);
/*!40000 ALTER TABLE `da_domain_localization` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `da_event`
--

DROP TABLE IF EXISTS `da_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `da_event` (
  `id_instance` int(8) DEFAULT NULL COMMENT 'ИД экземпляра',
  `id_event` int(8) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `id_event_type` int(8) NOT NULL DEFAULT '0' COMMENT 'Тип события',
  `event_message` longtext COMMENT 'Содержимое',
  `event_create` int(8) NOT NULL DEFAULT '0' COMMENT 'Дата создания события',
  PRIMARY KEY (`id_event`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='События';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `da_event`
--

LOCK TABLES `da_event` WRITE;
/*!40000 ALTER TABLE `da_event` DISABLE KEYS */;
INSERT INTO `da_event` VALUES (NULL,1,107,'<h1>Новый заказ</h1>\n\n<p>Данные заказа</p>\n\nАбонентский номер: 1234 <br>\n\n <br>\nФио: Александр<br>\nТелефон: g34g3 <br>\nАдресс: 4g34g3 <br>\nemail: cranky4.89@gmail.com <br>\nУдобное время: cq35g <br>\n<br>\nПримечание: 43 <br>\n<br>\n\nЗаказ:\n<table>\n  <tr>\n    <td>\n      Наименование\n    </td>\n    <td>\n      Кол-во\n    </td>\n  </tr>\n  </table>\n',1417789305),(NULL,2,107,'<h1>Новый заказ</h1>\n\n<p>Данные заказа</p>\n\nАбонентский номер: 2rc3r <br>\n\n <br>\nФио: Александр<br>\nТелефон: 23cr2 <br>\nАдресс: 3c232 <br>\nemail: cranky4.89@gmail.com <br>\nУдобное время: 2r23r <br>\n<br>\nПримечание:  <br>\n<br>\n\nЗаказ:\n<table>\n  <tr>\n    <td>\n      Наименование\n    </td>\n    <td>\n      Цена\n    </td>\n    <td>\n      Кол-во\n    </td>\n    <td>\n      Стоимость\n    </td>\n  </tr>\n    <tr>\n    <td colspan=\"3\">\n      ИТОГО:\n    </td>\n    <td>\n      0    </td>\n  </tr>\n</table>\n',1417789493),(NULL,3,107,'<h1>Новый заказ</h1>\n\n<p>Данные заказа</p>\n\nАбонентский номер: 12c413c <br>\n\n <br>\nФио: Александр23<br>\nТелефон: c23c23c <br>\nАдресс: 42c3 <br>\nemail: cranky4.89@gmail.com <br>\nУдобное время: c234c <br>\n<br>\nПримечание: 2314c2 <br>\n<br>\n\nЗаказ:\n<table>\n  <tr>\n    <td>\n      Наименование\n    </td>\n    <td>\n      Цена\n    </td>\n    <td>\n      Кол-во\n    </td>\n    <td>\n      Стоимость\n    </td>\n  </tr>\n      <tr>\n      <td>\n        Краснозатонская Серебряная      </td>\n      <td>\n        1      </td>\n      <td>\n        4      </td>\n      <td>\n        4      </td>\n    </tr>\n    <tr>\n    <td colspan=\"3\">\n      ИТОГО:\n    </td>\n    <td>\n      4    </td>\n  </tr>\n</table>\n',1417789613);
/*!40000 ALTER TABLE `da_event` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `da_event_format`
--

DROP TABLE IF EXISTS `da_event_format`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `da_event_format` (
  `id_event_format` int(8) NOT NULL COMMENT 'id',
  `description` varchar(255) NOT NULL COMMENT 'Описание',
  `place` int(1) NOT NULL DEFAULT '1' COMMENT 'Расположение',
  `file_name` varchar(50) DEFAULT NULL COMMENT 'Имя файла во вложении',
  `name` varchar(20) NOT NULL COMMENT 'Сокращённое название (для разработчика)',
  PRIMARY KEY (`id_event_format`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Формат сообщения';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `da_event_format`
--

LOCK TABLES `da_event_format` WRITE;
/*!40000 ALTER TABLE `da_event_format` DISABLE KEYS */;
INSERT INTO `da_event_format` VALUES (1,'Текстовое содержимое',1,'text.txt','TXT'),(2,'HTML-письмо',1,NULL,'HTML');
/*!40000 ALTER TABLE `da_event_format` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `da_event_process`
--

DROP TABLE IF EXISTS `da_event_process`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `da_event_process` (
  `id_event` int(8) NOT NULL DEFAULT '0',
  `email` varchar(70) NOT NULL,
  `notify_date` int(10) DEFAULT NULL,
  `id_event_subscriber` int(8) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_event`,`email`,`id_event_subscriber`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `da_event_process`
--

LOCK TABLES `da_event_process` WRITE;
/*!40000 ALTER TABLE `da_event_process` DISABLE KEYS */;
INSERT INTO `da_event_process` VALUES (1,'shander_ae@cvek.ru',NULL,2),(2,'shander_ae@cvek.ru',NULL,2),(3,'shander_ae@cvek.ru',NULL,2);
/*!40000 ALTER TABLE `da_event_process` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `da_event_subscriber`
--

DROP TABLE IF EXISTS `da_event_subscriber`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `da_event_subscriber` (
  `id_event_subscriber` int(8) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `id_event_type` int(8) NOT NULL DEFAULT '0' COMMENT 'Тип события',
  `id_user` int(8) DEFAULT NULL COMMENT 'Пользователь',
  `format` int(2) NOT NULL COMMENT 'Формат сообщения',
  `archive_attach` int(1) NOT NULL DEFAULT '0' COMMENT 'Архивировать ли вложение',
  `email` varchar(60) DEFAULT NULL COMMENT 'E-mail адрес',
  `name` varchar(255) DEFAULT NULL COMMENT 'Имя подписчика',
  PRIMARY KEY (`id_event_subscriber`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 PACK_KEYS=0 COMMENT='Подписчики на события';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `da_event_subscriber`
--

LOCK TABLES `da_event_subscriber` WRITE;
/*!40000 ALTER TABLE `da_event_subscriber` DISABLE KEYS */;
INSERT INTO `da_event_subscriber` VALUES (1,105,NULL,1,0,NULL,NULL),(2,107,NULL,2,0,'shander_ae@cvek.ru',NULL);
/*!40000 ALTER TABLE `da_event_subscriber` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `da_event_type`
--

DROP TABLE IF EXISTS `da_event_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `da_event_type` (
  `id_event_type` int(8) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `id_object` varchar(255) DEFAULT NULL COMMENT 'Объект для работы',
  `last_time` int(10) DEFAULT NULL COMMENT 'Дата последей обработки',
  `interval_value` int(8) DEFAULT NULL COMMENT 'Интервал времени, через которое будет проходить обработка события',
  `sql_condition` varchar(255) DEFAULT NULL COMMENT 'SQL получающая экемпляры ("AS id_instance")',
  `name` varchar(100) NOT NULL COMMENT 'Название',
  `condition_done` varchar(255) DEFAULT NULL COMMENT 'SQL выражение, срабатывающее после обработки экземпляра (<<id_instance>>)',
  `id_mail_account` int(8) NOT NULL DEFAULT '0' COMMENT 'Используемый почтовый аккаунт',
  PRIMARY KEY (`id_event_type`)
) ENGINE=InnoDB AUTO_INCREMENT=108 DEFAULT CHARSET=utf8 PACK_KEYS=0 COMMENT='Тип события';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `da_event_type`
--

LOCK TABLES `da_event_type` WRITE;
/*!40000 ALTER TABLE `da_event_type` DISABLE KEYS */;
INSERT INTO `da_event_type` VALUES (50,NULL,1344593834,300,NULL,'Новый комментарий',NULL,10),(100,NULL,1344593834,90,NULL,'Консультации » Появился ответ',NULL,10),(101,NULL,1344593834,90,NULL,'Консультации » Новый вопрос',NULL,10),(102,NULL,1344593834,90,NULL,'Консультации » Новый комментарий',NULL,10),(103,NULL,NULL,90,NULL,'Вопрос-ответ » Новый вопрос',NULL,10),(105,NULL,NULL,90,NULL,'Восстановление пароля',NULL,10),(106,NULL,NULL,90,NULL,'Регистрация нового пользователя',NULL,10),(107,'',NULL,90,NULL,'Новый заказ',NULL,10);
/*!40000 ALTER TABLE `da_event_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `da_file_extension`
--

DROP TABLE IF EXISTS `da_file_extension`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `da_file_extension` (
  `id_file_extension` int(8) NOT NULL COMMENT 'id',
  `ext` varchar(20) NOT NULL COMMENT 'Расширение',
  `id_file_type` int(8) NOT NULL DEFAULT '0' COMMENT 'Тип файла',
  PRIMARY KEY (`id_file_extension`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Расширения файлов';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `da_file_extension`
--

LOCK TABLES `da_file_extension` WRITE;
/*!40000 ALTER TABLE `da_file_extension` DISABLE KEYS */;
INSERT INTO `da_file_extension` VALUES (1,'jpg',1),(2,'gif',1),(3,'png',1),(4,'doc',2),(5,'xls',2),(6,'txt',2),(9,'css',4),(11,'pdf',2),(12,'docx',2),(13,'jpeg',1),(14,'flv',5),(201,'bmp',1),(202,'xlsx',2);
/*!40000 ALTER TABLE `da_file_extension` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `da_file_type`
--

DROP TABLE IF EXISTS `da_file_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `da_file_type` (
  `id_file_type` int(8) NOT NULL COMMENT 'id',
  `name` varchar(255) NOT NULL COMMENT 'Название',
  PRIMARY KEY (`id_file_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Типы файлов';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `da_file_type`
--

LOCK TABLES `da_file_type` WRITE;
/*!40000 ALTER TABLE `da_file_type` DISABLE KEYS */;
INSERT INTO `da_file_type` VALUES (1,'Картинка'),(2,'Документ'),(4,'Стили'),(5,'Flash-видео');
/*!40000 ALTER TABLE `da_file_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `da_files`
--

DROP TABLE IF EXISTS `da_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `da_files` (
  `id_file` int(8) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `file_path` varchar(255) NOT NULL COMMENT 'Путь к файлу',
  `id_file_type` int(8) DEFAULT NULL COMMENT 'Тип файла',
  `count` int(6) DEFAULT NULL COMMENT 'Количество загрузок',
  `id_object` varchar(255) DEFAULT NULL COMMENT 'Объект',
  `id_instance` int(8) DEFAULT NULL COMMENT 'ИД экземпляра',
  `id_parameter` varchar(255) DEFAULT NULL COMMENT 'Свойство объекта',
  `id_property` int(8) DEFAULT NULL COMMENT 'Пользовательское свойство',
  `create_date` int(10) DEFAULT NULL COMMENT 'Дата создания файла',
  `id_parent_file` int(8) DEFAULT NULL COMMENT 'Родительский файл',
  `id_tmp` varchar(32) DEFAULT NULL COMMENT 'Временный ИД',
  `status_process` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Статус создания превью-файла',
  PRIMARY KEY (`id_file`),
  KEY `id_tmp` (`id_tmp`,`id_object`,`id_instance`,`id_parameter`,`id_parent_file`,`id_file_type`,`file_path`)
) ENGINE=InnoDB AUTO_INCREMENT=674 DEFAULT CHARSET=utf8 COMMENT='Файлы';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `da_files`
--

LOCK TABLES `da_files` WRITE;
/*!40000 ALTER TABLE `da_files` DISABLE KEYS */;
INSERT INTO `da_files` VALUES (500,'content/domain/1/left.png',1,0,'31',1,'638',NULL,1310019966,NULL,NULL,0),(505,'content/vitrine/4/bg1.png',1,NULL,'520',4,'1634',NULL,1415276406,NULL,NULL,0),(511,'content/vitrine/7/2-fon-.jpg',1,NULL,'520',7,'1634',NULL,1415278860,NULL,NULL,0),(513,'content/vitrine/8/3-fon.png',1,NULL,'520',8,'1634',NULL,1415278894,NULL,NULL,0),(515,'content/vitrine/9/4-fon-.png',1,NULL,'520',9,'1634',NULL,1415278908,NULL,NULL,0),(517,'content/product/1/1369660001_5.jpg',1,NULL,'511',1,'1576',NULL,1416481616,NULL,NULL,0),(519,'content/news/1/1397467001_6.jpg',1,NULL,'502',1,'1519',NULL,1416814949,NULL,NULL,0),(526,'content/news/2/3IVFaUE9nhs_1(1).jpg',1,NULL,'502',2,'1519',NULL,1416815038,NULL,NULL,0),(529,'content/product_category/1/1397467001_6.jpg',1,NULL,'509',1,'1555',NULL,1416903026,NULL,NULL,0),(536,'content/menu/130/Krasnozatonskaya.jpg',1,NULL,'ygin-menu',130,'11',NULL,1417031725,NULL,NULL,0),(540,'content/menu/135/novichkam1.jpg',1,NULL,'ygin-menu',135,'11',NULL,1417427767,NULL,NULL,0),(542,'content/menu/139/novichkam5.jpg',1,NULL,'ygin-menu',139,'11',NULL,1417428313,NULL,NULL,0),(544,'temp/d5672ec5b9f5f103896d1d9eefd95752/zakaz-krasnozatonoskoi.jpg',1,NULL,'ygin-menu',NULL,'229',NULL,1417428789,NULL,'d5672ec5b9f5f103896d1d9eefd95752',0),(546,'temp/d5672ec5b9f5f103896d1d9eefd95752/mineraly-serebryannoi.jpg',1,NULL,'ygin-menu',NULL,'229',NULL,1417428977,NULL,'d5672ec5b9f5f103896d1d9eefd95752',0),(548,'temp/d5672ec5b9f5f103896d1d9eefd95752/sostav-iodirovannoi.jpg',1,NULL,'ygin-menu',NULL,'229',NULL,1417429026,NULL,'d5672ec5b9f5f103896d1d9eefd95752',0),(550,'temp/d5672ec5b9f5f103896d1d9eefd95752/iodirovannyya.jpg',1,NULL,'ygin-menu',NULL,'229',NULL,1417429030,NULL,'d5672ec5b9f5f103896d1d9eefd95752',0),(552,'content/menu/130/zakaz-krasnozatonoskoi.jpg',1,NULL,'ygin-menu',130,'229',NULL,1417429222,NULL,NULL,0),(554,'content/menu/130/mineraly-serebryannoi.jpg',1,NULL,'ygin-menu',130,'229',NULL,1417429240,NULL,NULL,0),(556,'content/menu/130/iodirovannyya.jpg',1,NULL,'ygin-menu',130,'229',NULL,1417429245,NULL,NULL,0),(558,'content/menu/130/sostav-iodirovannoi.jpg',1,NULL,'ygin-menu',130,'229',NULL,1417429250,NULL,NULL,0),(562,'content/menu/150/Kompaniya-1.jpg',1,NULL,'ygin-menu',150,'11',NULL,1417687961,NULL,NULL,0),(564,'content/menu/151/Kompaniya-2.jpg',1,NULL,'ygin-menu',151,'11',NULL,1417688050,NULL,NULL,0),(567,'content/menu/152/Kompaniya-3.jpg',1,NULL,'ygin-menu',152,'11',NULL,1417688143,NULL,NULL,0),(569,'content/menu/156/Kompaniya-4.jpg',1,NULL,'ygin-menu',156,'11',NULL,1417688235,NULL,NULL,0),(572,'content/menu/153/produkziya-1.jpg',1,NULL,'ygin-menu',153,'11',NULL,1417688817,NULL,NULL,0),(574,'content/menu/155/proizvodstvo-1.jpg',1,NULL,'ygin-menu',155,'11',NULL,1417688425,NULL,NULL,0),(576,'content/menu/157/proizvodstvo-2.jpg',1,NULL,'ygin-menu',157,'11',NULL,1417688490,NULL,NULL,0),(579,'content/menu/158/proizvodstvo-3.jpg',1,NULL,'ygin-menu',158,'11',NULL,1417688588,NULL,NULL,0),(581,'content/menu/159/proizvodstvo-4.jpg',1,NULL,'ygin-menu',159,'11',NULL,1417688661,NULL,NULL,0),(583,'content/menu/160/proizvodstvo-5.jpg',1,NULL,'ygin-menu',160,'11',NULL,1417688724,NULL,NULL,0),(587,'content/menu/154/Produkziya-2.jpg',1,NULL,'ygin-menu',154,'11',NULL,1417688839,NULL,NULL,0),(589,'content/menu/161/Produkziya-3.jpg',1,NULL,'ygin-menu',161,'11',NULL,1417688862,NULL,NULL,0),(591,'content/menu/162/produkziya-4.png',1,NULL,'ygin-menu',162,'11',NULL,1417688895,NULL,NULL,0),(593,'content/menu/163/produkziya-5.jpg',1,NULL,'ygin-menu',163,'11',NULL,1417688929,NULL,NULL,0),(595,'content/menu/164/Produkziya-6.jpg',1,NULL,'ygin-menu',164,'11',NULL,1417688961,NULL,NULL,0),(602,'content/menu/165/etiketka_48h15sm.jpg',1,NULL,'ygin-menu',165,'11',NULL,1417701833,NULL,NULL,0),(604,'content/water/1/butyl-1.3.png',1,NULL,'project-vid-vody',1,'project-vid-vody-photo',NULL,1418198171,NULL,NULL,0),(606,'content/water/2/butyl-1.4.png',1,NULL,'project-vid-vody',2,'project-vid-vody-photo',NULL,1418197955,NULL,NULL,0),(608,'content/menu/169/1.-Pompa-naklonnaya-podstavka.png',1,NULL,'ygin-menu',169,'11',NULL,1418023481,NULL,NULL,0),(610,'content/menu/170/2.-Kulery.png',1,NULL,'ygin-menu',170,'11',NULL,1418023510,NULL,NULL,0),(612,'content/menu/171/3.-Sanitarnaya-obrabotka.jpg',1,NULL,'ygin-menu',171,'11',NULL,1418023552,NULL,NULL,0),(614,'content/menu/172/4.-Prochee-oborudovanie.png',1,NULL,'ygin-menu',172,'11',NULL,1418023668,NULL,NULL,0),(620,'content/menu/167/pervyi-raz-voda-v-podarok.png',1,NULL,'ygin-menu',167,'11',NULL,1418024390,NULL,NULL,0),(622,'content/menu/168/Privedi-druga.png',1,NULL,'ygin-menu',168,'11',NULL,1418024480,NULL,NULL,0),(626,'content/menu/173/Schkoly-sady.png',1,NULL,'ygin-menu',173,'11',NULL,1418024628,NULL,NULL,0),(628,'content/menu/174/SKIDKA-10-RUBLEI-NA-VODU-DOSTAVLENNUYu-V-VOSKRESENE.png',1,NULL,'ygin-menu',174,'11',NULL,1418024673,NULL,NULL,0),(630,'content/menu/175/2-butyli.jpg',1,NULL,'ygin-menu',175,'11',NULL,1418024755,NULL,NULL,0),(664,'content/menu/167/pervyi-raz-voda-v-podarok_menu1.png',1,NULL,'ygin-menu',167,'11',NULL,1418216667,620,NULL,0),(665,'content/menu/168/Privedi-druga_menu1.png',1,NULL,'ygin-menu',168,'11',NULL,1418216668,622,NULL,0),(666,'content/menu/173/Schkoly-sady_menu1.png',1,NULL,'ygin-menu',173,'11',NULL,1418216669,626,NULL,0),(667,'content/menu/174/SKIDKA-10-RUBLEI-NA-VODU-DOSTAVLENNUYu-V-VOSKRESENE_menu1.png',1,NULL,'ygin-menu',174,'11',NULL,1418216669,628,NULL,0),(668,'content/menu/175/2-butyli_menu1.jpg',1,NULL,'ygin-menu',175,'11',NULL,1418216670,630,NULL,0),(669,'content/menu/153/produkziya-1_menu1.jpg',1,NULL,'ygin-menu',153,'11',NULL,1418217282,572,NULL,0),(670,'content/menu/175/2-butyli_da.jpg',1,NULL,'ygin-menu',175,'11',NULL,1418217400,630,NULL,0),(671,'content/water/1/butyl-1.3_list.png',1,NULL,'project-vid-vody',1,'project-vid-vody-photo',NULL,1418234770,604,NULL,0),(672,'content/water/2/butyl-1.4_list.png',1,NULL,'project-vid-vody',2,'project-vid-vody-photo',NULL,1418234771,606,NULL,0),(673,'content/menu/154/Produkziya-2_menu1.jpg',1,NULL,'ygin-menu',154,'11',NULL,1418234925,587,NULL,0);
/*!40000 ALTER TABLE `da_files` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `da_group_system_parameter`
--

DROP TABLE IF EXISTS `da_group_system_parameter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `da_group_system_parameter` (
  `id_group_system_parameter` int(8) NOT NULL COMMENT 'id',
  `name` varchar(100) NOT NULL COMMENT 'Название',
  PRIMARY KEY (`id_group_system_parameter`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 PACK_KEYS=0 COMMENT='Группы системных параметров';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `da_group_system_parameter`
--

LOCK TABLES `da_group_system_parameter` WRITE;
/*!40000 ALTER TABLE `da_group_system_parameter` DISABLE KEYS */;
INSERT INTO `da_group_system_parameter` VALUES (1,'Системные настройки'),(2,'Настройки сайта');
/*!40000 ALTER TABLE `da_group_system_parameter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `da_instruction`
--

DROP TABLE IF EXISTS `da_instruction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `da_instruction` (
  `id_instruction` int(8) NOT NULL COMMENT 'id',
  `name` varchar(255) NOT NULL COMMENT 'Название',
  `content` longtext NOT NULL COMMENT 'Описание',
  `desc_type` int(1) NOT NULL DEFAULT '1' COMMENT 'Относится только к этому сайту',
  `visible` int(1) NOT NULL DEFAULT '1' COMMENT 'Видимость',
  `num_seq` int(3) NOT NULL DEFAULT '1' COMMENT 'п/п',
  PRIMARY KEY (`id_instruction`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Инструкции';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `da_instruction`
--

LOCK TABLES `da_instruction` WRITE;
/*!40000 ALTER TABLE `da_instruction` DISABLE KEYS */;
INSERT INTO `da_instruction` VALUES (6,'Модуль Вопрос-ответ','<ol>  <li>  <h2>Общий вид</h2>  <img style=\"float: left;\" src=\"http://ygin.ru/instruction/engine/faq/faq-vid.jpg\" alt=\"\"></li>  <li>  <h2>Создание</h2>  <p>При нажатии на кнопку «Создать» появится страница с указанными полями:</p>  <ol style=\"list-style-type: lower-alpha;\">  <li>  <p>Поле «Вопрос» отображает вопрос пришедший от пользователей на сайт.</p>  <img src=\"http://ygin.ru/instruction/engine/faq/faq-sozd-1.jpg\" alt=\"Вопрос\"></li>  <li>  <p>Поле «Ответ» позволяет написать ответ по вышеуказанному вопросу.</p>  <img src=\"http://ygin.ru/instruction/engine/faq/faq-sozd-2.jpg\" alt=\"Ответ\"></li>  <li>  <p>Поле «Дата» указывает дату подачи вопроса.</p>  <img src=\"http://ygin.ru/instruction/engine/faq/faq-sozd-3.jpg\" alt=\"Дата\"></li>  <li>  <p>Поле «Показать» отвечает за видимость данного вопроса и ответа для пользователей сайта. В любом случае, вопрос останется видимым в списке вопросов в панели администрирования. Как правило, данная галочка предназначена для того, чтобы скрыть нежелательные или не прошедшие модерацию вопросы.</p>  <img src=\"http://ygin.ru/instruction/engine/faq/faq-sozd-4.jpg\" alt=\"Видимость\"></li>  </ol></li>  </ol>',0,0,18),(11,'Размещение видеороликов с Rutube','<ol>  <li>Чтобы добавить видеоролик в раздел сайта, его необходимо загрузить на любой сервис, предоставляющий услуги хостинга видеоматериалов, например <a href=\"http://www.rutube.ru/\">http://rutube.ru/register.html</a>.</li>  <li>Для загрузки видеороликов на сайте <a href=\"http://www.rutube.ru/\">www.rutube.ru</a> необходимо зарегистрироваться и войти на сайт как авторизованный пользователь.</li>  <li>В меню пользователя появится возможность загрузки видеороликов.<br><span><img style=\"margin-top: 15px; cursor: pointer;\" onclick=\"this.src=\'http://ygin.ru/instruction/engine/rutube/3.png\';\" src=\"http://ygin.ru/instruction/engine/rutube/3s.png\" alt=\"\"></span> <br><br><br><br></li>  <li>Для загрузки видеоролика нужно указать к нему путь, нажав кнопку «Обзор». Заполнить все необходимые поля и нажать на кнопку «Загрузить файл»<br> <img style=\"margin-top: 15px; cursor: pointer;\" onclick=\"this.src=\'http://ygin.ru/instruction/engine/rutube/4.png\';\" src=\"http://ygin.ru/instruction/engine/rutube/4s.png\" alt=\"\"><br><br><br><br></li>  <li>После загрузки видеоролика необходимо зайти в раздел «мои ролики»<br> <img style=\"margin-top: 15px; cursor: pointer;\" onclick=\"this.src=\'http://ygin.ru/instruction/engine/rutube/5.png\';\" src=\"http://ygin.ru/instruction/engine/rutube/5s.png\" alt=\"\"><br><br><br><br></li>  <li>Выбрать свой загруженный ролик, нажать на вкладку «ссылка и код» и скопировать ссылку на видеоролик.<br> <img style=\"margin-top: 15px; cursor: pointer;\" onclick=\"this.src=\'http://ygin.ru/instruction/engine/rutube/6.png\';\" src=\"http://ygin.ru/instruction/engine/rutube/6s.png\" alt=\"\"><br><br><br><br></li>  <li>После копирования ссылки, нужно зайти в Систему администрирования сайта, открыть редактируемый раздел где необходимо вставить видеоролик, выделить курсором мыши место расположения видеоролика в содержании раздела и нажать кнопку «Вставить/редактировать медиа-объект»<br> <img style=\"margin-top: 15px; cursor: pointer;\" onclick=\"this.src=\'http://ygin.ru/instruction/engine/rutube/7.png\';\" src=\"http://ygin.ru/instruction/engine/rutube/7s.png\" alt=\"\"><br><br><br><br></li>  <li>В сплывающем окне, в поле «Файл/адрес» нужно вставить скопированную ссылку на видеоролик, при необходимости отредактировать размеры и нажать кнопку «Вставить»<br> <img style=\"margin-top: 15px; cursor: pointer;\" onclick=\"this.src=\'http://ygin.ru/instruction/engine/rutube/8.png\';\" src=\"http://ygin.ru/instruction/engine/rutube/8s.png\" alt=\"\"><br><br><br><br></li>  <li>После чего в редактируемом разделе появится вставленный видеоролик.<br> <img style=\"margin-top: 15px; cursor: pointer;\" onclick=\"this.src=\'http://ygin.ru/instruction/engine/rutube/9.png\';\" src=\"http://ygin.ru/instruction/engine/rutube/9s.png\" alt=\"\"><br><br><br><br></li>  </ol>',0,1,19),(12,'Размещение видеороликов с Youtube','<ol>  <li class=\"noindent\">Чтобы добавить видеоролик в раздел сайта, его необходимо загрузить на любой сервис, предоставляющий услуги хостинга видеоматериалов, например http://www.youtube.com/create_account?next=%2F</li>  <li>Для загрузки видеороликов на сайте www.youtube.ru необходимо зарегистрироваться и войти на сайт как авторизованный пользователь.</li>  <li>В меню пользователя появится возможность загрузки видеороликов.<br><br><img style=\"margin-top: 15px; cursor: pointer;\" onclick=\"this.src=\'http://ygin.ru/instruction/engine/youtube/3.png\';\" src=\"http://ygin.ru/instruction/engine/youtube/3s.png\" alt=\"\"><br><br></li>  <li>Для загрузки видеоролика нужно указать к нему путь, нажав кнопку «Обзор» и после чего начнется загрузка файла.</li>  <li>По окончании загрузки нужно скопировать ссылку на видеоролик.<br><br><img style=\"margin-top: 15px; cursor: pointer;\" onclick=\"this.src=\'http://ygin.ru/instruction/engine/youtube/5.png\';\" src=\"http://ygin.ru/instruction/engine/youtube/5s.png\" alt=\"\"><br><br></li>  <li>После копирования ссылки, нужно зайти в Систему администрирования сайта, открыть редактируемый раздел где необходимо вставить видеоролик, выделить курсором мыши место расположения видеоролика в содержании раздела и нажать кнопку «Вставить/редактировать медиа-объект»<br><br><img style=\"margin-top: 15px; cursor: pointer;\" onclick=\"this.src=\'http://ygin.ru/instruction/engine/youtube/6.png\';\" src=\"http://ygin.ru/instruction/engine/youtube/6s.png\" alt=\"\"><br><br></li>  <li>В сплывающем окне, в поле «Файл/адрес» нужно вставить скопированную ссылку на видеоролик, при необходимости отредактировать размеры и нажать кнопку «Вставить»<br><br><img style=\"margin-top: 15px; cursor: pointer;\" onclick=\"this.src=\'http://ygin.ru/instruction/engine/youtube/7.png\';\" src=\"http://ygin.ru/instruction/engine/youtube/7s.png\" alt=\"\"><br><br></li>  <li>После чего в редактируемом разделе появится вставленный видеоролик.<br><br><img style=\"margin-top: 15px; cursor: pointer;\" onclick=\"this.src=\'http://ygin.ru/instruction/engine/youtube/8.png\';\" src=\"http://ygin.ru/instruction/engine/youtube/8s.png\" alt=\"\"><br><br></li>  </ol>',0,1,20),(13,'Основные возможности','<p>Интернет-сайт разработан с использованием системы управления сайтами <strong>ygin</strong>. В состав ее программного обеспечения входят:</p>  <ul>  <li>система взаимодействия с пользователем, реализующая графический оконный интерфейс;</li>  <li>система администрирования, обеспечивающая управление, ввод и  редактирование информации, используемой в процессе функционирования Интернет-сайта.</li>  </ul>  <p>Объектами автоматизации являются процессы вывода периодически изменяющейся информации, отправки сообщений, содержащих вопросы и предложения по различным проблемным областям. Основными субъектами информационного взаимодействия, реализуемого программным обеспечением Интернет-сайта, являются:</p>  <ol>  <li>посетители сайта, получающие доступ в открытые разделы;</li>  <li>редакторы сайта;</li>  <li>администраторы сайта.</li>  </ol>  <p> </p>  <ul></ul>',0,1,1),(14,'Авторизация в системе','<p>Для того чтобы начать работу в системе администрирования, потребуется выполнить ряд действий:</p>  <ol>  <li>Вход в систему администрирования начинается с ввода в адресной строке браузера особого адреса, который скрыт от пользователей. Для того чтобы попасть на скрытую страницу, необходимо ввести: http://название_сайта/admin/ , после чего откроется форма авторизации.  <p><img src=\"http://ygin.ru/instruction/engine/auto/clip_image001_0006.gif\" alt=\"\" height=\"204\" width=\"274\"><br><em>Авторизация в системе</em></p>  </li>  <li>В форму авторизации необходимо ввести логин и пароль, которые Вам сообщаются заранее администратором сайта.</li>  <li>В случае удачного входа, перед вами откроется основное меню с доступными для Вашего пользователя элементами.</li>  </ol>  <p style=\"text-align: center;\"><img src=\"http://ygin.ru/instruction/engine/auto/clip_image003_0001.jpg\" alt=\"\" height=\"351\" width=\"623\"><br> <em>Главная страница системы управления сайтом</em></p>',0,1,2),(15,'Структура системы управления','<p>Раздел – структурная единица сайта, которая предназначена для выделения или объединения информации по какому-либо признаку. На сайте он представлен в виде странички с информацией. Раздел может иметь подразделы, которые в свою очередь могут иметь свои подразделы. Данное свойство называется вложенностью раздела.</p>  <p>Каталог разделов (папка) – раздел сайта, содержащий в себе другие разделы. <br> Обычно система каталогов имеет иерархическую структуру: каталог разделов (папка) может дополнительно содержать каталог более низкого уровня (подкаталог).</p>  <p style=\"text-align: center;\"><img src=\"http://ygin.ru/instruction/engine/strukt/clip_image002_0009.jpg\" alt=\"\" height=\"463\" width=\"623\"></p>  <p style=\"text-align: center;\">Структура панели управления</p>  <p>Основное меню – главный элемент навигации по панели администрирования (см. область 1), разбит на списки экземпляров, каждый из которых именуется согласно задачам, которые можно решить. Например, внутри раздела «Меню» находится инструментарий, предназначенный для редактирования основного содержимого сайта, то есть структуры меню.</p>  <p>Панель управления – набор кнопок, предназначенных для выполнения различных операций над элементами списка (см. область 2). Например, на изображении выше можно увидеть две кнопки – «Создать» и «Упорядочить», соответственно добавляющую новый элемент в список и упорядочивающую элементы списка.</p>  <p>Основное содержание – внутри этого блока находится основное содержимое страницы администрирования (списки экземпляров объектов или свойства редактируемого объекта), на изображении выше присутствует список, с помощью которого редактируется меню (см. область 3).</p>  <p><strong>Рассмотрим более подробно список экземпляров в основном содержании:</strong></p>  <ul>  <li>ИД – уникальный идентификационный номер, предназначенный для однозначного определения раздела системой управления сайтом, т.е. одному ИД всегда соответствует только один экземпляр списка (экземпляром списка может быть, например, раздел, новость и т.д.).</li>  <li>Позиция раздела – порядковый номер отображения раздела в меню сайта. Чем меньше номер, тем выше отображается данный раздел.</li>  <li>Видимость раздела – если галочка стоит, то этот раздел будет виден в меню. В противном случае раздел не будет виден в меню сайта, но при прямом обращении по ссылке в адресной строке браузера он, по-прежнему, доступен.</li>  <li>Просмотр вложенных объектов – нажав на данную кнопку, можно просмотреть все вложенные разделы. Даже при отсутствии таковых существует возможность перейти к вложенным разделам и создать их при необходимости.</li>  <li>Редактировать – при нажатии на кнопку открывается список свойств элемента списка, который позволяет, например, менять заголовок и текст разделов или дату новостей.</li>  <li>Удалить – удаляет указанный элемент списка без возможности восстановления.</li>  </ul>',0,1,3),(16,'Переключение локализации данных (языков)','<p>С помощью блока управления языками, у редактора есть возможность переключить текущую локализацию данных. При этом весь последующий ввод данных будет применим к выбранной локализации данных.</p>  <p style=\"text-align: center;\"><img src=\"http://ygin.ru/instruction/engine/lokal/clip_image002_0009.jpg\" alt=\"\" height=\"29\" width=\"134\"></p>  <p style=\"text-align: center;\"><em>Блок работы с локализацией</em></p>  <p style=\"text-align: center;\"><img src=\"http://ygin.ru/instruction/engine/lokal/clip_image004_0000.jpg\" alt=\"\" height=\"322\" width=\"661\"></p>  <p style=\"text-align: center;\"><em>Список разделов при работе с английской локализацией данных</em></p>  <p>Для удобства восприятия шапка таблицы со списком экземпляров данных раскрашивается в определённый цвет, разный для каждого языка. Для перевода на другие языки отдельных фраз, используемых на сайте, в системе управления предусмотрена страница «Interface localization» (Основное меню -&gt; Локализация -&gt; Interface localization), где все используемые фразы перечислены в списке.</p>',0,0,4),(17,'Поиск по спискам экземпляров','<p>Поиск по спискам является крайне необходимым, когда количество экземпляров какого-либо объекта становится слишком велико. Ярким примером такой ситуации является наличие большого количества новостей или разделов сайта. В такой ситуации бывает очень трудно отыскать нужный раздел сайта, особенно, учитывая их иерархическую вложенность.</p>  <p style=\"text-align: center;\"><img src=\"http://ygin.ru/instruction/engine/poisk//clip_image002_0009.jpg\" alt=\"\"></p>  <p style=\"text-align: center;\"><em>Элементы формы поиска</em></p>  <p style=\"text-align: left;\"><strong>Форма поиска состоит из следующих элементов:</strong></p>  <ol>  <li>Список свойств объекта - перечень всех свойств объекта (см. область 1). Например, для объекта «Раздел» можно увидеть свойства «Заголовок», «Содержимое» и т.д. Из этого списка выбирается свойство, по которому будет вестись поиск.</li>  <li>Искомое значение свойства - указывается именно то значение свойства, которое нас интересует у экземпляров списка (см. область 2).</li>  <li>Кнопка «Найти» - после нажатия этой кнопки формируется список именно тех экземпляров списка, которые удовлетворяют условиям поиска (см. область 3).</li>  </ol>  <p>В качестве примера можно рассмотреть поиск нужного нам раздела по заголовку. Предположим, требуется найти на сайте раздел, заголовок которого «О компании». Для этого выполним ряд действий:</p>  <ol>  <li>В основном меню системы администрирования выбираем «Меню», чтобы перейти к списку всех разделов сайта.</li>  <li>В форме поиска, которая размещается над списком разделов, указываем свойство, по которому будет осуществляться поиск, т.е. «Заголовок раздела».</li>  <li>В качестве искомого значения свойства указываем «О компании» и нажимаем на кнопку «Найти»</li>  </ol>  <p><img src=\"http://ygin.ru/instruction/engine/poisk/clip_image004_0000.jpg\" alt=\"\" height=\"98\" width=\"601\"><br><em>Форма поиска пункта меню</em></p>  <p>В результате поиска мы получим только раздел, имеющий заголовок «О компании».</p>',0,1,5),(18,'Элементы панели управления','<table border=\"1\" cellpadding=\"0\" cellspacing=\"0\">  <tbody>  <tr>  <td width=\"171\">  <p align=\"center\"><img src=\"http://ygin.ru/instruction/engine/uprav/clip_image001_0006.gif\" alt=\"\" height=\"25\" width=\"98\"></p>  </td>  <td valign=\"top\" width=\"505\">  <p>Кнопка «Создать». Данная кнопка предназначена для создания экземпляров открытого списка.</p>  </td>  </tr>  <tr>  <td width=\"171\">  <p align=\"center\"><img src=\"http://ygin.ru/instruction/engine/uprav/clip_image002_0006.gif\" alt=\"\" height=\"25\" width=\"123\"></p>  </td>  <td valign=\"top\" width=\"505\">  <p>Кнопка «Упорядочить» предназначена для упорядочивания элементов списка в каком-либо разделе основного меню, при этом порядок задаётся для каждой строки при помощи столбца «Порядковый номер».</p>  </td>  </tr>  <tr>  <td width=\"171\">  <p align=\"center\"><img src=\"http://ygin.ru/instruction/engine/uprav/clip_image003_0006.gif\" alt=\"\" height=\"25\" width=\"76\"></p>  </td>  <td valign=\"top\" width=\"505\">  <p>Кнопка «Вверх». Позволяет перейти из вложенной страницы на страницу уровнем выше.</p>  </td>  </tr>  </tbody>  </table>',0,1,6),(19,'Редактор страниц','<p></p>  <p><img src=\"http://ygin.ru/instruction/engine/redaktor/clip_image002_0009.jpg\" alt=\"\" height=\"395\" width=\"623\"><br> Визуальный редактор</p>  <p>В данном поле находится всё содержимое раздела, которое будет отображаться на сайте. Для редактирования содержимого прилагается встроенный визуальный редактор. Редактор позволяет вводить и форматировать текст, таблицы, добавлять изображения, создавать в теле элемента ссылки на другие веб-ресурсы, вставлять текст из буфера обмена Windows, в том числе предварительно отформатированный, как например, из Microsoft Word.</p>  <p>Вставку форматированного текста из буфера обмена рекомендуется делать с использованием кнопки <img src=\"http://ygin.ru/instruction/engine/redaktor/clip_image003_0006.gif\" alt=\"\" height=\"14\" width=\"16\"> - Вставить из Word, чтобы имелась возможность удаления ненужного форматирования. После нажатия на кнопку выдается окно, в котором можно удалить определение стилей, шрифтов и лишние отступы и просмотреть результат. Затем производится форматирование текста по своему усмотрению в окне визуального редактора.</p>  <p><strong>Возможности редактора: </strong></p>  <table border=\"1\" cellpadding=\"0\" cellspacing=\"0\">  <tbody>  <tr>  <td colspan=\"2\" valign=\"top\" width=\"676\"><br> <em>Форматирование текста</em></td>  </tr>  <tr>  <td width=\"103\">  <p><img src=\"http://ygin.ru/instruction/engine/redaktor/clip_image004.gif\" alt=\"\" height=\"20\" width=\"20\"></p>  </td>  <td valign=\"top\" width=\"573\">  <p>выравнивает текст по левому краю страницы</p>  </td>  </tr>  <tr>  <td width=\"103\">  <p><img src=\"http://ygin.ru/instruction/engine/redaktor/clip_image005.gif\" alt=\"\" height=\"20\" width=\"20\"></p>  </td>  <td valign=\"top\" width=\"573\">  <p>выравнивает текст по правому краю страницы</p>  </td>  </tr>  <tr>  <td width=\"103\">  <p><img src=\"http://ygin.ru/instruction/engine/redaktor/clip_image006.gif\" alt=\"\" height=\"20\" width=\"20\"></p>  </td>  <td valign=\"top\" width=\"573\">  <p>выравнивает текст по центру страницы</p>  </td>  </tr>  <tr>  <td width=\"103\">  <p><img src=\"http://ygin.ru/instruction/engine/redaktor/clip_image007.gif\" alt=\"\" height=\"20\" width=\"20\"></p>  </td>  <td valign=\"top\" width=\"573\">  <p>выравнивает текст по ширине страницы</p>  </td>  </tr>  <tr>  <td width=\"103\">  <p><img src=\"http://ygin.ru/instruction/engine/redaktor/clip_image008.gif\" alt=\"\" height=\"20\" width=\"20\"></p>  </td>  <td valign=\"top\" width=\"573\">  <p>делает текст Жирным</p>  </td>  </tr>  <tr>  <td width=\"103\">  <p><img src=\"http://ygin.ru/instruction/engine/redaktor/clip_image009.gif\" alt=\"\" height=\"20\" width=\"20\"></p>  </td>  <td valign=\"top\" width=\"573\">  <p>делает текст Курсивным</p>  </td>  </tr>  <tr>  <td width=\"103\">  <p><img src=\"http://ygin.ru/instruction/engine/redaktor/clip_image010.gif\" alt=\"\" height=\"20\" width=\"20\"></p>  </td>  <td valign=\"top\" width=\"573\">  <p>делает текст Зачёркнутым</p>  </td>  </tr>  <tr>  <td width=\"103\">  <p><img src=\"http://ygin.ru/instruction/engine/redaktor/clip_image011.gif\" alt=\"\" height=\"20\" width=\"20\"></p>  </td>  <td valign=\"top\" width=\"573\">  <p>делает текст Подчёркнутым</p>  </td>  </tr>  <tr>  <td width=\"103\">  <p><img src=\"http://ygin.ru/instruction/engine/redaktor/clip_image012.gif\" alt=\"\" height=\"22\" width=\"86\"></p>  </td>  <td valign=\"top\" width=\"573\">  <p>меняет формат текста</p>  </td>  </tr>  <tr>  <td width=\"103\">  <p><img src=\"http://ygin.ru/instruction/engine/redaktor/clip_image013.gif\" alt=\"\" height=\"20\" width=\"20\"></p>  </td>  <td valign=\"top\" width=\"573\">  <p>меняет цвет текста</p>  </td>  </tr>  <tr>  <td width=\"103\">  <p><img src=\"http://ygin.ru/instruction/engine/redaktor/clip_image014.gif\" alt=\"\" height=\"20\" width=\"20\"></p>  </td>  <td valign=\"top\" width=\"573\">  <p>меняет цвет фона</p>  </td>  </tr>  <tr>  <td colspan=\"2\" valign=\"top\" width=\"676\">  <p><em>Работа с текстом</em></p>  </td>  </tr>  <tr>  <td width=\"103\">  <p><img src=\"http://ygin.ru/instruction/engine/redaktor/clip_image015.gif\" alt=\"\" height=\"20\" width=\"20\"></p>  </td>  <td valign=\"top\" width=\"573\">  <p>вырезать текст</p>  </td>  </tr>  <tr>  <td width=\"103\">  <p><img src=\"http://ygin.ru/instruction/engine/redaktor/clip_image016.gif\" alt=\"\" height=\"20\" width=\"20\"></p>  </td>  <td valign=\"top\" width=\"573\">  <p>копировать текст</p>  </td>  </tr>  <tr>  <td width=\"103\">  <p><img src=\"http://ygin.ru/instruction/engine/redaktor/clip_image017.gif\" alt=\"\" height=\"20\" width=\"20\"></p>  </td>  <td valign=\"top\" width=\"573\">  <p>вставить</p>  </td>  </tr>  <tr>  <td width=\"103\">  <p><img src=\"http://ygin.ru/instruction/engine/redaktor/clip_image018.gif\" alt=\"\" height=\"14\" width=\"16\"></p>  </td>  <td valign=\"top\" width=\"573\">  <p>вставить как простой (без формата) текст</p>  </td>  </tr>  <tr>  <td width=\"103\">  <p><img src=\"http://ygin.ru/instruction/engine/redaktor/clip_image003_0007.gif\" alt=\"\" height=\"14\" width=\"16\"></p>  </td>  <td valign=\"top\" width=\"573\">  <p>вставить из Word (с форматом) текст</p>  </td>  </tr>  <tr>  <td width=\"103\">  <p><img src=\"http://ygin.ru/instruction/engine/redaktor/clip_image019.gif\" alt=\"\" height=\"13\" width=\"15\"></p>  </td>  <td valign=\"top\" width=\"573\">  <p>выделить весь текст</p>  </td>  </tr>  <tr>  <td width=\"103\">  <p><img src=\"http://ygin.ru/instruction/engine/redaktor/clip_image020.gif\" alt=\"\" height=\"20\" width=\"20\"></p>  </td>  <td valign=\"top\" width=\"573\">  <p>отменить последнее действие</p>  </td>  </tr>  <tr>  <td width=\"103\">  <p><img src=\"http://ygin.ru/instruction/engine/redaktor/clip_image021.gif\" alt=\"\" height=\"20\" width=\"20\"></p>  </td>  <td valign=\"top\" width=\"573\">  <p>повторить отменённое действие</p>  </td>  </tr>  <tr>  <td width=\"103\">  <p><img src=\"http://ygin.ru/instruction/engine/redaktor/clip_image022.gif\" alt=\"\" height=\"20\" width=\"20\"><img src=\"http://ygin.ru/instruction/engine/redaktor/clip_image023.gif\" alt=\"\" height=\"20\" width=\"20\"></p>  </td>  <td valign=\"top\" width=\"573\">  <p>задать отступы</p>  </td>  </tr>  <tr>  <td width=\"103\">  <p><img src=\"http://ygin.ru/instruction/engine/redaktor/clip_image024.gif\" alt=\"\" height=\"20\" width=\"20\"></p>  </td>  <td valign=\"top\" width=\"573\">  <p>рисует горизонтальную разделительную линию</p>  </td>  </tr>  <tr>  <td width=\"103\">  <p><img src=\"http://ygin.ru/instruction/engine/redaktor/clip_image025.gif\" alt=\"\" height=\"20\" width=\"20\"></p>  </td>  <td valign=\"top\" width=\"573\">  <p>позволяет для выделенного текста или картинки создать гиперссылку, указав адрес ссылки. В качестве гиперссылки можно указать раздел сайта или загруженный в текущий раздел файл при помощи выбора из выпадающего списка</p>  </td>  </tr>  <tr>  <td width=\"103\">  <p><img src=\"http://ygin.ru/instruction/engine/redaktor/clip_image026.gif\" alt=\"\" height=\"20\" width=\"20\"></p>  </td>  <td valign=\"top\" width=\"573\">  <p>позволяет убрать гиперссылку из текста</p>  </td>  </tr>  <tr>  <td width=\"103\">  <p><img src=\"http://ygin.ru/instruction/engine/redaktor/clip_image027.gif\" alt=\"\" height=\"20\" width=\"20\"></p>  </td>  <td valign=\"top\" width=\"573\">  <p>позволяет вставлять изображения в текст, указав при этом адрес картинки. Существует возможность выбрать изображение, загруженное ранее в раздел сайта, при помощи функционального блока «Файлы»</p>  </td>  </tr>  <tr>  <td width=\"103\">  <p><img src=\"http://ygin.ru/instruction/engine/redaktor/clip_image028.gif\" alt=\"\" height=\"20\" width=\"20\"></p>  </td>  <td valign=\"top\" width=\"573\">  <p>позволяет делать текст в виде нижнего индекса</p>  </td>  </tr>  <tr>  <td width=\"103\">  <p><img src=\"http://ygin.ru/instruction/engine/redaktor/clip_image029.gif\" alt=\"\" height=\"20\" width=\"20\"></p>  </td>  <td valign=\"top\" width=\"573\">  <p>позволяет делать текст в виде верхнего индекса</p>  </td>  </tr>  <tr>  <td width=\"103\">  <p><img src=\"http://ygin.ru/instruction/engine/redaktor/clip_image030.gif\" alt=\"\" height=\"20\" width=\"20\"></p>  </td>  <td valign=\"top\" width=\"573\">  <p>позволяет создавать маркированный список</p>  </td>  </tr>  <tr>  <td width=\"103\">  <p><img src=\"http://ygin.ru/instruction/engine/redaktor/clip_image031.gif\" alt=\"\" height=\"20\" width=\"20\"></p>  </td>  <td valign=\"top\" width=\"573\">  <p>позволяет создавать нумерованный список</p>  </td>  </tr>  <tr>  <td colspan=\"2\" valign=\"top\" width=\"676\">  <p><em>Работа с таблицами</em></p>  </td>  </tr>  <tr>  <td width=\"103\">  <p><img src=\"http://ygin.ru/instruction/engine/redaktor/clip_image032.gif\" alt=\"\" height=\"16\" width=\"16\"></p>  </td>  <td width=\"573\">  <p>позволяет вставить новую таблицу или отредактировать свойства выделенной таблицы</p>  </td>  </tr>  <tr>  <td width=\"103\">  <p><img src=\"http://ygin.ru/instruction/engine/redaktor/clip_image033.gif\" alt=\"\" height=\"13\" width=\"14\"> <img src=\"http://ygin.ru/instruction/engine/redaktor/clip_image034.gif\" alt=\"\" height=\"13\" width=\"14\"></p>  </td>  <td width=\"573\">  <p>позволяет просматривать свойства выделенной ячейки или строки</p>  </td>  </tr>  <tr>  <td width=\"103\">  <p><img src=\"http://ygin.ru/instruction/engine/redaktor/clip_image035.gif\" alt=\"\" height=\"15\" width=\"16\"></p>  </td>  <td width=\"573\">  <p>позволяет вставить строку перед выделенной нами строкой</p>  </td>  </tr>  <tr>  <td width=\"103\">  <p><img src=\"http://ygin.ru/instruction/engine/redaktor/clip_image036.gif\" alt=\"\" height=\"15\" width=\"16\"></p>  </td>  <td width=\"573\">  <p>позволяет вставить строку после выделенной нами строки</p>  </td>  </tr>  <tr>  <td width=\"103\">  <p><img src=\"http://ygin.ru/instruction/engine/redaktor/clip_image037.gif\" alt=\"\" height=\"13\" width=\"16\"></p>  </td>  <td width=\"573\">  <p>позволяет удалить строку</p>  </td>  </tr>  <tr>  <td width=\"103\">  <p><img src=\"http://ygin.ru/instruction/engine/redaktor/clip_image038.gif\" alt=\"\" height=\"16\" width=\"15\"></p>  </td>  <td width=\"573\">  <p>позволяет вставить столбец перед выделенным нами столбцом</p>  </td>  </tr>  <tr>  <td width=\"103\">  <p><img src=\"http://ygin.ru/instruction/engine/redaktor/clip_image039.gif\" alt=\"\" height=\"16\" width=\"15\"></p>  </td>  <td width=\"573\">  <p>позволяет вставить столбец после выделенного нами столбца</p>  </td>  </tr>  <tr>  <td width=\"103\">  <p><img src=\"http://ygin.ru/instruction/engine/redaktor/clip_image040.gif\" alt=\"\" height=\"14\" width=\"13\"></p>  </td>  <td width=\"573\">  <p>позволяет удалить столбец</p>  </td>  </tr>  <tr>  <td width=\"103\">  <p><img src=\"http://ygin.ru/instruction/engine/redaktor/clip_image041.gif\" alt=\"\" height=\"13\" width=\"16\"> <img src=\"http://ygin.ru/instruction/engine/redaktor/clip_image042.gif\" alt=\"\" height=\"13\" width=\"16\"></p>  </td>  <td width=\"573\">  <p>позволяет объединять и разделять ячейки таблицы</p>  </td>  </tr>  <tr>  <td width=\"103\">  <p><img src=\"http://ygin.ru/instruction/engine/redaktor/clip_image043.gif\" alt=\"\" height=\"12\" width=\"14\"></p>  </td>  <td width=\"573\">  <p>позволяет удалить любой формат заданного текста</p>  </td>  </tr>  <tr>  <td width=\"103\">  <p><img src=\"http://ygin.ru/instruction/engine/redaktor/clip_image044.gif\" alt=\"\" height=\"20\" width=\"20\"></p>  </td>  <td width=\"573\">  <p>очищает исходный код страницы и исправление в нём ошибок</p>  </td>  </tr>  <tr>  <td width=\"103\">  <p><img src=\"http://ygin.ru/instruction/engine/redaktor/clip_image045.gif\" alt=\"*\" height=\"5\" width=\"18\"></p>  </td>  <td width=\"573\">  <p>позволяет просмотреть страницу в языке разметки (HTML)</p>  </td>  </tr>  </tbody>  </table>',0,1,7),(20,'Работа с разделами и материалами сайта (Меню сайта)','<p>Меню имеет иерархическую структуру с неограниченным уровнем вложенности. Список разделов в меню:</p>  <p style=\"text-align: center;\"><br> <img src=\"http://ygin.ru/instruction/engine/rabota/clip_image002_0009.jpg\" alt=\"\" height=\"447\" width=\"601\"></p>  <p style=\"text-align: center;\">Список разделов меню</p>  <p><strong>Создание нового раздела в меню:</strong></p>  <table border=\"1\" cellpadding=\"0\" cellspacing=\"0\">  <tbody>  <tr>  <td colspan=\"2\" valign=\"top\" width=\"670\"><em>Основные свойства</em></td>  </tr>  <tr>  <td valign=\"top\" width=\"175\">  <p>Имя</p>  </td>  <td valign=\"top\" width=\"494\">  <p>Введённое имя будет использоваться как название раздела в системе администрирования, а также в главном меню сайта.</p>  </td>  </tr>  <tr>  <td valign=\"top\" width=\"175\">  <p>Заголовок</p>  </td>  <td valign=\"top\" width=\"494\">  <p>Введённый текст будет использоваться в качестве заголовка страницы сайта (раздела) в тот момент, когда пользователь будет его просматривать на сайте.</p>  </td>  </tr>  <tr>  <td valign=\"top\" width=\"175\">  <p>Видимость</p>  </td>  <td valign=\"top\" width=\"494\">  <p>Признак, показывающий отображать ли раздел в меню сайта. <br> Несмотря на отсутствие видимости раздела в меню сайта, он всё равно будет доступен пользователям при наборе адреса раздела вручную.</p>  </td>  </tr>  <tr>  <td valign=\"top\" width=\"175\">  <p>Ссылка на страничку</p>  </td>  <td valign=\"top\" width=\"494\">  <p>В меню сайта для данного раздела будет сгенерирована ссылка на значение данного свойства</p>  </td>  </tr>  <tr>  <td valign=\"top\" width=\"175\">  <p>Файлы</p>  </td>  <td valign=\"top\" width=\"494\">  <p>При помощи функционального блока «Файлы» возможна загрузка файлов, доступных для данного раздела. Это могут быть изображения, архивы, документы и прочее. Для того чтобы загрузить файл, необходимо нажать на кнопку «Обзор», указать при помощи стандартного диалогового окна местоположение файла и нажать на кнопку «Загрузить» <br> Все загруженные файлы можно увидеть в выпадающем списке доступных файлов в этом же функциональном блоке.</p>  </td>  </tr>  <tr>  <td valign=\"top\" width=\"175\">  <p>Содержимое раздела</p>  </td>  <td valign=\"top\" width=\"494\">  <p>Заполняется с помощью визуального редактора. Подробное описание редактора см. в разделе «Редактор страниц»</p>  </td>  </tr>  <tr>  <td valign=\"top\" width=\"175\">  <p>При отсутствии контента</p>  </td>  <td valign=\"top\" width=\"494\">  <p>Признак перехода к потомкам (вложенным разделам) при отсутствии контента необходим, когда структура меню становится очень ветвистой, т.е. присутствует множество разделов вложенных друг в друга. Если данный признак установлен, то при обращении к разделу сначала произойдёт проверка на наличие содержимого. При отсутствии контента происходит либо переход к списку вложенных разделов сайта, либо к содержимому первого раздела, либо выводится сообщение о том, что раздел находится в стадии разработки.</p>  </td>  </tr>  <tr>  <td valign=\"top\" width=\"175\">  <p>Название раздела в адресной строке (на англ) - одно слово</p>  </td>  <td valign=\"top\" width=\"494\">  <p>Необходимо записать название раздела на английском языке без пробелов (например, для раздела «Контактная информация» можно вписать «contacts»). Вписанное значение будет дополняться в адресе к разделу (URL), например, <a href=\"http://www.site.ru/contacts/\">www.site.ru/contacts/</a>. Ссылка формируется по иерархии меню, т.е. сначала идёт данный параметр для корневого раздела и далее по иерархии до редактируемого раздела (/parent-alias/child-alias/).</p>  </td>  </tr>  <tr>  <td colspan=\"2\" valign=\"top\" width=\"670\">  <p><em>Дополнительные свойства</em></p>  </td>  </tr>  <tr>  <td valign=\"top\" width=\"175\">  <p>Модули</p>  </td>  <td valign=\"top\" width=\"494\">  <p>В данном пункте присутствует список доступных шаблонов модулей (наборов модулей) для раздела.</p>  </td>  </tr>  <tr>  <td valign=\"top\" width=\"175\">  <p>Имя папки для хранения данных</p>  </td>  <td valign=\"top\" width=\"494\">  <p>Свойство определяет имя папки на диске, в которой будут храниться данные раздела (изображения и прочие загружаемые файлы). Рекомендуем оставлять его пустым.</p>  </td>  </tr>  <tr>  <td valign=\"top\" width=\"175\">  <p>Мета тэг description</p>  </td>  <td valign=\"top\" width=\"494\">  <p>короткое описание содержимого странички. Если оставить это поле пустым, то будет взят заголовок данного раздела.</p>  </td>  </tr>  <tr>  <td valign=\"top\" width=\"175\">  <p>Мета тэг keywords</p>  </td>  <td valign=\"top\" width=\"494\">  <p>ключевые слова раздела</p>  </td>  </tr>  <tr>  <td valign=\"top\" width=\"175\">  <p>Значение тэга title</p>  </td>  <td valign=\"top\" width=\"494\">  <p>Установка значения заголовка окна браузера для данного раздела. При отсутствии заполняется автоматически.</p>  </td>  </tr>  </tbody>  </table>  <p>Для сохранения внесенных изменений необходимо нажать кнопку «Сохранить», при этом произойдет переход к списку элементов. При нажатии на кнопку «Применить», происходит сохранение всех данных без перехода куда-либо (обычно нужно, чтобы сохранить данные и продолжить работу со свойствами раздела). Для выхода из режима редактирования без сохранения изменений нажмите кнопку «Отменить».</p>',0,1,8),(21,'Управление фотоальбомом и видеоматериалами','<p></p>  <p style=\"text-align: center;\"><img src=\"http://ygin.ru/instruction/engine/foto/clip_image002_0009.jpg\" alt=\"\" height=\"356\" width=\"623\"></p>  <p style=\"text-align: center;\">Фотоальбомы</p>  <p style=\"text-align: left;\">В фотогалерее есть возможность создавать новые фотоальбомы, а также создавать вложенные фотоальбомы, т.е. многоуровневую иерархию альбомов.</p>  <p style=\"text-align: left;\">Для добавления альбома в текущий список (т.е. на данный уровень вложенности) служит кнопка «Создать», расположенная на панели управления.</p>  <p style=\"text-align: left;\">Для создания вложенного альбома, нужно перейти на нужный уровень вложенности при помощи кнопки «Вложенные объекты» (см. область 1 рисунка 10) и воспользоваться кнопкой «Создать».</p>  <table border=\"1\" cellpadding=\"0\" cellspacing=\"0\">  <tbody>  <tr>  <td colspan=\"2\" valign=\"top\" width=\"676\"><em>Свойства альбома</em></td>  </tr>  <tr>  <td valign=\"top\" width=\"162\">  <p>Название</p>  </td>  <td valign=\"top\" width=\"514\">  <p>Заголовок фотоальбома.</p>  </td>  </tr>  <tr>  <td valign=\"top\" width=\"162\">  <p>Превью-фото</p>  </td>  <td valign=\"top\" width=\"514\">  <p>Изображение, отображаемое в списке фотоальбомов на сайте рядом с названием и текстом в списке галерей.</p>  </td>  </tr>  <tr>  <td valign=\"top\" width=\"162\">  <p>Текст в списке галерей</p>  </td>  <td valign=\"top\" width=\"514\">  <p>В разделе «Фотоальбом» на сайте выводится список имеющихся на сайте фотоальбомов. Если данное поле заполнено, то введённый в него текст отображается рядом с превью-фото фотоальбома в этом списке.</p>  </td>  </tr>  <tr>  <td valign=\"top\" width=\"162\">  <p>Текст в галерее</p>  </td>  <td valign=\"top\" width=\"514\">  <p>Если данное поле заполнено, то введённый в него текст отображается на сайте во время просмотра фотоальбома перед фотографиями.</p>  </td>  </tr>  </tbody>  </table>  <p>Для загрузки фотографий в нужный альбом необходимо перейти по ссылке «Фотографии» (см. область 2 рисунка 10) и воспользоваться кнопкой «Создать»</p>  <p style=\"text-align: center;\"><img src=\"http://ygin.ru/instruction/engine/foto/clip_image004_0000.jpg\" alt=\"\" height=\"344\" width=\"470\"></p>  <p style=\"text-align: center;\">Фотографии фотоальбома «Чудо России – столбы выветривания плато Маньпупунёр»</p>  <table border=\"1\" cellpadding=\"0\" cellspacing=\"0\">  <tbody>  <tr>  <td colspan=\"2\" valign=\"top\" width=\"676\"><em>Свойства фотографий</em></td>  </tr>  <tr>  <td valign=\"top\" width=\"197\">  <p>Название</p>  </td>  <td valign=\"top\" width=\"479\">  <p>Название фотографии (если введено) отображается на сайте рядом с фотографией.</p>  </td>  </tr>  <tr>  <td valign=\"top\" width=\"197\">  <p>Изображение</p>  </td>  <td valign=\"top\" width=\"479\">  <p>Исходное изображение, на основании которого будет сгенерирована превью-картинка (уменьшенная до небольшого размера) для просмотра в фотоальбоме</p>  </td>  </tr>  </tbody>  </table>  <p>Управление видеоматериалами производится аналогичным образом, с той разницей, что к видеоролику можно добавлять изображение для его первого кадра.</p>',0,0,9),(22,'Новостная система','<p></p>  <p align=\"center\"><img src=\"http://ygin.ru/instruction/engine/novosti/clip_image002_0009.jpg\" alt=\"\" height=\"360\" width=\"494\"> <br> Рис.12. Список новостей</p>  <p style=\"text-align: left;\"><br> Для добавления новости необходимо нажать на кнопку «Создать», расположенную над списком новостей.<br> <strong>На экране редактирования свойств новости заполните поля:</strong></p>  <table border=\"1\" cellpadding=\"0\" cellspacing=\"0\">  <tbody>  <tr>  <td colspan=\"2\" valign=\"top\" width=\"676\">  <p><em>Свойства новости</em></p>  </td>  </tr>  <tr>  <td valign=\"top\" width=\"163\">  <p align=\"left\">Заголовок новости</p>  </td>  <td valign=\"top\" width=\"513\">  <p>Введённое значение будет использоваться как заголовок новости на сайте.</p>  </td>  </tr>  <tr>  <td valign=\"top\" width=\"163\">  <p align=\"left\">Дата начала</p>  </td>  <td valign=\"top\" width=\"513\">  <p>Новость либо относится к данной дате, либо начинается с неё.</p>  </td>  </tr>  <tr>  <td valign=\"top\" width=\"163\">  <p align=\"left\">Показывать время</p>  </td>  <td valign=\"top\" width=\"513\">  <p>Показывать ли время в датах новости.</p>  </td>  </tr>  <tr>  <td valign=\"top\" width=\"163\">  <p align=\"left\">Дата окончания (анонс)</p>  </td>  <td valign=\"top\" width=\"513\">  <p>Иногда новость трактуется как событие, происходящее в определённый промежуток времени. Тогда «Дата анонса» (а позднее «Дата новости») на сайте отображается как «Дата начала — Дата окончания (анонс)».</p>  </td>  </tr>  <tr>  <td valign=\"top\" width=\"163\">  <p align=\"left\">Дата для главной страницы (закрепление)</p>  </td>  <td valign=\"top\" width=\"513\">  <p>Используется только на главной странице портала. Предназначена для определения порядка новостей на ленте (закрепления новости до указанной даты).</p>  </td>  </tr>  <tr>  <td valign=\"top\" width=\"163\">  <p align=\"left\">Показывать ли дату окончания</p>  </td>  <td valign=\"top\" width=\"513\">  <p>Признак, показывающий, отображать ли дату окончания события на сайте или нет.</p>  </td>  </tr>  <tr>  <td valign=\"top\" width=\"163\">  <p align=\"left\">Картинка</p>  </td>  <td valign=\"top\" width=\"513\">  <p>Изображение, привязанное к новости. Отображается как на главной странице сайта, так и в разделе конкретной новости.</p>  </td>  </tr>  <tr>  <td valign=\"top\" width=\"163\">  <p align=\"left\">Краткое содержание</p>  </td>  <td valign=\"top\" width=\"513\">  <p>Отображается на главной странице.</p>  </td>  </tr>  <tr>  <td valign=\"top\" width=\"163\">  <p align=\"left\">Файлы</p>  </td>  <td valign=\"top\" width=\"513\">  <p>Список файлов, ссылки на которые (либо сами файлы) необходимо вставить в «Текст новости». Для того, чтобы загрузить файл, необходимо нажать на кнопку «Обзор», указать при помощи стандартного диалогового окна местоположение файла и нажать на кнопку «Загрузить». Все загруженные файлы можно увидеть в выпадающем списке доступных файлов.</p>  </td>  </tr>  <tr>  <td valign=\"top\" width=\"163\">  <p align=\"left\">Текст новости</p>  </td>  <td valign=\"top\" width=\"513\">  <p>Заполняется с помощью визуального редактора. Выводится на сайте при отображении конкретной новости.</p>  </td>  </tr>  <tr>  <td valign=\"top\" width=\"163\">  <p align=\"left\">Анонс</p>  </td>  <td valign=\"top\" width=\"513\">  <p>В системе анонс становится новостью после того, как дата начала становится больше текущего времени. Пока анонс новостью не стал, содержимое этого поля выводится на главной странице в блоке анонсов вместе с заголовком.</p>  </td>  </tr>  <tr>  <td valign=\"top\" width=\"163\">  <p align=\"left\">Категории событий</p>  </td>  <td valign=\"top\" width=\"513\">  <p>Здесь перечислены категории, к которым может относиться новость. Одна новость может относиться к нескольким категориям.</p>  </td>  </tr>  <tr>  <td valign=\"top\" width=\"163\">  <p align=\"left\">Видимость</p>  </td>  <td valign=\"top\" width=\"513\">  <p>Признак, отвечающий за отображение новости как на главной странице сайта, так и в архиве новостей.</p>  </td>  </tr>  <tr>  <td valign=\"top\" width=\"163\">  <p align=\"left\">Видео</p>  </td>  <td valign=\"top\" width=\"513\">  <p>Видеоролик, связанный с новостью.</p>  </td>  </tr>  <tr>  <td valign=\"top\" width=\"163\">  <p align=\"left\">Первый кадр видеоролика</p>  </td>  <td valign=\"top\" width=\"513\">  <p>Если есть видеоролик, то для его корректного отображения на странице (до начала воспроизведения) загружается изображение первого кадра. Оно отображается в плейере на странице новости на сайте.</p>  </td>  </tr>  </tbody>  </table>',0,0,10);
/*!40000 ALTER TABLE `da_instruction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `da_instruction_rel`
--

DROP TABLE IF EXISTS `da_instruction_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `da_instruction_rel` (
  `id_instruction` int(8) NOT NULL,
  `rel_text` varchar(255) DEFAULT NULL,
  `id_instruction_rel` int(8) NOT NULL,
  PRIMARY KEY (`id_instruction`,`id_instruction_rel`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `da_instruction_rel`
--

LOCK TABLES `da_instruction_rel` WRITE;
/*!40000 ALTER TABLE `da_instruction_rel` DISABLE KEYS */;
INSERT INTO `da_instruction_rel` VALUES (13,'Структура системы управления',15),(14,'Основные возможности',13),(14,'Меню сайта',20),(15,'Основные возможности',13),(15,'Поиск данных',17),(15,'Элементы панели управления',18),(17,'Структура системы управления',15),(17,'Элементы панели управления',18),(19,'Размещение видео с Rutube',11),(19,'Размещение видео с YouTube',12),(20,'Редактор страниц',19);
/*!40000 ALTER TABLE `da_instruction_rel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `da_job`
--

DROP TABLE IF EXISTS `da_job`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `da_job` (
  `id_job` varchar(255) NOT NULL COMMENT 'ИД задачи',
  `interval_value` int(8) NOT NULL DEFAULT '0' COMMENT 'Интервал запуска задачи (в секундах)',
  `error_repeat_interval` int(8) NOT NULL DEFAULT '0' COMMENT 'Интервал запуска задачи в случае ошибки (в секундах)',
  `first_start_date` int(10) DEFAULT NULL COMMENT 'Дата первого запуска',
  `last_start_date` int(10) DEFAULT NULL COMMENT 'Дата последнего запуска',
  `next_start_date` int(10) DEFAULT NULL COMMENT 'Дата следущего запуска',
  `failures` int(2) NOT NULL DEFAULT '0' COMMENT 'Количество ошибок',
  `name` varchar(255) NOT NULL COMMENT 'Имя задачи',
  `class_name` varchar(255) NOT NULL COMMENT 'Имя класса задачи',
  `active` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Вкл.',
  `priority` tinyint(2) NOT NULL DEFAULT '0' COMMENT 'Приоритет запуска',
  `start_date` int(11) DEFAULT NULL COMMENT 'Дата запуска текущего потока',
  `max_second_process` int(10) DEFAULT NULL COMMENT 'Максимальное число секунд выполнения задачи',
  PRIMARY KEY (`id_job`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 PACK_KEYS=0 COMMENT='Планировщик';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `da_job`
--

LOCK TABLES `da_job` WRITE;
/*!40000 ALTER TABLE `da_job` DISABLE KEYS */;
INSERT INTO `da_job` VALUES ('1',90,180,1313061006,1344593833,1344593882,0,'Отсылка сообщений','ygin.modules.mail.components.DispatchEvents',1,0,NULL,120);
/*!40000 ALTER TABLE `da_job` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `da_link_message_user`
--

DROP TABLE IF EXISTS `da_link_message_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `da_link_message_user` (
  `id_message` int(8) NOT NULL,
  `id_user` int(8) NOT NULL,
  PRIMARY KEY (`id_message`,`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `da_link_message_user`
--

LOCK TABLES `da_link_message_user` WRITE;
/*!40000 ALTER TABLE `da_link_message_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `da_link_message_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `da_localization`
--

DROP TABLE IF EXISTS `da_localization`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `da_localization` (
  `id_localization` int(8) NOT NULL COMMENT 'id',
  `name` varchar(60) NOT NULL COMMENT 'Название',
  `code` char(3) NOT NULL COMMENT 'Код',
  `is_use` int(1) NOT NULL DEFAULT '1' COMMENT 'Используется ли локализация',
  PRIMARY KEY (`id_localization`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Доступные локализации';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `da_localization`
--

LOCK TABLES `da_localization` WRITE;
/*!40000 ALTER TABLE `da_localization` DISABLE KEYS */;
INSERT INTO `da_localization` VALUES (1,'Русская','ru',1),(2,'Английская','en',0),(3,'Финская','fi',0),(4,'Коми','km',0),(5,'Немецкая','de',0),(6,'Французская','fr',0),(7,'Албанская','al',0);
/*!40000 ALTER TABLE `da_localization` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `da_mail_account`
--

DROP TABLE IF EXISTS `da_mail_account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `da_mail_account` (
  `id_mail_account` int(8) NOT NULL COMMENT 'id',
  `email_from` varchar(50) DEFAULT NULL COMMENT 'E-mail для поля "От"',
  `from_name` varchar(100) DEFAULT NULL COMMENT 'Имя отправителя',
  `default_subject` varchar(255) DEFAULT NULL COMMENT 'Тема по умолчанию',
  `host` varchar(50) NOT NULL COMMENT 'HOST',
  `user_name` varchar(50) DEFAULT NULL COMMENT 'Имя пользователя для авторизации',
  `user_password` varchar(50) DEFAULT NULL COMMENT 'Пароль для авторизации',
  `smtp_auth` int(1) NOT NULL DEFAULT '1' COMMENT 'Требуется ли авторизация',
  PRIMARY KEY (`id_mail_account`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 PACK_KEYS=0 COMMENT='Почтовые аккаунты';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `da_mail_account`
--

LOCK TABLES `da_mail_account` WRITE;
/*!40000 ALTER TABLE `da_mail_account` DISABLE KEYS */;
INSERT INTO `da_mail_account` VALUES (10,'robot@domain.ru','domain.ru',NULL,'smtpold.baitek.org','robot@cvek.ru','robotrobot14',1);
/*!40000 ALTER TABLE `da_mail_account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `da_menu`
--

DROP TABLE IF EXISTS `da_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `da_menu` (
  `id` int(8) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(255) NOT NULL COMMENT 'Название в меню',
  `caption` varchar(255) DEFAULT NULL COMMENT 'Заголовок раздела',
  `content` longtext COMMENT 'Содержимое раздела',
  `visible` tinyint(1) DEFAULT '1' COMMENT 'Видимость',
  `id_parent` int(8) DEFAULT NULL COMMENT 'Смена родительского раздела',
  `sequence` int(2) NOT NULL DEFAULT '1' COMMENT '&nbsp;',
  `note` varchar(200) DEFAULT NULL COMMENT 'Примечания',
  `alias` varchar(100) DEFAULT NULL COMMENT 'Название в адресной строке',
  `title_teg` varchar(255) DEFAULT NULL COMMENT '<title>',
  `meta_description` varchar(255) DEFAULT NULL COMMENT '<meta name="description">',
  `meta_keywords` varchar(255) DEFAULT NULL COMMENT '<meta name="keywords">',
  `go_to_type` tinyint(1) DEFAULT '1' COMMENT 'При отсутствии контента:',
  `id_module_template` int(8) DEFAULT NULL COMMENT 'Набор модулей',
  `external_link` varchar(255) DEFAULT NULL COMMENT 'Ссылка (авто)',
  `external_link_type` tinyint(1) DEFAULT NULL COMMENT 'Открывать в новом окне (авто)',
  `removable` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Разрешить удаление',
  `image` int(8) DEFAULT NULL COMMENT 'Картинка для раздела',
  PRIMARY KEY (`id`),
  UNIQUE KEY `alias` (`alias`),
  KEY `id` (`id`,`id_parent`,`sequence`)
) ENGINE=InnoDB AUTO_INCREMENT=176 DEFAULT CHARSET=utf8 COMMENT='Меню';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `da_menu`
--

LOCK TABLES `da_menu` WRITE;
/*!40000 ALTER TABLE `da_menu` DISABLE KEYS */;
INSERT INTO `da_menu` VALUES (100,'Главная','Дорогие друзья!','',0,NULL,1,NULL,'/',NULL,NULL,NULL,4,4,NULL,0,1,NULL),(102,'Поиск по сайту','Поиск по сайту',NULL,0,NULL,3,NULL,'search',NULL,NULL,NULL,1,1,'',0,0,NULL),(104,'Категория новостей','Категория новостей',NULL,0,103,5,NULL,'category',NULL,NULL,NULL,1,1,'',0,0,NULL),(105,'Консультации','Консультации',NULL,0,NULL,2,NULL,'consultation',NULL,NULL,NULL,1,1,'',0,0,NULL),(112,'Модерирование комментариев','Модерирование комментариев',NULL,0,NULL,4,NULL,'comment_moderation',NULL,NULL,NULL,1,1,'',0,0,NULL),(114,'Контакты','Контакты','<table class=\"b-contact-table\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\r\n<tbody>\r\n<tr>\r\n<td>\r\n<p style=\"text-align: left;\"><strong>Телефоны:</strong><br>Москва: +7(916) 833-15-26<br> Сыктывкар: +7(8212) 39-14-81, 39-14-68<br> Ухта: +7(904) 863-88-05 <br> Киров: +7(908) 717-57-92</p>\r\n</td>\r\n<td>\r\n<p><strong>Адрес:</strong><br> Республика Коми, г. Сыктывкар<br> ул. Перовмайская 62, оф. блок \"А\", 5 этаж, офис 505А</p>\r\n</td>\r\n<td>\r\n<p style=\"text-align: left;\"><strong>Электронная почта:</strong></p>\r\n<p><a href=\"mailto:web@cvek.ru\">web@cvek.ru</a></p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>',1,140,9,NULL,'feedback',NULL,NULL,NULL,1,2,'',0,1,NULL),(115,'О компании',NULL,'',1,NULL,5,NULL,'o_kompanii',NULL,NULL,NULL,2,1,NULL,0,1,NULL),(116,'Контакты',NULL,'<div style=\"text-align: left;\">Наша страничка вконтакте: <a href=\"https://vk.com/club42586416\">http://vk.com/club42586416</a></div>\r\n<div style=\"text-align: left;\"></div>\r\n<div style=\"text-align: left;\"><strong>Контакты компании \"Эколайн\":</strong></div>\r\n<div style=\"text-align: left;\">Офис компании «Эколайн» в Сыктывкар: ул. Ленина, 118, тел. (8212) 202-802<br>e-mail: info@ecoline-komi.ru</div>\r\n<div style=\"text-align: left;\"><br><strong>Директор компании</strong> - Игнатов Михаил Иванович<br>Офис компании «Эколайн» в Ухте: ул. Оплеснина, д. 20, тел. (8216) 76-25-25<br>e-mail: ekoline_uhta@list.ru</div>\r\n<div style=\"text-align: left;\"></div>\r\n<div style=\"text-align: left;\">Заказ воды с доставкой в офис или на дом</div>\r\n<div style=\"text-align: left;\">Прием заказов с 8.00 до 20.00 - в будни, <br>с 8.00 до 17.00 - в cубботу и воскресенье,<br>тел. в Сыктывкаре (8212) 202-802, в Ухте (8216) 76-25-25</div>\r\n<div style=\"text-align: left;\"></div>\r\n<div style=\"text-align: left;\"><strong>Отдел по продаже воды в ПЭТ-таре</strong><br>Руководитель отдела в Сыктывкаре - Столярова Наталья Ивановна, тел. (8212) 21-61-12<br>Прием заказов : ежедневно с 8.30 до 17.00<br>Руководитель отдела продаж воды в ПЭТ-таре в Ухте - (8216) 76-25-26</div>\r\n<div style=\"text-align: left;\"></div>\r\n<div style=\"text-align: left;\"><strong>Отдел продвижения</strong><br>Руководитель отдела - Анисовец Жанна Ивановна, тел. (8212) 202-802</div>\r\n<div style=\"text-align: left;\"></div>\r\n<div style=\"text-align: left;\"><strong>Отдел продаж</strong><br>Руководитель отдела в Сыктывкаре - Филипова Наталья Васильевна тел. (8212) 202-802</div>\r\n<div style=\"text-align: left;\"></div>\r\n<div style=\"text-align: left;\"><strong>Фирменные магазины</strong><br>Адреса : <br>Сыктывкар, ул. Ленина, д. 28, тел. (8212) 579-240 <br>Cыктывкар, Димитрова, д.48, тел. (8212) 57-97-90<br>Ухта, ул. 40 лет Коми, д. 10, тел.</div>\r\n<div style=\"text-align: left;\"></div>\r\n<div style=\"text-align: left;\"><strong>Сервисный центр</strong><br>Режим работы : понедельник-пятница с 8.30 до 17.30, тел. в Сыктывкаре (8212) 21-16-09<br>Руководитель - Кокис Андрей Петрович, адрес: Станционная, 64.</div>\r\n<div style=\"text-align: left;\">Сервисный центр в Ухте тел. (8216) 76-25-26</div>\r\n<div style=\"text-align: left;\">Дилеры в Сосногорске:</div>\r\n<div style=\"text-align: left;\">Телефон доставки воды 54-888, 9-06-46 (сотовый)<br>Прием заказов:<br>ежедневно: с 9.00 до 17.00<br>Доставка воды:<br>ежедневно: с 9.00 до 13.00, с 14.00 до 18.00<br>Пункт обмена воды \"Краснозатонская Серебряная\" <br>г. Сосногорск, пер-к Сосновский, 2<br>Режим работы - ежедневно с 9.00 до 18.00</div>\r\n<div style=\"text-align: left;\"></div>\r\n<div style=\"text-align: left;\"><strong>Магазины \"Чай Кофе\"</strong></div>\r\n<div style=\"text-align: left;\">«Чай Кофе» в пункте обмена - Сыктывкар, ул. Ленина, 28, тел. 57-92-40<br>«Чай Кофе» в пункте обмена - Сыктывкар, Димитрова, 48, тел. 57-97-90</div>\r\n<div style=\"text-align: center;\"></div>',1,NULL,6,NULL,'kontakty',NULL,NULL,NULL,1,1,NULL,0,1,NULL),(117,'Кулеры и помпы',NULL,'',1,NULL,7,NULL,'kulery_i_pompy',NULL,NULL,NULL,2,7,NULL,0,1,NULL),(118,'Вакансии',NULL,'',1,NULL,8,NULL,'vakansii',NULL,NULL,NULL,1,1,NULL,0,1,NULL),(120,'Задать вопрос','Вопрос-ответ','',1,NULL,10,NULL,'faq',NULL,NULL,NULL,1,3,'/faq/',0,0,NULL),(121,'Компания',NULL,'',1,115,1,NULL,'Kompanii',NULL,NULL,NULL,2,1,NULL,0,1,NULL),(122,'Продукция',NULL,'',1,115,3,NULL,'o_produktsii',NULL,NULL,NULL,2,7,NULL,0,1,NULL),(123,'Производство',NULL,'',1,115,2,NULL,'o_proizvodstve',NULL,NULL,NULL,2,7,NULL,0,1,NULL),(127,'Фотогалерея','Фотогалерея',NULL,0,NULL,1,NULL,'photogallery',NULL,NULL,NULL,1,3,'/photogallery/',NULL,0,NULL),(129,'Левое меню',NULL,'',0,NULL,17,NULL,'levoe_menju',NULL,NULL,NULL,1,1,NULL,0,1,NULL),(130,'Заказ и доставка',NULL,'<p>\"Краснозатонская Серебряная\" цена с доставкой: 170 руб. в фирменном магазине: 140 руб.</p>\r\n<p><img src=\"/content/menu/130/zakaz-krasnozatonoskoi.jpg\" width=\"89\" height=\"160\" alt=\"zakaz-krasnozatonoskoi.jpg\" title=\"zakaz-krasnozatonoskoi.jpg\"></p>\r\n<p>Чистая питьевая артезианская вода высшей категории, отвечающая не только критериям безопасности, но и критерию полезности. Разливается на современном высокотехнологичном оборудовании и не продается в розлив на улице.</p>\r\n<p>«Краснозатонская Серебряная» оптимально сбалансирована по содержанию необходимых человеку биогенных элементов: кальция, магния, калия и фтора.</p>\r\n<p><img src=\"/content/menu/130/mineraly-serebryannoi.jpg\" width=\"242\" height=\"204\" alt=\"mineraly-serebryannoi.jpg\" title=\"mineraly-serebryannoi.jpg\"></p>\r\n<p>\"Краснозатонская Йодированная\" цена с доставкой: 190 руб. в фирменном магазине: 160 руб. <br><img src=\"/content/menu/130/iodirovannyya.jpg\" width=\"84\" height=\"160\" alt=\"iodirovannyya.jpg\" title=\"iodirovannyya.jpg\"></p>\r\n<p>Чистая питьевая артезианская вода высшей категории, обогащенная йодом. Йод в воде находится в виде ионов. За счет чего практически полностью усваивается организмом человека и не разрушается при кипячении. Вода способствует профилактике йододефицита</p>\r\n<p><img src=\"/content/menu/130/sostav-iodirovannoi.jpg\" width=\"242\" height=\"202\" alt=\"sostav-iodirovannoi.jpg\" title=\"sostav-iodirovannoi.jpg\"></p>\r\n<p>Как заказать?</p>\r\n<p>• Заказать воду с доставкой можно по телефону 202-802 в Сыктывкаре<br>Прием заказов:<br>в будни: с 8.00 до 19.00<br>в выходные: с 8.00 до 17.00<br>• заказать с сайта круглосуточно (кнопка-ссылка на заказ воды)<br>Вы можете указать желаемое время доставки. При невозможности доставить Вам воду в указанный на сайте интервал, Вы получите уведомление от оператора с откорректированным реальным временем доставки воды.<br>• купить в фирменном магазине по адресам Ленина, 28 (ссылка на google карту)<br>и Димитрова, 48(ссылка на google карту)</p>\r\n<p><br>Условия заказа и доставка:<br>Доставка воды производится:<br>в будни: с 9.00 до 20.00<br>в выходные: с 9.00 до 18.00<br>• Доставка воды осуществляется в день приема заказа, если заказ был сделан до 12 часов дня, если позже, то на следующий день после поступления заявки.<br>• Услуги по доставке и подъему воды и оборудования на любой этаж оказываются бесплатно.<br>Стоимость и оплата:<br>Цена с доставкой указана за доставку 1 бутыли. <br>Стоимость доставки от 2 бутылей одновременно за наличный расчет ниже на 20 рублей и составляет 150 рублей за бутыль «Краснозатонской Серебряной» и 170 рублей за бутыль «Краснозатонской Йодированной».</p>\r\n<p></p>',1,129,18,'icon-1','zakaz_i_dostavka',NULL,NULL,NULL,1,1,NULL,0,1,536),(131,'Новичкам',NULL,'',1,129,19,NULL,'novichkam',NULL,NULL,NULL,2,7,NULL,0,1,NULL),(132,'Акции',NULL,'',1,129,20,NULL,'aktsii',NULL,NULL,NULL,4,7,NULL,0,1,NULL),(133,'Новости',NULL,'',1,129,21,NULL,'novosti',NULL,NULL,NULL,2,7,NULL,0,1,NULL),(134,'Новости','Новости',NULL,0,NULL,1,NULL,'news',NULL,NULL,NULL,1,6,'/news/',NULL,0,NULL),(135,'Как выбрать лучшую воду?',NULL,'<p><br>Выбирая производителя воды учитывайте следующие моменты:<br>• категорию воды (полноценной по содержанию макро- и микроэлементов может быть только вода высшей категории)<br>• месторасположение источника воды<br>• технологический уровень производства<br>• вкус воды<br>• сколько лет компания работает на рынке (надежность поставщика)<br>• удаленность производства воды от источника (наличие дополнительных перевозок, которые негативно сказываются на свойствах воды)</p>',1,131,22,NULL,'kak_vybrat',NULL,NULL,NULL,2,7,NULL,0,1,540),(136,'Ищите для себя и близких лучшее? Почему стоит выбрать воду «Краснозатонскую»? ',NULL,'<p><br>Пользу и вкусовые свойства «Краснозатонской» сегодня ценят тысячи наших постоянных клиентов в Сыктывкаре, Ухте, Сосногорске и других населенных пунктах Республики Коми.<br>Вода \"Краснозатонская\" - единственная в Республике Коми бутилированная вода высшей категории. Разливается на современном оборудовании и исключительно в стерильных условиях, в месте добычи. Ни в коем случае не продается в розлив.<br>Высшая категория воды гарантирует не только ее чистоту и безопасность, но и полезность! «Краснозатонская» сбалансирована по основным биологически необходимым человеку микро- и макроэлементам.<br>- рекомендована Институтом питания РАМН для детей с первого года жизни, восстановления сухих молочных смесей, <br>- источник добычи –артезианская скважина, расположен вдали от города, в экологически чистом месте - поселке Краснозатонский.<br>- выбирая «Краснозатонскую Йодированную», Вы укрепляете иммунитет, стимулируете умственную деятельность, повышаете свою активность и работоспособность.<br>Компания \"Эколайн\" - ведущий производитель воды высшей категории, имеет отличную репутацию как добросовестного производителя и надежного партнера, так и активного участника общественной жизни Республики Коми уже на протяжении 15 лет!</p>',1,131,23,NULL,'ishite',NULL,NULL,NULL,2,7,NULL,0,1,NULL),(137,'Как заказать «Краснозатонскую»?',NULL,'<p><br>Доставку воды Вы можете заказать:<br>— по телефону 202-802<br>— через сайт, заполнив форму заказа (ссылка на заказ воды)<br>Заказывая воду, вы всегда можете указать удобное для вас время доставки. При доставке воды на дом оплата производится по факту получения.</p>\r\n<p>Доставка воды в офисы осуществляется в соответствии с выбранными условиями за наличный или безналичный расчет.</p>',1,131,24,NULL,'kak_zakazat_krasnozatonskuju',NULL,NULL,NULL,2,7,NULL,0,1,NULL),(138,'Фирменные магазины',NULL,'<p></p>\r\n<p>Для тех, кому удобней прийти за водой в магазин, могут также сэкономить!</p>\r\n<p>Адреса фирменных магазинов «Краснозатонская Серебряная»:</p>\r\n<p>г. Сыктывкар: ул. Ленина, 28; ул. Димитрова, 48;<br>г. Ухта: ул. 40 лет Коми, 10<br>Режим работы:<br>будни: с 9.00 до 20.00<br>выходные: с 9.00 до 18.00</p>\r\n<p>г. Сосногорск: Сосновский пер-к, д. 2<br>ежедневно с 9.00 до 18.00</p>\r\n<p>все адреса с ссылками на карты</p>',1,131,25,NULL,'firmennye_magaziny',NULL,NULL,NULL,2,7,NULL,0,1,NULL),(139,'Оборотная тара',NULL,'<p><span>Продажа воды осуществляется в 19-литровых бутылях. Эти бутыли изготовлены из поликарбоната — материала, не оказывающего негативного воздействия на воду, легко моющегося, прочного, устойчивого к высоким и низким температурам.</span></p>\r\n<p>Если Вы заказываете воду в первый раз, и у Вас нет оборотной тары, то при первой поставке необходимо оплатить стоимость бутыли. При последующих поставках Вы будете обменивать пустые бутыли на полные!<br>Если Вы раньше заказывали воду других компаний, и у Вас уже есть бутыли, то мы готовы их принять на обмен, но только в том случае, если они изготовлены из поликарбоната и находятся в хорошем состоянии (имеют царапины и потертости не более допустимого уровня). Состояние бутыли проверяет водитель-экспедитор на доставке, либо продавец в обменном пункте.<br>Обратите внимание, что цена воды не включает стоимость бутыли.</p>',1,131,26,NULL,'oborotnaya_tara',NULL,NULL,NULL,2,7,NULL,0,1,542),(140,'Оборудование для розлива воды',NULL,'<p><br>Пить полезную и вкусную воду из 19 л бутылей - удобно!</p>\r\n<p>Чтобы получить от нашей воды не только пользу, но и удовольствие, попробуйте использовать специально разработанное для бутилированной воды.</p>\r\n<p>Помпа. Ручной насос, подает воду порциями при каждом нажатии. Незаменимый помощник для удобного потребления воды дома.</p>\r\n<p>Наклонная подставка. Бутыль располагается на подставке горлышком вниз, на которое одевается пробка с краником. <br>Легким нажатием на краник можно быстро и легко наполнить водой нужную емкость. Хит в школах и детских садах.</p>\r\n<p>Кулеры для воды. <br>Современный кулер - это устройство для розлива, подогрева и охлаждения питьевой воды. Без него уже почти невозможно приготовления офисной кружки горячего чая или кофе. В нашей компании только качественные аппараты от лидеров рынка Bioray, AEL, Hotfrost, сервисное обслуживание и послегарантийный ремонт.<br>Доставка кулеров юридическим лицам бесплатно.</p>',1,131,27,NULL,'oborudovanie_dlya_rozliva_vody',NULL,NULL,NULL,2,7,NULL,0,1,NULL),(141,'Авторизация','Авторизация',NULL,0,NULL,1,NULL,'login',NULL,NULL,NULL,1,1,'/login/',NULL,0,NULL),(142,'Регистрация','Регистрация',NULL,0,NULL,1,NULL,'register',NULL,NULL,NULL,1,1,'/register/',NULL,0,NULL),(143,'Восстановление пароля','Восстановление пароля',NULL,0,NULL,1,NULL,'recover',NULL,NULL,NULL,1,1,'/recover/',NULL,0,NULL),(144,'Профиль','Профиль',NULL,0,NULL,1,NULL,'profile',NULL,NULL,NULL,1,1,'/profile/',NULL,0,NULL),(145,'Пользователь','Пользователь',NULL,0,NULL,1,NULL,'user',NULL,NULL,NULL,1,1,'/user/',NULL,0,NULL),(150,'Комп_1',NULL,'<span style=\"white-space: pre;\"> </span>Компания «Эколайн» – один из лидеров в отрасли производства питьевых и минеральных вод в России, ведущий производитель питьевой воды высшей категории в республике Коми.',1,121,28,NULL,'komp_1',NULL,NULL,NULL,1,7,NULL,0,1,562),(151,'Комп_2',NULL,'<p>Миссия компании:</p>\r\n<p>• быть лидерами в обеспечении жителей Республики Коми питьевой бутилированной водой;<br>• пропагандировать потребление чистой воды как элемента здорового образа жизни;<br>• предоставлять нашим клиентам продукцию стабильно высокого качества и комфортный сервис, которые они будут рекомендовать друзьям и знакомым;<br>• находить пути совершенствования продукции и услуг, максимально учитывая потребности наших клиентов;<br>• заботиться о сотрудниках компании, заслугой которых является успех компании в долгосрочной перспективе</p>',1,121,29,NULL,'komp_2',NULL,NULL,NULL,1,7,NULL,0,1,564),(152,'Комп_3',NULL,'<p>Истории Компании</p>\r\n<p>Каждое утро так уважительно приветствуют друг друга партнеры по бизнесу компании\"Эколайн\".<br> Относиться с почтением к тем, кого ценишь, уважаешь и кому действительно желаешь здоровья и долгих лет жизни - добрая российская традиция. Почему она прижилась в \"Эколайне\", понимаешь, когда ближе знакомишься с учредителями этой фирмы.<br>Из ученых - в предприниматели<br> Кандидат физико-математических наук Михаил Иванович Игнатов долгое время работал научным сотрудником одного из институтов Коми научного центра. Читать далее (Далее текст открывается в окне прокрутки) Перестройка круто изменила жизнь ученого. Чтобы прокормить семью, пришлось уйти из науки в бизнес. Но привычка - вторая натура. Вот и сохранилось с тех пор доброжелательное, уважительное отношение к своим коллегам, партнерам, собеседникам, среди которых преобладают бывшие и нынешние ученые, врачи, преподаватели, инженеры.<br> Михаил Иванович не только им желает доброго здравия, но и всем своим землякам. Цель его бизнеса - забота о нашем с вами здоровье. Нет, он не занимается врачебной практикой, он просто приучает нас с рождения пить чистую, физиологически полноценную воду, доставляя ее в наши квартиры. В Коми он был пионером этого нового вида деятельности.<br> Причина для того, чтобы заняться производством и доставкой воды сыктывкарцам, оказалась самой что ни на есть житейской. У одного из друзей Михаила Ивановича - заядлого аквариумиста - в один печальный день погибли все рыбки. А ведь он в своем огромном аквариуме заменил одно-единственное ведро воды… Супруги Игнатовы как раз в это время ожидали пополнения семейства и были просто обескуражены произошедшим. \"Я как представил, что нашему новорожденному малышу придется такой водой разводить молочные смеси, готовить на ней пюре и кашки, мурашки побежали по спине, - вспоминает Михаил Иванович. - Нет, решил, надо срочно что-то делать!\"<br>Как-то в разговоре его знакомый Руслан Черемискин высказал идею о бутилированной воде и попал, что называется, в самую точку, задел за живое. Так Руслан Геннадьевич и Михаил Иванович, ударив по рукам, стали партнерами по бизнесу. Конечно, поначалу пришлось много поездить по российским городам и весям, добывать информацию, изучать производство, прежде чем наладить в Сыктывкаре подобное. К делу подходили с дотошностью ученых мужей: листали справочники, делали расчеты и обоснования.<br>Поиск источника<br> Чтобы найти место, где разместить предприятие, советовались со специалистами местной санэпидемслужбы, с геологами. Уж они-то о воде все знают. Самым подходящим оказалось местечко Красный Затон неподалеку от города. Это единственный поселок в черте Сыктывкара, который с начала 60-х снабжается артезианской водой. (Кстати, в советское время вся верхушка партноменклатуры Коми пила исключительно краснозатонскую воду, которую доставляли партийным боссам флягами, а на их дачи в местечко Лемью из Затона был протянут водопровод.) В дома остальных горожан вода, как и прежде, поступает из реки. Да и к тому же на окраине поселка нашлось подходящее для производства помещение бывшего речного порта. Вопроса, как назвать воду, не возникло - конечно,\"Краснозатонская\".<br> <br>Самой главной трудностью в начале пути было приучить людей покупать чистую воду и отказаться от употребления воды из-под крана. В цивилизованных странах две трети населения для питья и приготовления пищи используют только бутилированную воду, даже не смотря на то, что стоит она недешево. Предприниматели поступили просто: первые бутыли с очищенной \"Краснозатонской\" водой развезли по офисам и домам своих друзей и знакомых. Предложили: попробуйте заварить чай или кофе на нашей воде - совсем другой вкус! Что называется, почувствуйте разницу.<br>Так, буквально с нескольких бутылей, началась история компании «Эколайн». С тех пор прошло уже более 13 лет.<br>Хронология развития «Эколайн» 1998 - 2011 гг.<br>октябрь 1998 - организация компании;<br>23 декабря 1998 - начало производства воды «Краснозатонская серебряная» (19л);<br>апрель 2002 - открытие нового цеха по выпуску воды (19л);<br>сентябрь 2002 - установлена американская линия по разливу воды Baby Works (200 бутылей в час);<br>февраль 2003 - получен сертификат на воду высшей категории качества «Краснозатозанская серебряная»;<br>март 2003 - открылся первый пункт обмена воды в Сыктывкаре;<br>март 2003 - запуск барамембранной установки по очистке воды;<br>март 2003 - налажено производство йодированной воды «Будущий гений»;<br>март 2003 - получен сертификат на воду высшей категории качества «Краснозатозанская йодированная»;<br>декабрь 2003 - в системе очистки воды появилась станция озонирования;<br>23 декабря 2003 - открылось представительство компании «Эколайн» в Ухте;<br>октябрь 2004 - открылся первый магазин «Чай Кофе»;<br>июнь 2005 - начал работать участок производства по разливу воды в ПЭТ-тару (0.5л, 1.5л, 5.0л);<br>январь 2007 - установлена новая, линия по разливу воды «Steelhead» (250 бутылей в час);<br>февраль 2007 - начало производства минеральной воды «Сереговская»;<br>29 марта 2010 - открытие второго фирменного магазина (ул.Димитрова, 48);<br>20 января 2011 - запуск новой, более мощной линии розлива воды в ПЭТ-тару;<br>18 апреля 2011 - открытие нового цеха по выпуску воды (19 л и ПЭТ-тара);<br>1 сентября 2011 - запуск автоматической линии по розливу воды 5-10 литров в ПЭТ-таре.</p>',1,121,30,NULL,'komp_3',NULL,NULL,NULL,1,7,NULL,0,1,567),(153,'Прод_1','Прод_1','',1,122,31,NULL,'prod_1',NULL,NULL,NULL,1,7,NULL,0,1,572),(154,'Прод_2','Прод_2','',1,122,32,NULL,'prod_2',NULL,NULL,NULL,1,7,NULL,0,1,587),(155,'Проз_1','Завод','<p> Завод.</p>\r\n<p>Наше производство располагается в 12 км от Сыктывкара в поселке Краснозатонский. Цех по производству воды - это современный, высокотехнологичный комплекс, который был построен в 2011 году. <br>Его площадь более 2000 кв.м.</p>',1,123,33,NULL,'proz_1',NULL,NULL,NULL,1,7,NULL,0,1,574),(156,'Комп_4',NULL,'<p>Социальная ответственность<br>Компания «Эколайн» всегда принимает активное участие в общественной жизни Республики Коми. Постоянно реализуются проекты по поддержке общественных инициатив и организации питьевого режима в сфере спорта, культуры и образования. Активная работа с представителями различных социальных групп и объединений помогает нам предлагать свою помощь именно там, где она необходима.</p>\r\n<p></p>',1,121,34,NULL,'komp_4',NULL,NULL,NULL,1,7,NULL,0,1,569),(157,'Произ_2','Схема водоподготовки и розлива питьевой воды','Схема водоподготовки и розлива питьевой воды',1,123,35,NULL,'proiz_2',NULL,NULL,NULL,1,7,NULL,0,1,576),(158,'Произ_3','Водоподготовка','<p><span style=\"white-space: pre;\"> </span>Водоподготовка.</p>\r\n<p>Производство воды стартует с этапа водоподготовки. Вода добывается с глубины 50 метров, проходит многоступенчатую очистку с последующей корректировкой по минеральному составу. <br>Это позволяет убрать из воды все взвешенные частицы и железо, а затем скорректировать ее состав в соответствии с нормативами физиологической полноценности.</p>',1,123,36,NULL,'proiz_3',NULL,NULL,NULL,1,7,NULL,0,1,579),(159,'Произ_4','Розлив воды в 19л бутыли','<p> Розлив воды в 19л бутыли.</p>\r\n<p>Вода разливается в 19-литровые бутыли из поликарбоната, которые разработаны специально для хранения питьевой воды, экологически безопасны и подлежат многократному использованию. <br>Предварительно бутыли моются на автоматической линии «Steelhead» при высокой температуре (65-70ºС), что позволяет обеспечить их абсолютную стерильность. <br>После помывки бутыли стерилизуются гиперозонированной водой, т.е. водой с большим содержанием озона. <br>Разливается и укупоривается вода автоматически, без контакта с руками человека.</p>',1,123,37,NULL,'proiz_4',NULL,NULL,NULL,1,1,NULL,0,1,581),(160,'Произ_5','Контроль качества воды','<p>Контроль качества воды.</p>\r\n<p>Контроль качества воды производится несколько раз в день и подтверждается ежедневными анализами Роспотребнадзора. <br>Склад. Удобный складской комплекс рассчитан на хранение и погрузку продукции поддонами. Он отвечает самым высоким требованиям, предъявляемым к хранению бутилированной воды: регулярная санитарная обработка полов, контроль температуры и влажности.</p>',1,123,38,NULL,'proiz_5',NULL,NULL,NULL,1,7,NULL,0,1,583),(161,'Прод_3',NULL,'',1,122,39,NULL,'prod_3',NULL,NULL,NULL,1,7,NULL,0,1,589),(162,'Прод_4',NULL,'',1,122,40,NULL,'prod_4',NULL,NULL,NULL,1,7,NULL,0,1,591),(163,'Прод_5',NULL,'',1,122,41,NULL,'prod_5',NULL,NULL,NULL,1,1,NULL,0,1,593),(164,'Прод_6',NULL,'',1,122,42,NULL,'prod_6',NULL,NULL,NULL,1,7,NULL,0,1,595),(165,'Нов_1',NULL,'<p>6 ноября мы запустили в продажу часть бутылок с водой 19 л \"Краснозатонская Серебряная\" с новыми этикетками. <br>Постепенно все старые этикетки будут заменены новыми, в том числе и на маленьких бутылках в ПЭТ-таре, которые вы сможете приобрести в любом магазине.<br>этикетка 4,8х15см</p>\r\n<p>С новой этикеткой наш продукт выглядит более современным и ярким. А Вы легко отличите нашу питьевую воду высшей категории от других вод, большинство из которых более низкого качества - воды питьевые первой категории или воды питьевые столовые.</p>\r\n<p>Изменилась только этикетка, а в бутылках по-прежнему Ваша любимая вкусная и полезная вода «Краснозатонская Серебряная» производства Компании «Эколайн».</p>',1,133,43,NULL,'nov_1',NULL,NULL,NULL,2,7,NULL,0,1,602),(166,'Нов_2',NULL,'',1,133,44,NULL,'nov_2',NULL,NULL,NULL,1,7,NULL,0,1,NULL),(167,'Первый раз вода в подарок','Первый раз вода в подарок','',1,132,45,NULL,'Voda_v_podarok',NULL,NULL,NULL,1,7,NULL,0,1,620),(168,'Приведи друга','Приведи друга','',1,132,46,NULL,'privedi_druga',NULL,NULL,NULL,2,7,NULL,0,1,622),(169,'Помпа',NULL,'',1,117,47,NULL,'pompa',NULL,NULL,NULL,2,7,NULL,0,1,608),(170,'Кулеры',NULL,'',1,117,48,NULL,'kulery',NULL,NULL,NULL,1,7,NULL,0,1,610),(171,'Санитарная обработка',NULL,'',1,117,49,NULL,'sanitarnaya_obrabotka',NULL,NULL,NULL,1,7,NULL,0,1,612),(172,'Прочее оборудование',NULL,'',1,117,50,NULL,'prochee_oborudovanie',NULL,NULL,NULL,1,7,NULL,0,1,614),(173,'Школам и садам',NULL,'',1,132,51,NULL,'shkolam_i_sadam',NULL,NULL,NULL,1,7,NULL,0,1,626),(174,'Скидка',NULL,'',1,132,52,NULL,'skidka',NULL,NULL,NULL,1,7,NULL,0,1,628),(175,'Две бутылки',NULL,'qwqweqweqweqwe',1,132,53,NULL,'dve_butylki',NULL,NULL,NULL,1,7,NULL,0,1,630);
/*!40000 ALTER TABLE `da_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `da_message`
--

DROP TABLE IF EXISTS `da_message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `da_message` (
  `id_message` int(8) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `text` longtext NOT NULL COMMENT 'Текст',
  `date` int(10) unsigned NOT NULL COMMENT 'Дата создания',
  `type` int(8) NOT NULL DEFAULT '1' COMMENT 'тип',
  `sender` varchar(255) NOT NULL,
  PRIMARY KEY (`id_message`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Уведомления';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `da_message`
--

LOCK TABLES `da_message` WRITE;
/*!40000 ALTER TABLE `da_message` DISABLE KEYS */;
/*!40000 ALTER TABLE `da_message` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `da_migration`
--

DROP TABLE IF EXISTS `da_migration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `da_migration` (
  `version` varchar(255) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `da_migration`
--

LOCK TABLES `da_migration` WRITE;
/*!40000 ALTER TABLE `da_migration` DISABLE KEYS */;
INSERT INTO `da_migration` VALUES ('m000000_000000_base',1342684221),('m120716_174729_macro',1342684221),('m120718_122532_macro',1342690162),('m120720_002139_ngin',1342729247),('m120720_223512_ngin',1343114947),('m120724_125848_ngin',1343130984),('m120724_210920_ngin',1343214415),('m120725_075905_add_field_to_system_parameters',1343204594),('m120727_000458_ngin',1344594601),('m120728_223710_ngin',1344594601),('m120731_013940_ngin',1344594601),('m120731_121338_ngin',1344594601),('m120731_211556_ngin',1344594601),('m120802_162026_ngin',1344594601),('m120804_002349_ngin',1344594601),('m120806_103133_mail_add_html_format_type',1344594601),('m120807_065040_drop_field_is_send_from_feedback',1344594601),('m120807_083424_drop_field_send_status_from_faq',1344594601),('m120807_114536_make_autoincrement_pk_for_da_event',1344594601),('m120807_131907_ngin',1344594601),('m120807_164940_ngin',1344594601),('m120808_152726_ngin',1344594601),('m120809_134126_ngin',1344836799),('m120810_102553_update_shop_offer_event_type',1344594601),('m120814_173220_ngin',1345536580),('m120814_230010_ngin',1345536580),('m120815_005422_ngin',1345536581),('m120820_134546_shop_module',1345529983),('m120820_134744_shop_module',1345529983),('m120820_135212_shop_module',1345529983),('m120820_135324_shop_module',1345529984),('m120820_174205_ngin',1345536581),('m120820_182032_ngin',1345536581),('m120821_221310_ngin',1346662247),('m120822_170237_ngin',1346662247),('m120828_103629_ngin',1347362840),('m120828_164130_ngin',1347362840),('m120828_174955_ngin',1347362841),('m120904_121407_shop_module',1347362841),('m120904_150721_ngin',1347362841),('m120913_171217_ngin',1347626896),('m120914_121927_ngin',1347626896),('m120915_223619_ngin',1347808318),('m120918_081514_shop_module',1348481234),('m120919_073944_shop_module',1348481234),('m121002_083734_shop_module',1349179578),('m121004_055741_ngin',1349360888),('m121004_080450_change_tables_engine_to_innodb',1349360910),('m121004_114202_ngin',1349360912),('m121004_130202_ngin',1349360912),('m121005_071430_menu_module',1349449650),('m121009_071450_scheduler_module',1349791566),('m121009_071615_scheduler_module',1349791567),('m121009_071744_scheduler_module',1349791567),('m121009_100803_mail_module',1349791567),('m121009_132010_struct_ngin',1349791569),('m121009_132011_ngin',1349791569),('m121009_153554_struct_ngin',1349791570),('m121009_153555_ngin',1349791570),('m121009_160202_ngin',1349791570),('m121010_020603_ngin',1349863743),('m121010_133117_ngin',1350371052),('m121015_123829_user_module',1351367964),('m121015_163208_ngin',1350371056),('m121016_131442_struct_ngin',1351367966),('m121016_131443_ngin',1351367967),('m121016_134502_struct_ngin',1351367967),('m121018_062400_photogallery_module',1351367967),('m121018_062954_photogallery_module',1351367968),('m121023_130918_struct_ngin',1351367974),('m121023_130919_ngin',1351367974),('m121025_130718_ngin',1351367974),('m121029_114119_ngin',1351501041),('m121030_105729_ngin',1352115207),('m121031_193910_ngin',1352115209),('m121101_150836_ngin',1352115210),('m121102_141700_news_module',1352543161),('m121107_145242_ngin',1352543162),('m121108_095825_ngin',1352543162),('m121109_095000_messenger_module',1352543162),('m121109_095521_messenger_module',1352543162),('m121109_100548_messenger_module',1352543163),('m121109_131529_messenger_module',1352543163),('m121115_194907_ngin',1353672671),('m121210_153052_struct_ngin',1355989242),('m121210_153053_ngin',1355989243),('m121227_071724_ngin',1359392575),('m130110_221227_ngin',1359392575),('m130116_075255_messanger_module',1359392576),('m130206_161859_ngin',1363760020),('m130212_205359_add_comments_to_tables',1363760046),('m130220_165514_ngin',1363760046),('m130222_211623_ngin',1363760046),('m130304_105600_ngin',1363760046),('m130312_094709_struct_ngin',1363760047),('m130312_094710_ngin',1363760048),('m130313_163310_struct_ngin',1363760048),('m130313_163311_ngin',1363760048),('m130313_172038_struct_ngin',1363760049),('m130313_172039_ngin',1363760050),('m130328_151605_struct_ngin',1365597562),('m130328_151606_ngin',1365597563),('m130404_131839_ngin',1365597563),('m130405_132019_filename_translit',1365597563),('m130409_094600_project_bootstrap',1365597563),('m130419_131440_user_module',1369160489),('m130422_065535_new_users',1369160489),('m130423_121920_struct_ngin',1369160491),('m130423_121921_ngin',1369160491),('m130423_122356_new_users_password',1369160491),('m130514_104329_comments_module',1369160491),('m130515_142007_ngin',1369160491),('m130516_140735_struct_ngin',1369160494),('m130516_140736_ngin',1369160494),('m130516_190627_menu_visible_view_column',1369160494),('m130517_230538_struct_ngin',1369160495),('m130517_230539_ngin',1369160495),('m130520_123302_struct_ngin',1369160496),('m130520_170216_ngin',1369160496),('m130521_224420_ngin',1369639016),('m130522_122615_ngin',1369639016),('m130522_171926_ngin',1369639016),('m130522_175400_backend',1369681065),('m130522_185724_ngin',1369681066),('m130523_112000_ygin',1369987438),('m130523_133419_ygin',1369681069),('m130524_013011_struct_ygin',1369681072),('m130524_013012_ygin',1369681072),('m130528_204220_struct_ygin',1371535225),('m130528_204221_ygin',1371535226),('m130606_160848_struct_ygin',1371535226),('m130606_160849_ygin',1371535226),('m130607_155511_ygin',1371535226),('m130607_171901_struct_ygin',1371535228),('m130607_171902_ygin',1371535228),('m130610_225445_struct_ygin',1371535228),('m130610_225446_ygin',1371535229),('m130611_164844_ygin',1371535229),('m130611_204701_struct_ygin',1371535229),('m130611_204702_ygin',1371535230),('m130611_205034_struct_ygin',1371535230),('m130611_205035_ygin',1371535230),('m130611_205234_struct_ygin',1371535230),('m130614_131857_ygin',1371535230),('m130614_152109_ygin',1371542921),('m130618_115456_ygin',1371542921),('m130618_155228_ygin',1372422741),('m130620_143444_struct_ygin',1381610179),('m130620_143445_ygin',1381610180),('m130621_130836_ygin',1381610180),('m130622_115016_ygin',1381610181),('m130626_100540_faq_categories_mail',1389644653),('m130708_174249_ygin',1389644653),('m130710_114016_struct_ygin',1389644654),('m130710_114017_ygin',1389644655),('m130710_142133_struct_ygin',1389644655),('m130711_112950_struct_ygin',1389644655),('m130711_112951_ygin',1389644655),('m130714_230340_ygin',1389644655),('m130716_120239_ygin',1389644656),('m130717_113251_struct_ygin',1389644656),('m130717_113252_ygin',1389644656),('m130718_213659_struct_ygin',1389644659),('m130723_095944_ygin',1389644659),('m130724_100320_ygin',1389644659),('m130820_064119_comments_column',1389644659),('m130826_135610_struct_ygin',1389644660),('m130826_135611_ygin',1389644660),('m130909_174245_ygin',1389644660),('m130916_162832_ygin',1389644660),('m130920_163425_struct_ygin',1389644661),('m130920_163426_ygin',1389644661),('m130924_120617_ygin',1389644661),('m131001_131235_struct_ygin',1389644661),('m131001_131236_ygin',1389644661),('m131002_193452_banner_module_aggregate_job',1389644661),('m131021_144947_ygin',1389644661),('m140108_191559_ygin',1389644661),('m140109_163100_backend_config',1389644661),('m140208_112056_ygin',1396943120),('m140224_111615_struct_ygin',1408175128),('m140224_111616_ygin',1408175128),('m140305_055619_ygin_banner_id',1408175129),('m140421_125605_ygin',1408175133),('m140430_115900_ygin',1408175133),('m140605_112159_ygin',1408175133),('m240919_073944_shop_plugin',1348572502);
/*!40000 ALTER TABLE `da_migration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `da_object`
--

DROP TABLE IF EXISTS `da_object`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `da_object` (
  `id_object` varchar(255) NOT NULL COMMENT 'id',
  `name` varchar(255) NOT NULL COMMENT 'Имя объекта',
  `id_field_order` varchar(255) DEFAULT NULL COMMENT 'Свойство для порядка',
  `order_type` int(2) NOT NULL DEFAULT '1' COMMENT 'Тип порядка',
  `table_name` varchar(255) DEFAULT NULL COMMENT 'Таблица / Обработчик / Объект',
  `id_field_caption` varchar(255) DEFAULT NULL COMMENT 'Свойство для отображения',
  `object_type` int(2) NOT NULL DEFAULT '1' COMMENT 'Тип объекта',
  `folder_name` varchar(255) DEFAULT NULL COMMENT 'Путь к документам',
  `parent_object` varchar(255) DEFAULT NULL COMMENT 'Родитель',
  `sequence` int(3) NOT NULL DEFAULT '1' COMMENT '&nbsp;',
  `use_domain_isolation` int(1) NOT NULL DEFAULT '0' COMMENT 'Использовать доменную изоляцию данных',
  `field_caption` varchar(255) DEFAULT NULL COMMENT 'Свойство модели для отображения',
  `yii_model` varchar(255) DEFAULT NULL COMMENT 'yii-модель',
  PRIMARY KEY (`id_object`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Объекты';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `da_object`
--

LOCK TABLES `da_object` WRITE;
/*!40000 ALTER TABLE `da_object` DISABLE KEYS */;
INSERT INTO `da_object` VALUES ('1','Разработка','',1,NULL,'',1,NULL,NULL,3,0,NULL,NULL),('101','Наборы виджетов','16',2,'da_site_module_template','13',1,NULL,'102',7,0,'name','SiteModuleTemplate'),('102','Модули',NULL,1,NULL,NULL,1,NULL,'4',13,0,NULL,NULL),('103','Виджеты сайта','542',1,'da_site_module','542',1,'content/modules','102',8,0,'name','SiteModule'),('105','Голосование','536',2,'pr_voting','531',1,'content/vote/','4',7,0,'name','vote.models.Voting'),('106','Ответы на голосование',NULL,2,'pr_voting_answer',NULL,1,NULL,'105',9,0,NULL,NULL),('2','Пользователи и группы',NULL,1,NULL,NULL,1,NULL,NULL,4,0,NULL,NULL),('20','Объекты','104',1,'da_object','64',1,NULL,'1',1,0,'name','ygin.models.object.DaObject'),('21','Свойства объекта','77',1,'da_object_parameters','79',1,'','20',2,0,'caption','ygin.models.object.ObjectParameter'),('22','Типы данных','105',1,'da_object_parameter_type','86',1,NULL,'1',16,0,'name',NULL),('24','Пользователи',NULL,1,'da_users','90',1,'content/user','2',3,0,'name','user.models.User'),('25','Типы прав доступа',NULL,1,'da_permission_type','97',1,NULL,'2',3,0,'name',NULL),('250','Комментарии','1206',2,'pr_comment','1207',1,NULL,'4',8,0,'comment_text','comments.models.CommentYii'),('260','Баннеры','1310',1,'pr_banner',NULL,1,'content/bnimg','261',18,0,NULL,'banners.models.Banner'),('261','Баннерные места','1318',1,'pr_banner_place','1312',1,NULL,'4',12,0,'title','banners.models.BannerPlace'),('27','Справочники','109',1,'da_references','110',1,NULL,'3',4,0,'name','ygin.models.Reference'),('28','Значения справочника','112',1,'da_reference_element','114',1,NULL,'27',3,0,'value','ygin.models.ReferenceElement'),('29','Группы системных параметров','48',2,'da_group_system_parameter','49',1,NULL,'3',5,0,'name',NULL),('3','Общие настройки',NULL,1,NULL,NULL,1,NULL,NULL,5,0,NULL,NULL),('30','Настройки сайта','117',1,'da_system_parameter',NULL,1,NULL,'3',3,0,NULL,'ygin.models.SystemParameter'),('31','Домены сайта','281',1,'da_domain','283',1,'content/domain/','3',2,0,'name','ygin.models.Domain'),('313','Настройка кэширования','',1,'engine/admin/special/cacheSettings.php','',3,NULL,'1',14,0,NULL,NULL),('33','Формат сообщения','250',1,'da_event_format','31',1,NULL,'6',3,0,'description',NULL),('34','Подписчики на события',NULL,2,'da_event_subscriber','37',1,NULL,'6',3,0,'id_event_type','ygin.modules.mail.models.NotifierEventSubscriber'),('35','Тип события','50',2,'da_event_type','51',1,NULL,'6',3,0,'name','ygin.modules.mail.models.NotifierEventType'),('37','Файлы','134',1,'da_files','135',1,NULL,'3',7,0,'file_path','ygin.models.File'),('39','Типы файлов','129',1,'da_file_type','130',1,NULL,'37',11,0,'name',NULL),('4','Контент',NULL,1,NULL,NULL,1,NULL,NULL,1,0,NULL,NULL),('40','Расширения файлов','131',1,'da_file_extension','132',1,NULL,'37',12,0,'ext',NULL),('41','Пользовательские свойства',NULL,1,'da_object_property',NULL,1,NULL,'1',15,0,NULL,NULL),('43','SQL','',1,'backend/special/sql','',3,NULL,'1',9,0,NULL,NULL),('49','События','253',2,'da_event',NULL,1,NULL,'6',2,0,NULL,NULL),('50','Почтовые аккаунты','262',1,'da_mail_account','262',1,NULL,'6',2,0,'host',NULL),('500','Фотогалереи','1500',1,'pr_photogallery','1501',1,'content/photogallery','4',3,0,'name','photogallery.models.Photogallery'),('501','Фотографии','1511',1,'pr_photogallery_photo','1507',1,'content/photos','500',14,0,'id_photogallery_photo','photogallery.models.PhotogalleryPhoto'),('502','Новости','1516',2,'pr_news',NULL,1,'content/news','4',2,0,NULL,'ygin.modules.news.models.News'),('503','Категории новостей','1523',1,'pr_news_category','1522',1,NULL,'502',3,0,'name',NULL),('504','Консультации',NULL,1,NULL,NULL,1,NULL,'4',5,0,NULL,NULL),('505','Вопрос','1530',2,'pr_consultation_ask','1528',1,'content/consultation_ask','504',1,0,'ask',NULL),('506','Ответ','1536',2,'pr_consultation_answer','1538',1,NULL,'504',2,0,'answer',NULL),('507','Отвечающий','1547',1,'pr_consultation_answerer','1547',1,'content/answerer','504',3,0,'name',NULL),('508','Специализация отвечающего','1551',1,'pr_consultation_specialization','1551',1,NULL,'504',4,0,'specialization',NULL),('509','Категории продукции','1636',1,'pr_product_category','1553',1,'content/product_category','518',2,0,'name','shop.models.ProductCategory'),('51','Планировщик','267',1,'da_job','274',1,NULL,'1',4,0,'name','ygin.modules.scheduler.models.Job'),('511','Продукция','1564',1,'pr_product','1567',1,'content/product','518',1,0,'name','shop.models.Product'),('512','Вопрос-ответ','1579',2,'pr_question','1581',1,'content/question','4',6,0,'question',NULL),('517','Обратная связь','1609',2,'pr_feedback','1614',1,NULL,'4',4,0,'date',NULL),('518','Магазин',NULL,1,NULL,NULL,1,NULL,NULL,2,0,NULL,NULL),('519','Заказы пользователей','1626',2,'pr_offer','1621',1,NULL,'518',40,0,'fio',NULL),('520','Витрина','1635',1,'pr_vitrine','1632',1,'content/vitrine','4',9,0,'title',NULL),('521','Викторины','1638',2,'pr_quiz','1639',1,'content/quiz','4',10,0,'name',NULL),('522','Вопросы викторины','1648',1,'pr_quiz_question','1646',1,'content/quiz_quest','521',43,0,'question',NULL),('523','Варианты ответов','1653',1,'pr_quiz_answer','1651',1,NULL,'521',44,0,'answer',NULL),('524','Ответ пользователя','1661',2,'pr_quiz_answer_user','1656',1,NULL,'521',45,0,'name',NULL),('525','Брэнды','1670',1,'pr_product_brand','1667',1,'content/product_brand','518',46,0,'name',NULL),('527','Плагины системы','',1,'da_plugin','',1,'content/plugin','3',1,0,NULL,NULL),('528','Настройка плагинов',NULL,1,'backend/plugin/plugin',NULL,3,NULL,'527',48,0,NULL,NULL),('529','Статусы остатка продукции','1679',1,'pr_remain_status','1680',1,NULL,'518',49,0,NULL,NULL),('530','Отзывы клиентов','1686',2,'pr_client_review','1685',1,NULL,'4',11,0,'name',NULL),('531','Уведомления','1695',2,'da_message','1694',1,'content/messages','3',6,0,'text','ygin.modules.messenger.models.Message'),('54','Доступные локализации','310',1,'da_localization','311',1,NULL,'3',8,0,'name',NULL),('6','Почта',NULL,1,NULL,NULL,1,NULL,NULL,7,0,NULL,NULL),('60','Проверка файлов','',1,'backend/special/clearPreview','',3,NULL,'1',10,0,NULL,NULL),('61','Инструкции','145',1,'da_instruction','141',1,NULL,'1',11,0,'name',NULL),('63','Представление','408',1,'da_object_view','406',1,'','1',3,0,'name','backend.models.DaObjectView'),('66','Колонка представления','419',1,'da_object_view_column',NULL,1,'','63',9,0,NULL,'backend.models.DaObjectViewColumn'),('80','php-скрипты','337',1,'da_php_script_type','342',1,NULL,'1',2,0,'description','ygin.models.PhpScript'),('86','Интерфейс php-скрипта','351',1,'da_php_script_interface','348',1,NULL,'80',4,0,'name',NULL),('89','Сброс кэша','',1,'backend/special/clearCache','',3,NULL,'1',7,0,NULL,NULL),('91','Поисковый индекс','',1,'backend/special/recreateSearchIndex','',3,NULL,'1',8,0,NULL,NULL),('94','Логи','',1,'backend/special/logView','',3,NULL,'1',12,0,NULL,NULL),('project-kolichestvo-vody','Количество воды','project-kolichestvo-vody-id-water-count',2,'pr_water_count','project-kolichestvo-vody-id-water-count',1,'content/water/count','project-zakaz',54,0,'id_water','application.modules.order.models.WaterCount'),('project-vid-vody','Вид воды','project-vid-vody-sequence',1,'pr_water','project-vid-vody-nazvanie',1,'content/water/','4',52,0,'title','application.modules.order.models.Water'),('project-zakaz','Заказ','project-zakaz-date',2,'pr_order','project-zakaz-fio',1,'content/order','4',53,0,'fio','application.modules.order.models.Order'),('ygin-gii','gii (debug=true)','',1,'/gii/','',5,NULL,'1',6,0,NULL,NULL),('ygin-invoice','Счета','ygin-invoice-create-date',2,'pr_invoice','ygin-invoice-create-date',1,NULL,'518',51,0,'create_date','Invoice'),('ygin-menu','Меню','7',1,'da_menu','7',1,'content/menu','4',1,0,'name','Menu'),('ygin-override','Переопределение представлений (debug=true)','',1,'/override/','',5,NULL,'1',5,0,NULL,NULL),('ygin-views-generator','Генерация вьюхи','',1,'viewGenerator/default/index','',3,NULL,'1',50,0,NULL,NULL);
/*!40000 ALTER TABLE `da_object` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `da_object_instance`
--

DROP TABLE IF EXISTS `da_object_instance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `da_object_instance` (
  `id_object` varchar(255) NOT NULL,
  `id_instance` int(8) NOT NULL,
  `id_domain` int(8) NOT NULL,
  `create_date` int(10) DEFAULT NULL,
  `last_modify_date` int(10) DEFAULT NULL,
  `delete_date` int(10) DEFAULT NULL,
  `id_user_creator` int(8) DEFAULT NULL,
  PRIMARY KEY (`id_object`,`id_instance`),
  KEY `id_object` (`id_object`,`id_instance`,`id_domain`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `da_object_instance`
--

LOCK TABLES `da_object_instance` WRITE;
/*!40000 ALTER TABLE `da_object_instance` DISABLE KEYS */;
/*!40000 ALTER TABLE `da_object_instance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `da_object_parameter_type`
--

DROP TABLE IF EXISTS `da_object_parameter_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `da_object_parameter_type` (
  `id_parameter_type` int(8) NOT NULL COMMENT 'ИД',
  `sequence` int(3) NOT NULL DEFAULT '1' COMMENT 'Порядок',
  `name` varchar(100) NOT NULL COMMENT 'Название типа данных',
  `sql_type` varchar(255) DEFAULT NULL COMMENT 'sql тип',
  PRIMARY KEY (`id_parameter_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Типы данных';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `da_object_parameter_type`
--

LOCK TABLES `da_object_parameter_type` WRITE;
/*!40000 ALTER TABLE `da_object_parameter_type` DISABLE KEYS */;
INSERT INTO `da_object_parameter_type` VALUES (1,2,'Число','INT(8)'),(2,3,'Строка','VARCHAR(255)'),(3,7,'HTML-редактор','LONGTEXT'),(4,4,'Дата','INT(10) UNSIGNED'),(6,10,'Справочник','INT(8)'),(7,11,'Внешний ключ (Объект)','INT(8)'),(8,8,'Файл','INT(8)'),(9,5,'Логический (bool)','TINYINT(1)'),(10,12,'Абстрактный',NULL),(11,1,'Первичный ключ','INT(8)'),(12,14,'Родительский ключ','INT(8)'),(13,17,'Последовательность','INT(8)'),(14,6,'Текст (text area)','LONGTEXT'),(15,9,'Список файлов',NULL),(17,16,'Скрытое поле','VARCHAR(255)');
/*!40000 ALTER TABLE `da_object_parameter_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `da_object_parameters`
--

DROP TABLE IF EXISTS `da_object_parameters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `da_object_parameters` (
  `id_object` varchar(255) NOT NULL DEFAULT '0' COMMENT 'Объект',
  `id_parameter` varchar(255) NOT NULL COMMENT 'ИД',
  `id_parameter_type` int(2) NOT NULL DEFAULT '0' COMMENT 'Тип свойства',
  `sequence` int(3) NOT NULL DEFAULT '1' COMMENT '&nbsp;',
  `widget` varchar(255) DEFAULT NULL COMMENT 'Виджет',
  `caption` varchar(255) NOT NULL COMMENT 'Название',
  `field_name` varchar(255) DEFAULT NULL COMMENT 'Имя поля в БД',
  `add_parameter` varchar(255) DEFAULT NULL COMMENT 'Параметр',
  `default_value` varchar(255) DEFAULT NULL COMMENT 'Значение по умолчанию',
  `not_null` tinyint(1) DEFAULT '1' COMMENT 'Обязательное',
  `sql_parameter` varchar(255) DEFAULT NULL COMMENT 'SQL условие',
  `is_unique` tinyint(1) DEFAULT NULL COMMENT 'Уникальное',
  `group_type` int(8) DEFAULT NULL COMMENT 'Связь с главным объектом',
  `need_locale` int(1) NOT NULL DEFAULT '0' COMMENT 'Переводить на другие языки',
  `search` int(1) NOT NULL DEFAULT '0' COMMENT 'Доступно для поиска',
  `is_additional` int(1) NOT NULL DEFAULT '0' COMMENT 'Дополнительное',
  `hint` text COMMENT 'Всплывающая подсказка',
  `visible` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_object`,`id_parameter`),
  UNIQUE KEY `id_parameter` (`id_parameter`),
  KEY `id_object` (`id_object`),
  KEY `id_object_2` (`id_object`,`sequence`),
  KEY `id_object_3` (`id_object`,`is_additional`,`sequence`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Свойства объекта';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `da_object_parameters`
--

LOCK TABLES `da_object_parameters` WRITE;
/*!40000 ALTER TABLE `da_object_parameters` DISABLE KEYS */;
INSERT INTO `da_object_parameters` VALUES ('101','12',11,8,NULL,'id','id_module_template',NULL,NULL,1,NULL,0,NULL,0,0,0,NULL,1),('101','13',2,8,NULL,'Название набора','name','0',NULL,1,NULL,0,0,0,0,0,'',1),('101','14',10,10,'menu.backend.widgets.manageModule.ManageModuleWidget','Виджеты',NULL,'2',NULL,0,'SiteModuleTemplateListOfModule',0,0,0,0,0,'',1),('101','16',9,8,NULL,'Использовать по умолчанию','is_default_template','0','0',1,NULL,0,0,0,0,0,'Набор виджетов будет применяться к новым пунктам Меню корневого уровня',1),('101','ygin-widget-list-sequence',13,291,NULL,'п/п','sequence',NULL,NULL,0,NULL,0,0,0,0,0,'',1),('103','541',11,1,NULL,'id','id_module',NULL,NULL,1,NULL,0,NULL,0,0,0,NULL,1),('103','542',2,2,NULL,'Имя','name',NULL,NULL,1,NULL,0,NULL,1,0,0,NULL,1),('103','544',10,4,'menu.backend.widgets.phpScript.PhpScriptWidget','Обработчик','id_php_script','2',NULL,0,NULL,0,0,0,0,0,'',1),('103','545',14,6,NULL,'Простой текст','content',NULL,NULL,0,NULL,0,NULL,1,0,0,'Используется для хранения JavaScript и текст без форматирования',1),('103','546',9,3,NULL,'Видимость','is_visible',NULL,'1',0,NULL,0,0,0,0,0,'Позволяет временно отключать модуль из всех наборов модулей',1),('103','554',3,8,NULL,'Форматированный текст','html',NULL,NULL,0,NULL,0,0,1,0,0,'Используется, если необходимо хранить форматированный текст, ссылки, файлы',1),('103','556',15,7,NULL,'Загрузить файлы',NULL,NULL,NULL,0,NULL,0,0,0,0,0,NULL,1),('105','530',11,7,NULL,'ID','id_voting',NULL,NULL,1,NULL,0,NULL,0,0,0,NULL,1),('105','531',2,7,NULL,'Название голосования','name',NULL,NULL,1,NULL,0,NULL,0,0,0,NULL,1),('105','532',10,7,'vote.backend.widgets.answerList.AnswerListWidget','Варианты ответов',NULL,'2',NULL,0,'VoteAnswerVisualElement',0,0,0,0,0,'',1),('105','533',9,7,NULL,'Множество ответов','is_checkbox',NULL,NULL,1,NULL,0,NULL,0,0,0,NULL,1),('105','534',9,7,NULL,'Активное','is_active',NULL,'1',1,NULL,0,NULL,0,0,0,NULL,1),('105','535',9,7,NULL,'Показать в модуле','in_module',NULL,'1',1,NULL,0,NULL,0,0,0,NULL,1),('105','536',4,7,NULL,'Дата создания голосования','create_date',NULL,NULL,1,NULL,0,NULL,0,0,0,NULL,1),('106','537',11,7,NULL,'id','id_voting_answer',NULL,NULL,1,NULL,0,NULL,0,0,0,NULL,1),('106','538',7,7,NULL,'Голосование','id_voting','105',NULL,1,NULL,0,1,0,0,0,NULL,1),('106','539',2,7,NULL,'Вариант ответа','name',NULL,NULL,1,NULL,0,NULL,0,0,0,NULL,1),('106','540',1,10,NULL,'Количество голосов','count',NULL,'0',1,NULL,0,0,0,0,0,NULL,1),('20','104',13,2,NULL,'&nbsp;','sequence',NULL,NULL,1,NULL,0,0,0,0,0,NULL,1),('20','124',9,13,NULL,'Использовать доменную изоляцию данных','use_domain_isolation',NULL,'0',0,NULL,0,0,0,0,1,NULL,1),('20','151',10,19,'backend.backend.object.objectPermission.ObjectPermissionWidget','Права доступа',NULL,'2','',0,'ObjectPermissionWidget',0,0,0,0,0,'',1),('20','155',2,7,NULL,'Путь к документам','folder_name','0',NULL,0,NULL,0,0,0,0,0,'Путь к папке на сервере относительно корня сайта для хранения загружаемых файлов. Например, \"content/news\" для сохранения загрузки фотографий к новостям.',1),('20','157',2,16,NULL,'yii-модель','yii_model','0',NULL,0,NULL,0,0,0,0,0,'Например: ygin.models.File или просто Domain, если есть точная уверенность, что система уже знает о модели',1),('20','207',2,8,NULL,'Свойство модели для отображения','field_caption',NULL,NULL,0,NULL,0,0,0,0,0,'Используется для отображения названия экземпляра в списках и других местах.\r\nНапример, для отображения имени раздела можно указать name\r\nВ этом случае будет выполнено такое выражение: $model->name;\r\nТ.о. можно указывать любые доступные атрибуты объекта модели.',1),('20','62',10,15,'backend.backend.object.objectManageView.ObjectManageViewWidget','Создать представление',NULL,'2','',0,'ObjectManageViewWidget',0,0,0,0,1,'',1),('20','63',11,1,'backend.backend.objectParameter.autoPrimaryKey.AutoPrimaryKeyWidget','id','id_object','1',NULL,1,'name',0,0,0,0,0,'Уникальный ИД объекта в виде строки. Например для новостей ИД будет равен ygin-news  Префикс ygin зарезервирован для системных объектов, вместо него необходимо использовать название проекта или компании.',1),('20','64',2,4,NULL,'Имя объекта','name',NULL,NULL,1,NULL,0,NULL,1,0,0,NULL,1),('20','65',7,10,NULL,'Свойство для порядка','id_field_order','21','',0,'t.id_object=:id_instance',0,0,0,0,0,'По умолчанию выводимые в системе администрирования экземпляры данного объекта будут упорядочены по этому свойству',1),('20','66',6,11,NULL,'Тип порядка','order_type','30',NULL,1,NULL,0,NULL,0,0,0,NULL,1),('20','67',2,6,NULL,'Таблица / Контроллер / Ссылка','table_name',NULL,NULL,0,NULL,0,0,0,0,0,'В зависимости от типа объекта, поле хранит различные значения. При стандартном - имя таблицы, при контроллере - алиас класса',1),('20','68',7,9,NULL,'Свойство для отображения','id_field_caption','21','',0,'t.id_object=:id_instance',0,0,0,0,0,'Значение данного свойства будет выводится в списке, когда экземпляр данного объекта будет выбираться в качестве свойства другого объекта',1),('20','69',6,5,NULL,'Тип объекта','object_type','31',NULL,1,NULL,0,NULL,0,0,0,NULL,1),('20','70',12,3,NULL,'Родитель','parent_object','1',NULL,0,NULL,0,0,0,0,1,NULL,1),('21','156',9,14,NULL,'Уникальное','is_unique',NULL,NULL,0,NULL,0,0,0,0,0,'Слежение за уникальностью значения на уровне системы администрирования',1),('21','205',9,15,NULL,'Связь с главным объектом','group_type',NULL,'0',0,NULL,0,0,0,0,0,'У главного объекта в системном представлении появится возможность переходить к зависимым данным этого объекта, связанным по данному свойству',1),('21','206',9,12,NULL,'Переводить на другие языки','need_locale',NULL,'0',0,NULL,0,0,0,0,0,'Определяет возможность перевода значения свойства на другие языки (английский, немецкий). Перевод возможен только для параметров с типом данных Строка, TextArea, CLOB.',1),('21','220',9,13,NULL,'Доступно для поиска','search',NULL,'0',0,NULL,0,0,0,0,0,'Добавляет это поле в поисковый индекс, который используется при поиске по сайту',1),('21','73',9,16,NULL,'Дополнительное','is_additional',NULL,'0',0,NULL,0,0,0,0,0,'Данное поле будет вынесено в отдельный контейнер, который изначально скрыт при редактировании объекта и появляется при нажатии на кнопку \"Дополнительные свойства\"',1),('21','74',11,6,'backend.backend.objectParameter.autoPrimaryKey.AutoPrimaryKeyWidget','id','id_parameter','1',NULL,1,'field_name/caption; id_object',0,0,0,0,0,'Уникальный ИД свойства объекта в виде строки. Например для заголовка новостей ИД будет равен ygin-news-title  Префикс ygin зарезервирован для системных свойств, вместо него необходимо использовать название проекта или компании.',1),('21','75',7,3,NULL,'Объект','id_object','20',NULL,1,'',0,1,0,0,0,NULL,1),('21','76',7,1,'backend.backend.objectParameter.typeObjectParameterWidget.TypeObjectParameterWidget','Тип свойства','id_parameter_type','22','2',1,'',0,0,0,0,0,'',1),('21','77',13,4,NULL,'&nbsp;','sequence',NULL,'1',1,NULL,0,NULL,0,0,0,NULL,1),('21','78',2,7,NULL,'Виджет','widget','0','',0,'',0,0,0,0,0,'Алиас до виджета. Например ygin.widgets.textField.TextFieldWidget',1),('21','79',2,2,NULL,'Название','caption',NULL,NULL,1,NULL,0,0,1,0,0,'Краткое название свойства на русском языке. Используется в заголовке столбца при отображении списка экземпляров объекта.',1),('21','80',2,5,NULL,'Имя поля в БД','field_name','0','',0,'',0,0,0,0,0,'',1),('21','81',2,8,NULL,'Параметр','add_parameter','0',NULL,0,NULL,0,0,0,0,0,'Поле хранит значения, применяемые в различных ситуациях разработчиками',1),('21','82',14,17,NULL,'Всплывающая подсказка','hint',NULL,NULL,0,NULL,0,0,1,0,0,'Если заполнено, рядом с полем будет отображаться вопросик, при наведении на который всплывает подсказка',1),('21','83',2,10,NULL,'Значение по умолчанию','default_value',NULL,NULL,0,NULL,0,NULL,0,0,0,NULL,1),('21','84',9,11,NULL,'Обязательное','not_null',NULL,'1',0,NULL,0,NULL,0,0,0,NULL,1),('21','87',2,9,NULL,'SQL условие','sql_parameter',NULL,NULL,0,NULL,0,0,0,0,0,'Дополнительное условие для выборки нужных значений. Обращение к таблице текущего объекта следует использовать синоним <<current_instance>>.',1),('21','ygin-object-parameter-visible',9,288,NULL,'Видимость','visible',NULL,'1',1,NULL,0,0,0,0,0,'Доступно ли свойство при редактировании экземпляра объекта',1),('22','105',13,117,NULL,'Порядок','sequence',NULL,'1',1,NULL,0,0,0,0,0,NULL,1),('22','85',11,8,NULL,'ИД','id_parameter_type',NULL,NULL,1,NULL,0,NULL,0,0,0,NULL,1),('22','86',2,9,NULL,'Название типа данных','name',NULL,NULL,1,NULL,0,NULL,0,0,0,NULL,1),('22','ygin-objectParameterType-sqlType',2,287,NULL,'sql тип','sql_type','0',NULL,0,NULL,0,0,0,0,0,'',1),('24','122',9,11,NULL,'Активен','active',NULL,'1',0,NULL,0,0,0,0,0,'Есть ли возможность пройти авторизацию и пользоваться сервисами сайта',1),('24','123',17,18,NULL,'count_post','count_post',NULL,'0',0,NULL,0,0,0,0,1,'',1),('24','1697',9,13,NULL,'Необходимо сменить пароль','requires_new_password',NULL,NULL,0,NULL,0,0,0,0,1,NULL,1),('24','238',2,17,NULL,'Регистрационный ИД','rid',NULL,NULL,0,NULL,0,0,0,0,1,'Токен в ссылке, которая приходит пользователю на почту для подтверждения регистрации. Пока это поле заполнено,пользователь не сможет авторизоваться.',1),('24','239',4,16,NULL,'Дата регистрации пользователя','create_date','0',NULL,1,NULL,0,0,0,0,1,'',1),('24','24-address',2,7,NULL,'Адрес','address',NULL,NULL,0,NULL,0,0,0,0,0,'',1),('24','24-company',2,10,NULL,'Компания','company',NULL,NULL,0,NULL,0,0,0,0,0,'',1),('24','24-convenient-time',2,8,NULL,'Предпочитаемое время доставки','convenient_time',NULL,NULL,0,NULL,0,0,0,0,0,'',1),('24','24-personal-data-agreement',2,12,NULL,'Обработка персональных данных','personal_data_agreement',NULL,NULL,0,NULL,0,0,0,0,0,'',1),('24','24-phone',2,6,NULL,'Телефон','phone',NULL,NULL,0,NULL,0,0,0,0,0,'',1),('24','24-subscriber-number',2,2,NULL,'Абонентский номер','subscriber_number',NULL,NULL,0,NULL,0,0,0,0,0,'',1),('24','24-yur',9,9,NULL,'Юр. лицо','yur',NULL,'0',1,NULL,0,0,0,0,0,'',1),('24','88',11,1,NULL,'id','id_user',NULL,NULL,1,NULL,0,NULL,0,0,0,NULL,1),('24','89',10,14,'user.backend.widgets.roles.RolesWidget','Роли пользователя',NULL,'2',NULL,0,NULL,0,0,0,0,1,'',1),('24','90',2,3,NULL,'Логин','name',NULL,NULL,1,NULL,1,NULL,0,0,0,NULL,1),('24','91',2,15,'user.backend.widgets.userPassword.UserPasswordWidget','Пароль','user_password','2',NULL,0,NULL,0,0,0,0,1,'',1),('24','92',2,4,NULL,'E-mail','mail',NULL,NULL,1,NULL,0,NULL,0,0,0,NULL,1),('24','93',2,5,NULL,'Имя пользователя','full_name',NULL,NULL,0,NULL,0,NULL,0,0,0,NULL,1),('25','96',11,8,NULL,'id','id_permission_type',NULL,NULL,1,NULL,0,NULL,0,0,0,NULL,1),('25','97',2,8,NULL,'name','name',NULL,NULL,1,NULL,0,NULL,0,0,0,NULL,1),('250','1201',11,1,NULL,'id','id_comment',NULL,NULL,1,NULL,0,0,0,0,0,NULL,1),('250','1202',7,2,NULL,'Объект','id_object','20',NULL,1,NULL,0,0,0,0,0,NULL,1),('250','1203',1,3,NULL,'Экземпляр','id_instance',NULL,NULL,1,NULL,0,0,0,0,0,NULL,1),('250','1204',2,4,NULL,'Автор','comment_name',NULL,NULL,0,NULL,0,0,0,0,0,NULL,1),('250','1205',7,5,NULL,'Пользователь','id_user','24',NULL,0,NULL,0,0,0,0,0,NULL,1),('250','1206',4,7,NULL,'Дата','comment_date',NULL,NULL,1,NULL,0,0,0,0,0,NULL,1),('250','1207',14,6,NULL,'Комментарий','comment_text',NULL,NULL,1,NULL,0,0,0,0,0,NULL,1),('250','1208',6,8,NULL,'Отмодерировано','moderation','ygin-comment-reference-status',NULL,1,NULL,0,0,0,0,0,'',1),('250','1209',2,9,NULL,'IP','ip',NULL,NULL,0,NULL,0,0,0,0,0,NULL,1),('250','1210',2,7,NULL,'Тема','comment_theme',NULL,NULL,0,NULL,0,0,0,0,0,NULL,1),('250','1211',12,11,NULL,'id_parent','id_parent',NULL,NULL,0,NULL,0,0,0,0,0,NULL,1),('250','1588',2,195,NULL,'Токен','token',NULL,NULL,1,NULL,0,0,0,0,0,NULL,1),('260','1300',11,1,NULL,'ID','id_banner',NULL,NULL,1,NULL,0,0,0,0,0,NULL,1),('260','1302',2,3,NULL,'Уникальное название баннера (на английском языке)','unique_name',NULL,NULL,1,NULL,0,0,0,0,0,NULL,1),('260','1303',2,4,NULL,'Ссылка на сайт','link',NULL,'http://',0,NULL,0,0,0,0,0,NULL,1),('260','1304',2,6,NULL,'Текстовое описание','alt',NULL,NULL,0,NULL,0,0,0,0,0,NULL,1),('260','1305',8,7,NULL,'Файл','file',NULL,NULL,1,NULL,0,0,0,0,0,NULL,1),('260','1306',7,8,NULL,'Баннерное место','id_banner_place','261',NULL,1,NULL,0,1,0,0,0,NULL,1),('260','1307',10,9,'banners.backend.widgets.bannerStatistic.BannerStatisticWidget','Статистика',NULL,'2','',0,'BannerStatisticVisualElement',0,0,0,0,0,'',1),('260','1309',9,5,NULL,'Видимость','visible',NULL,'1',0,NULL,0,0,0,0,0,NULL,1),('260','1310',13,11,NULL,'Порядок','sequence',NULL,NULL,0,NULL,0,0,0,0,0,NULL,1),('261','1311',11,4,NULL,'ID','id_banner_place',NULL,NULL,1,NULL,0,0,0,0,0,NULL,1),('261','1312',2,1,NULL,'Название','title',NULL,NULL,1,NULL,0,0,0,0,0,NULL,1),('261','1313',6,5,NULL,'Тип показа баннеров','showing','50','3',1,NULL,0,0,0,0,0,NULL,1),('261','1314',1,6,NULL,'id_instance','id_instance',NULL,NULL,1,NULL,0,0,0,0,0,NULL,1),('261','1315',1,7,NULL,'id_object','id_object',NULL,NULL,1,NULL,0,0,0,0,0,NULL,1),('261','1317',12,9,NULL,'Родительский ключ','id_parent',NULL,NULL,0,NULL,0,0,0,0,0,NULL,1),('261','1318',13,10,NULL,'Порядок','sequence',NULL,NULL,0,NULL,0,0,0,0,0,NULL,1),('261','1320',1,3,NULL,'Высота','height',NULL,NULL,0,NULL,0,0,0,0,0,NULL,1),('261','1321',1,2,NULL,'Ширина','width',NULL,NULL,0,NULL,0,0,0,0,0,NULL,1),('27','109',11,8,'backend.backend.objectParameter.autoPrimaryKey.AutoPrimaryKeyWidget','id','id_reference','1',NULL,1,'name;-; reference',0,0,0,0,0,'Уникальный ИД справочника. Например ygin-shop-reference-orderStatus  Префикс ygin зарезервирован для системных справочников, вместо него необходимо использовать название проекта или компании.',1),('27','110',2,8,NULL,'Название справочника','name',NULL,NULL,1,NULL,0,NULL,0,0,0,NULL,1),('28','111',11,1,'backend.backend.objectParameter.autoPrimaryKey.AutoPrimaryKeyWidget','id','id_reference_element_instance','1',NULL,1,'value; id_reference',0,0,0,0,0,'Уникальный ИД элемента справочника. Например ygin-shop-reference-orderStatus-new  Префикс ygin зарезервирован для системных справочников, вместо него необходимо использовать название проекта или компании.',1),('28','112',7,2,NULL,'Справочник','id_reference','27',NULL,1,NULL,0,1,0,0,0,NULL,1),('28','113',1,3,NULL,'Значение элемента','id_reference_element',NULL,'1',1,NULL,0,NULL,0,0,0,'Идентификатор текущего значения справочника',1),('28','114',2,4,NULL,'Описание значения','value',NULL,NULL,1,NULL,0,NULL,0,0,0,'Описывает текущее значение справочника и, как правило, отображается в списке',1),('28','115',2,5,NULL,'Картинка для значения','image_element',NULL,NULL,0,NULL,0,NULL,0,0,0,NULL,1),('29','48',11,8,NULL,'id','id_group_system_parameter',NULL,NULL,1,NULL,0,NULL,0,0,0,NULL,1),('29','49',2,9,NULL,'Название','name',NULL,NULL,1,NULL,0,NULL,0,0,0,NULL,1),('30','116',11,8,'backend.backend.objectParameter.autoPrimaryKey.AutoPrimaryKeyWidget','id','id_system_parameter','1',NULL,1,'name;-;parameter',0,0,0,0,0,'Уникальный ИД системного параметра. Например ygin-parameter-phone   Префикс ygin зарезервирован для системных параметров, вместо него необходимо использовать название проекта или компании.',1),('30','117',7,8,NULL,'Группа параметров','id_group_system_parameter','29',NULL,1,NULL,0,1,0,0,0,NULL,1),('30','118',2,9,NULL,'Имя для разработчика','name',NULL,NULL,1,NULL,0,NULL,0,0,0,NULL,1),('30','119',2,10,NULL,'Значение параметра','value',NULL,NULL,0,NULL,0,NULL,0,0,0,NULL,1),('30','120',2,12,NULL,'Описание','note',NULL,NULL,0,NULL,0,NULL,0,0,0,NULL,1),('30','125',14,280,NULL,'Значение для больших текстовых данных (longtext)','long_text_value',NULL,NULL,0,NULL,0,0,0,0,1,NULL,1),('30','600',7,12,NULL,'Тип\r\nданных','id_parameter_type','22','2',0,NULL,0,NULL,0,0,0,NULL,1),('31','281',11,1,NULL,'id','id_domain',NULL,NULL,1,NULL,0,0,0,0,0,NULL,1),('31','282',2,6,NULL,'Путь к содержимому домена','domain_path',NULL,NULL,0,NULL,0,0,0,0,0,'Заполняется для многодоменных сайтов.\r\nВсе загружаемые файлы для домена будут попадать относительно заданной директории.\r\nНапример, если для новостей указан путь к данным как \"content/news\", а в данном свойстве указано \"my_domain\", то файлы будут загружаться по такому пути: \"/my_domain/content/news/1/sample.jpg\". При этом в формируемых ссылках папка my_domain не включается.',1),('31','283',2,2,NULL,'Доменное имя','name',NULL,NULL,1,NULL,0,0,0,0,0,NULL,1),('31','284',2,4,NULL,'Описание','description',NULL,NULL,0,NULL,0,0,0,0,0,NULL,1),('31','285',2,7,NULL,'Путь к данным по http','path2data_http',NULL,NULL,0,NULL,0,0,0,0,0,'Заполняется для многодоменных сайтов.\r\nДополнительный путь, автоматически приписываемый ко всем загружаемым файлам домена при использовании метода getPath() у класса Files.',1),('31','286',10,8,'backend.backend.domain.DomainLocalizationVisualElement','Доступные локализации',NULL,'2','',0,'DomainLocalizationVisualElement',0,0,0,0,0,'',1),('31','287',1,5,NULL,'ID страницы по умолчанию','id_default_page',NULL,NULL,0,NULL,0,0,0,0,0,NULL,1),('31','289',14,10,NULL,'Настройки','settings',NULL,NULL,0,NULL,0,0,0,0,0,NULL,1),('31','303',2,11,NULL,'Ключевые слова','keywords',NULL,NULL,0,NULL,0,0,1,0,0,'Ключевые слова подставляются в мета-тег keywords в случае отсутствия такового у раздела меню.',1),('31','304',9,3,NULL,'Активен','active',NULL,'1',0,NULL,0,0,0,0,0,NULL,1),('31','638',8,12,NULL,'Картинка для сохранения в закладках','image_src','1',NULL,0,NULL,0,0,0,0,1,NULL,1),('33','250',11,8,NULL,'id','id_event_format',NULL,NULL,1,NULL,0,NULL,0,0,0,NULL,1),('33','31',2,8,NULL,'Описание','description',NULL,NULL,1,NULL,0,NULL,0,0,0,NULL,1),('33','32',6,9,NULL,'Расположение','place','2',NULL,1,NULL,0,NULL,0,0,0,NULL,1),('33','33',2,10,NULL,'Имя файла во вложении','file_name',NULL,NULL,0,NULL,0,NULL,0,0,0,NULL,1),('33','34',2,11,NULL,'Сокращённое название (для разработчика)','name',NULL,NULL,1,NULL,0,NULL,0,0,0,NULL,1),('34','37',7,2,NULL,'Тип события','id_event_type','35',NULL,1,NULL,0,NULL,0,0,0,NULL,1),('34','38',7,4,NULL,'Пользователь','id_user','24',NULL,0,NULL,0,NULL,0,0,0,NULL,1),('34','40',7,9,NULL,'Формат сообщения','format','33',NULL,1,NULL,0,0,0,0,0,NULL,1),('34','41',9,8,NULL,'Архивировать ли вложение','archive_attach',NULL,NULL,1,NULL,0,NULL,0,0,0,NULL,1),('34','42',2,6,NULL,'E-mail адрес','email',NULL,NULL,0,NULL,0,NULL,0,0,0,NULL,1),('34','43',11,3,NULL,'id','id_event_subscriber',NULL,NULL,1,NULL,0,NULL,0,0,0,NULL,1),('34','44',2,7,NULL,'Имя подписчика','name',NULL,NULL,0,NULL,0,0,0,0,0,NULL,1),('35','245',7,9,NULL,'Объект для работы','id_object','20',NULL,0,'',0,NULL,0,0,0,NULL,1),('35','246',4,10,NULL,'Дата последей обработки','last_time',NULL,NULL,0,NULL,0,NULL,0,0,0,NULL,1),('35','248',2,13,NULL,'SQL получающая экемпляры (\"AS id_instance\")','sql_condition',NULL,NULL,0,NULL,0,NULL,0,0,0,NULL,1),('35','249',2,14,NULL,'SQL выражение, срабатывающее после обработки экземпляра (<<id_instance>>)','condition_done',NULL,NULL,0,NULL,0,NULL,0,0,0,NULL,1),('35','252',1,16,NULL,'Интервал времени, через которое будет проходить обработка события','interval_value',NULL,'90',0,NULL,0,0,0,0,0,NULL,1),('35','266',7,8,NULL,'Используемый почтовый аккаунт','id_mail_account','50',NULL,1,NULL,0,NULL,0,0,0,NULL,1),('35','50',11,8,NULL,'id','id_event_type',NULL,NULL,1,NULL,0,NULL,0,0,0,NULL,1),('35','51',2,8,NULL,'Название','name',NULL,NULL,1,NULL,0,NULL,0,0,0,NULL,1),('37','128',9,112,NULL,'Статус создания превью-файла','status_process',NULL,'0',0,NULL,0,0,0,0,0,NULL,1),('37','134',11,8,NULL,'id','id_file',NULL,NULL,1,NULL,0,NULL,0,0,0,NULL,1),('37','135',2,8,NULL,'Путь к файлу','file_path',NULL,NULL,1,NULL,0,NULL,0,0,0,NULL,1),('37','136',7,9,NULL,'Тип файла','id_file_type','39',NULL,0,NULL,0,NULL,0,0,0,NULL,1),('37','137',1,10,NULL,'Количество загрузок','count',NULL,NULL,0,NULL,0,NULL,0,0,0,NULL,1),('37','138',7,11,NULL,'Объект','id_object','20',NULL,0,'',0,NULL,0,0,0,NULL,1),('37','139',1,12,NULL,'ИД экземпляра','id_instance',NULL,NULL,0,NULL,0,NULL,0,0,0,NULL,1),('37','230',7,17,NULL,'Свойство объекта','id_parameter','21',NULL,0,NULL,0,NULL,0,0,0,NULL,1),('37','231',7,51,NULL,'Пользовательское свойство','id_property','41',NULL,0,NULL,0,NULL,0,0,0,NULL,1),('37','232',4,22,NULL,'Дата создания файла','create_date',NULL,NULL,0,NULL,0,NULL,0,0,0,NULL,1),('37','508',2,27,NULL,'Временный ИД','id_tmp',NULL,NULL,0,NULL,0,0,0,0,0,NULL,1),('37','509',7,27,NULL,'Родительский файл','id_parent_file','37',NULL,0,NULL,0,NULL,0,0,0,NULL,1),('39','129',11,8,NULL,'id','id_file_type',NULL,NULL,1,NULL,0,NULL,0,0,0,NULL,1),('39','130',2,8,NULL,'Название','name',NULL,NULL,1,NULL,0,0,1,0,0,NULL,1),('40','131',11,8,NULL,'id','id_file_extension',NULL,NULL,1,NULL,0,NULL,0,0,0,NULL,1),('40','132',2,8,NULL,'Расширение','ext',NULL,NULL,1,NULL,0,NULL,0,0,0,NULL,1),('40','133',7,8,NULL,'Тип файла','id_file_type','39',NULL,1,NULL,0,NULL,0,0,0,NULL,1),('41','290',11,8,NULL,'ID_PROPERTY','ID_PROPERTY',NULL,NULL,1,NULL,0,NULL,0,0,0,NULL,1),('41','292',9,9,NULL,'Обязательно для заполнения','IS_NECESSARILY',NULL,'1',1,NULL,0,NULL,0,0,0,NULL,1),('41','293',7,10,NULL,'Значение из справочника','ID_REFERENCE','27',NULL,0,NULL,0,NULL,0,0,0,NULL,1),('41','294',7,11,NULL,'Тип свойства','ID_PROPERTY_TYPE','22',NULL,1,'id_parameter_type IN (1,2,3,4,6,7,8,9,14,17)',0,NULL,0,0,0,NULL,1),('41','295',7,12,NULL,'Объект','ID_OBJECT','20',NULL,1,'',0,0,0,0,0,NULL,1),('41','296',2,12,NULL,'Значение по умолчанию','DEFAULT_VALUE',NULL,NULL,0,NULL,0,NULL,0,0,0,NULL,1),('41','297',7,13,NULL,'Значение берется из объекта','ID_SELECTOR_OBJECT','20',NULL,0,'',0,NULL,0,0,0,NULL,1),('41','298',2,8,NULL,'Описание','caption',NULL,NULL,1,NULL,0,NULL,0,0,0,NULL,1),('41','299',2,8,NULL,'Имя свойства','NAME',NULL,NULL,1,NULL,0,NULL,0,0,0,NULL,1),('49','253',11,8,NULL,'id','id_event',NULL,NULL,1,NULL,0,NULL,0,0,0,NULL,1),('49','254',7,8,NULL,'Тип события','id_event_type','35',NULL,1,NULL,0,NULL,0,0,0,NULL,1),('49','255',1,10,NULL,'ИД экземпляра','id_instance',NULL,NULL,0,NULL,0,NULL,0,0,0,NULL,1),('49','256',4,11,NULL,'Дата создания события','event_create',NULL,NULL,1,NULL,0,NULL,0,0,0,NULL,1),('49','257',14,16,NULL,'Содержимое','event_message',NULL,NULL,0,NULL,0,NULL,0,0,0,NULL,1),('50','258',11,8,NULL,'id','id_mail_account',NULL,NULL,1,NULL,0,NULL,0,0,0,NULL,1),('50','259',2,8,NULL,'E-mail для поля \"От\"','email_from',NULL,NULL,0,NULL,0,NULL,0,0,0,NULL,1),('50','260',2,8,NULL,'Имя отправителя','from_name',NULL,NULL,0,NULL,0,NULL,0,0,0,NULL,1),('50','261',2,10,NULL,'Тема по умолчанию','default_subject',NULL,NULL,0,NULL,0,NULL,0,0,0,NULL,1),('50','262',2,12,NULL,'HOST','host',NULL,NULL,1,NULL,0,NULL,0,0,0,NULL,1),('50','263',2,14,NULL,'Имя пользователя для авторизации','user_name',NULL,NULL,0,NULL,0,NULL,0,0,0,NULL,1),('50','264',9,13,NULL,'Требуется ли авторизация','smtp_auth',NULL,'1',1,NULL,0,NULL,0,0,0,NULL,1),('50','265',2,15,NULL,'Пароль для авторизации','user_password',NULL,NULL,0,NULL,0,NULL,0,0,0,NULL,1),('500','1500',11,1,NULL,'ID','id_photogallery',NULL,NULL,1,NULL,0,0,0,0,0,NULL,1),('500','1501',2,2,NULL,'Название','name',NULL,NULL,1,NULL,0,0,1,0,0,NULL,1),('500','1503',3,5,NULL,'Текст в галерее','text_in_gallery',NULL,NULL,0,NULL,0,0,1,0,0,NULL,1),('500','1504',13,7,NULL,'п/п','sequence',NULL,NULL,0,NULL,0,0,0,0,0,NULL,1),('500','1505',12,6,NULL,'Родительский раздел','id_parent',NULL,NULL,0,NULL,0,0,0,0,0,NULL,1),('501','1507',11,1,NULL,'ID','id_photogallery_photo',NULL,NULL,1,NULL,0,0,0,0,0,NULL,1),('501','1508',2,4,NULL,'Название','name',NULL,NULL,0,NULL,0,0,1,0,0,NULL,1),('501','1509',1,3,NULL,'Экземпляр-фотогалерея','id_photogallery_instance','411',NULL,1,NULL,0,1,0,0,0,'Экземпляр объекта, являющийся галереей фотографий',1),('501','1510',8,5,NULL,'Файл','file',NULL,NULL,1,NULL,0,0,0,0,0,NULL,1),('501','1511',13,6,NULL,'п/п','sequence',NULL,NULL,0,NULL,0,0,0,0,0,NULL,1),('501','1512',1,2,NULL,'Объект','id_photogallery_object','20',NULL,1,NULL,0,0,0,0,0,'Объект системы, экземпляр которого является фотогалереей',1),('502','1513',14,7,NULL,'Краткое содержание','short',NULL,NULL,0,NULL,0,0,0,1,0,NULL,1),('502','1514',7,5,NULL,'Категория','id_news_category','503',NULL,0,NULL,0,0,0,0,0,NULL,1),('502','1515',3,9,NULL,'Содержание','content',NULL,NULL,1,NULL,0,0,0,1,0,NULL,1),('502','1516',4,4,NULL,'Дата','date',NULL,NULL,1,NULL,0,0,0,0,0,NULL,1),('502','1517',2,2,NULL,'Заголовок','title',NULL,NULL,1,NULL,0,0,0,1,0,NULL,1),('502','1518',11,1,NULL,'ID','id_news',NULL,NULL,1,NULL,0,0,0,0,0,NULL,1),('502','1519',8,6,NULL,'Картинка','photo','1',NULL,0,NULL,0,0,0,0,0,'Позволяет прикреплять иллюстрацию к новости',1),('502','1520',9,3,NULL,'Видимость','is_visible',NULL,'1',0,NULL,0,0,0,0,0,NULL,1),('502','1577',15,8,NULL,'Загрузить файлы',NULL,NULL,NULL,0,NULL,0,0,0,0,0,NULL,1),('503','1521',11,52,NULL,'id','id_news_category',NULL,NULL,1,NULL,0,0,0,0,0,NULL,1),('503','1522',2,53,NULL,'Название','name',NULL,NULL,1,NULL,0,0,0,1,0,NULL,1),('503','1523',13,54,NULL,'п/п','seq',NULL,NULL,0,NULL,0,0,0,0,0,NULL,1),('503','1524',9,149,NULL,'Видимость','is_visible',NULL,'1',0,NULL,0,0,0,0,0,NULL,1),('505','1525',11,1,NULL,'id','id_consultation_ask',NULL,NULL,1,NULL,0,0,0,0,0,NULL,1),('505','1526',2,2,NULL,'ФИО спрашивающего','user_fio',NULL,NULL,1,NULL,0,0,0,0,0,NULL,1),('505','1527',2,3,NULL,'E-mail спрашивающего','email',NULL,NULL,0,NULL,0,0,0,0,0,NULL,1),('505','1528',14,4,NULL,'Вопрос','ask',NULL,NULL,1,NULL,0,0,0,1,0,NULL,1),('505','1529',10,5,'project/plugin/consultation/ConsultationSpecializationVisualElement.php','Специализация вопроса',NULL,'2',NULL,0,'ConsultationSpecializationVisualElement',0,0,0,0,0,NULL,1),('505','1530',4,6,NULL,'Дата вопроса','ask_date',NULL,NULL,1,NULL,0,0,0,0,0,NULL,1),('505','1531',2,7,NULL,'IP спрашивающего','ip',NULL,NULL,0,NULL,0,0,0,0,0,NULL,1),('505','1532',9,147,NULL,'Видимость','is_visible',NULL,'1',0,NULL,0,0,0,0,0,NULL,1),('505','1587',8,0,NULL,'Приложение','attachment',NULL,NULL,0,NULL,0,0,0,0,0,NULL,1),('506','1533',11,1,NULL,'id','id_consultation_answer',NULL,NULL,1,NULL,0,0,0,0,0,NULL,1),('506','1534',7,2,NULL,'Отвечающий','id_consultation_answerer','507',NULL,1,NULL,0,0,0,0,0,NULL,1),('506','1535',2,3,NULL,'Отвечающий (ручной ввод)','answerer',NULL,NULL,0,NULL,0,0,0,1,0,NULL,1),('506','1536',4,4,NULL,'Дата ответа','answer_date',NULL,NULL,1,NULL,0,0,0,0,0,NULL,1),('506','1537',7,5,NULL,'На вопрос','id_consultation_ask','505',NULL,1,NULL,0,1,0,0,0,'Указываем вопрос, на который даётся ответ',1),('506','1538',3,6,NULL,'Ответ','answer',NULL,NULL,1,NULL,0,0,0,1,0,NULL,1),('506','1539',2,7,NULL,'IP отвечающего','ip',NULL,NULL,0,NULL,0,0,0,0,0,NULL,1),('507','1540',10,6,'project/plugin/consultation/AnswererSpecializationVisualElement.php','Специализация',NULL,'2',NULL,0,'AnswererSpecializationVisualElement',0,0,0,0,0,NULL,1),('507','1541',3,9,NULL,'Полное описание','full_info',NULL,NULL,0,NULL,0,0,0,0,0,NULL,1),('507','1542',3,8,NULL,'Краткое описание','short_info',NULL,NULL,0,NULL,0,0,0,1,0,NULL,1),('507','1543',2,4,NULL,'Подпись после ФИО','caption_after',NULL,NULL,0,NULL,0,0,0,0,0,NULL,1),('507','1544',8,7,NULL,'Фото','photo','1',NULL,0,NULL,0,0,0,0,0,NULL,1),('507','1545',2,2,NULL,'Подпись перед ФИО','caption_before',NULL,NULL,0,NULL,0,0,0,0,0,NULL,1),('507','1546',2,5,NULL,'e-mail','email',NULL,NULL,0,NULL,0,0,0,0,0,NULL,1),('507','1547',2,3,NULL,'ФИО отвечающего','name',NULL,NULL,1,NULL,0,0,0,1,0,NULL,1),('507','1548',11,1,NULL,'id','id_consultation_answerer',NULL,NULL,1,NULL,0,0,0,0,0,NULL,1),('508','1549',14,81,NULL,'Описание','description',NULL,NULL,0,NULL,0,0,0,0,0,NULL,1),('508','1550',11,79,NULL,'id','id_consultation_specialization',NULL,NULL,1,NULL,0,0,0,0,0,NULL,1),('508','1551',2,80,NULL,'Специализация','specialization',NULL,NULL,1,NULL,0,0,0,1,0,NULL,1),('509','1552',11,2,NULL,'id','id_product_category',NULL,NULL,1,NULL,0,0,0,0,0,NULL,1),('509','1553',2,3,NULL,'Название','name',NULL,NULL,1,NULL,0,0,0,1,0,NULL,1),('509','1554',12,5,NULL,'Родитель','id_parent','1',NULL,0,NULL,0,0,0,0,0,NULL,1),('509','1555',8,6,NULL,'Изображение','image','1',NULL,0,NULL,0,0,0,0,1,'Изображение категории каталога',1),('509','1557',1,7,NULL,'Наценка','price_markup',NULL,'0',1,NULL,0,0,0,0,0,'Наценка на оптовую цену перед выводом на сайт',1),('509','1636',13,1,NULL,'п/п','sequence',NULL,NULL,0,NULL,0,0,0,0,0,NULL,1),('509','1637',9,4,NULL,'Видимость','visible',NULL,'1',0,NULL,0,0,0,0,0,NULL,1),('51','267',11,1,'backend.backend.objectParameter.autoPrimaryKey.AutoPrimaryKeyWidget','id','id_job','1',NULL,1,'name;-;job',0,0,0,0,0,'Уникальный ИД задачи планировщика. Например для отправки уведомлений это будет ygin-job-sendMail   Префикс ygin зарезервирован для системных задач, вместо него необходимо использовать название проекта или компании.',1),('51','268',1,4,NULL,'Интервал запуска задачи (в секундах)','interval_value',NULL,NULL,1,NULL,0,NULL,0,0,0,NULL,1),('51','269',1,5,NULL,'Интервал запуска задачи в случае ошибки (в секундах)','error_repeat_interval',NULL,NULL,1,NULL,0,NULL,0,0,0,NULL,1),('51','270',4,6,NULL,'Дата первого запуска','first_start_date',NULL,NULL,0,NULL,0,NULL,0,0,1,NULL,1),('51','271',4,7,NULL,'Дата последнего запуска','last_start_date',NULL,NULL,0,NULL,0,0,0,0,0,NULL,1),('51','272',4,8,NULL,'Дата следущего запуска','next_start_date',NULL,NULL,0,NULL,0,0,0,0,0,NULL,1),('51','273',1,9,NULL,'Количество ошибок','failures',NULL,'0',1,NULL,0,NULL,0,0,0,NULL,1),('51','274',2,2,NULL,'Имя задачи','name',NULL,NULL,1,NULL,0,NULL,0,0,0,NULL,1),('51','276',2,10,NULL,'Имя класса задачи','class_name',NULL,NULL,1,NULL,0,0,0,0,0,NULL,1),('51','291',9,3,NULL,'Вкл.','active',NULL,'1',0,NULL,0,0,0,0,0,NULL,1),('51','300',1,11,NULL,'Приоритет запуска','priority',NULL,'0',1,NULL,0,0,0,0,1,NULL,1),('51','301',4,12,NULL,'Дата запуска текущего потока','start_date',NULL,NULL,0,NULL,0,0,0,0,1,NULL,1),('51','302',1,13,NULL,'Максимальное число секунд выполнения задачи','max_second_process',NULL,NULL,0,NULL,0,0,0,0,1,'NULL или 0 - без ограничений',1),('511','1564',11,1,NULL,'id','id_product',NULL,NULL,1,NULL,0,0,0,0,0,NULL,1),('511','1565',7,2,NULL,'Каталог продукции','id_product_category','509',NULL,1,NULL,0,1,0,0,0,NULL,1),('511','1566',2,3,NULL,'Артикул','code',NULL,NULL,0,NULL,0,0,0,1,0,'Артикул, внутренний код и т.п.',1),('511','1567',2,4,NULL,'Название','name',NULL,NULL,1,NULL,0,0,0,1,0,NULL,1),('511','1568',1,7,NULL,'Оптовая цена','trade_price',NULL,NULL,1,NULL,0,0,0,0,0,NULL,1),('511','1569',1,8,NULL,'Мал. оптовая цена','sm_trade_price',NULL,NULL,1,NULL,0,0,0,0,0,NULL,1),('511','1570',1,9,NULL,'Розничная цена','retail_price',NULL,NULL,1,NULL,0,0,0,0,0,NULL,1),('511','1571',2,10,NULL,'Единица измерения','unit',NULL,NULL,0,NULL,0,0,0,0,0,NULL,1),('511','1573',2,11,NULL,'Остаток','remain',NULL,NULL,0,NULL,0,0,0,0,0,NULL,1),('511','1574',14,13,NULL,'Описание товара','description',NULL,NULL,0,NULL,0,0,0,1,0,NULL,1),('511','1576',8,6,NULL,'Изображение','image','1',NULL,0,NULL,0,0,0,0,0,NULL,1),('511','1616',9,14,NULL,'Удален','deleted',NULL,NULL,1,NULL,0,0,0,0,0,NULL,1),('511','1617',3,15,NULL,'Характеристики','properties',NULL,NULL,0,NULL,0,0,0,1,0,NULL,1),('511','1619',3,16,NULL,'Монтаж','additional_desc',NULL,NULL,0,NULL,0,0,0,1,0,NULL,1),('511','1663',9,5,NULL,'Видимость','visible',NULL,'1',0,NULL,0,0,0,0,0,NULL,1),('511','1671',7,12,NULL,'Брэнд','id_brand','525',NULL,0,NULL,0,0,0,0,0,NULL,1),('511','1691',14,0,NULL,'Видео','video',NULL,NULL,0,NULL,0,0,0,0,1,'HTML-код видео (напр. с сайта youtube.com)',1),('512','1578',11,1,NULL,'id','id_question',NULL,NULL,1,NULL,0,0,0,0,0,NULL,1),('512','1579',4,2,NULL,'Дата','ask_date',NULL,NULL,1,NULL,0,0,0,0,0,NULL,1),('512','1580',2,3,NULL,'Спрашивающий','name',NULL,NULL,1,NULL,0,0,0,0,0,NULL,1),('512','1581',14,5,NULL,'Вопрос','question',NULL,NULL,1,NULL,0,0,0,1,0,NULL,1),('512','1582',2,4,NULL,'E-mail','email',NULL,NULL,0,NULL,0,0,0,0,0,NULL,1),('512','1583',3,7,NULL,'Ответ','answer',NULL,NULL,0,NULL,0,0,0,1,0,NULL,1),('512','1585',9,6,NULL,'Видимость','visible',NULL,'1',0,NULL,0,0,0,0,0,NULL,1),('512','ygin-faq-category',6,0,NULL,'Категория','category','ygin-faq-reference-categoryQuestion','1',1,NULL,0,0,0,0,0,'',1),('512','ygin-faq-send',9,0,NULL,'Отправить ответ на email','send',NULL,'0',1,NULL,0,0,0,0,0,'',1),('517','1609',11,1,NULL,'id','id_feedback',NULL,NULL,1,NULL,0,NULL,0,0,0,NULL,1),('517','1610',2,0,NULL,'ФИО','fio',NULL,NULL,1,NULL,0,0,0,0,0,NULL,1),('517','1611',2,0,NULL,'Телефон','phone',NULL,NULL,0,NULL,0,0,0,0,0,NULL,1),('517','1612',2,257,NULL,'e-mail','mail',NULL,NULL,0,NULL,0,0,0,0,0,NULL,1),('517','1613',14,0,NULL,'Сообщение','message',NULL,NULL,1,NULL,0,0,0,0,0,NULL,1),('517','1614',4,0,NULL,'Дата сообщения','date',NULL,NULL,1,NULL,0,0,0,0,0,NULL,1),('517','1615',2,0,NULL,'ip','ip',NULL,NULL,1,NULL,0,0,0,0,0,NULL,1),('519','1620',11,1,NULL,'id','id_offer',NULL,NULL,1,NULL,0,NULL,0,0,0,NULL,1),('519','1621',2,2,NULL,'ФИО','fio',NULL,NULL,1,NULL,0,0,0,0,0,NULL,1),('519','1622',2,4,NULL,'Телефон','phone',NULL,NULL,1,NULL,0,0,0,0,0,NULL,1),('519','1623',2,6,NULL,'e-mail','mail',NULL,NULL,0,NULL,0,0,0,0,0,NULL,1),('519','1624',14,10,NULL,'Пожелания','comment',NULL,NULL,0,NULL,0,0,0,0,0,NULL,1),('519','1625',14,18,NULL,'Заказ','offer_text',NULL,NULL,0,NULL,0,0,0,0,0,'',0),('519','1626',4,12,NULL,'Дата заказа','create_date',NULL,NULL,1,NULL,0,0,0,0,0,NULL,1),('519','1627',9,14,NULL,'Обработано','is_process',NULL,NULL,0,NULL,0,0,0,0,0,NULL,1),('519','1628',2,13,NULL,'ip','ip',NULL,NULL,1,NULL,0,0,0,0,1,NULL,1),('519','1629',9,15,NULL,'Отправлено ли уведомление','is_send',NULL,NULL,0,NULL,0,0,0,0,1,NULL,1),('519','1665',6,11,NULL,'Статус','status','101',NULL,1,NULL,0,0,0,0,0,NULL,1),('519','519-address',2,5,NULL,'Адрес','address',NULL,NULL,0,NULL,0,0,0,0,0,'',1),('519','519-client',2,7,NULL,'Клиентский номер','client',NULL,NULL,0,NULL,0,0,0,0,0,'',1),('519','519-company',2,3,NULL,'Компания','company',NULL,NULL,0,NULL,0,0,0,0,0,'',1),('519','519-date-delivery',4,8,NULL,'Дата доставки','date_delivery','0',NULL,1,NULL,0,0,0,0,0,'',1),('519','519-first',9,9,NULL,'Впервые','first',NULL,'0',1,NULL,0,0,0,0,0,'',1),('519','ygin-offer-amount',1,16,NULL,'Сумма','amount',NULL,NULL,1,NULL,0,0,0,0,0,'',1),('519','ygin-offer-id-invoice',7,17,NULL,'Счет','id_invoice','ygin-invoice',NULL,0,NULL,0,0,0,0,0,'',1),('520','1630',11,1,NULL,'id','id_vitrine',NULL,NULL,1,NULL,0,0,0,0,0,NULL,1),('520','1631',2,3,NULL,'Ссылка на переход','link',NULL,NULL,0,NULL,0,0,0,0,0,'',0),('520','1632',2,4,NULL,'Заголовок','title',NULL,NULL,0,NULL,0,0,0,0,0,'',0),('520','1633',14,5,NULL,'Дополнительный текст','text',NULL,NULL,0,NULL,0,0,0,0,0,'',0),('520','1634',8,2,NULL,'Фото','image','1',NULL,0,NULL,0,0,0,0,0,NULL,1),('520','1635',13,0,NULL,'п/п','sequence',NULL,NULL,0,NULL,0,0,0,0,0,NULL,1),('521','1638',11,1,NULL,'id','id_quiz',NULL,NULL,1,NULL,0,NULL,0,0,0,NULL,1),('521','1639',2,0,NULL,'name','name',NULL,NULL,1,NULL,0,0,0,0,0,NULL,1),('521','1640',15,0,NULL,'Файлы',NULL,'1',NULL,0,NULL,0,0,0,0,0,NULL,1),('521','1641',3,0,NULL,'description','description',NULL,NULL,0,NULL,0,0,0,0,0,NULL,1),('521','1642',9,0,NULL,'active','active',NULL,'1',0,NULL,0,0,0,0,0,NULL,1),('522','1643',11,1,NULL,'id','id_quiz_question',NULL,NULL,1,NULL,0,NULL,0,0,0,NULL,1),('522','1644',7,269,NULL,'Викторина','id_quiz','521',NULL,1,NULL,0,1,0,0,0,NULL,1),('522','1645',15,0,NULL,'Файлы',NULL,'1',NULL,0,NULL,0,0,0,0,0,NULL,1),('522','1646',3,271,NULL,'Текст вопроса','question',NULL,NULL,1,NULL,0,0,0,0,0,NULL,1),('522','1647',6,272,NULL,'Тип ответов','type','100','1',1,NULL,0,0,0,0,0,NULL,1),('522','1648',13,0,NULL,'п/п','sequence','521',NULL,0,NULL,0,0,0,0,0,NULL,1),('523','1649',11,1,NULL,'id','id_quiz_answer',NULL,NULL,1,NULL,0,NULL,0,0,0,NULL,1),('523','1650',7,0,NULL,'Вопрос','id_quiz_question','522',NULL,1,NULL,0,1,0,0,0,NULL,1),('523','1651',2,0,NULL,'Текст ответа','answer',NULL,NULL,1,NULL,0,0,0,0,0,NULL,1),('523','1652',9,0,NULL,'Правильный ли ответ','is_right',NULL,NULL,0,NULL,0,0,0,0,0,NULL,1),('523','1653',13,0,NULL,'п/п','sequence','522',NULL,0,NULL,0,0,0,0,0,NULL,1),('524','1654',11,1,NULL,'id','id_quiz_answer_user',NULL,NULL,1,NULL,0,NULL,0,0,0,NULL,1),('524','1655',7,278,NULL,'Викторина','id_quiz','521',NULL,1,NULL,0,1,0,0,0,NULL,1),('524','1656',2,279,NULL,'ФИО','name',NULL,NULL,1,NULL,0,0,0,0,0,NULL,1),('524','1657',2,0,NULL,'mail','mail',NULL,NULL,1,NULL,0,0,0,0,0,NULL,1),('524','1658',2,0,NULL,'Читательский билет','library_card',NULL,NULL,0,NULL,0,0,0,0,0,NULL,1),('524','1659',2,0,NULL,'Контактная информация','contact',NULL,NULL,0,NULL,0,0,0,0,0,NULL,1),('524','1660',14,0,NULL,'Ответ','answer',NULL,NULL,1,NULL,0,0,0,0,0,NULL,1),('524','1661',4,0,NULL,'Дата','create_date',NULL,NULL,1,NULL,0,0,0,0,0,NULL,1),('524','1662',2,0,NULL,'ip','ip',NULL,NULL,1,NULL,0,0,0,0,0,NULL,1),('525','1666',11,1,NULL,'id','id_brand',NULL,NULL,1,NULL,0,0,0,0,0,NULL,1),('525','1667',2,2,NULL,'Название','name',NULL,NULL,1,NULL,0,0,0,1,0,NULL,1),('525','1668',12,4,NULL,'Родительский брэнд','id_parent','1',NULL,0,NULL,0,0,0,0,0,NULL,1),('525','1669',8,3,NULL,'Логотип брэнда','image','1',NULL,0,NULL,0,0,0,0,0,NULL,1),('525','1670',13,0,NULL,'п/п','sequence','-1',NULL,0,NULL,0,0,0,0,0,NULL,1),('527','1673',11,1,NULL,'id','id_plugin',NULL,NULL,1,NULL,0,NULL,0,0,0,NULL,1),('527','1674',2,0,NULL,'Название','name',NULL,NULL,1,NULL,0,0,0,0,0,NULL,1),('527','1675',2,0,NULL,'Код','code',NULL,NULL,1,NULL,0,0,0,0,0,NULL,1),('527','1676',1,282,NULL,'Статус','status',NULL,'1',1,NULL,0,0,0,0,0,NULL,1),('527','1677',14,283,NULL,'Сериализованные настройки','config',NULL,NULL,0,NULL,0,0,0,0,0,NULL,1),('527','1678',2,0,NULL,'Класс плагина','class_name',NULL,NULL,1,NULL,0,0,0,0,0,NULL,1),('527','1681',14,286,NULL,'data','data',NULL,NULL,0,NULL,0,0,0,0,0,NULL,1),('529','1679',11,1,NULL,'id','id_remain_status',NULL,NULL,1,NULL,0,NULL,0,0,0,NULL,1),('529','1680',2,0,NULL,'Название','name',NULL,NULL,1,NULL,0,0,0,0,0,NULL,1),('529','1682',1,0,NULL,'Макс. значение по-умолчанию','max_value',NULL,NULL,1,NULL,0,0,0,0,0,NULL,1),('529','1683',1,0,NULL,'Мин. значение по-умолчанию','min_value',NULL,NULL,1,NULL,0,0,0,0,0,NULL,1),('529','1692',2,0,NULL,'Иконка','icon',NULL,NULL,0,NULL,0,0,0,0,0,NULL,1),('530','1684',11,1,NULL,'id','id_client_review',NULL,NULL,1,NULL,0,NULL,0,0,0,NULL,1),('530','1685',2,0,NULL,'ФИО','name',NULL,NULL,1,NULL,0,0,0,0,0,NULL,1),('530','1686',4,0,NULL,'Дата','create_date',NULL,NULL,1,NULL,0,0,0,0,0,NULL,1),('530','1687',14,0,NULL,'Текст отзыва','review',NULL,NULL,1,NULL,0,0,0,0,0,NULL,1),('530','1688',2,0,NULL,'ip','ip',NULL,NULL,1,NULL,0,0,0,0,0,NULL,1),('530','1689',9,0,NULL,'Видимость на сайте','visible',NULL,NULL,0,NULL,0,0,0,0,0,NULL,1),('530','1690',2,0,NULL,'Контакты клиента','contact',NULL,NULL,0,NULL,0,0,0,0,0,NULL,1),('531','1693',11,1,NULL,'id','id_message',NULL,NULL,1,NULL,0,NULL,0,0,0,NULL,1),('531','1694',14,0,NULL,'Текст','text',NULL,NULL,1,NULL,0,0,0,0,0,NULL,1),('531','1695',4,0,NULL,'Дата создания','date',NULL,NULL,1,NULL,0,0,0,0,0,NULL,1),('531','1696',1,0,NULL,'тип','type',NULL,'1',1,NULL,0,0,0,0,0,NULL,1),('54','310',11,8,NULL,'id','id_localization',NULL,NULL,1,NULL,0,NULL,0,0,0,NULL,1),('54','311',2,8,NULL,'Название','name',NULL,NULL,1,NULL,0,NULL,0,0,0,NULL,1),('54','312',2,8,NULL,'Код','code',NULL,NULL,1,NULL,0,NULL,0,0,0,NULL,1),('54','313',9,12,NULL,'Используется ли локализация','is_use',NULL,'1',1,NULL,0,NULL,0,0,0,NULL,1),('61','140',11,9,NULL,'id','id_instruction',NULL,NULL,1,NULL,0,NULL,0,0,0,NULL,1),('61','141',2,10,NULL,'Название','name',NULL,NULL,1,NULL,0,NULL,0,0,0,NULL,1),('61','142',3,13,NULL,'Описание','content',NULL,NULL,1,NULL,0,NULL,0,0,0,NULL,1),('61','143',9,12,NULL,'Относится только к этому сайту','desc_type',NULL,'0',0,NULL,0,NULL,0,0,0,NULL,1),('61','144',9,11,NULL,'Видимость','visible',NULL,'1',0,NULL,0,NULL,0,0,0,NULL,1),('61','145',13,8,NULL,'п/п','num_seq',NULL,NULL,0,NULL,0,NULL,0,0,0,NULL,1),('63','400',11,2,'backend.backend.objectParameter.autoPrimaryKey.AutoPrimaryKeyWidget','id','id_object_view','1',NULL,1,'name; id_object; view',0,0,0,0,0,'Уникальный ИД представления объекта в виде строки. Обычно имеет имя ygin-news-view-main  Префикс ygin зарезервирован для системных представлений, вместо него необходимо использовать название проекта или компании.',1),('63','401',7,5,NULL,'Объект','id_object','20',NULL,1,NULL,0,1,0,0,0,NULL,1),('63','402',2,8,NULL,'SELECT','sql_select',NULL,NULL,0,NULL,0,0,0,0,0,NULL,1),('63','403',2,9,NULL,'FROM','sql_from',NULL,NULL,0,NULL,0,0,0,0,0,NULL,1),('63','404',2,10,NULL,'WHERE','sql_where',NULL,NULL,0,NULL,0,NULL,0,0,0,NULL,1),('63','405',2,11,NULL,'ORDER BY','sql_order_by',NULL,NULL,0,NULL,0,NULL,0,0,0,NULL,1),('63','406',2,1,NULL,'Имя представления','name',NULL,NULL,1,NULL,0,0,0,0,0,NULL,1),('63','407',1,12,NULL,'Экземпляров на странице','count_data','0','50',1,NULL,0,0,0,0,0,'Определяет количество отображаемых экземпляров объекта на одной странице',1),('63','408',13,6,NULL,'п/п','order_no',NULL,'1',0,NULL,0,0,0,0,0,NULL,1),('63','409',9,7,NULL,'Видимость','visible','0','1',1,NULL,0,0,0,0,0,'Определяет видимость представления в меню системы управления. В случае отсутствия видимости, представление остаётся доступным по прямой ссылке.',1),('63','410',2,3,NULL,'css-класс иконки','icon_class',NULL,NULL,0,NULL,0,0,0,0,0,'Css-класс иконки из Twitter Bootstrap, который будет отображаться в меню',1),('63','411',2,4,NULL,'Иерархия по полю','id_parent',NULL,NULL,0,NULL,0,0,0,0,0,'Родительское поле для построения иерархии данных',1),('63','ygin-object-view-description',14,292,NULL,'Описание представления','description',NULL,NULL,0,NULL,0,0,0,0,0,'Данное описание будет выводится в списке экземпляров сразу после заголовка представления. Служит для подробного описания назначения объекта и представления.',1),('66','415',11,1,'backend.backend.objectParameter.autoPrimaryKey.AutoPrimaryKeyWidget','id','id_object_view_column','1',NULL,1,'field_name/caption; id_object_view',0,0,0,0,0,'Уникальный ИД колонки представления. Например для заголовка новости ИД будет равен ygin-news-view-main-title    Префикс ygin зарезервирован для системных колонок, вместо него необходимо использовать название проекта или компании.',1),('66','416',7,2,NULL,'Представление','id_object_view','63',NULL,1,NULL,0,1,0,0,0,NULL,1),('66','417',7,3,NULL,'Объект','id_object','20',NULL,1,NULL,0,0,0,0,0,NULL,1),('66','418',2,4,NULL,'Заголовок','caption','0',NULL,0,NULL,0,0,0,0,0,'',1),('66','419',13,6,NULL,'п/п','order_no',NULL,'1',1,NULL,0,0,0,0,0,NULL,1),('66','420',7,7,'backend.backend.objectParameter.selectObjectParameterWidget.SelectObjectParameterWidget','Свойство объекта','id_object_parameter','21',NULL,0,NULL,0,0,0,0,0,'',1),('66','421',7,8,NULL,'Тип данных','id_data_type','22','2',1,'id_parameter_type IN (1, 2, 3, 4, 6, 7, 8, 9, 10, 14)',0,0,0,0,0,NULL,1),('66','422',2,9,NULL,'Имя поля','field_name',NULL,NULL,1,NULL,0,0,0,0,0,NULL,1),('66','423',7,10,NULL,'Обработчик','handler','80',NULL,0,'id_php_script_interface IN (6, 7)',0,0,0,0,0,'Особый зарегистрированный в системе скрипт, который будет формировать колонку',1),('66','424',9,5,NULL,'Видимость','visible',NULL,'1',0,NULL,0,0,0,0,0,NULL,1),('80','334',9,4,NULL,'Активен','active',NULL,'1',1,NULL,0,0,0,0,0,NULL,1),('80','337',7,2,NULL,'Интерфейс','id_php_script_interface','86','1',1,NULL,0,1,0,0,0,NULL,1),('80','338',11,1,'backend.backend.objectParameter.autoPrimaryKey.AutoPrimaryKeyWidget','id','id_php_script_type','1',NULL,1,'description; -; php',0,0,0,0,0,'Уникальный ИД пхп обработчика. Например для модуля новостей это будет ygin-news-module-last  Префикс ygin зарезервирован для системных обработчиков, вместо него необходимо использовать название проекта или компании.',1),('80','339',2,5,NULL,'Алиас','file_path',NULL,NULL,1,NULL,0,0,0,0,0,'',1),('80','342',2,3,NULL,'Название скрипта','description',NULL,NULL,1,NULL,0,NULL,0,0,0,NULL,1),('86','347',11,1,NULL,'id','id_php_script_interface',NULL,NULL,1,NULL,0,0,0,0,0,NULL,1),('86','348',2,3,NULL,'Название','name',NULL,NULL,1,NULL,0,0,0,0,0,NULL,1),('86','350',14,4,NULL,'Шаблон файла','template',NULL,NULL,0,NULL,0,0,0,0,0,'Не надо открывать и закрывать php (<?php ?>), пишется только тело кода.\r\nДопускаются следующие переменные шаблона:\r\n<<class_name>> - имя класса',1),('86','351',13,2,NULL,'Порядок','sequence',NULL,'1',0,NULL,0,0,0,0,0,NULL,1),('project-kolichestvo-vody','project-kolichestvo-vody-count',1,309,NULL,'Количество','count',NULL,'0',1,NULL,0,0,0,0,0,'',1),('project-kolichestvo-vody','project-kolichestvo-vody-id-order',7,308,NULL,'Заказ','id_order','project-zakaz',NULL,1,NULL,0,1,0,0,0,'',1),('project-kolichestvo-vody','project-kolichestvo-vody-id-water',7,310,NULL,'Вода','id_water','project-vid-vody',NULL,1,NULL,0,1,0,0,0,'',1),('project-kolichestvo-vody','project-kolichestvo-vody-id-water-count',11,1,NULL,'id','id_water_count',NULL,NULL,1,NULL,NULL,NULL,0,0,0,NULL,1),('project-vid-vody','project-vid-vody-id-water',11,1,NULL,'id','id_water',NULL,NULL,1,NULL,NULL,NULL,0,0,0,NULL,1),('project-vid-vody','project-vid-vody-nazvanie',2,3,NULL,'Название','title',NULL,NULL,1,NULL,0,0,0,0,0,'',1),('project-vid-vody','project-vid-vody-photo',8,2,NULL,'Фото','photo','1',NULL,0,NULL,0,0,0,0,0,'',1),('project-vid-vody','project-vid-vody-price',2,5,NULL,'Цена','price',NULL,NULL,0,NULL,0,0,0,0,0,'',1),('project-vid-vody','project-vid-vody-sequence',13,297,NULL,'п/п','sequence',NULL,NULL,0,NULL,0,0,0,0,0,'',1),('project-vid-vody','project-vid-vody-short',14,4,NULL,'Краткое описание','short',NULL,NULL,0,NULL,0,0,0,0,0,'',1),('project-vid-vody','project-vid-vody-visible',9,298,NULL,'Видимость','visible',NULL,'1',1,NULL,0,0,0,0,0,'',1),('project-zakaz','project-zakaz-address',2,301,NULL,'Адрес','address',NULL,NULL,0,NULL,0,0,0,0,0,'',1),('project-zakaz','project-zakaz-company',2,306,NULL,'Компания','company',NULL,NULL,0,NULL,0,0,0,0,0,'',1),('project-zakaz','project-zakaz-convenient-time',2,303,NULL,'Удобное время доставки','convenient_time',NULL,NULL,0,NULL,0,0,0,0,0,'',1),('project-zakaz','project-zakaz-date',4,307,NULL,'Дата','date','1',NULL,0,NULL,0,0,0,0,0,'',1),('project-zakaz','project-zakaz-description',14,305,NULL,'Примечание','description',NULL,NULL,0,NULL,0,0,0,0,0,'',1),('project-zakaz','project-zakaz-email',2,302,NULL,'email','email',NULL,NULL,0,NULL,0,0,0,0,0,'',1),('project-zakaz','project-zakaz-fio',2,299,NULL,'ФИО','fio',NULL,NULL,0,NULL,0,0,0,0,0,'',1),('project-zakaz','project-zakaz-id-order',11,1,NULL,'id','id_order',NULL,NULL,1,NULL,NULL,NULL,0,0,0,NULL,1),('project-zakaz','project-zakaz-phone',2,300,NULL,'Телефон','phone',NULL,NULL,0,NULL,0,0,0,0,0,'',1),('project-zakaz','project-zakaz-subscriber-number',2,304,NULL,'Абонентский номер','subscriber_number',NULL,NULL,0,NULL,0,0,0,0,0,'',1),('ygin-invoice','ygin-invoice-amount',1,295,NULL,'Сумма','amount',NULL,NULL,1,NULL,0,0,0,0,0,'',1),('ygin-invoice','ygin-invoice-create-date',4,293,NULL,'Дата создания','create_date','0',NULL,1,NULL,0,0,0,0,0,'',1),('ygin-invoice','ygin-invoice-id-invoice',11,1,NULL,'id','id_invoice',NULL,NULL,1,NULL,NULL,NULL,0,0,0,NULL,1),('ygin-invoice','ygin-invoice-id-offer',7,296,NULL,'Заказ','id_offer','519',NULL,1,NULL,0,0,0,0,0,'',1),('ygin-invoice','ygin-invoice-pay-date',4,294,NULL,'Дата оплаты','pay_date','0',NULL,0,NULL,0,0,0,0,0,'',1),('ygin-menu','1',11,1,NULL,'id','id',NULL,NULL,1,NULL,0,0,0,0,0,NULL,1),('ygin-menu','10',6,12,NULL,'Дополнительные опции обработки контента:','go_to_type','33','1',1,NULL,0,0,0,0,0,'Определяет дополнительные действия для содержимого раздела',1),('ygin-menu','11',8,22,NULL,'Картинка для раздела','image','1',NULL,0,NULL,0,0,0,0,1,NULL,1),('ygin-menu','127',2,3,NULL,'В адресной строке','alias','0',NULL,0,NULL,1,0,0,0,0,'Обязательно на английском языке. Необходимо для формирования человекопонятных URLов.',1),('ygin-menu','15',7,13,NULL,'Набор виджетов','id_module_template','101','SiteModuleTemplate::getIdDefaultTemplate();',0,NULL,0,1,0,0,1,'',1),('ygin-menu','152',2,16,NULL,'<title>','title_teg',NULL,NULL,0,NULL,0,0,1,1,1,'При отсутствии значения, заполняется автоматически заголовком раздела',1),('ygin-menu','153',2,18,NULL,'<meta name=\"description\">','meta_description',NULL,NULL,0,NULL,0,0,1,0,1,NULL,1),('ygin-menu','154',2,17,NULL,'<meta name=\"keywords\">','meta_keywords',NULL,NULL,0,NULL,0,0,1,1,1,NULL,1),('ygin-menu','2',2,2,'menu.backend.widgets.menuName.MenuNameWidget','Название в меню','name','0','Имя раздела',1,'',0,0,1,1,0,'Краткое название раздела, отображаемое в меню сайта',1),('ygin-menu','229',15,7,NULL,'Загрузка файлов',NULL,NULL,NULL,0,NULL,0,0,0,0,0,'Форма загрузки изображений, вордовских и экселевских документов, архивов и прочих файлов в этот раздел сайта',1),('ygin-menu','3',2,4,NULL,'Заголовок раздела','caption','0',NULL,0,NULL,0,0,1,1,0,'Заголовок не виден в меню сайта, но отображается крупно над текстом в самом разделе. Заголовок обычно длиннее названия в меню.',1),('ygin-menu','330',9,21,NULL,'Разрешить удаление','removable','0','1',1,NULL,0,0,0,0,1,'Актуально для динамических разделов, удаление которых приводит к возникновению критических ошибок.',1),('ygin-menu','4',3,8,NULL,'Содержимое раздела','content',NULL,NULL,0,NULL,0,0,1,1,0,NULL,1),('ygin-menu','5',9,5,NULL,'Видимость','visible',NULL,'1',0,NULL,0,0,0,0,0,'Раздел не отображается в меню, но остаётся доступным по пряму адресу (URL)',1),('ygin-menu','549',10,6,'menu.backend.widgets.externalLink.ExternalLinkWidget','Ссылка на страницу','external_link','2','',0,'MenuHref',0,0,0,0,1,'При отображении в меню раздел будет иметь указанную ссылку. Используется для создания внешних ссылок в меню или прямых ссылок на файл (например, прайс).',1),('ygin-menu','6',12,14,NULL,'Смена родительского раздела','id_parent','1',NULL,0,NULL,0,0,0,0,1,'',1),('ygin-menu','7',13,11,NULL,'&nbsp;','sequence','-1',NULL,1,NULL,0,0,0,0,0,NULL,1),('ygin-menu','8',2,9,NULL,'Примечания','note',NULL,NULL,0,NULL,0,0,1,0,1,'Нигде не отображаются, исключительно для комментариев в системе администрирования',1);
/*!40000 ALTER TABLE `da_object_parameters` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `da_object_property`
--

DROP TABLE IF EXISTS `da_object_property`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `da_object_property` (
  `ID_PROPERTY` int(8) NOT NULL COMMENT 'ID_PROPERTY',
  `NAME` varchar(255) NOT NULL COMMENT 'Имя свойства',
  `IS_NECESSARILY` int(1) NOT NULL DEFAULT '0' COMMENT 'Обязательно для заполнения',
  `ID_REFERENCE` int(8) DEFAULT NULL COMMENT 'Значение из справочника',
  `ID_PROPERTY_TYPE` int(2) NOT NULL DEFAULT '0' COMMENT 'Тип свойства',
  `ID_OBJECT` int(8) NOT NULL DEFAULT '0' COMMENT 'Объект',
  `DEFAULT_VALUE` varchar(255) DEFAULT NULL COMMENT 'Значение по умолчанию',
  `ID_SELECTOR_OBJECT` int(8) DEFAULT NULL COMMENT 'Значение берется из объекта',
  `caption` varchar(255) NOT NULL COMMENT 'Описание',
  PRIMARY KEY (`ID_PROPERTY`),
  KEY `ID_OBJECT` (`ID_OBJECT`,`ID_PROPERTY`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 PACK_KEYS=0 COMMENT='Пользовательские свойства';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `da_object_property`
--

LOCK TABLES `da_object_property` WRITE;
/*!40000 ALTER TABLE `da_object_property` DISABLE KEYS */;
/*!40000 ALTER TABLE `da_object_property` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `da_object_property_value`
--

DROP TABLE IF EXISTS `da_object_property_value`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `da_object_property_value` (
  `ID_PROPERTY` int(8) NOT NULL DEFAULT '0',
  `ID_REFERENCE_ELEMENT` int(8) DEFAULT NULL,
  `VALUE` text,
  `ID_OBJECT_INSTANCE_VAL` int(8) DEFAULT NULL,
  `id_object_instance` int(8) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID_PROPERTY`,`id_object_instance`),
  KEY `ID_PROPERTY` (`ID_PROPERTY`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `da_object_property_value`
--

LOCK TABLES `da_object_property_value` WRITE;
/*!40000 ALTER TABLE `da_object_property_value` DISABLE KEYS */;
/*!40000 ALTER TABLE `da_object_property_value` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `da_object_view`
--

DROP TABLE IF EXISTS `da_object_view`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `da_object_view` (
  `id_object_view` varchar(255) NOT NULL COMMENT 'id',
  `id_object` varchar(255) NOT NULL COMMENT 'Объект',
  `name` varchar(255) NOT NULL COMMENT 'Имя представления',
  `order_no` int(8) DEFAULT '1' COMMENT 'п/п',
  `visible` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Видимость в панели',
  `sql_select` varchar(255) DEFAULT NULL COMMENT 'SELECT',
  `sql_from` varchar(255) DEFAULT NULL COMMENT 'FROM',
  `sql_where` varchar(255) DEFAULT NULL COMMENT 'WHERE',
  `sql_order_by` varchar(255) DEFAULT NULL COMMENT 'ORDER BY',
  `count_data` int(8) NOT NULL DEFAULT '50' COMMENT 'Количество отображаемых результатов на страницу',
  `icon_class` varchar(255) DEFAULT NULL COMMENT 'css-класс иконки',
  `id_parent` varchar(255) DEFAULT NULL COMMENT 'Иерархия по полю',
  `description` longtext COMMENT 'Описание представления',
  PRIMARY KEY (`id_object_view`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Представление';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `da_object_view`
--

LOCK TABLES `da_object_view` WRITE;
/*!40000 ALTER TABLE `da_object_view` DISABLE KEYS */;
INSERT INTO `da_object_view` VALUES ('10','28','Значения справочника',1,0,NULL,NULL,NULL,'id_reference ASC',50,NULL,NULL,NULL),('100','261','Баннерные места',1,1,NULL,NULL,NULL,'sequence ASC',50,'glyphicon glyphicon-import','id_parent',NULL),('101','260','Баннеры',1,0,NULL,NULL,NULL,'sequence ASC',50,NULL,NULL,NULL),('11','29','Группы системных параметров',1,1,NULL,NULL,NULL,'id_group_system_parameter DESC',50,NULL,NULL,NULL),('12','30','Настройки сайта',1,1,NULL,NULL,NULL,'id_group_system_parameter ASC',50,'glyphicon glyphicon-cog',NULL,NULL),('13','31','Домены сайта',1,1,NULL,NULL,NULL,'id_domain ASC',50,'glyphicon glyphicon-pushpin',NULL,NULL),('15','33','Формат сообщения',1,1,NULL,NULL,NULL,'id_event_format DESC',50,NULL,NULL,NULL),('16','34','Подписчики на события',1,1,NULL,NULL,NULL,NULL,50,'glyphicon glyphicon-envelope',NULL,NULL),('17','35','Тип события',1,1,NULL,NULL,NULL,'id_event_type DESC',50,NULL,NULL,NULL),('19','37','Файлы',1,1,NULL,NULL,NULL,'id_file ASC',50,NULL,NULL,NULL),('2','20','Объекты',1,1,NULL,NULL,NULL,'sequence ASC',50,'glyphicon glyphicon-th-large','parent_object',NULL),('2000','500','Фотогалереи',1,1,NULL,NULL,NULL,'id_photogallery ASC',50,'glyphicon glyphicon-picture','id_parent',NULL),('2001','501','Фотографии',1,0,NULL,NULL,NULL,'id_photogallery_photo ASC',50,NULL,NULL,NULL),('2002','502','Новости',1,1,NULL,NULL,NULL,'date DESC',50,'glyphicon glyphicon-bullhorn',NULL,NULL),('2003','503','Новости » Категории',1,1,NULL,NULL,NULL,NULL,50,NULL,NULL,NULL),('2004','505','Консультации » Вопросы',1,0,'IF(LENGTH(ask)<300, ask, CONCAT(SUBSTR(ask, 1, 300), \'...\')) AS short_ask',NULL,NULL,'ask_date DESC',50,NULL,NULL,NULL),('2005','506','Консультации » Ответ',1,0,'IF(LENGTH(answer)<300, answer, CONCAT(SUBSTR(answer, 1, 300), \'...\')) AS short_answer',NULL,NULL,'answer_date DESC',50,NULL,NULL,NULL),('2006','507','Консультации » Специалисты',1,0,NULL,NULL,NULL,'name ASC',50,NULL,NULL,NULL),('2007','508','Консультации » Специализации',1,0,NULL,NULL,NULL,'specialization ASC',50,NULL,NULL,NULL),('2008','509','Каталог продукции',1,1,NULL,NULL,NULL,NULL,50,'glyphicon glyphicon-gift','id_parent',NULL),('2010','511','Продукция',1,0,NULL,NULL,NULL,'id_product ASC',50,NULL,NULL,NULL),('2011','512','Вопрос-ответ',1,1,NULL,NULL,NULL,'ask_date DESC',50,'glyphicon glyphicon-retweet',NULL,NULL),('2016','517','Обратная связь',1,1,NULL,NULL,NULL,'id_feedback DESC',50,'glyphicon glyphicon-share-alt',NULL,NULL),('2017','519','Заказы пользователей',1,1,NULL,NULL,NULL,'create_date DESC',50,'glyphicon glyphicon-shopping-cart',NULL,NULL),('2018','520','Витрина',1,1,NULL,NULL,NULL,'sequence ASC',50,'glyphicon glyphicon-fullscreen',NULL,NULL),('2019','521','Викторины',1,1,NULL,NULL,NULL,'id_quiz DESC',50,NULL,NULL,NULL),('2020','522','Вопросы викторины',1,0,NULL,NULL,NULL,'sequence ASC',50,NULL,NULL,NULL),('2021','523','Варианты ответов',1,0,NULL,NULL,NULL,'sequence ASC',50,NULL,NULL,NULL),('2022','524','Ответ пользователя',1,0,NULL,NULL,NULL,'create_date DESC',50,NULL,NULL,NULL),('2023','525','Брэнды',1,1,NULL,NULL,NULL,'sequence ASC',50,'glyphicon glyphicon-bookmark','id_parent',NULL),('2024','529','Статусы остатка продукции',1,1,NULL,NULL,NULL,'id_remain_status ASC',50,NULL,NULL,NULL),('2025','530','Отзывы клиентов',1,1,NULL,NULL,NULL,'create_date DESC',50,'glyphicon glyphicon-book',NULL,NULL),('2026','531','Уведомления',1,1,NULL,NULL,NULL,'date DESC',50,NULL,NULL,NULL),('21','39','Типы файлов',1,1,NULL,NULL,NULL,'id_file_type ASC',50,NULL,NULL,NULL),('22','40','Расширения файлов',1,1,NULL,NULL,NULL,'id_file_extension ASC',50,NULL,NULL,NULL),('22-view-main','22','Типы данных',1,1,NULL,NULL,NULL,'sequence',50,NULL,NULL,NULL),('23','41','Пользовательские свойства',1,1,NULL,NULL,NULL,NULL,50,NULL,NULL,NULL),('3','21','Свойства объекта',1,0,NULL,NULL,NULL,'sequence ASC',50,NULL,NULL,NULL),('31','49','События',1,1,NULL,NULL,NULL,'id_event DESC',50,NULL,NULL,NULL),('32','50','Почтовые аккаунты',1,1,NULL,NULL,NULL,'host ASC',50,'glyphicon glyphicon-inbox',NULL,NULL),('33','51','Планировщик',1,1,NULL,NULL,NULL,'id_job ASC',50,'glyphicon glyphicon-calendar',NULL,NULL),('35','54','Доступные локализации',1,1,NULL,NULL,NULL,'id_localization ASC',50,NULL,NULL,NULL),('41','61','Инструкции',1,1,NULL,NULL,NULL,'num_seq ASC',50,NULL,NULL,NULL),('43','63','Представление',1,0,NULL,NULL,NULL,'order_no ASC',50,NULL,NULL,'<p>Для получения возможности редактирования экземпляров объекта (осуществления CRUD-операций) необходимо создать представление. Один объект может иметь множество представлений. Представление хранит информацию о свойствах объекта, отображаемых в списке экземпляров, а также о порядке их сортировки.</p>  <p>Быстро создать представление для объекта и его колонки можно при редактировании объекта в блоке Создать представление. Подробнее читай в <a target=\"_blank\" href=\"http://www.ygin.ru/documentation/sozdanie-predstavlenii-obekta-17/\">документации</a> к системе.</p>'),('44','66','Колонка представления',1,0,NULL,NULL,NULL,'order_no ASC',50,NULL,NULL,NULL),('50','80','php-скрипты',1,0,NULL,NULL,NULL,'id_php_script_type ASC',50,NULL,NULL,NULL),('52','86','Интерфейс php-скрипта',1,1,NULL,NULL,NULL,'id_php_script_interface ASC',50,NULL,NULL,NULL),('55','101','Виджеты &raquo; Наборы виджетов',1,1,NULL,NULL,NULL,'id_module_template ASC',50,'glyphicon glyphicon-tags',NULL,NULL),('57','103','Виджеты &raquo; Виджеты сайта',1,1,NULL,NULL,NULL,'name ASC',50,'glyphicon glyphicon-tag',NULL,NULL),('58','105','Голосование',1,1,NULL,NULL,NULL,'create_date DESC',50,'icon-check',NULL,NULL),('59','106','Ответы на голосование',1,0,NULL,NULL,NULL,NULL,50,NULL,NULL,NULL),('6','24','Пользователи',1,1,NULL,NULL,NULL,NULL,50,'glyphicon glyphicon-user',NULL,NULL),('67','250','Комментарии',1,1,'id_object, id_instance',NULL,NULL,'comment_date ASC',50,'glyphicon glyphicon-comment','id_parent',NULL),('7','25','Типы прав доступа',1,1,NULL,NULL,NULL,NULL,50,NULL,NULL,NULL),('9','27','Справочники',1,1,NULL,NULL,NULL,'id_reference ASC',50,'glyphicon glyphicon-book',NULL,NULL),('project-kolichestvo-vody-view-main','project-kolichestvo-vody','Количество воды',1,0,NULL,NULL,NULL,'id_water_count DESC',50,NULL,NULL,''),('project-vid-vody-view-main','project-vid-vody','Вода',1,1,NULL,NULL,NULL,'sequence ASC',50,NULL,NULL,''),('project-zakaz-view-main','project-zakaz','Заказ',1,1,NULL,NULL,NULL,'date DESC',50,NULL,NULL,NULL),('ygin-invoice-view-main','ygin-invoice','Счета',1,1,NULL,NULL,NULL,'create_date DESC',50,NULL,NULL,NULL),('ygin-menu-view-main','ygin-menu','Меню',1,1,'external_link, go_to_type, alias, id_parent, content',NULL,NULL,'sequence ASC',50,'glyphicon glyphicon-list-alt','id_parent',NULL);
/*!40000 ALTER TABLE `da_object_view` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `da_object_view_column`
--

DROP TABLE IF EXISTS `da_object_view_column`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `da_object_view_column` (
  `id_object_view_column` varchar(255) NOT NULL COMMENT 'id',
  `id_object_view` varchar(255) NOT NULL COMMENT 'Представление',
  `id_object` varchar(255) NOT NULL COMMENT 'Объект',
  `caption` varchar(255) DEFAULT NULL COMMENT 'Заголовок колонки',
  `order_no` int(8) NOT NULL DEFAULT '1' COMMENT 'п/п',
  `id_object_parameter` varchar(255) DEFAULT NULL COMMENT 'Параметр объекта',
  `id_data_type` int(8) NOT NULL DEFAULT '2' COMMENT 'Тип данных',
  `field_name` varchar(255) NOT NULL COMMENT 'Имя поля',
  `handler` varchar(255) DEFAULT NULL COMMENT 'Обработчик',
  `visible` tinyint(1) DEFAULT '1' COMMENT 'Видимость',
  PRIMARY KEY (`id_object_view_column`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Колонка представления';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `da_object_view_column`
--

LOCK TABLES `da_object_view_column` WRITE;
/*!40000 ALTER TABLE `da_object_view_column` DISABLE KEYS */;
INSERT INTO `da_object_view_column` VALUES ('105','50','80','Доступен',0,'334',9,'active',NULL,1),('106','50','80','Интерфейс',1,'337',7,'id_php_script_interface',NULL,1),('107','50','80','Путь к скрипту',3,'339',2,'file_path',NULL,1),('11','6','24','Имя пользователя',1,'93',2,'full_name',NULL,1),('112','52','86','Название',1,'348',2,'name',NULL,1),('115','ygin-menu-view-main','ygin-menu','Название в меню',1,'2',2,'name',NULL,1),('116','ygin-menu-view-main','ygin-menu',NULL,2,'5',10,'visible','3',1),('117','55','101','Название шаблона',1,'13',2,'name',NULL,1),('118','55','101','Шаблон по умолчанию',2,'16',9,'is_default_template',NULL,1),('119','50','80','Имя',2,'342',2,'description',NULL,1),('12','6','24','Группа пользователя',2,'89',7,'id_group',NULL,0),('120','57','103','Видимость',3,'546',9,'is_visible',NULL,1),('121','57','103',NULL,2,'544',10,'link','4',1),('122','57','103','Имя',1,'542',2,'name',NULL,1),('123','58','105','Название голосования',1,'531',2,'name',NULL,1),('124','58','105','Активное',2,'534',9,'is_active',NULL,1),('125','58','105','Показать в модуле',3,'535',9,'in_module',NULL,1),('126','58','105','Дата создания голосования',4,'536',4,'create_date',NULL,1),('127','59','106','Вариант ответа',1,'539',2,'name',NULL,1),('13','6','24','Логин',3,'90',2,'name',NULL,1),('14','6','24','Дата регистрации пользователя',4,'239',4,'create_date',NULL,1),('145','67','250','Автор',1,'1204',2,'comment_name',NULL,1),('146','67','250','Дата',2,'1206',4,'comment_date',NULL,1),('147','67','250','Статус',5,'1208',6,'moderation','',1),('148','67','250','Ссылка',3,NULL,10,'-','443',0),('149','50','80','',5,NULL,10,'cache','5',0),('15','7','25','name',1,'97',2,'name',NULL,1),('150','57','103',NULL,3,NULL,10,'cache','5',0),('170','100','261','Заголовок группы',1,'1312',2,'title',NULL,1),('171','100','261','Тип показа баннеров',2,'1313',6,'showing',NULL,1),('172','101','260','Текстовое описание',1,'1304',2,'alt',NULL,1),('173','101','260','Ссылка на сайт',2,'1303',2,'link',NULL,1),('2','2','20','Имя объекта',1,'64',2,'name',NULL,1),('20','9','27','Название справочника',1,'110',2,'name',NULL,1),('21','10','28','Справочник',1,'112',7,'id_reference',NULL,1),('22','10','28','Значение элемента',2,'113',1,'id_reference_element',NULL,1),('22-view-main-name','22-view-main','22','Название типа данных',1,'86',2,'name',NULL,1),('23','10','28','Имя значения',3,'114',2,'value',NULL,1),('24','11','29','Название',1,'49',2,'name',NULL,1),('25','12','30','Группа параметров',1,'117',7,'id_group_system_parameter',NULL,1),('26','12','30','Значение параметра',2,'119',2,'value',NULL,1),('27','12','30','Описание',3,'120',2,'note',NULL,1),('28','13','31','Путь к содержимому домена',1,'282',2,'domain_path',NULL,1),('29','13','31','Доменное имя',2,'283',2,'name',NULL,1),('3','3','21','Объект',1,'75',7,'id_object',NULL,1),('30','13','31','Описание',3,'284',2,'description',NULL,1),('31','15','33','Описание',1,'31',2,'description',NULL,1),('32','16','34','Тип события',1,'37',7,'id_event_type',NULL,1),('33','16','34','E-mail адрес',2,'42',2,'email',NULL,1),('34','16','34','Имя подписчика',3,'44',2,'name',NULL,1),('35','17','35','Название',1,'51',2,'name',NULL,1),('36','17','35','Объект для работы',2,'245',7,'id_object',NULL,1),('37','6','24','Активен',5,'122',9,'active',NULL,1),('39','19','37','Путь к файлу',1,'135',2,'file_path',NULL,1),('4','3','21','Тип свойства',2,'76',7,'id_parameter_type',NULL,1),('40','19','37','Объект',2,'138',7,'id_object',NULL,1),('41','21','39','Название',1,'130',2,'name',NULL,1),('42','22','40','Тип файла',1,'133',7,'id_file_type',NULL,1),('43','22','40','Расширение',2,'132',2,'ext',NULL,1),('44','23','41','Описание',1,'298',2,'caption',NULL,1),('45','23','41','Имя свойства',2,'299',2,'NAME',NULL,1),('46','23','41','Тип свойства',3,'294',7,'ID_PROPERTY_TYPE',NULL,1),('47','23','41','Объект',4,'295',7,'ID_OBJECT',NULL,1),('5','3','21','Виджет',3,'78',2,'widget',NULL,1),('6','3','21','Описание свойства',4,'79',2,'caption',NULL,1),('60','31','49','Тип события',1,'254',7,'id_event_type',NULL,1),('6000','2000','500','Название',1,'1501',2,'name',NULL,1),('6001','2001','501','Файл',1,'1510',8,'file',NULL,1),('6002','2001','501','Название',16,'1508',2,'name',NULL,1),('6003','2000','500','Фото',20,NULL,10,'-','1001',1),('6004','2002','502','Категория',3,'1514',7,'id_news_category',NULL,1),('6005','2002','502','Заголовок',2,'1517',2,'title',NULL,1),('6006','2002','502','Дата',4,'1516',4,'date',NULL,1),('6008','2002','502','Фото',1,'1519',8,'photo',NULL,1),('6009','2002','502','Видимость',14,'1520',9,'is_visible',NULL,1),('6010','2003','503','Название',1,'1522',2,'name',NULL,1),('6011','2003','503','Видимость',13,'1524',9,'is_visible',NULL,1),('6012','2004','505','Спрашивает',1,'1526',2,'user_fio',NULL,1),('6014','2004','505','Дата',1,'1530',4,'ask_date',NULL,1),('6015','2004','505','Вопрос',1,'1528',14,'short_ask',NULL,1),('6016','2004','505','Видимость',12,'1532',9,'is_visible',NULL,1),('6017','2005','506','Отвечающий',2,'1534',7,'id_consultation_answerer',NULL,1),('6018','2005','506','Отвечающий (ручной ввод)',3,'1535',2,'answerer',NULL,1),('6019','2005','506','Дата',1,'1536',4,'answer_date',NULL,1),('6020','2005','506','Ответ',4,'1538',3,'short_answer',NULL,1),('6021','2005','506','IP',5,'1539',2,'ip',NULL,1),('6022','2006','507','ФИО отвечающего',2,'1547',2,'name',NULL,1),('6023','2006','507','e-mail',3,'1546',2,'email',NULL,1),('6024','2006','507','Фото',1,'1544',8,'photo',NULL,1),('6025','2007','508','Описание',2,'1549',14,'description',NULL,1),('6026','2007','508','Специализация',1,'1551',2,'specialization',NULL,1),('6027','2002','502','Комменты',10,NULL,10,'-','250',0),('6028','2004','505','Комменты',10,NULL,10,'-','250',1),('6029','2008','509','Название',2,'1553',2,'name',NULL,1),('6030','2008','509','Наценка (%)',3,'1557',1,'price_markup',NULL,1),('6033','2010','511','Название',3,'1567',2,'name',NULL,1),('6036','2010','511','Розничная цена',4,'1570',1,'retail_price',NULL,1),('6037','2010','511','Изображение',1,'1576',8,'image',NULL,1),('6038','2010','511','Код',2,'1566',1,'code',NULL,1),('6039','2011','512','Дата',1,'1579',4,'ask_date',NULL,1),('6040','2011','512','Спрашивающий',1,'1580',2,'name',NULL,1),('6041','2011','512','Вопрос',1,'1581',14,'question',NULL,1),('6043','2011','512','Видимость',1,'1585',9,'visible',NULL,1),('6056','2016','517','ФИО',1,'1610',2,'fio',NULL,1),('6057','2016','517','Сообщение',1,'1613',14,'message',NULL,1),('6058','2016','517','Дата сообщения',1,'1614',4,'date',NULL,1),('6059','2010','511',NULL,5,NULL,10,'-','1001',1),('6060','2017','519','ФИО',2,'1621',2,'fio',NULL,1),('6061','2017','519','e-mail',3,'1623',2,'mail',NULL,1),('6062','2017','519','Заказ',4,'1625',14,'offer_text',NULL,1),('6063','2017','519','Обработано',5,'1627',9,'is_process',NULL,1),('6064','2018','520','Фото',3,'1634',8,'image',NULL,1),('6065','2018','520','Ссылка на переход',2,'1631',2,'link',NULL,0),('6066','2018','520','Заголовок',1,'1632',2,'title',NULL,0),('6068','101','260','Видимость',3,'1309',9,'visible',NULL,1),('6069','2019','521','name',1,'1639',2,'name',NULL,1),('6070','2019','521','active',1,'1642',9,'active',NULL,1),('6071','2020','522','Викторина',1,'1644',7,'id_quiz',NULL,1),('6072','2020','522','Текст вопроса',1,'1646',3,'question',NULL,1),('6073','2020','522','Тип ответов',1,'1647',6,'type',NULL,1),('6074','2021','523','Вопрос',1,'1650',7,'id_quiz_question',NULL,1),('6075','2021','523','Текст ответа',1,'1651',2,'answer',NULL,1),('6076','2021','523','Правильный ли ответ',1,'1652',9,'is_right',NULL,1),('6077','2022','524','Викторина',1,'1655',7,'id_quiz',NULL,1),('6078','2022','524','ФИО',1,'1656',2,'name',NULL,1),('6079','2022','524','Дата',1,'1661',4,'create_date',NULL,1),('6080','2008','509','Фото',1,'1555',8,'image',NULL,1),('6081','2017','519','Дата',1,'1626',4,'create_date',NULL,1),('6082','2008','509','Видимость',4,'1637',9,'visible',NULL,1),('6083','2010','511','Видимость',6,'1663',9,'visible',NULL,1),('6084','2023','525','Название',1,'1667',2,'name',NULL,1),('6085','2023','525','Логотип брэнда',1,'1669',8,'image',NULL,1),('6086','2024','529','Название',1,'1680',2,'name',NULL,1),('6087','2025','530','ФИО',1,'1685',2,'name',NULL,1),('6088','2025','530','Дата',1,'1686',4,'create_date',NULL,1),('6089','2025','530','Текст отзыва',1,'1687',14,'review',NULL,1),('6090','2025','530','Видимость на сайте',1,'1689',9,'visible',NULL,1),('6091','2026','531','Текст',1,'1694',14,'text',NULL,1),('6092','2026','531','Дата создания',1,'1695',4,'date',NULL,1),('6093','67','250','Комментарий',4,'1207',10,'comment_text','1046',1),('6094','ygin-menu-view-main','ygin-menu','Видимость',23,'5',9,'visible',NULL,1),('61','31','49','Дата создания события',2,'256',4,'event_create',NULL,1),('62','32','50','E-mail для поля \"От\"',1,'259',2,'email_from',NULL,1),('63','32','50','Имя отправителя',2,'260',2,'from_name',NULL,1),('64','32','50','HOST',3,'262',2,'host',NULL,1),('65','33','51','Имя задачи',1,'274',2,'name',NULL,1),('66','33','51','Дата последнего запуска',2,'271',4,'last_start_date',NULL,1),('67','33','51','Дата следущего запуска',3,'272',4,'next_start_date',NULL,1),('68','33','51','Количество ошибок',4,'273',1,'failures',NULL,1),('69','33','51','Имя класса задачи',5,'276',2,'class_name',NULL,1),('7','3','21','Имя поля в БД',5,'80',2,'field_name',NULL,1),('70','33','51','Вкл.',6,'291',9,'active',NULL,1),('71','35','54','Код',1,'312',2,'code',NULL,1),('72','35','54','Название',2,'311',2,'name',NULL,1),('73','35','54','Используется ли локализация',3,'313',9,'is_use',NULL,1),('79','13','31','Активен',4,'304',9,'active',NULL,1),('8','3','21','NOT NULL',6,'84',9,'not_null',NULL,1),('85','41','61','Название',1,'141',2,'name',NULL,1),('86','41','61','Видимость',2,'144',9,'visible',NULL,1),('87','41','61','Относится только к этому сайту',3,'143',9,'desc_type',NULL,1),('88','43','63','Объект',1,'401',7,'id_object',NULL,1),('89','43','63','Имя представления',2,'406',2,'name',NULL,1),('9','2','20','Таблица',0,'67',2,'table_name',NULL,1),('90','44','66','Представление',1,'416',7,'id_object_view',NULL,1),('91','44','66','Заголовок колонки',2,'418',2,'caption',NULL,1),('92','44','66','Параметр объекта',3,'420',7,'id_object_parameter',NULL,1),('93','44','66','Тип данных',4,'421',7,'id_data_type',NULL,1),('94','44','66','Имя поля',5,'422',2,'field_name',NULL,1),('95','44','66','Видимость',6,'424',9,'visible',NULL,1),('project-kolichestvo-vody-view-main-count','project-kolichestvo-vody-view-main','project-kolichestvo-vody','Количество',1,'project-kolichestvo-vody-count',1,'count',NULL,1),('project-kolichestvo-vody-view-main-id-order','project-kolichestvo-vody-view-main','project-kolichestvo-vody','Заказ',1,'project-kolichestvo-vody-id-order',7,'id_order',NULL,1),('project-kolichestvo-vody-view-main-id-water','project-kolichestvo-vody-view-main','project-kolichestvo-vody','Вода',1,'project-kolichestvo-vody-id-water',7,'id_water',NULL,1),('project-vid-vody-view-main-photo','project-vid-vody-view-main','project-vid-vody','Фото',1,'project-vid-vody-photo',8,'photo',NULL,1),('project-vid-vody-view-main-price','project-vid-vody-view-main','project-vid-vody','Цена',3,'project-vid-vody-price',2,'price',NULL,1),('project-vid-vody-view-main-title','project-vid-vody-view-main','project-vid-vody','Название',2,'project-vid-vody-nazvanie',2,'title',NULL,1),('project-zakaz-view-main-date','project-zakaz-view-main','project-zakaz','Дата',1,'project-zakaz-date',4,'date',NULL,1),('project-zakaz-view-main-fio','project-zakaz-view-main','project-zakaz','ФИО',1,'project-zakaz-fio',2,'fio',NULL,1),('project-zakaz-view-main-phone','project-zakaz-view-main','project-zakaz','Телефон',1,'project-zakaz-phone',2,'phone',NULL,1),('project-zakaz-view-main-subscriber-number','project-zakaz-view-main','project-zakaz','Абонентский номер',1,'project-zakaz-subscriber-number',2,'subscriber_number',NULL,1),('ygin-faq-view-categoryQuestion','2011','512','Категория',0,'ygin-faq-category',6,'category','',1),('ygin-invoice-view-main-amount','ygin-invoice-view-main','ygin-invoice','Сумма',1,'ygin-invoice-amount',1,'amount',NULL,1),('ygin-invoice-view-main-create-date','ygin-invoice-view-main','ygin-invoice','Дата создания',1,'ygin-invoice-create-date',4,'create_date',NULL,1),('ygin-invoice-view-main-id-offer','ygin-invoice-view-main','ygin-invoice','Заказ',1,'ygin-invoice-id-offer',7,'id_offer',NULL,1),('ygin-invoice-view-main-pay-date','ygin-invoice-view-main','ygin-invoice','Дата оплаты',1,'ygin-invoice-pay-date',4,'pay_date',NULL,1);
/*!40000 ALTER TABLE `da_object_view_column` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `da_php_script`
--

DROP TABLE IF EXISTS `da_php_script`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `da_php_script` (
  `id_php_script` int(8) NOT NULL AUTO_INCREMENT,
  `id_php_script_type` varchar(255) NOT NULL,
  `id_module` int(8) DEFAULT NULL,
  `params_value` longtext,
  PRIMARY KEY (`id_php_script`)
) ENGINE=InnoDB AUTO_INCREMENT=1036 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `da_php_script`
--

LOCK TABLES `da_php_script` WRITE;
/*!40000 ALTER TABLE `da_php_script` DISABLE KEYS */;
INSERT INTO `da_php_script` VALUES (1016,'1032',1007,NULL),(1023,'1042',1014,'a:7:{s:11:\"htmlOptions\";s:32:\"array(\'class\' => \'nav nav-list\')\";s:14:\"activeCssClass\";s:6:\"active\";s:12:\"itemCssClass\";s:4:\"item\";s:11:\"encodeLabel\";s:5:\"false\";s:18:\"submenuHtmlOptions\";s:46:\"array(\'class\' => \'nav nav-list sub-item-list\')\";s:13:\"maxChildLevel\";s:1:\"2\";s:12:\"baseTemplate\";s:42:\"<div class=\"b-menu-side-list\">{menu}</div>\";}'),(1027,'1045',1000,'a:0:{}'),(1028,'1039',NULL,'a:0:{}'),(1029,'project-php-knopka-kupit',NULL,NULL),(1030,'project-php-o-kompanii',NULL,NULL),(1031,'1045',NULL,'a:0:{}'),(1035,'1005',NULL,'a:1:{s:7:\"maxNews\";i:3;}');
/*!40000 ALTER TABLE `da_php_script` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `da_php_script_interface`
--

DROP TABLE IF EXISTS `da_php_script_interface`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `da_php_script_interface` (
  `id_php_script_interface` int(8) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `sequence` int(3) NOT NULL DEFAULT '1' COMMENT 'Порядок',
  `name` varchar(255) NOT NULL COMMENT 'Название',
  `template` longtext COMMENT 'Шаблон файла',
  PRIMARY KEY (`id_php_script_interface`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='Интерфейс php-скрипта';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `da_php_script_interface`
--

LOCK TABLES `da_php_script_interface` WRITE;
/*!40000 ALTER TABLE `da_php_script_interface` DISABLE KEYS */;
INSERT INTO `da_php_script_interface` VALUES (1,1,'Контент раздела сайта','class <<class_name>> extends ViewModule {\r\n  public function run() {\r\n\r\n  }\r\n}'),(2,2,'Контент модуля сайта','class <<class_name>> extends ViewModule {\r\n  public function run() {\r\n\r\n  }\r\n}'),(6,6,'Колонка представления','<?php\r\n\r\nclass <<class_name>> implements ColumnType {\r\n\r\n  public function init($idObject, Array $arrayOfIdInstances, $idObjectParameter) {\r\n\r\n  }\r\n  \r\n  public function process($idInstance, $value, $idObjectParameter) {\r\n    return $value;\r\n  }\r\n  \r\n  public function getStyleClass() {\r\n    return null;\r\n  }\r\n}\r\n\r\n?>'),(9,9,'Визуальный элемент','class <> extends VisualElement implements DaVisualElement {\r\n\r\n public function callBeforeProcessInstance() {\r\n\r\n }\r\n\r\n public function callAfterProcessInstance() {\r\n\r\n }\r\n\r\n protected function validate() {\r\n return true;\r\n }\r\n\r\n public function getValueFromClient() {\r\n return null;\r\n }\r\n}');
/*!40000 ALTER TABLE `da_php_script_interface` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `da_php_script_type`
--

DROP TABLE IF EXISTS `da_php_script_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `da_php_script_type` (
  `id_php_script_type` varchar(255) NOT NULL COMMENT 'id',
  `file_path` varchar(255) NOT NULL COMMENT 'Путь к скрипту',
  `description` varchar(255) DEFAULT NULL COMMENT 'Название скрипта',
  `id_php_script_interface` int(8) NOT NULL COMMENT 'Интерфейс',
  `active` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Активен',
  PRIMARY KEY (`id_php_script_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='php-скрипты';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `da_php_script_type`
--

LOCK TABLES `da_php_script_type` WRITE;
/*!40000 ALTER TABLE `da_php_script_type` DISABLE KEYS */;
INSERT INTO `da_php_script_type` VALUES ('1000','project/plugin/photogallery/PhotogalleryView.php','Фотогалерея » Раздел сайта',1,1),('1001','photogallery.backend.column.PhotoColumn','Фотогалерея » Колонка представления',6,1),('1002','project/plugin/search/SearchView.php','Поиск по всему сайту',1,1),('1003','project/plugin/news/NewsView.php','Новости » Список',1,1),('1004','project/plugin/news/NewsCategoryView.php','Новости » Список в категории',1,1),('1005','news.widgets.news.NewsWidget','Новости » Список последних на главной',2,1),('1006','project/plugin/consultation/ConsultationView.php','Консультации',1,1),('1007','project/plugin/internet_magazin/OfferFormView.php','Интернет-магазин » Форма оформления заказа',1,1),('1008','project/plugin/internet_magazin/CatalogView.php','Интернет-магазин » Список товаров',1,1),('1009','project/plugin/internet_magazin/CatalogModuleView.php','Интернет-магазин » Модуль категорий каталога',2,0),('1010','shop.widgets.cart.CartWidget','Интернет-магазин » Корзина',2,0),('1012','project/plugin/faq/FaqView.php','Вопрос-ответ',1,1),('1027','project/plugin/internet_magazin/CatalogMainView.php','Интернет-магазин » Главная страница магазина',1,1),('1029','project/plugin/comment/CommentModerationView.php','Модерирование комментариев',1,1),('1032','user.widgets.login.LoginWidget','Авторизация',2,1),('1037','feedback.widgets.FeedbackWidget','Кнопка обратной связи',2,1),('1038','shop.widgets.category.CategoryWidget','Интернет-магазин » Категории',2,0),('1039','ygin.widgets.vitrine.VitrineWidget','Витрина',2,1),('1040','banners.widgets.specialOffer.SpecialOfferWidget','Спецпредложения',2,0),('1041','vote.widgets.VoteWidget','Голосование',2,0),('1042','menu.widgets.MenuWidget','Меню',2,1),('1043','shop.widgets.brand.BrandWidget','Интернет-магазин » Брэнды',2,0),('1044','ygin.widgets.cloudim.CloudimWidget','Онлайн-консультации Cloudim',2,0),('1045','photogallery.widgets.randomPhoto.RandomPhotoWidget','Случайное фото',2,1),('1046','backend.components.column.abstract.StrippedColumn','Текст без тегов и заменой nl на br',6,1),('250','comments.backend.CommentsColumn','Комментарии » Комментарии экземпляра',6,1),('3','menu.backend.column.InfoStatus','Статус раздела в объекте Меню',6,1),('4','menu.backend.column.SiteModuleInfoStatus','Статус модуля сайта',6,1),('443','comments.backend.CommentsViewLinkColumn','Комментарии » Ссылка на объект',6,1),('5','engine/admin/column/abstract/PhpScriptTypeCacheClear.php','Кэш в базе',6,1),('project-php-knopka-kupit','application.widgets.purchase.PurchaseWidget','Кнопка купить',2,1),('project-php-o-kompanii','application.widgets.carousel.CarouselWidget','Карусель',2,1),('ygin-banner-widget-base','ygin.modules.banners.widgets.BannerWidget','Баннер',2,0);
/*!40000 ALTER TABLE `da_php_script_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `da_plugin`
--

DROP TABLE IF EXISTS `da_plugin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `da_plugin` (
  `id_plugin` int(8) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(255) NOT NULL COMMENT 'Название',
  `code` varchar(255) NOT NULL COMMENT 'Код',
  `status` int(8) NOT NULL DEFAULT '1' COMMENT 'Статус',
  `config` longtext COMMENT 'Сериализованные настройки',
  `class_name` varchar(255) NOT NULL COMMENT 'Класс плагина',
  `data` longtext COMMENT 'data',
  PRIMARY KEY (`id_plugin`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COMMENT='Плагины системы';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `da_plugin`
--

LOCK TABLES `da_plugin` WRITE;
/*!40000 ALTER TABLE `da_plugin` DISABLE KEYS */;
INSERT INTO `da_plugin` VALUES (5,'Новости','ygin.news',2,'a:1:{s:7:\"modules\";a:1:{s:9:\"ygin.news\";a:1:{s:14:\"showCategories\";b:1;}}}','ygin.modules.news.NewsPlugin','a:8:{s:25:\"id_php_script_type_module\";i:1005;s:37:\"id_php_script_type_module_param_count\";i:1002;s:17:\"id_sysytem_module\";i:1002;s:9:\"id_object\";i:502;s:18:\"id_object_category\";i:503;s:23:\"id_menu_module_template\";s:1:\"6\";s:7:\"id_menu\";s:3:\"134\";s:14:\"id_site_module\";s:3:\"127\";}'),(6,'Поиск по сайту','ygin.search',3,'a:1:{s:7:\"modules\";a:1:{s:11:\"ygin.search\";a:0:{}}}','ygin.modules.search.SearchPlugin',NULL),(7,'Обратная связь','ygin.feedback',3,'a:1:{s:7:\"modules\";a:1:{s:13:\"ygin.feedback\";a:1:{s:11:\"idEventType\";s:3:\"108\";}}}','ygin.modules.feedback.FeedbackPlugin','a:4:{s:25:\"id_php_script_type_module\";i:1037;s:17:\"id_sysytem_module\";i:1005;s:9:\"id_object\";i:517;s:17:\"site_module_place\";a:2:{i:0;a:3:{s:5:\"place\";s:1:\"1\";s:8:\"sequence\";s:1:\"1\";s:18:\"id_module_template\";s:1:\"2\";}i:1;a:3:{s:5:\"place\";s:1:\"1\";s:8:\"sequence\";s:1:\"2\";s:18:\"id_module_template\";s:1:\"4\";}}}'),(8,'Фотогалерея','ygin.photogallery',2,'a:1:{s:7:\"modules\";a:1:{s:17:\"ygin.photogallery\";a:0:{}}}','ygin.modules.photogallery.PhotogalleryPlugin','a:9:{s:17:\"id_object_gallery\";i:500;s:15:\"id_object_photo\";i:501;s:14:\"handler_column\";i:1001;s:10:\"useGallery\";b:1;s:23:\"id_menu_module_template\";s:1:\"3\";s:16:\"id_system_module\";i:1000;s:38:\"id_php_script_type_widget_random_photo\";i:1045;s:22:\"id_widget_random_photo\";s:3:\"123\";s:7:\"id_menu\";s:3:\"127\";}'),(9,'Интернет-магазин','ygin.shop',3,'a:1:{s:7:\"modules\";a:1:{s:9:\"ygin.shop\";a:10:{s:8:\"pageSize\";i:10;s:19:\"displayTypeElements\";s:3:\"all\";s:15:\"viewProductList\";s:19:\"_product_list_table\";s:9:\"showPrice\";b:1;s:17:\"subCategoryOnMain\";b:1;s:19:\"imageCategoryOnMain\";b:1;s:16:\"makeOfferByOrder\";b:0;s:11:\"showToolbar\";b:0;s:16:\"useOnlinePayment\";b:0;s:19:\"idEventTypeNewOffer\";s:3:\"104\";}}}','ygin.modules.shop.ShopPlugin','a:16:{s:34:\"id_php_script_type_module_category\";i:1038;s:30:\"id_php_script_type_module_cart\";i:1010;s:50:\"id_php_script_type_module_cart_param_visible_count\";i:1028;s:31:\"id_php_script_type_module_brand\";i:1043;s:17:\"id_sysytem_module\";i:1004;s:17:\"id_object_product\";i:511;s:18:\"id_object_category\";i:509;s:15:\"id_object_offer\";i:519;s:15:\"id_object_brand\";i:525;s:19:\"idEventTypeNewOffer\";s:3:\"104\";s:23:\"id_menu_module_template\";s:1:\"5\";s:23:\"id_object_remain_status\";i:529;s:17:\"id_object_invoice\";s:17:\"id_object_invoice\";s:26:\"site_module_place_category\";a:0:{}s:22:\"site_module_place_cart\";a:0:{}s:23:\"site_module_place_brand\";a:0:{}}'),(10,'Вопрос-ответ','ygin.faq',2,'a:1:{s:7:\"modules\";a:1:{s:8:\"ygin.faq\";a:7:{s:8:\"pageSize\";i:15;s:8:\"moderate\";b:1;s:8:\"sendMail\";b:0;s:13:\"useCategories\";b:0;s:17:\"idEventTypeAnswer\";N;s:23:\"idEventSubscriberAnswer\";N;s:11:\"idEventType\";s:3:\"103\";}}}','ygin.modules.faq.FaqPlugin','a:4:{s:9:\"id_object\";i:512;s:23:\"id_menu_module_template\";s:1:\"3\";s:13:\"id_event_type\";s:3:\"103\";s:7:\"id_menu\";s:3:\"120\";}'),(11,'Опросы','ygin.vote',3,'a:1:{s:7:\"modules\";a:1:{s:9:\"ygin.vote\";a:4:{s:13:\"expiredTimout\";i:24;s:13:\"checkByCookie\";b:1;s:9:\"checkByIp\";b:1;s:9:\"numVoteIp\";i:1;}}}','ygin.modules.vote.VotePlugin','a:6:{s:25:\"id_php_script_type_module\";i:1041;s:17:\"id_sysytem_module\";i:1013;s:16:\"id_object_voting\";i:105;s:23:\"id_object_voting_answer\";i:106;s:23:\"id_menu_module_template\";N;s:17:\"site_module_place\";a:3:{i:0;a:3:{s:5:\"place\";s:1:\"1\";s:8:\"sequence\";s:1:\"2\";s:18:\"id_module_template\";s:1:\"1\";}i:1;a:3:{s:5:\"place\";s:1:\"1\";s:8:\"sequence\";s:1:\"3\";s:18:\"id_module_template\";s:1:\"4\";}i:2;a:3:{s:5:\"place\";s:1:\"1\";s:8:\"sequence\";s:1:\"1\";s:18:\"id_module_template\";s:1:\"6\";}}}'),(12,'Баннеры','ygin.banners',3,'a:1:{s:7:\"modules\";a:1:{s:12:\"ygin.banners\";a:0:{}}}','ygin.modules.banners.BannerPlugin','a:3:{s:17:\"id_sysytem_module\";i:300;s:22:\"id_object_banner_place\";i:261;s:16:\"id_object_banner\";i:260;}'),(13,'Спецпредложения','ygin.specoffers',3,'a:0:{}','ygin.modules.banners.widgets.specialOffer.SpecialOfferPlugin','a:4:{s:25:\"id_php_script_type_module\";i:1040;s:37:\"id_php_script_type_module_param_place\";i:1016;s:17:\"id_sysytem_module\";i:1012;s:17:\"site_module_place\";a:1:{i:0;a:3:{s:5:\"place\";s:1:\"4\";s:8:\"sequence\";s:1:\"2\";s:18:\"id_module_template\";s:1:\"4\";}}}'),(14,'Витрина','ygin.vitrine',2,'a:0:{}','ygin.widgets.vitrine.VitrinePlugin','a:4:{s:25:\"id_php_script_type_module\";i:1039;s:17:\"id_sysytem_module\";i:1011;s:9:\"id_object\";i:520;s:14:\"id_site_module\";s:3:\"120\";}'),(15,'Онлайн-консультант','ygin.cloudim',3,'a:1:{s:10:\"components\";a:1:{s:13:\"widgetFactory\";a:1:{s:7:\"widgets\";a:1:{s:13:\"CloudimWidget\";a:1:{s:8:\"htmlCode\";s:0:\"\";}}}}}','ygin.widgets.cloudim.CloudimPlugin','a:3:{s:25:\"id_php_script_type_module\";i:1044;s:17:\"id_sysytem_module\";i:1015;s:17:\"site_module_place\";a:0:{}}'),(16,'Отзывы','ygin.review',3,'a:1:{s:7:\"modules\";a:1:{s:11:\"ygin.review\";a:3:{s:8:\"pageSize\";i:15;s:8:\"moderate\";b:1;s:11:\"idEventType\";i:107;}}} ','ygin.modules.review.ReviewPlugin','a:2:{s:9:\"id_object\";i:530;s:23:\"id_menu_module_template\";s:1:\"1\";}'),(17,'Карта сайта','ygin.siteMap',1,NULL,'ygin.modules.siteMap.SiteMapPlugin',NULL),(18,'Личный кабинет','ygin.cabinet',2,'a:1:{s:7:\"modules\";a:1:{s:9:\"ygin.user\";a:4:{s:18:\"idEventTypeRecover\";s:3:\"105\";s:24:\"idEventSubscriberRecover\";s:1:\"1\";s:19:\"idEventTypeRegister\";s:3:\"106\";s:14:\"cabinetEnabled\";b:1;}}}','ygin.modules.user.CabinetPlugin','a:12:{s:35:\"id_php_script_type_widget_authorize\";i:1032;s:16:\"id_system_module\";i:1007;s:11:\"idMenuLogin\";s:3:\"141\";s:14:\"idMenuRegister\";s:3:\"142\";s:13:\"idMenuRecover\";s:3:\"143\";s:13:\"idMenuProfile\";s:3:\"144\";s:10:\"idMenuUser\";s:3:\"145\";s:18:\"idEventTypeRecover\";s:3:\"105\";s:24:\"idEventSubscriberRecover\";s:1:\"1\";s:19:\"idEventTypeRegister\";s:3:\"106\";s:19:\"id_widget_authorize\";i:108;s:14:\"cabinetEnabled\";b:1;}');
/*!40000 ALTER TABLE `da_plugin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `da_reference_element`
--

DROP TABLE IF EXISTS `da_reference_element`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `da_reference_element` (
  `id_reference` varchar(255) NOT NULL DEFAULT '0' COMMENT 'Справочник',
  `id_reference_element` int(8) NOT NULL DEFAULT '0' COMMENT 'Значение элемента',
  `value` varchar(255) NOT NULL COMMENT 'Описание значения',
  `image_element` varchar(150) DEFAULT NULL COMMENT 'Картинка для значения',
  `id_reference_element_instance` varchar(255) NOT NULL COMMENT 'id',
  PRIMARY KEY (`id_reference_element_instance`),
  UNIQUE KEY `id_reference` (`id_reference`,`id_reference_element`),
  UNIQUE KEY `id_reference_2` (`id_reference`,`id_reference_element`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Значения справочника';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `da_reference_element`
--

LOCK TABLES `da_reference_element` WRITE;
/*!40000 ALTER TABLE `da_reference_element` DISABLE KEYS */;
INSERT INTO `da_reference_element` VALUES ('50',1,'Баннеры меняются поочерёдно в заданном порядке',NULL,'100'),('50',2,'Баннеры меняются в произвольном порядке',NULL,'101'),('50',3,'Вывод всех баннеров',NULL,'102'),('30',1,'ASC',NULL,'16'),('30',2,'DESC',NULL,'17'),('31',1,'Стандартный',NULL,'18'),('31',3,'Контроллер',NULL,'19'),('32',1,'Слева',NULL,'20'),('32',2,'Справа',NULL,'21'),('32',3,'Снизу',NULL,'22'),('32',4,'Сверху',NULL,'23'),('32',5,'В контенте',NULL,'24'),('32',7,'Сверху в контенте',NULL,'25'),('32',95,'В архиве',NULL,'26'),('37',1,'Раздел',NULL,'38'),('37',2,'Модуль',NULL,'39'),('37',3,'Другое',NULL,'40'),('38',1,'JsHttpRequest',NULL,'41'),('38',2,'XAJAX',NULL,'42'),('38',3,'jQuery',NULL,'43'),('38',4,'ValumsFileUpload',NULL,'44'),('2',1,'В тексте письма',NULL,'5'),('33',1,'Отобразить список вложенных разделов при отсутствии контента',NULL,'50'),('100',1,'один верный',NULL,'500'),('100',2,'много верных',NULL,'501'),('100',3,'произвольный',NULL,'502'),('32',6,'Подвал',NULL,'503'),('101',1,'Новый',NULL,'504'),('101',2,'Согласован',NULL,'505'),('101',3,'Оплачен',NULL,'506'),('101',4,'Выполнен',NULL,'507'),('101',5,'Отменен',NULL,'508'),('33',4,'При отсутствии контента не выводить предупреждение, отобразить пустую страницу',NULL,'509'),('33',2,'Переходить к первому вложенному разделу при отсутствии контента',NULL,'51'),('33',3,'Открыть первый загруженный файл при отсутствии контента',NULL,'52'),('2',2,'Во вложении',NULL,'6'),('ygin-comment-reference-status',1,'Отмодерирован',NULL,'ygin-comment-reference-status-approved'),('ygin-comment-reference-status',3,'Удален',NULL,'ygin-comment-reference-status-deleted'),('ygin-comment-reference-status',2,'Ожидает модерации',NULL,'ygin-comment-reference-status-pending'),('ygin-faq-reference-categoryQuestion',1,'Общие',NULL,'ygin-faq-reference-categoryQuestion-general'),('ygin-faq-reference-categoryQuestion',2,'Личные',NULL,'ygin-faq-reference-categoryQuestion-personal'),('33',5,'Вывести список вложенных разделов после контента',NULL,'ygin-menu-content-show-included-items-after-content'),('33',6,'Вывести список вложенных разделов перед контентом',NULL,'ygin-menu-content-show-included-items-before-content'),('31',5,'Ссылка',NULL,'ygin-object-reference-type-link');
/*!40000 ALTER TABLE `da_reference_element` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `da_references`
--

DROP TABLE IF EXISTS `da_references`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `da_references` (
  `id_reference` varchar(255) NOT NULL COMMENT 'id',
  `name` varchar(100) NOT NULL COMMENT 'Название справочника',
  PRIMARY KEY (`id_reference`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Справочники';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `da_references`
--

LOCK TABLES `da_references` WRITE;
/*!40000 ALTER TABLE `da_references` DISABLE KEYS */;
INSERT INTO `da_references` VALUES ('100','Тип вопроса в викторине'),('101','Статус заказа пользователя'),('2','Расположение данные в почтовом сообщении'),('30','Типы порядка'),('31','Тип объекта'),('32','Положение модулей'),('33','Дополнительные варианты обработки контента в разделе'),('37','Место использования AJAX'),('38','AJAX-движки'),('50','Тип показа баннеров'),('ygin-comment-reference-status','Статусы комментария'),('ygin-faq-reference-categoryQuestion','Категории вопросов');
/*!40000 ALTER TABLE `da_references` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `da_search_data`
--

DROP TABLE IF EXISTS `da_search_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `da_search_data` (
  `id_object` varchar(255) NOT NULL,
  `id_instance` int(8) NOT NULL,
  `id_lang` int(8) NOT NULL,
  `value` longtext NOT NULL,
  PRIMARY KEY (`id_object`,`id_instance`,`id_lang`),
  KEY `id_object` (`id_object`),
  FULLTEXT KEY `value` (`value`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `da_search_data`
--

LOCK TABLES `da_search_data` WRITE;
/*!40000 ALTER TABLE `da_search_data` DISABLE KEYS */;
INSERT INTO `da_search_data` VALUES ('ygin-menu',140,1,'Оборудование для розлива воды Пить полезную и вкусную воду из 19 л бутылей   удобно!\r\nЧтобы получить от нашей воды не только пользу, но и удовольствие, попробуйте использовать специально разработанное для бутилированной воды.\r\nПомпа. Ручной насос, подает воду порциями при каждом нажатии. Незаменимый помощник для удобного потребления воды дома.\r\nНаклонная подставка. Бутыль располагается на подставке горлышком вниз, на которое одевается пробка с краником. Легким нажатием на краник можно быстро и легко наполнить водой нужную емкость. Хит в школах и детских садах.\r\nКулеры для воды. Современный кулер   это устройство для розлива, подогрева и охлаждения питьевой воды. Без него уже почти невозможно приготовления офисной кружки горячего чая или кофе. В нашей компании только качественные аппараты от лидеров рынка Bioray, AEL, Hotfrost, сервисное обслуживание и послегарантийный ремонт.Доставка кулеров юридическим лицам бесплатно.'),('ygin-menu',141,1,'Устав Устав '),('ygin-menu',142,1,'Муниципальные задания Муниципальные задания '),('ygin-menu',100,1,'Главная Дорогие друзья!'),('ygin-menu',115,1,'О компании'),('ygin-menu',116,1,'Контакты Наша страничка вконтакте: http://vk.com/club42586416\r\nКонтакты компании  Эколайн :\r\nОфис компании  Эколайн  в Сыктывкар: ул. Ленина, 118, тел. (8212) 202 802e mail: info@ecoline komi.ru\r\nДиректор компании   Игнатов Михаил ИвановичОфис компании  Эколайн  в Ухте: ул. Оплеснина, д. 20, тел. (8216) 76 25 25e mail: ekoline_uhta@list.ru\r\nЗаказ воды с доставкой в офис или на дом\r\nПрием заказов с 8.00 до 20.00   в будни, с 8.00 до 17.00   в cубботу и воскресенье,тел. в Сыктывкаре (8212) 202 802, в Ухте (8216) 76 25 25\r\nОтдел по продаже воды в ПЭТ тареРуководитель отдела в Сыктывкаре   Столярова Наталья Ивановна, тел. (8212) 21 61 12Прием заказов : ежедневно с 8.30 до 17.00Руководитель отдела продаж воды в ПЭТ таре в Ухте   (8216) 76 25 26\r\nОтдел продвиженияРуководитель отдела   Анисовец Жанна Ивановна, тел. (8212) 202 802\r\nОтдел продажРуководитель отдела в Сыктывкаре   Филипова Наталья Васильевна тел. (8212) 202 802\r\nФирменные магазиныАдреса : Сыктывкар, ул. Ленина, д. 28, тел. (8212) 579 240 Cыктывкар, Димитрова, д.48, тел. (8212) 57 97 90Ухта, ул. 40 лет Коми, д. 10, тел.\r\nСервисный центрРежим работы : понедельник пятница с 8.30 до 17.30, тел. в Сыктывкаре (8212) 21 16 09Руководитель   Кокис Андрей Петрович, адрес: Станционная, 64.\r\nСервисный центр в Ухте тел. (8216) 76 25 26\r\nДилеры в Сосногорске:\r\nТелефон доставки воды 54 888, 9 06 46 (сотовый)Прием заказов:ежедневно: с 9.00 до 17.00Доставка воды:ежедневно: с 9.00 до 13.00, с 14.00 до 18.00Пункт обмена воды  Краснозатонская Серебряная  г. Сосногорск, пер к Сосновский, 2Режим работы   ежедневно с 9.00 до 18.00\r\nМагазины  Чай Кофе \r\n Чай Кофе  в пункте обмена   Сыктывкар, ул. Ленина, 28, тел. 57 92 40 Чай Кофе  в пункте обмена   Сыктывкар, Димитрова, 48, тел. 57 97 90\r\n'),('ygin-menu',114,1,'Контакты Контакты \r\nТелефоны:Москва:  7(916) 833 15 26 Сыктывкар:  7(8212) 39 14 81, 39 14 68 Ухта:  7(904) 863 88 05 Киров:  7(908) 717 57 92\r\nАдрес: Республика Коми, г. Сыктывкар ул. Перовмайская 62, оф. блок  А , 5 этаж, офис 505А\r\nЭлектронная почта:\r\nweb@cvek.ru\r\n '),('ygin-menu',144,1,'Наши библиотеки Наши библиотеки Корткеросская центральная библиотека им. М.Н. Лебедева\r\nКорткеросская центральная детская библиотека\r\nСторожевский филиал\r\nАджеромский филиал\r\nБогородский филиал\r\nБольшелугский филиал\r\nВизябожский филиал\r\nВомынский филиал им. А.А. Сухановой\r\nДодзьский филиал\r\nКересский филиал\r\nНамский филиал\r\nМаджский филиал им. Ф.Ф. Павленкова\r\nМординский филиал им. Ф.Ф. Павленкова\r\nНебдинский филиал\r\nНившерский филиал им. Ф.Ф. Павленкова\r\nПезмегский филиал\r\nПодтыбокский филиал\r\nПодъельский филиал\r\nПозтыкеросский филиал\r\nПриозерный филиал\r\nУсть Лэкчимский филиал\r\n '),('ygin-menu',145,1,'Справочно правовая система Справочно правовая система '),('ygin-menu',150,1,'Комп_1 Компания  Эколайн  – один из лидеров в отрасли производства питьевых и минеральных вод в России, ведущий производитель питьевой воды высшей категории в республике Коми.'),('ygin-menu',151,1,'Комп_2 Миссия компании:\r\n• быть лидерами в обеспечении жителей Республики Коми питьевой бутилированной водой;• пропагандировать потребление чистой воды как элемента здорового образа жизни;• предоставлять нашим клиентам продукцию стабильно высокого качества и комфортный сервис, которые они будут рекомендовать друзьям и знакомым;• находить пути совершенствования продукции и услуг, максимально учитывая потребности наших клиентов;• заботиться о сотрудниках компании, заслугой которых является успех компании в долгосрочной перспективе'),('ygin-menu',152,1,'Комп_3 Истории Компании\r\nКаждое утро так уважительно приветствуют друг друга партнеры по бизнесу компании Эколайн . Относиться с почтением к тем, кого ценишь, уважаешь и кому действительно желаешь здоровья и долгих лет жизни   добрая российская традиция. Почему она прижилась в  Эколайне , понимаешь, когда ближе знакомишься с учредителями этой фирмы.Из ученых   в предприниматели Кандидат физико математических наук Михаил Иванович Игнатов долгое время работал научным сотрудником одного из институтов Коми научного центра. Читать далее (Далее текст открывается в окне прокрутки) Перестройка круто изменила жизнь ученого. Чтобы прокормить семью, пришлось уйти из науки в бизнес. Но привычка   вторая натура. Вот и сохранилось с тех пор доброжелательное, уважительное отношение к своим коллегам, партнерам, собеседникам, среди которых преобладают бывшие и нынешние ученые, врачи, преподаватели, инженеры. Михаил Иванович не только им желает доброго здравия, но и всем своим землякам. Цель его бизнеса   забота о нашем с вами здоровье. Нет, он не занимается врачебной практикой, он просто приучает нас с рождения пить чистую, физиологически полноценную воду, доставляя ее в наши квартиры. В Коми он был пионером этого нового вида деятельности. Причина для того, чтобы заняться производством и доставкой воды сыктывкарцам, оказалась самой что ни на есть житейской. У одного из друзей Михаила Ивановича   заядлого аквариумиста   в один печальный день погибли все рыбки. А ведь он в своем огромном аквариуме заменил одно единственное ведро воды… Супруги Игнатовы как раз в это время ожидали пополнения семейства и были просто обескуражены произошедшим.  Я как представил, что нашему новорожденному малышу придется такой водой разводить молочные смеси, готовить на ней пюре и кашки, мурашки побежали по спине,   вспоминает Михаил Иванович.   Нет, решил, надо срочно что то делать! Как то в разговоре его знакомый Руслан Черемискин высказал идею о бутилированной воде и попал, что называется, в самую точку, задел за живое. Так Руслан Геннадьевич и Михаил Иванович, ударив по рукам, стали партнерами по бизнесу. Конечно, поначалу пришлось много поездить по российским городам и весям, добывать информацию, изучать производство, прежде чем наладить в Сыктывкаре подобное. К делу подходили с дотошностью ученых мужей: листали справочники, делали расчеты и обоснования.Поиск источника Чтобы найти место, где разместить предприятие, советовались со специалистами местной санэпидемслужбы, с геологами. Уж они то о воде все знают. Самым подходящим оказалось местечко Красный Затон неподалеку от города. Это единственный поселок в черте Сыктывкара, который с начала 60 х снабжается артезианской водой. (Кстати, в советское время вся верхушка партноменклатуры Коми пила исключительно краснозатонскую воду, которую доставляли партийным боссам флягами, а на их дачи в местечко Лемью из Затона был протянут водопровод.) В дома остальных горожан вода, как и прежде, поступает из реки. Да и к тому же на окраине поселка нашлось подходящее для производства помещение бывшего речного порта. Вопроса, как назвать воду, не возникло   конечно, Краснозатонская . Самой главной трудностью в начале пути было приучить людей покупать чистую воду и отказаться от употребления воды из под крана. В цивилизованных странах две трети населения для питья и приготовления пищи используют только бутилированную воду, даже не смотря на то, что стоит она недешево. Предприниматели поступили просто: первые бутыли с очищенной  Краснозатонской  водой развезли по офисам и домам своих друзей и знакомых. Предложили: попробуйте заварить чай или кофе на нашей воде   совсем другой вкус! Что называется, почувствуйте разницу.Так, буквально с нескольких бутылей, началась история компании  Эколайн . С тех пор прошло уже более 13 лет.Хронология развития  Эколайн  1998   2011 гг.октябрь 1998   организация компании;23 декабря 1998   начало производства воды  Краснозатонская серебряная  (19л);апрель 2002   открытие нового цеха по выпуску воды (19л);сентябрь 2002   установлена американская линия по разливу воды Baby Works (200 бутылей в час);февраль 2003   получен сертификат на воду высшей категории качества  Краснозатозанская серебряная ;март 2003   открылся первый пункт обмена воды в Сыктывкаре;март 2003   запуск барамембранной установки по очистке воды;март 2003   налажено производство йодированной воды  Будущий гений ;март 2003   получен сертификат на воду высшей категории качества  Краснозатозанская йодированная ;декабрь 2003   в системе очистки воды появилась станция озонирования;23 декабря 2003   открылось представительство компании  Эколайн  в Ухте;октябрь 2004   открылся первый магазин  Чай Кофе ;июнь 2005   начал работать участок производства по разливу воды в ПЭТ тару (0.5л, 1.5л, 5.0л);январь 2007   установлена новая, линия по разливу воды  Steelhead  (250 бутылей в час);февраль 2007   начало производства минеральной воды  Сереговская ;29 марта 2010   открытие второго фирменного магазина (ул.Димитрова, 48);20 января 2011   запуск новой, более мощной линии розлива воды в ПЭТ тару;18 апреля 2011   открытие нового цеха по выпуску воды (19 л и ПЭТ тара);1 сентября 2011   запуск автоматической линии по розливу воды 5 10 литров в ПЭТ таре.'),('ygin-menu',153,1,'Прод_1 Прод_1'),('ygin-menu',154,1,'Прод_2 Прод_2'),('ygin-menu',155,1,'Проз_1 Завод Завод.\r\nНаше производство располагается в 12 км от Сыктывкара в поселке Краснозатонский. Цех по производству воды   это современный, высокотехнологичный комплекс, который был построен в 2011 году. Его площадь более 2000 кв.м.'),('ygin-menu',156,1,'Комп_4 Социальная ответственностьКомпания  Эколайн  всегда принимает активное участие в общественной жизни Республики Коми. Постоянно реализуются проекты по поддержке общественных инициатив и организации питьевого режима в сфере спорта, культуры и образования. Активная работа с представителями различных социальных групп и объединений помогает нам предлагать свою помощь именно там, где она необходима.\r\n'),('ygin-menu',157,1,'Произ_2 Схема водоподготовки и розлива питьевой воды Схема водоподготовки и розлива питьевой воды'),('ygin-menu',158,1,'Произ_3 Водоподготовка Водоподготовка.\r\nПроизводство воды стартует с этапа водоподготовки. Вода добывается с глубины 50 метров, проходит многоступенчатую очистку с последующей корректировкой по минеральному составу. Это позволяет убрать из воды все взвешенные частицы и железо, а затем скорректировать ее состав в соответствии с нормативами физиологической полноценности.'),('ygin-menu',159,1,'Произ_4 Розлив воды в 19л бутыли Розлив воды в 19л бутыли.\r\nВода разливается в 19 литровые бутыли из поликарбоната, которые разработаны специально для хранения питьевой воды, экологически безопасны и подлежат многократному использованию. Предварительно бутыли моются на автоматической линии  Steelhead  при высокой температуре (65 70ºС), что позволяет обеспечить их абсолютную стерильность. После помывки бутыли стерилизуются гиперозонированной водой, т.е. водой с большим содержанием озона. Разливается и укупоривается вода автоматически, без контакта с руками человека.'),('ygin-menu',160,1,'Произ_5 Контроль качества воды Контроль качества воды.\r\nКонтроль качества воды производится несколько раз в день и подтверждается ежедневными анализами Роспотребнадзора. Склад. Удобный складской комплекс рассчитан на хранение и погрузку продукции поддонами. Он отвечает самым высоким требованиям, предъявляемым к хранению бутилированной воды: регулярная санитарная обработка полов, контроль температуры и влажности.'),('ygin-menu',161,1,'Прод_3'),('ygin-menu',162,1,'Прод_4'),('ygin-menu',163,1,'Прод_5'),('ygin-menu',164,1,'Прод_6'),('ygin-menu',165,1,'Нов_1 6 ноября мы запустили в продажу часть бутылок с водой 19 л  Краснозатонская Серебряная  с новыми этикетками. Постепенно все старые этикетки будут заменены новыми, в том числе и на маленьких бутылках в ПЭТ таре, которые вы сможете приобрести в любом магазине.этикетка 4,8х15см\r\nС новой этикеткой наш продукт выглядит более современным и ярким. А Вы легко отличите нашу питьевую воду высшей категории от других вод, большинство из которых более низкого качества   воды питьевые первой категории или воды питьевые столовые.\r\nИзменилась только этикетка, а в бутылках по прежнему Ваша любимая вкусная и полезная вода  Краснозатонская Серебряная  производства Компании  Эколайн .'),('ygin-menu',166,1,'Нов_2'),('ygin-menu',167,1,'Первый раз вода в подарок Первый раз вода в подарок'),('ygin-menu',168,1,'Приведи друга Приведи друга'),('ygin-menu',169,1,'Помпа'),('ygin-menu',170,1,'Кулеры'),('ygin-menu',171,1,'Санитарная обработка'),('ygin-menu',172,1,'Прочее оборудование'),('ygin-menu',173,1,'Школам и садам'),('ygin-menu',174,1,'Скидка'),('ygin-menu',175,1,'Две бутылки qwqweqweqweqwe'),('ygin-menu',176,1,'Сайты для детей Сайты для детей Детский портал  Солнышко \r\nПортал для детей и любящих их взрослых. Конкурсы, сценарии, flash игры, мульты, раскраски, загадки ...\r\nhttp://www.solnet.ee/\r\nПочемуЧка   Сайт для детей и их родителей\r\nДетям о профессиях. Почемучка. Праздничные стенгазеты. Всё для детского сада. Всё для школы.\r\nhttp://pochemu4ka.ru/\r\nТеремок – детский сайт\r\nМультики, игры онлайн для малышей, загадки. Дети будут здесь учиться, играть и веселиться.\r\nhttp://www.teremoc.ru/\r\nМягкие игрушки своими руками\r\nВыкройки, схемы вязания, инструкции по изготовлению. Азбука рукоделия: кройка и шитье, азы вязания и вышивания, оформление игрушек. История игрушки.\r\nhttp://www.hand made toys.net/\r\nЛохматик\r\nСайт для детей. Бесплатные детские развивающие игры. Красивые раскраски. Обводилки для детей. Настольные игры распечатай и играй. Игры лабиринты, лабиринты бесплатно. Развитие речи дошкольников. Другая интересная и полезная информация.\r\nhttp://www.lohmatik.ru/\r\n Ералаш \r\nПриглашаем мальчишек и девчонок, а так же их родителей, посетить детский информационно развлекательный портал киножурнала  ЕРАЛАШ . Участвуйте в конкурсах, общайтесь на форуме и находите новых друзей!\r\nhttp://www.eralash.ru\r\nДетский ресурс для бесплатного скачивания развивающих и обучающих игр, программ, сказок, раскрасок, картинок, тестов, загадок, текстов, песен детской музыки, статей для беременных и молодых мам, книг, детских мультфильмов, фильмов и много полезной информации, как для детей, так и для родителей.\r\nhttp://baby best.ru/\r\nМатрешкино Семейство   Портал для всей семьи\r\nПосвящен народным традициям, культуре и ориентирован на аудиторию   для всей семьи. На сайте можно узнать о народных традициях и о русском фольклоре. Обряды, советы и приметы, пословицы и поговорки, загадки и сказки.\r\nhttp://matreshkino.ru/\r\nМузыкальный сайт для детей и родителей\r\nМузыкальная грамота для детей в сказках. Музыкальные инструменты. Рассказы о великих композиторах.\r\nhttp://www.muz urok.ru/\r\nДетский университет\r\nСайт бесплатного обучения, где проводятся 30 минутные научно популярные  лекции интерактивы  по различным отраслям знаний. Занятия проходят по выходным дням и в дни школьных каникул. Увлекаешься химией, физикой, биологией, историей, и другими науками? Тогда тебе точно пора в  детский университет ! Изучай любимые предметы не выходя из дома! Совершенствуй свои знания!\r\nwww.childrensuniversity.ru\r\nПроверим эрудицию?\r\nКакова глубина Марианской впадины? Назовите самую маленькую птицу в мире? Сколько планет в Солнечной системе? Отвечать на вопросы и сражаться в интеллектуальной игре с реальными соперниками в режиме реального времени на данном сайте привычное дело! Вы сможете проверить свою эрудицию и узнать много нового! Играйте с удовольствием!\r\nwww.eruditi.ru\r\nСайт Президента России гражданам школьного возраста\r\nЕсли ты хочешь больше узнать о нашем государстве, о демократии, об истории Кремля и российской власти, то этот сайт для тебя! Здесь каждого из нас ждут приключения, игры, загадки, анкеты, задачки, ожившие картинки.\r\nТы можешь задать вопрос Президенту, узнать о символике нашего государства и еще много много много всего интересного.\r\nwww.uznay prezidenta.ru\r\nРоссийское образование. Федеральный портал\r\nСайт будет особенно полезен школьникам старших классов. Здесь можно найти демонстрационные варианты тестов ЕГЭ на текущий год, узнать о федеральных образовательных ресурсах, прочитать рекомендации для абитуриентов, узнать перечень вступительных испытаний и порядок приема в ВУЗы и ССУЗы, уточнить особенности приема в высшие и средние учебные заведения инвалидов, пройти он лайн тесты. Кроме того, здесь представлены нормативные документы и государственные образовательные стандарты.\r\nwww.edu.ru\r\nРебзики\r\nЧем заняться в свободное время? Начать знакомство с  Ребзиками ! На сайте представлено множество игр на развитие внимания и памяти. Любишь рисовать   тогда для тебя раскраски, любишь общаться   заходи в удобный детский чат. Проведи свой досуг с пользой!\r\nwww.rebzi.ru '),('ygin-menu',177,1,'Конкурсы Конкурсы '),('ygin-menu',178,1,'Наши конкурсы Наши конкурсы '),('ygin-menu',179,1,'Конкурсы НДБ им. Маршака Конкурсы НДБ им. Маршака '),('ygin-menu',180,1,'Книжная полка Книжная полка '),('ygin-menu',181,1,'Для детей Для детей \r\nДрагунский, В.Ю. Денискины рассказы. – Москва: Стрекоза,2008. – 62 с.: ил. – (Книга в подарок). \r\nЭту книгу можно по нескольку раз перечитывать и получать от них удовольствие, потому что по настоящему талантливые книги (вот счастье то) не стареют, а наоборот, возвращают нас в детство. Герои книги, Дениска Кораблев и Мишка Слонов – настоящие выдумщики. Чего с ними только не происходит! Они делают трещотку для велосипеда из гусиного горла, умудряются выкрасить краской свою подружку Аленку, выигрывают подписку на журнал  Мурзилка , строят космический корабль и еще много много всего. Дениска Кораблёв   хороший человек, с которым каждому ребёнку хотелось бы подружиться. Он очень доверчивый, так как взрослым людям получается его обмануть....\r\nДениска Кораблёв старается хорошо себя вести, радовать своих родителей   но это ему не всегда удаётся.\r\nГабова, Е.В. Дождь из прошлого века: Провести. – Сыктывкар: Издательство  Титул , 2012. – 280 с.\r\nСборник включает в себя четыре новые повести, еще нигде не опубликованные. Ее новые герои, как и персонажи остальных повестей, – ученики 10 класса. Книга написана для подростков, но она будет интересна и их родителям,   говорит Елена Габова.   Техника, которая нас окружает, стремительно развивается, жизнь становится ярче и удобнее, а душа подростка не меняется: она по прежнему ждет тепла и понимания. \r\nОбразцов П.А. Мы растём: Стихи, рассказы, загадки/ Составитель Е.П.Березина. – Сыктывкар,  Анбур , 2011. – 176 с.\r\nТворчество Пантелеймона Образцова связано с прекрасной северной землей – Республикой Коми. Стихи Пантелеймона Образцова легкие для чтения, содержательные по смыслу, привлекают малышей и их наставников.\r\nПарр, Мария. – Вафельное сердце. – Москва: Самокат, 2008. – 208 с.: ил. – (Серия  Лучшая новая книжка ).\r\nДебют молодой норвежской писательницы Марии Парр, которую критики дружно называют новой Астрид Линдгрен. В год из жизни двух маленьких жителей бухты Щепки Матильды   девятилетнего Трилле, от лица которого ведется повествование, и его соседки и одноклассницы Лены   вмещается немыслимо много событий и приключений   забавных, трогательных, опасных... Идиллическое житье бытье на норвежском хуторе нарушается   но не разрушается – печальными обстоятельствами. Но дружба, конечно же, оказывается сильнее!\r\nПарр, Мария. Тоня Глиммердал. – Москва: Самокат, 2011. – 280 с.: ил. – (Серия  Лучшая новая книжка ).\r\nДетская книга про небольшую норвежскую горную деревню, где остался всего один ребенок, а остальные все взрослые. И этот ребенок, Тоня, творит разные шалости и дела, а потом у нее появляются и друзья сверстники. По настроению очень напоминает повести Астрид Линдгрен, тоже очень доброе повествование, где дети порой помогают взрослым что то понять и решить свои взрослые проблемы.\r\nПеннак Даниэль. – Глаз волка/ пер. с фр. Н.Шаховской. – Москва: Самокат, 2012. – 96 с.: ил. – (Лучшая новая книжка).\r\nОдноглазый полярный волк заперт в клетке парижского зоопарка. Люди принесли ему столько зла, что он поклялся никогда больше не думать о них. Но мальчик по имени Африка, обладающий удивительным даром слушать и рассказывать истории, заставит волка взглянуть на мир другими глазами. Эта книга о дружбе и предательстве, о страхе и самопожертвовании, и о том, как один   единственный случай может навсегда изменить характер и жизнь.\r\nТокмакова И. Поиграем!: Стихи. – Москва: Эксмо,2009. – 104 с.: ил.\r\nВеликолепные детские стихи. Понимаются детьми с 1,5   2 лет. К тому же они полезные. Вот, например, одно из полезнейших стихотворений  Один Дома  внушает ребенку, что мальчику не страшно остаться дому одному, мама и папа ушли в кино, а он дома остался за главного. Хочет чай попьет, хочет телевизор посмотрит, хочет соседям в стенку постучит. Книгу не хочется выпускать из рук! У Токмаковой изюминка скрыта в любом стихотворении, это маленькие шедевры. Здесь и типичные английские песенки, и игривый французский стишок, и русские народные поговорки потешки. Феномен знаменитой поэтессы в том, что она говорит на детском языке. Она ничему не поучает, но дети ее понимают как себя.\r\nЖвалевский А., Пастернак Е. Гимназия № 13: Роман сказка. – Москва: Время, 2010. – 368 с.: ил.\r\nНе надо было трогать дуб! Тогда бы ничего страшного и не случилось. А когда тронули, тут и началось. Из всех щелей полезла нечисть. Домовые и кабинетные   за наших гимназистов, нечисть   против. Перун мечет молнии на крыше, Кощей пытается проломить заколдованный круг, говорящий кот подкармливает русалку ворованной колбасой, второй закон Ньютона временно не работает,  Слово о полку Игореве  встает перед глазами, словно в формате 3D, а на самом деле наяву   помог волшебный растворитель... Хотите дальше? Сами читайте.\r\nКозлова Е.В. Волшебные очки: Повесть сказка. Гöгыля вугыля: Повесть мойд. – Сыктывкар:  Анбур , 2008. – 112 с.: ил.\r\nКто из нас не ждет в жизни чудес? Кто в детстве не мечтал о волшебной силе или хотя бы волшебном предмете? А в этой повести чудеса органично вплетаются в жизнь героев. Что ждет главного героя, мальчика Колю, когда в его руки попадут волшебные очки? И в чем же волшебство этих самых очков? А может и не нужны обыкновенному мальчишке волшебные очки? Вы как думаете?\r\nЖуравлев А. Где живу я, отгадай! – Сыктывкар: Издательство  Титул , 2009. – 60 с.: ил.\r\nИмя Александра Журавлева известно не одному поколению северян. Его стихи – задорные, поучительные, смешные и немного волшебные – стали настоящей детской классикой. В оформлении книги приняли участие ребята, ваши ровесники. Все они живут и учатся в городе Ухта. Рисунки ребят подстать стихам, такие яркие и веселые.\r\nНурдквист Свен. Переполох в огороде. – Москва:  МД Медиа , 2011. – 23 с.:ил.\r\nЧудесным весенним утром в саду у Петсона кипела жизнь. Шум, свист и пение птиц... Пробивалась из земли молодая травка.\r\n  Пора приниматься за работу,   сказал Петсон котенку Финдусу, который носился вокруг дома. На этот раз Петсон с Финдусом решили посадить в огороде овощи. Правда посадить овощи решил Петсон, у Финдуса было свое мнение о том, чем надо заниматься в огороде. Финдус решил посадить тефтельку   вдруг вырастет. Свое мнение было также у кур и коров. Итак, все идет своим чередом, вернее, не совсем своим…\r\nНурдквист Свен. Именинный пирог. – Москва:  МД Медиа , 2011. – 22 с.:ил.\r\nПетсон и его котенок Финдус живут в маленьком домике в небольшой деревушке. У Петсона есть курятник, два сарая и сад. Вокруг деревушки зеленеют луга и поля, а за ними начинается густой лес.\r\nВ этой книге рассказывается о том, как Петсон пек пирог Финдусу, который празднует день рождения три раза в год. Вот такой находчивый котенок. Петсон решил приготовить ему праздничный пирог. Но всё опять пошло кувырком: яйца для пирога разбиваются, лестница падает, мука кончается… Неужели Финдус не получит своего пирога? В конце концов насмешив и удивив всех вокруг Петсон с Финдусом справились со всеми трудностями, испекли пирог и отпраздновали день рождения Финдуса! С удовольствием!\r\n '),('ygin-menu',182,1,'Для родителей Для родителей \r\nГиппенрейтер,Ю. Общаться с ребенком. Как? – Москва: АСТ: Астрель, 2009. – 238 с.: ил.\r\nЮлия Борисовна Гиппенрейтер   профессор Московского Государственного Университета, известный ученый и талантливый педагог, автор многочисленных статей, монографий, учебных пособий. Работает в многообразных областях психологии познания до семейной терапии. Блестящий популяризатор   умеет просто и доступно рассказать о важных достижениях современной психологии.\r\nКнига в легкой и доступной форме рассказывает родителям, как найти подход к ребенку, как его понять и найти с ним дружеский тон общения. В книге, кроме советов о том, как понять ребенка есть еще масса упражнений для родителей для того, чтобы родители нашли в себе проблемы и смогли их решить. Все родители стараются воспитывать своих детей и учить их как нибудь и чему нибудь. А книга Юлии Борисовны заставляет задуматься над методами, которыми мы это делаем. Главное с помощью этой книги можно еще и себя понять и посмотреть на себя со стороны. Единственный минус, чтобы воспитывать детей правильно, трудно в одночасье изменить свои методы и признать свои ошибки. Книга Ю. Гиппенрейтер не про воспитание детей как таковое, а про налаживание отношений с детьми подростками, если они  выбились , про начало развития отношений с маленькими детьми. Одним словом, мудрая книга.\r\nГиппенрейтер,Ю. Продолжаем общаться с ребенком. Так? – Москва: АСТ: Астрель, 2009. – 256 с.: ил.\r\nНастоящая книга расширяет и углубляет темы предыдущей книги автора  Общаться с ребенком. Как?  В новой книге обсуждаются многочисленные вопросы, которые волнуют родителей:  Как его воспитывать? Как приучать к дисциплине? Как наказывать? Как заставить его хорошо учиться?  Разбираются и объясняются новые важные подробности и приемы искусства эффективного общения.\r\nМальцева И.В. Раннее развитие: лучшие методики и игры. – Санкт Петербург: Издательский Дом  Азбука классика , 2009. – 255 с. – (Программа для мамы).\r\nОсновные законы раннего развития – начать обучение как можно раньше, изучить различные методики, выбрать подходящие игры для своего ребенка, и систематически применять их на практике. Книга очень полезная, лаконично описаны разные методики развития – по Доману, Зайцеву, Монтессори. Игр в книге великое множество. Здесь и активные игры (особенно весело переползать через подушку при игре в прятки!), и игры, обучающие счету, чтению и письму. Простые игры с грудничками, обучение детей навыкам самообслуживания: самостоятельное одевание, пользование разными предметами обихода. Оказывается, где бы вы ни находились, везде вы можете позаниматься с малышом — даже в бассейне!\r\nДалее разбор часто задаваемых вопросов и знакомство с нужным инвентарем.\r\nТитова Ю., Фролова О., Винникова Л. Играть с ребенком. Как?: развитие воспитания, памяти, мышления, речи у детей 1 5 лет. – Москва: Эксмо, 2011. – 96 с. – (Учиться? Легко! Советы нейропсихолога).\r\nВ этой книге собраны игры для развития восприятия, памяти и мышления. Развитие этих функций у дошкольников намного важнее, чем даже умение читать и считать. При этом игры не занимают много времени и не требуют особой подготовки. Зато сколько приносят пользы и удовольствия! А играть в них можно, когда вы с ребенком идете домой из садика или готовите вместе с ним обед. Итак, играть всегда, играть везде!\r\nМонтессори М. Мой метод: начальное обучение. – Москва: Астрель: АСТ, 2007. – 349 с.\r\nКнига является первой публикацией на русском языке фундаментального труда выдающегося итальянского педагога, психолога и философа Марии Монтессори. Она была написана почти 100 лет назад, но идеи свободного саморазвития детей в специально обустроенном пространстве актуальны и сегодня. В первой части книги М. Монтессори излагает основные принципы своей педагогической системы: философские, психологические и педагогические основы. Во второй части описываются методы работы с детьми 6   10 летнего возраста, приемы работы при обучении детей грамматике, математике и основам других наук. Интересный упор автора дается и на развитие мышления ребенка, на творческое восприятие предмета. Пособие позволяет проводить занятия с детьми, превращая их в увлекательный досуг и живую беседу, при этом, действительно, развивая ребенка и его чувственное восприятие и внимание на высоком и глубоком уровне.\r\nКнига Монтессори по педагогике и обучению детей дошкольного и младшего школьного возраста до сих пор с успехом используется многими педагогами и родителями. В книге приведены черно белые иллюстрации, демонстрирующие, как необходимо правильно составить и нарисовать задание ребенку. Все иллюстрации преподаватель сам без труда может рисовать на обычной доске или листе бумаги, и предлагать детям внимательно анализировать увиденное и решать задания.\r\nГруэн С. Воды слонам. – М.:Гаятри, 2011. – 400с.\r\nСтарику Якобу Янковскому, обитающему теперь в доме престарелых, есть что вспомнить: во времена Великой депрессии судьба забросила его, студента ветеринара, в передвижной  Цирк Братьев Бензини . Парад алле, клоуны, дрессированные львы и слоны, карлики и силачи, кровь и пот, фанфары и крики  браво! . Закулисье цирка оказывается вовсе не таким чарующим и прекрасным, как представлялось Якобу сначала. Однако именно здесь он встречает лучших друзей, злейших врагов и ту единственную, ради которой можно вытерпеть любые унижения и пойти на подвиг.\r\n Воды слонам!    бестселлер американской писательницы Сары Груэн   книга правдивая и гротескная, горькая и смешная одновременно. Созданный в ней удивительный цирковой мир не отпускает читателя до последней страницы. Итак, почтеннейшая публика, спешите видеть! Занавес открывается!..\r\nГалактионова В. Спящие от печали. – М.:АСТ:Астрель, 2011. – 607 с.\r\n Спящие от печали    это повесть о жизни в небольшом азиатском селении Столбцы. Повесть буквально сплетена из снов его обитателей, где они переживают вновь и вновь свои неудачи, утраты, страхи. Само место   тоже, словно порождение сна:  Эта негодная местность считалась у тюрков Воротами ветра, а ветры зарождаются и вертятся духами опасными, непонятными . Здесь  в азиатской России, русской Азии  словно затаилась сама жизнь  в темноте, обступившей … со всех сторон , здесь  затаилось будущее . Спящих, несомненно, ждет пробуждение   его предвестником становится странствующий монах Порфирий, чье появление в Столбцах приносит покой их жителям. После ухода Порфирия над Столбцами вдруг взмывают голуби, что служит добрым предзнаменованием.\r\nМэтьюз О. Антисоветский роман. – М.:Астрель, 2010. –384 с.\r\nИзвестный британский журналист Оуэн Мэтьюз — наполовину русский, и именно о своих русских корнях он написал эту книгу, ставшую мировым бестселлером и переведенную на 22 языка. Мэтьюз учился в Оксфорде, а после работал репортером в горячих точках — от Югославии до Ирака. Значительная часть его карьеры связана с Россией: он много писал о Чечне, работал в The Moscow Times, а ныне возглавляет московское бюро журнала Newsweek.\r\nРассказывая о драматичной судьбе трех поколений своей семьи, Мэтьюз делает особый акцент на необыкновенной истории любви его родителей. Их роман начался в 1963 году, когда отец Оуэна Мервин, приехавший из Оксфорда в Москву по студенческому обмену, влюбился в дочь расстрелянного в 37 м коммуниста, Людмилу. Советская система и всесильный КГБ разлучили влюбленных на целых шесть лет, но самоотверженный и неутомимый Мервин ценой огромных усилий и жертв добился триумфа —  антисоветская  любовь восторжествовала.\r\nТрелина В. Жила была девочка: Повесть о детстве, прошедшем в СССР. – М.:АСТ:АСТ;СПб.:Астрель:СПб, 2011. – 317 с.\r\nЭто и в самом деле необычная книга    Жила была девочка  Виктории Трелиной. Необычность ее парадоксальна: такую книгу мог бы написать, наверное, каждый третий из тех, чье детство и юность пришлись на период с конца 70 х и до 1991 года. Мог бы – да что то не написал...\r\nА вот Виктории Трелиной это удалось блестяще, а особый вкус этой автобиографической книге придают многочисленные иллюстрации, являющиеся составной и неотъемлемой частью текста – от факсимиле билетиков на автобус за 5 коп.и  Торжественного обещания пионера Советского Союза  до рисунков из школьных альбомов 80 х и первых постеров Патрисии Каас...\r\nХорошая и добрая книга Виктории Трелиной необходима всем, кто хочет не просто вспомнить свое октябрятско пионерское детство и комсомольскую юность, но и как то рассказать о том времени своим детям.\r\nМайрон В. Дьюи. Кот из библиотеки, который потряс весь мир. – М.:ЗАО Центрполиграф, 2009. – 255 с.\r\nКакие переживания может вынести животное? Сколько жизней у кошки? Как получилось, что несчастный котенок подкидыш сделал маленькую библиотеку местом встречи окрестных жителей и центром притяжения для туристов, а провинциальный американский городок — известным во всем мире? В самом ли деле мы спасаем бесприютных животных, принимая в свой дом, или, наоборот, эти бессловесные существа становятся для нас опорой в преодолении житейских невзгод и трудностей? Об этом и многом другом в потрясающей книге Вики Майрон, которая сумела тронуть душу миллионов читателей во всех уголках планеты.\r\nМайрон В. Девять жизней Дьюи. Наследники кота из библиотеки, который потряс весь мир. –М.:ЗАО Издательство Центрполиграф, 2011. – 350 с.\r\nТрогательная история о рыжем коте из библиотеки городка Спенсер, описанная в книге Вики Майрон  Дьюи , вызвала миллионы восторженных отзывов. Читатели так прониклись атмосферой душевного тепла, которое дарил людям Дьюи, что вдохновили Вики Майрон написать продолжение. В новой книге – девять историй о котах, которые объединили людей, подарили им надежду и помогли справиться с жизненными трудностями, научили радоваться, любить и сострадать. Две истории посвящены всемирно известному Дьюи. Книга заставляет смеяться, плакать и удивляться, каким волшебным образом пушистые любимцы преображают нашу жизнь.\r\nДашкова П.В. Пакт. – М.:Астрель, 2012. – 574 с.\r\nДействие романа происходит накануне Второй мировой войны. В Москве сотрудник  Особого сектора  при ЦК ВКП(б), спецреферент по Германии Илья Крылов составляет информационные сводки для Сталина. В Берлине журналистка Габриэль Дильс работает на советскую разведку. Никто не в силах остановить эпидемию массового безумия в СССР и в Третьем рейхе. Но все таки можно попытаться спасти жизнь хотя бы одного человека, пусть даже далекого и незнакомого.\r\nМортенсон Г. Три чашки чая/ Грег Мортенсон, Дэвид Оливер Релин. – М.:Эксмо, 2012. – 624 с.\r\nТри чашки чая  — это поразительная история о том, как самый обычный человек, не обладая ничем, кроме решительности, способен в одиночку изменить мир.\r\nГрег Мортенсон подрабатывал медбратом, ночевал в машине, а свое немногочисленное имущество держал в камере хранения. В память о погибшей сестре он решил покорить самую сложную гору К2. Эта попытка чуть ли не стоила ему жизни, если бы не помощь местных жителей. Несколько дней, проведенных в отрезанной от цивилизации пакистанской деревушке, потрясли Грега настолько, что он решил собрать необходимую сумму и вернуться в Пакистан, чтобы построить школу для деревенских детей.\r\nСегодня Мортенсон руководит одной из самых успешных благотворительных организаций в мире, он построил 145 школ и несколько десятков женских и медицинских центров в самых бедных деревнях Пакистана и Афганистана.\r\n Когда ты впервые пьешь чай с горцами балти, ты — чужак.\r\nВо второй раз — почетный гость.\r\nТретья чашка чая означает, что ты — часть семьи, а ради семьи они готовы на что угодно. Даже умереть .\r\nКнига была издана в 48 странах и в каждой из них стала бестселлером. Самого Грега Мортенсона дважды номинировали на Нобелевскую премию мира в 2009 и 2010 годах.\r\nРемер М. Даун.  М.:РИПОЛ классик, 2009. – 256 с.\r\nКостя никогда не притворяется и никому не желает зла. Но он умеет радоваться жизни так, как мало кто из нас – юноша ребенок с незапятнанной душой и богатым, но так непохожим на наш с вами внутренним миром.\r\nЮноша чувствует, что к их дому подкрадывается беда. Чувствует и пытается что то изменить. Где то наивно, где то с удивительной проницательностью. И иногда ему удается сделать мир чуточку добрее. Даже в условиях нашей непредсказуемой судьбы.\r\nИстория Кости недаром созвучна  Человеку дождя . Она написана для людей неравнодушных, для тех, чья душа еще не зачерствела окончательно.\r\nМетлицкая М. Машкино счастье.М.:ЭКСМО, 2012. – 288 с.\r\nВ сборник  Машкино счастье  включено девятнадцать рассказов. Девятнадцать коротких историй о разных людях, о разных женщинах.\r\nПочему одни счастливы, а другие несчастны? И вообще – что такое счастье? Кто то довольствуется малым – и счастлив. Кто то стремится к недостижимому и всегда несчастен. Среди героев этой книги есть и те, и другие. Наверное, дело в том, что счастливые вовремя осознали: жизнь – не борьба, а награда.\r\nСенчин Р. Елтышевы. – М.:ЭКСМО, 2010. – 320 с.\r\n Елтышевы  – семейный эпос Романа Сенчина.\r\nСтрашный и абсолютно реальный мир, в который попадает семья Елтышевых, – это мир современной российской деревни. Нет, не той деревни, куда принято ездить на уик энд из больших мегаполисов – пожарить шашлыки и попеть под караоке. А самой настоящей деревни, древней, как сама Россия: без дорог, без лекарств, без удобств и средств к существованию. Деревни, где лишний рот страшнее болезни и за вязанку дров зимой можно поплатиться жизнью.\r\nЛюди очень быстро теряют человеческий облик, когда сталкиваются с необходимостью выживать. И осуждать их за это может только тот, кто сам прошел путь возвращения: от успеха и денег – к нищете и страху, от сытости – к голоду и холоду…\r\nСенчин жесток и не жалеет никого – но в этой жестокости кроется очищение. После  Елтышевых  не так то просто будет сказать привычное  люблю . Это слово для вас изменится на вкус…\r\nПетрушевская Л. Котенок Господа Бога: рождественские истории. – М.:Астрель, 2012. – 412 с.\r\nКнига Людмилы Петрушевской  Котенок Господа Бога    это рождественские сказки для взрослых детей, и в каждой из них есть история любви. Причем любви не только простых принцесс к случайным знакомым, но и любви летчиков, ничьих невест и заблудившихся поклонников, любви бедных безвестных принцев к простым десятиклассницам, любви волшебных кукол, котят и улыбающихся лошадей... Любви маленьких мальчиков и взрослых девочек к своим непутевым мамам   и обратной горячей любви.\r\n '),('ygin-menu',183,1,'Виртуальная выставка Виртуальная выставка '),('ygin-menu',184,1,'Детское творчество Детское творчество '),('ygin-menu',185,1,'Детские периодические издания Детские периодические издания '),('ygin-menu',186,1,'Коллегам Коллегам '),('ygin-menu',187,1,'Методические рекомендации Методические рекомендации '),('ygin-menu',188,1,'Проекты Проекты '),('ygin-menu',189,1,'Программы МУ  Корткеросская ЦБС  Программы МУ  Корткеросская ЦБС  '),('ygin-menu',190,1,'Наши публикации: опыт работы библиотек МУ  Корткеросская ЦБС  Наши публикации: опыт работы библиотек МУ  Корткеросская ЦБС  '),('ygin-menu',191,1,'Профессиональные конкурсы Профессиональные конкурсы '),('ygin-menu',192,1,'Профессиональные события Профессиональные события '),('ygin-menu',193,1,'Полезные ссылки Полезные ссылки '),('ygin-menu',129,1,'Левое меню'),('ygin-menu',130,1,'Заказ и доставка  Краснозатонская Серебряная  цена с доставкой: 170 руб. в фирменном магазине: 140 руб.\r\nЧистая питьевая артезианская вода высшей категории, отвечающая не только критериям безопасности, но и критерию полезности. Разливается на современном высокотехнологичном оборудовании и не продается в розлив на улице.\r\n Краснозатонская Серебряная  оптимально сбалансирована по содержанию необходимых человеку биогенных элементов: кальция, магния, калия и фтора.\r\n Краснозатонская Йодированная  цена с доставкой: 190 руб. в фирменном магазине: 160 руб. \r\nЧистая питьевая артезианская вода высшей категории, обогащенная йодом. Йод в воде находится в виде ионов. За счет чего практически полностью усваивается организмом человека и не разрушается при кипячении. Вода способствует профилактике йододефицита\r\nКак заказать?\r\n• Заказать воду с доставкой можно по телефону 202 802 в СыктывкареПрием заказов:в будни: с 8.00 до 19.00в выходные: с 8.00 до 17.00• заказать с сайта круглосуточно (кнопка ссылка на заказ воды)Вы можете указать желаемое время доставки. При невозможности доставить Вам воду в указанный на сайте интервал, Вы получите уведомление от оператора с откорректированным реальным временем доставки воды.• купить в фирменном магазине по адресам Ленина, 28 (ссылка на google карту)и Димитрова, 48(ссылка на google карту)\r\nУсловия заказа и доставка:Доставка воды производится:в будни: с 9.00 до 20.00в выходные: с 9.00 до 18.00• Доставка воды осуществляется в день приема заказа, если заказ был сделан до 12 часов дня, если позже, то на следующий день после поступления заявки.• Услуги по доставке и подъему воды и оборудования на любой этаж оказываются бесплатно.Стоимость и оплата:Цена с доставкой указана за доставку 1 бутыли. Стоимость доставки от 2 бутылей одновременно за наличный расчет ниже на 20 рублей и составляет 150 рублей за бутыль  Краснозатонской Серебряной  и 170 рублей за бутыль  Краснозатонской Йодированной .\r\n'),('ygin-menu',131,1,'Новичкам'),('ygin-menu',132,1,'Акции'),('ygin-menu',133,1,'Новости'),('503',1,1,'Новости'),('503',2,1,'Акции'),('502',1,1,'Первый раз вода в подарок Вы у нас в первый раз? Мы Вам рады! \r\nВы у нас в первый раз? Мы Вам рады!Первый раз мы привезем Вам 19 л. чистой и полезной  Краснозатонской  В ПОДАРОК! \r\n При покупке подлежащей обмену бутыли. Специальная поликарбонатная бутыль выкупается 1 раз. Затем мы будем привозить Вам свежую воду, и забирать бутыль на обмен.Акция действует в городах Сыктывкар и Ухта.'),('502',2,1,'Вкусная и полезная  Краснозатонская Серебряная  с обновленной этикеткой! Дорогие друзья! \r\n6 ноября мы запустили в продажу часть бутылок с водой 19 л  Краснозатонская Серебряная  с новыми этикетками.  Дорогие друзья! \r\n6 ноября мы запустили в продажу часть бутылок с водой 19 л  Краснозатонская Серебряная  с новыми этикетками. Постепенно все старые этикетки будут заменены новыми, в том числе и на маленьких бутылках в ПЭТ таре, которые вы сможете приобрести в любом магазине.\r\nС новой этикеткой наш продукт выглядит более современным и ярким. А Вы легко отличите нашу питьевую воду высшей категории от других вод, большинство из которых более низкого качества   воды питьевые первой категории или воды питьевые столовые.\r\nИзменилась только этикетка, а в бутылках по прежнему Ваша любимая вкусная и полезная вода  Краснозатонская Серебряная  производства Компании  Эколайн .'),('ygin-menu',194,1,'Корткеросская центральная библиотека им. М.Н. Лебедева Корткеросская центральная библиотека им. М.Н. Лебедева Директор ЦБС Челпанова Светлана Аркадьевна\r\nс. Корткерос ул. Советская дом 187 /первый этаж/ т.: (82136)9 24 80 E mail Lebedevlib@mail.ru/\r\nРежим работы:\r\nПн. пт. с 10.00 до 18.00 Суббота – 10.00 16.00 Выходной – воскресенье\r\nРасписание на летний период: (с 01 июня по 30 сентября) Пн. пт. с 10.00 до 18.00 Перерыв на обед 13 14 Выходной – суббота, воскресенье\r\nПоследний день месяца – санитарный день '),('ygin-menu',195,1,'Корткеросская центральная детская библиотека Корткеросская центральная детская библиотека Шестакова Валентина Владимировна зам. директора по работе с детьми\r\nс. Корткерос ул. Советская дом 187 /первый этаж/ т.: (82136)9 24 80 E mail: Lebedevlib@mail.ru/\r\nРежим работы:\r\nПн. пт. с 10.00 до 18.00 Суббота – 10.00 16.00 Выходной – воскресенье\r\nРасписание на летний период: (с 01 июня по 30 сентября) Пн. пт. с 10.00 до 18.00 Перерыв на обед 13 14 Выходной – суббота, воскресенье\r\nПоследний день месяца – санитарный день '),('ygin-menu',196,1,'Сторожевский филиал Сторожевский филиал Королева Елена Геннадьевна   Заведующий филиалом Попова Мария Алексеевна   библиотекарь\r\nс. Сторожевск, ул. Первомайская, д.1 8 (82136) 9 14 33 E mail: bibstr@mail.ru/\r\nРежим работы:\r\nПн. птн. с 10.00 до 18.00 Перерыв на обед 14 15 Суббота – 10.00 16.00 Выходной – воскресенье\r\nЛетний период: (с 01 июня по 30 сентября) Пн. пт. с 10.00 до 18.00 Перерыв на обед 14 15 Выходной – суббота, воскресенье '),('ygin-menu',197,1,'Аджеромский филиал Аджеромский филиал Цывунина Анастасия Олеговна   библиотекарь\r\nп. Аджером, ул. ПМК, д.5  а \r\nРежим работы:\r\nВт. сб. с 10.00 до 18.00 Перерыв на обед 14 15 Выходной – воскресенье, понедельник\r\nРасписание на летний период: (с 01 июня по 30 сентября) Пн. пт. с 10.00 до 18.00 Перерыв на обед 14 15 Выходной – суббота, воскресенье '),('ygin-menu',198,1,'Богородский филиал Богородский филиал Игушева Светлана Владимировна   заведующий филиалом\r\nс. Богородск ул. Школьная, д.59  б \r\nРежим работы:\r\nПн. пт. с 10.00 до 18.00 Перерыв на обед 14 15 Выходной – суббота, воскресенье '),('ygin-menu',199,1,'Большелугский филиал Большелугский филиал Панюкова Роза Геннадиевна   заведующий филиалом\r\nс. Большелуг ул. Макарсиктская, д.90 т. 8 (82136)9 62 31 E mail: roz2861@mail.ru\r\nРежим работы:\r\nВт.   сб. с 10.00 до 18.00 Перерыв на обед 13 14 Выходной – воскресенье, понедельник '),('ygin-menu',200,1,'Вомынский филиал им. А.А. Сухановой Вомынский филиал им. А.А. Сухановой Каракчиева Лия Ивановна   библиотекарь\r\nс. Вомын д. 150\r\nРежим работы:\r\nПн. пт. с 11.00 до 19.00 Перерыв на обед 15 16 Выходной – суббота, воскресенье '),('ygin-menu',201,1,'Додзьский филиал Додзьский филиал Михайлова Елена Витальевна – библиотекарь\r\nс. Додзь ул. Центральная, д.90\r\nРежим работы:\r\nПн. пт. с 15.00 до 18.30 Перерыв на обед 15 16 Выходной – суббота, воскресенье '),('ygin-menu',202,1,'Кересский филиал Кересский филиал Ветошкина Надежда Ивановна – библиотекарь\r\nс. Керес ул. Центральная, д.69\r\nРежим работы:\r\nПн. пт. с 10.00 до 18.00 Перерыв на обед 14 15 Выходной – суббота, воскресенье '),('ygin-menu',203,1,'Намский филиал Намский филиал Бицадзе Валентина Александровна   заведующий филиалом\r\nп. Намск ул. Малая, д. 2\r\nРежим работы:\r\nПн. пт. с 11.00 до 18.30 Перерыв на обед 16.00 16.30 Выходной – суббота, воскресенье '),('ygin-menu',204,1,'Маджский филиал им. Ф.Ф. Павленкова Маджский филиал им. Ф.Ф. Павленкова Фихтнер Евгения Анатольевна – библиотекарь\r\nс. Маджа ул. Центральная, д. 3\r\nРежим работы:\r\nПн. пт. с 10.00 до 18.00 Перерыв на обед 14 15 Выходной – суббота, воскресенье '),('ygin-menu',205,1,'Мординский филиал им. Ф.Ф. Павленкова Мординский филиал им. Ф.Ф. Павленкова Рылькова Ольга Геннадьевна   заведующий филиалом\r\nс. Мордино ул. Пушкина, д. 9\r\nРежим работы:\r\nВт. сб. с 11.00 до 19.00 Перерыв на обед 14 15 Выходной – воскресенье, понедельник\r\nРасписание на летний период: (с 01 июня по 30 сентября) Пн. пт. с 11.00 до 18.30 Перерыв на обед 14.30  15.00 Выходной – суббота, воскресенье '),('ygin-menu',206,1,'Небдинский филиал Небдинский филиал Савина Галина Дмитриевна   Заведующий филиалом\r\nс. Небдино ул. Центральная, д. 90\r\nРежим работы:\r\nПн. пт. с 10.30 до 18.00 Перерыв на обед 14.30 15.00 Выходной – суббота, воскресенье  '),('ygin-menu',207,1,'Нившерский филиал им. Ф.Ф. Павленкова Нившерский филиал им. Ф.Ф. Павленкова Жижева Лариса Юрьевна   Заведующий филиалом Михайлова Татьяна Юрьевна – библиотекарь\r\nс.Нившера\r\nм. Дівыв, д. 728 т.: 8 (82136) 9 83 93 E mail: niwscheralib@mail.ru/\r\nРежим работы:\r\nПн.  птн. с 11.00 до 18.00 Перерыв на обед 15 16 Воскресенье – 11.00 18.00 Выходной – суббота\r\nРасписание на летний период: (с 01 июня по 30 сентября) Пн. птн. с 10.00 до 18.00 Перерыв на обед 14.00  15.00 Выходной – суббота, воскресенье '),('ygin-menu',208,1,'Пезмегский филиал Пезмегский филиал Юранева Лидия Кимовна – библиотекарь\r\nс. Пезмег ул. Бр. Покровских, д.40\r\nРежим работы:\r\nПн. птн. с 11.00 до 19.00 Перерыв на обед 15 16 Выходной – суббота, воскресенье '),('ygin-menu',209,1,'Подтыбокский филиал Подтыбокский филиал Семяшкина Екатерина Анатольевна   заведующий филиалом\r\nп. Подтыбок ул. Средняя, д.4\r\nРежим работы:\r\nПн. птн. с 10.30 до 18.00 Перерыв на обед 14.00 14.30 Выходной – суббота, воскресенье '),('ygin-menu',210,1,'Подъельский филиал Подъельский филиал Королева Вера Борисовна   Заведующий филиалом\r\nс. Подъельск ул. Восточная, д.11\r\nРежим работы:\r\nПн. птн. с 10.30 до 18.00 Перерыв на обед 15.30 16.00 Выходной – суббота, воскресенье '),('ygin-menu',211,1,'Позтыкеросский филиал Позтыкеросский филиал Гилева Нина Анатольевна   Заведующий филиалом\r\nРежим работы:\r\nВт. птн. с 10.00 до 18.00 Перерыв на обед 14.00 15.00 Выходной – воскресенье, понедельник '),('ygin-menu',212,1,'Приозерный филиал Приозерный филиал Котяш Галина Дмитриевна   библиотекарь\r\nп. Приозерный ул. Трактовская, д.1\r\nРежим работы:\r\nВт. сб. с 10.00 до 18.00 Перерыв на обед 13.00 14.00 Выходной – воскресенье, понедельник '),('ygin-menu',213,1,'Усть Лэкчимский филиал Усть Лэкчимский филиал Липина Анна Михайловна – библиотекарь\r\nп.Усть Лэкчим, ул.Школьная, д.1  а \r\nРежим работы:\r\nПн. птн. с 10.00 до 18.00 Перерыв на обед 13.00 14.00 Выходной – суббота, воскресенье '),('ygin-menu',214,1,'Визябожский филиал Визябожский филиал Петрова Екатерина Валерьевна – библиотекарь\r\nп.Визябож ул. Лесная, д.12\r\nРежим работы:\r\nВт.   птн –12.30 18 ч. Выходной – суббота, воскресенье, понедельник '),('502',13,1,'Содержание '),('ygin-menu',216,1,'Фотогалерея Фотогалерея '),('ygin-menu',217,1,'Новости Новости '),('ygin-menu',105,1,'Консультации Консультации '),('ygin-menu',117,1,'Кулеры и помпы'),('ygin-menu',118,1,'Вакансии'),('ygin-menu',120,1,'Задать вопрос Вопрос ответ'),('ygin-menu',121,1,'Компания'),('ygin-menu',122,1,'Продукция'),('ygin-menu',123,1,'Производство'),('509',1,1,'Вода'),('511',1,1,'Краснозатонская Серебряная Чистая питьевая артезианская вода высшей  категории. Вода высшей категории отвечает не только критериям безопасности, но и полезности, так как содержит сбалансированное количество основных биогенных элементов (таких как кальций, магний, калий и фтор) необходимых человеку.\r\n Краснозатонская Серебряная разливается в бутыли0,5, 1,5, 5, 10 и 19 литров. Вода в бутылях объемом 19 л доставляется на дом, в офис, в школы, детские сады, учреждения здравоохранения и т. д.\r\nводу в бутылках 10 л, 5л, 1,5 л, 0,5 л Вы найдете в продуктовых магазинах Сыктывкара, Ухты и близлежащих районах'),('ygin-menu',135,1,'Как выбрать лучшую воду? Выбирая производителя воды учитывайте следующие моменты:• категорию воды (полноценной по содержанию макро  и микроэлементов может быть только вода высшей категории)• месторасположение источника воды• технологический уровень производства• вкус воды• сколько лет компания работает на рынке (надежность поставщика)• удаленность производства воды от источника (наличие дополнительных перевозок, которые негативно сказываются на свойствах воды)'),('ygin-menu',136,1,'Ищите для себя и близких лучшее? Почему стоит выбрать воду  Краснозатонскую ? Пользу и вкусовые свойства  Краснозатонской  сегодня ценят тысячи наших постоянных клиентов в Сыктывкаре, Ухте, Сосногорске и других населенных пунктах Республики Коми.Вода  Краснозатонская    единственная в Республике Коми бутилированная вода высшей категории. Разливается на современном оборудовании и исключительно в стерильных условиях, в месте добычи. Ни в коем случае не продается в розлив.Высшая категория воды гарантирует не только ее чистоту и безопасность, но и полезность!  Краснозатонская  сбалансирована по основным биологически необходимым человеку микро  и макроэлементам.  рекомендована Институтом питания РАМН для детей с первого года жизни, восстановления сухих молочных смесей,   источник добычи –артезианская скважина, расположен вдали от города, в экологически чистом месте   поселке Краснозатонский.  выбирая  Краснозатонскую Йодированную , Вы укрепляете иммунитет, стимулируете умственную деятельность, повышаете свою активность и работоспособность.Компания  Эколайн    ведущий производитель воды высшей категории, имеет отличную репутацию как добросовестного производителя и надежного партнера, так и активного участника общественной жизни Республики Коми уже на протяжении 15 лет!'),('ygin-menu',137,1,'Как заказать  Краснозатонскую ? Доставку воды Вы можете заказать:— по телефону 202 802— через сайт, заполнив форму заказа (ссылка на заказ воды)Заказывая воду, вы всегда можете указать удобное для вас время доставки. При доставке воды на дом оплата производится по факту получения.\r\nДоставка воды в офисы осуществляется в соответствии с выбранными условиями за наличный или безналичный расчет.'),('ygin-menu',138,1,'Фирменные магазины \r\nДля тех, кому удобней прийти за водой в магазин, могут также сэкономить!\r\nАдреса фирменных магазинов  Краснозатонская Серебряная :\r\nг. Сыктывкар: ул. Ленина, 28; ул. Димитрова, 48;г. Ухта: ул. 40 лет Коми, 10Режим работы:будни: с 9.00 до 20.00выходные: с 9.00 до 18.00\r\nг. Сосногорск: Сосновский пер к, д. 2ежедневно с 9.00 до 18.00\r\nвсе адреса с ссылками на карты'),('ygin-menu',139,1,'Оборотная тара Продажа воды осуществляется в 19 литровых бутылях. Эти бутыли изготовлены из поликарбоната — материала, не оказывающего негативного воздействия на воду, легко моющегося, прочного, устойчивого к высоким и низким температурам.\r\nЕсли Вы заказываете воду в первый раз, и у Вас нет оборотной тары, то при первой поставке необходимо оплатить стоимость бутыли. При последующих поставках Вы будете обменивать пустые бутыли на полные!Если Вы раньше заказывали воду других компаний, и у Вас уже есть бутыли, то мы готовы их принять на обмен, но только в том случае, если они изготовлены из поликарбоната и находятся в хорошем состоянии (имеют царапины и потертости не более допустимого уровня). Состояние бутыли проверяет водитель экспедитор на доставке, либо продавец в обменном пункте.Обратите внимание, что цена воды не включает стоимость бутыли.');
/*!40000 ALTER TABLE `da_search_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `da_search_history`
--

DROP TABLE IF EXISTS `da_search_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `da_search_history` (
  `id_search_history` int(8) NOT NULL AUTO_INCREMENT,
  `phrase` varchar(255) DEFAULT NULL,
  `query` text,
  `info` varchar(255) DEFAULT NULL,
  `date` int(11) NOT NULL,
  `ip` varchar(32) NOT NULL,
  PRIMARY KEY (`id_search_history`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `da_search_history`
--

LOCK TABLES `da_search_history` WRITE;
/*!40000 ALTER TABLE `da_search_history` DISABLE KEYS */;
INSERT INTO `da_search_history` VALUES (1,'сыктывкар','FROM da_search_data a LEFT JOIN pr_news_category a1 ON a.id_object=503 AND a.id_instance=a1.id_news_category LEFT JOIN pr_news a2 ON a.id_object=502 AND a.id_instance=a2.id_news LEFT JOIN pr_consultation_ask a3 ON a.id_object=505 AND a.id_instance=a3.id_consultation_ask WHERE MATCH (a.value) AGAINST (\'+сыктывкар*\' IN BOOLEAN MODE) AND (( (a1.is_visible=1 AND a.id_object=503) OR a.id_object <> 503 ) AND ( (a2.is_visible=1 AND a.id_object=502) OR a.id_object <> 502 ) AND ( (a3.is_visible=1 AND a.id_object=505) OR a.id_object <> 505 )) AND a.id_object IN (503, 502, 505, 506, 507, 508) AND a.id_lang=1','page=1; count=15',1323095277,'192.168.0.5'),(2,'сыктывкар','FROM da_search_data a LEFT JOIN pr_news_category a1 ON a.id_object=503 AND a.id_instance=a1.id_news_category LEFT JOIN pr_news a2 ON a.id_object=502 AND a.id_instance=a2.id_news LEFT JOIN pr_consultation_ask a3 ON a.id_object=505 AND a.id_instance=a3.id_consultation_ask WHERE MATCH (a.value) AGAINST (\'+сыктывкар*\' IN BOOLEAN MODE) AND (( (a1.is_visible=1 AND a.id_object=503) OR a.id_object <> 503 ) AND ( (a2.is_visible=1 AND a.id_object=502) OR a.id_object <> 502 ) AND ( (a3.is_visible=1 AND a.id_object=505) OR a.id_object <> 505 )) AND a.id_object IN (503, 502, 505, 506, 507, 508) AND a.id_lang=1','page=1; count=15',1323152810,'192.168.0.254');
/*!40000 ALTER TABLE `da_search_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `da_site_module`
--

DROP TABLE IF EXISTS `da_site_module`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `da_site_module` (
  `id_module` int(8) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `id_php_script` int(8) DEFAULT NULL COMMENT 'Обработчик',
  `name` varchar(255) NOT NULL COMMENT 'Имя',
  `is_visible` tinyint(1) DEFAULT '1' COMMENT 'Видимость',
  `content` text COMMENT 'Простой текст',
  `html` longtext COMMENT 'Форматированный текст',
  PRIMARY KEY (`id_module`)
) ENGINE=InnoDB AUTO_INCREMENT=128 DEFAULT CHARSET=utf8 PACK_KEYS=0 COMMENT='Виджеты сайта';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `da_site_module`
--

LOCK TABLES `da_site_module` WRITE;
/*!40000 ALTER TABLE `da_site_module` DISABLE KEYS */;
INSERT INTO `da_site_module` VALUES (108,1016,'Авторизация',1,NULL,NULL),(116,NULL,'2Gis-карта',1,'<script charset=\"utf-8\" type=\"text/javascript\" src=\"http://firmsonmap.api.2gis.ru/js/DGWidgetLoader.js\"></script>\r\n<script charset=\"utf-8\" type=\"text/javascript\">new DGWidgetLoader({\"borderColor\":\"#a3a3a3\",\"width\":\"770\",\"height\":\"350\",\"wid\":\"47c4edf7e203ee08320f901da417d96a\",\"pos\":{\"lon\":\"50.825820502302\",\"lat\":\"61.668363228081\",\"zoom\":\"17\"},\"opt\":{\"ref\":\"hidden\",\"card\":[\"name\",\"contacts\"],\"city\":\"syktyvkar\"},\"org\":[{\"id\":\"10133627442576620\"}]});</script>\r\n<noscript style=\"color:#c00;font-size:16px;font-weight:bold;\">Виджет карты использует JavaScript. Включите его в настройках вашего браузера.</noscript>',NULL),(117,NULL,'Контакты организации',1,'<address class=\"vcard\">\r\n</address>',''),(118,NULL,'Счётчики',1,'<!-- Сюда вставлять код Яндекс.Метрики -->',NULL),(119,1023,'Левое меню',1,NULL,NULL),(120,1028,'Витрина',1,NULL,NULL),(121,1029,'Кнопка купить',1,'',''),(122,1030,'О компании',1,'',''),(123,1031,'Случайное фото',1,NULL,NULL),(127,1035,'Последние новости',1,NULL,NULL);
/*!40000 ALTER TABLE `da_site_module` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `da_site_module_rel`
--

DROP TABLE IF EXISTS `da_site_module_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `da_site_module_rel` (
  `id_module` int(8) NOT NULL DEFAULT '0',
  `place` int(3) NOT NULL DEFAULT '0',
  `sequence` int(3) NOT NULL DEFAULT '1',
  `id_module_template` int(8) NOT NULL,
  PRIMARY KEY (`id_module_template`,`id_module`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `da_site_module_rel`
--

LOCK TABLES `da_site_module_rel` WRITE;
/*!40000 ALTER TABLE `da_site_module_rel` DISABLE KEYS */;
INSERT INTO `da_site_module_rel` VALUES (117,6,1,1),(118,6,2,1),(116,3,1,2),(117,6,1,2),(118,6,2,2),(117,6,1,3),(118,6,2,3),(117,6,1,4),(118,6,2,4),(121,3,1,4),(117,6,1,5),(118,6,2,5),(117,6,1,6),(118,6,2,6),(122,5,1,7);
/*!40000 ALTER TABLE `da_site_module_rel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `da_site_module_template`
--

DROP TABLE IF EXISTS `da_site_module_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `da_site_module_template` (
  `id_module_template` int(8) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(255) NOT NULL COMMENT 'Название шаблона',
  `is_default_template` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Использовать по умолчанию',
  `sequence` int(8) DEFAULT NULL COMMENT 'п/п',
  PRIMARY KEY (`id_module_template`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='Наборы виджетов';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `da_site_module_template`
--

LOCK TABLES `da_site_module_template` WRITE;
/*!40000 ALTER TABLE `da_site_module_template` DISABLE KEYS */;
INSERT INTO `da_site_module_template` VALUES (1,'Основной',1,1),(2,'Контакты',0,5),(3,'Без модулей',0,6),(4,'Главная',0,2),(5,'Каталог товаров',0,3),(6,'Новости',0,4),(7,'О компании',0,7);
/*!40000 ALTER TABLE `da_site_module_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `da_stat_view`
--

DROP TABLE IF EXISTS `da_stat_view`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `da_stat_view` (
  `id_object` varchar(255) NOT NULL,
  `id_instance` int(11) NOT NULL,
  `last_date_process` int(11) NOT NULL,
  `view_type` tinyint(2) NOT NULL,
  `view_count` int(11) NOT NULL,
  PRIMARY KEY (`id_object`,`id_instance`,`view_type`,`last_date_process`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `da_stat_view`
--

LOCK TABLES `da_stat_view` WRITE;
/*!40000 ALTER TABLE `da_stat_view` DISABLE KEYS */;
/*!40000 ALTER TABLE `da_stat_view` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `da_system_parameter`
--

DROP TABLE IF EXISTS `da_system_parameter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `da_system_parameter` (
  `id_system_parameter` varchar(255) NOT NULL COMMENT 'id',
  `id_group_system_parameter` int(8) NOT NULL DEFAULT '0' COMMENT 'Группа параметров',
  `name` varchar(60) NOT NULL COMMENT 'Имя для разработчика',
  `value` varchar(255) DEFAULT NULL COMMENT 'Значение параметра',
  `note` varchar(255) DEFAULT '-' COMMENT 'Описание',
  `id_parameter_type` int(8) DEFAULT '2' COMMENT 'Тип\r\nданных',
  `long_text_value` longtext COMMENT 'Значение для больших текстовых данных (longtext)',
  PRIMARY KEY (`id_system_parameter`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Настройки сайта';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `da_system_parameter`
--

LOCK TABLES `da_system_parameter` WRITE;
/*!40000 ALTER TABLE `da_system_parameter` DISABLE KEYS */;
INSERT INTO `da_system_parameter` VALUES ('101',1,'translit_uploaded_file_name','1','Выполнять транслитерацию имен загружаемых файлов',9,NULL),('12',1,'count_day_for_delete_event','30','Срок, который хранятся отосланные события (в днях)',2,NULL),('31',1,'upload_image_width','0','Автоматически уменьшать до заданного размера ширину загружаемых изображений',1,''),('32',1,'upload_image_height','0','Автоматически уменьшать до заданного размера высоту загружаемых изображений',1,''),('500',2,'phone','22-13-14','Телефон',2,NULL),('6',1,'id_domain_default','1','ИД основного домена',1,NULL),('7',1,'count_sent_mail_for_session','3','Количество отсылаемых сообщений за сессию (0 - все)',2,NULL),('project-parameter-moneta-merchant-login',2,'moneta_merchant_login','123','Номер счета в мерчанте монеты',2,''),('project-parameter-moneta-merchant-pass1',2,'moneta_merchant_pass1','123456789','Пароль #1 в мерчанте монеты',2,''),('project-parameter-robokassa-merchant-login',2,'robokassa_merchant_login','robokass-login','Логин в мерчанте робокассы',2,''),('project-parameter-robokassa-merchant-pass1',2,'robokassa_merchant_pass1','robokass-pass1','Пароль #1 в мерчанте робокассы',2,''),('project-parameter-robokassa-merchant-pass2',2,'robokassa_merchant_pass2','robokass-pass2','Пароль #2 в мерчанте робокассы',2,''),('ygin-ext-main-lastChangeDate',1,'last_change_project_date','20140615','Последняя дата проектных обновлений',2,NULL);
/*!40000 ALTER TABLE `da_system_parameter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `da_users`
--

DROP TABLE IF EXISTS `da_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `da_users` (
  `id_user` int(8) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'Логин',
  `user_password` varchar(150) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT 'Пароль',
  `mail` varchar(255) NOT NULL COMMENT 'E-mail',
  `full_name` varchar(200) DEFAULT NULL COMMENT 'Имя пользователя',
  `rid` varchar(64) DEFAULT NULL COMMENT 'Регистрационный ИД',
  `create_date` int(10) NOT NULL DEFAULT '0' COMMENT 'Дата регистрации пользователя',
  `count_post` int(8) NOT NULL DEFAULT '0' COMMENT 'count_post',
  `active` tinyint(1) DEFAULT '1' COMMENT 'Активен',
  `requires_new_password` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Необходимо сменить пароль',
  `salt` varchar(255) DEFAULT NULL COMMENT 'Соль для пароля',
  `password_strategy` varchar(255) DEFAULT NULL COMMENT 'Стратегия для формирования пароля',
  `subscriber_number` varchar(255) DEFAULT NULL COMMENT 'Абонентский номер',
  `phone` varchar(255) DEFAULT NULL COMMENT 'Телефон',
  `address` varchar(255) DEFAULT NULL COMMENT 'Адрес',
  `convenient_time` varchar(255) DEFAULT NULL COMMENT 'Предпочитаемое время доставки',
  `yur` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Юр. лицо',
  `company` varchar(255) DEFAULT NULL COMMENT 'Компания',
  `personal_data_agreement` varchar(255) DEFAULT NULL COMMENT 'Обработка персональных данных',
  PRIMARY KEY (`id_user`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=117 DEFAULT CHARSET=utf8 PACK_KEYS=0 COMMENT='Пользователи';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `da_users`
--

LOCK TABLES `da_users` WRITE;
/*!40000 ALTER TABLE `da_users` DISABLE KEYS */;
INSERT INTO `da_users` VALUES (1,'admin','91a399ceafc4f3ecb187932030b020e78d94952e','web@cvek.ru','Разработчик',NULL,1178788080,1,1,0,'73561e3259556005dd91739f0c54744829a245d7','sha1',NULL,NULL,NULL,NULL,0,NULL,NULL),(39,'da_system','01a6651a94ab602dbfc47587d234b52c','','Системный пользователь',NULL,1176358114,0,1,0,NULL,'legacy',NULL,NULL,NULL,NULL,0,NULL,NULL),(46,'editor','8ed31d6da7e6d59e2df72e29fcd1ab4808cada10','test@test.ru','Администрация',NULL,1183535431,0,1,0,'5f79d560505f0914addf61079df7b7ba135eb904','sha1',NULL,NULL,NULL,NULL,0,NULL,NULL),(52,'lena','$2zMLQNDP3FJ6','fedorova_ev@baitek.org','Елена Фёдорова',NULL,1204018286,0,1,0,'$2a$14$HbVM5dgUOGx8ynSYKzDxYA','bcrypt',NULL,NULL,NULL,NULL,0,NULL,NULL),(53,'neworleans','$2SbeOhvdFG/E','izotova_dn@baitek.org','Даша',NULL,1310538588,0,1,0,'$2a$14$APyR3sAgUxDxQPom3gOd/Q','bcrypt',NULL,NULL,NULL,NULL,0,NULL,NULL),(60,'sanchezzzov','439efdd4fa5efdbc4907cbc542c89ade9eee5b65','kirilyuk_aa@cvek.ru','Александр',NULL,1326352963,0,1,0,'e029054d3e07d9935101b95269d151b86a7bab3a','sha1',NULL,NULL,NULL,NULL,0,NULL,NULL),(61,'balas_ks','f22bc875e394eca70218f37b2fcaffb0971bc1b6','balas_ks@cvek.ru','Ксения Сергевна',NULL,1339762276,0,1,0,'3be60405d5c8e7a84725e763fc074234ccddf4f1','sha1',NULL,NULL,NULL,NULL,0,NULL,NULL),(62,'petrov_ro','22879676d7aef76fbab2a644457b1233c3464b77','petrov_ro@cvek.ru','Роман',NULL,1340610076,0,1,0,'f0467494b0c9b768d6643ff35bb920e83ed529d6','sha1',NULL,NULL,NULL,NULL,0,NULL,NULL),(63,'derevyankina','$2JNTX29gX/KM','derevyankina_ts@cvek.ru','Татьяна',NULL,1347610383,0,1,0,'$2a$14$79Zdx3gltF6mcaph/UfXKw','bcrypt',NULL,NULL,NULL,NULL,0,NULL,NULL),(67,'Cranky4','4547ea879e7e43a25a3e42531c114c9e6395988e','cranky4.89@gmail.com','Александр',NULL,1377584006,0,1,0,'477eff48bf468e0bee8a2fc602a99dc323874924','sha1',NULL,NULL,NULL,NULL,0,NULL,NULL),(99,'ed','612c3c224da888827b6af974cb2a88ec','support@cvek.ru','ed',NULL,1231852546,0,1,0,NULL,'legacy',NULL,NULL,NULL,NULL,0,NULL,NULL),(106,'novozhilov_ip','c8878012998d22f6bcd2f2b175e84b1e849aa791','novozhilov_ip@cvek.ru','Иван',NULL,1391084291,0,1,0,'b1a3212d755fb0618316de649ca00eaf30309b98','sha1',NULL,NULL,NULL,NULL,0,NULL,NULL),(107,'cool','fcfbf135964698a0e5b413053b63544e00373a22','','Водун А.А',NULL,1417608280,0,1,0,'dad6e433ec85e6f58a601c262f2737e666d35fe6','sha1',NULL,'892025224353','fxfbt','11.00 - 21.00',1,'Водумне','1'),(108,'cool2','ca6453ee7acc3fc5ad3118eccb0c64e7282d21b9','qwf@qwe.tu','ergerge',NULL,1417617184,0,1,0,'26f65da461d574bf675eaaad02982f65f6d0f048','sha1',NULL,'123456','F 2W','1231',0,NULL,'1'),(109,'locado','80493299fffcc8d690494fc102b30c34b143977b','vicleeva@mail.ru','Пантелеева Виктория Владимирован',NULL,1417702880,0,1,0,'39ac20ba2aabf5630d15f59e488505ef50c24c50','sha1',NULL,'9121368742','Сыктывкар, Димитрова 22, кв 18','18.00',0,NULL,'1'),(111,'testperetest','bcd19652aaaba7416b7511549308c29da5724820','testperetest@mail.ru','testperetest',NULL,1418115308,0,1,0,'e8501830ef33c9bd48da9e79826ae63e56d91212','sha1',NULL,'22222222','testperetest','12',1,'testperetest','1'),(112,'cvek','72f87797ff1fbbf68741890f95577afa6d2937a3','cvek@cvek.ru','Балас Ксения Сергеевна',NULL,1418121911,0,1,0,'2bfcadcfbc62231daed9f1b85bdc877d770da3a3','sha1',NULL,'200-153','Кутузова 19,','16',1,'Байтек','1'),(113,'123','d3deca3e990a8381a5d0455c9428bdcd5784c4c1','123@mail.ru','123',NULL,1418122695,0,1,0,'c961f95b53cb488a9e72924cfc30d6047cef40eb','sha1',NULL,'123','123','123',0,NULL,'1'),(114,'213','4f6a8ee718985a0aafc20067da7c490cb3852972','213@mail.ru','312',NULL,1418122770,0,1,0,'8ec1b3caf217acdb9d45193dba5693e06b97d115','sha1',NULL,'321','321','213',0,NULL,'1'),(115,'rtfgyuj','0d5fb542adac6700a4aa0f367d913c8ae1446cfc','dygbjhf@mail.ru','ваыпвраопитль',NULL,1418123569,0,1,0,'a6ae1a1ec715aabe6591cb1137b927f8e3310ec2','sha1',NULL,'85635561232632','укаепнголджю.бьотирпрнг7ор','fghjkl;',0,NULL,'1'),(116,'ertertetert','8512ecc839b72f82c16436f155e9363163045551','345345@mail.ru','545',NULL,1418195490,0,1,0,'9d607581dd65e6c867343c8ccf85abffb85ea97a','sha1',NULL,'345345','345345345','ertertert',0,NULL,'1');
/*!40000 ALTER TABLE `da_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `da_visit_site`
--

DROP TABLE IF EXISTS `da_visit_site`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `da_visit_site` (
  `id_instance` int(8) NOT NULL DEFAULT '0',
  `id_object` varchar(255) NOT NULL DEFAULT '0',
  `date` int(10) NOT NULL DEFAULT '0',
  `ip` int(10) unsigned NOT NULL,
  `type_visit` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_object`,`id_instance`,`type_visit`,`date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `da_visit_site`
--

LOCK TABLES `da_visit_site` WRITE;
/*!40000 ALTER TABLE `da_visit_site` DISABLE KEYS */;
INSERT INTO `da_visit_site` VALUES (1,'105',1341917912,0,1),(1,'105',1342181177,0,1),(1,'105',1342414043,1603263583,1),(1,'105',1342605069,0,1),(1,'105',1343391047,0,1),(2,'105',1341917954,0,1),(3,'105',1341917961,0,1),(4,'105',1342163457,0,1),(5,'105',1342163538,0,1),(5,'105',1342604260,0,1),(6,'105',1342163590,0,1),(6,'105',1343391112,0,1);
/*!40000 ALTER TABLE `da_visit_site` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pr_banner`
--

DROP TABLE IF EXISTS `pr_banner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pr_banner` (
  `id_banner` int(8) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `unique_name` varchar(100) NOT NULL COMMENT 'Уникальное название баннера (на английском языке)',
  `link` varchar(255) DEFAULT NULL COMMENT 'Ссылка на сайт',
  `alt` varchar(255) DEFAULT NULL COMMENT 'Текстовое описание',
  `file` int(8) NOT NULL COMMENT 'Файл',
  `id_banner_place` int(11) NOT NULL COMMENT 'Баннерное место',
  `visible` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Видимость',
  `sequence` int(5) NOT NULL COMMENT 'Порядок',
  PRIMARY KEY (`id_banner`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Баннеры';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pr_banner`
--

LOCK TABLES `pr_banner` WRITE;
/*!40000 ALTER TABLE `pr_banner` DISABLE KEYS */;
/*!40000 ALTER TABLE `pr_banner` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pr_banner_place`
--

DROP TABLE IF EXISTS `pr_banner_place`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pr_banner_place` (
  `id_banner_place` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `id_object` varchar(255) DEFAULT NULL COMMENT 'id_object',
  `id_instance` int(11) DEFAULT NULL COMMENT 'id_instance',
  `title` varchar(255) NOT NULL COMMENT 'Название',
  `showing` tinyint(2) NOT NULL COMMENT 'Тип показа баннеров',
  `sequence` smallint(11) DEFAULT '1' COMMENT 'Порядок',
  `id_parent` int(11) DEFAULT NULL COMMENT 'Родительский ключ',
  `width` int(8) DEFAULT NULL COMMENT 'Ширина',
  `height` int(8) DEFAULT NULL COMMENT 'Высота',
  PRIMARY KEY (`id_banner_place`),
  KEY `id_module` (`id_instance`,`sequence`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='Баннерные места';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pr_banner_place`
--

LOCK TABLES `pr_banner_place` WRITE;
/*!40000 ALTER TABLE `pr_banner_place` DISABLE KEYS */;
INSERT INTO `pr_banner_place` VALUES (3,'-1',NULL,'Баннер слева',3,1,NULL,NULL,NULL);
/*!40000 ALTER TABLE `pr_banner_place` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pr_client_review`
--

DROP TABLE IF EXISTS `pr_client_review`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pr_client_review` (
  `id_client_review` int(8) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(255) NOT NULL COMMENT 'ФИО',
  `create_date` int(10) NOT NULL COMMENT 'Дата',
  `review` longtext NOT NULL COMMENT 'Текст отзыва',
  `ip` varchar(255) NOT NULL COMMENT 'ip',
  `visible` tinyint(1) DEFAULT NULL COMMENT 'Видимость на сайте',
  `contact` varchar(255) DEFAULT NULL COMMENT 'Контакты клиента',
  PRIMARY KEY (`id_client_review`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Отзывы клиентов';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pr_client_review`
--

LOCK TABLES `pr_client_review` WRITE;
/*!40000 ALTER TABLE `pr_client_review` DISABLE KEYS */;
/*!40000 ALTER TABLE `pr_client_review` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pr_comment`
--

DROP TABLE IF EXISTS `pr_comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pr_comment` (
  `id_comment` int(8) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `id_object` varchar(255) NOT NULL COMMENT 'Объект',
  `id_instance` int(10) NOT NULL COMMENT 'Экземпляр',
  `comment_name` varchar(255) DEFAULT NULL COMMENT 'Автор',
  `id_user` int(8) DEFAULT NULL COMMENT 'Пользователь',
  `comment_theme` varchar(255) DEFAULT NULL COMMENT 'Тема',
  `comment_date` int(10) NOT NULL COMMENT 'Дата',
  `comment_text` text NOT NULL COMMENT 'Комментарий',
  `moderation` int(8) NOT NULL COMMENT 'Отмодерировано',
  `ip` varchar(50) DEFAULT NULL COMMENT 'IP',
  `id_parent` int(11) DEFAULT NULL COMMENT 'id_parent',
  `token` varchar(255) NOT NULL COMMENT 'Токен',
  PRIMARY KEY (`id_comment`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Комментарии';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pr_comment`
--

LOCK TABLES `pr_comment` WRITE;
/*!40000 ALTER TABLE `pr_comment` DISABLE KEYS */;
/*!40000 ALTER TABLE `pr_comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pr_consultation_answer`
--

DROP TABLE IF EXISTS `pr_consultation_answer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pr_consultation_answer` (
  `id_consultation_answer` int(8) NOT NULL COMMENT 'id',
  `id_consultation_ask` int(8) NOT NULL COMMENT 'На вопрос',
  `id_consultation_answerer` int(8) NOT NULL COMMENT 'Отвечающий',
  `answerer` varchar(255) DEFAULT NULL COMMENT 'Отвечающий (ручной ввод)',
  `answer_date` int(10) unsigned DEFAULT NULL COMMENT 'Дата ответа',
  `answer` text NOT NULL COMMENT 'Ответ',
  `ip` varchar(255) DEFAULT NULL COMMENT 'IP отвечающего',
  PRIMARY KEY (`id_consultation_answer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Ответ';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pr_consultation_answer`
--

LOCK TABLES `pr_consultation_answer` WRITE;
/*!40000 ALTER TABLE `pr_consultation_answer` DISABLE KEYS */;
/*!40000 ALTER TABLE `pr_consultation_answer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pr_consultation_answerer`
--

DROP TABLE IF EXISTS `pr_consultation_answerer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pr_consultation_answerer` (
  `id_consultation_answerer` int(8) NOT NULL COMMENT 'id',
  `name` varchar(255) NOT NULL COMMENT 'ФИО отвечающего',
  `email` varchar(255) DEFAULT NULL COMMENT 'e-mail',
  `photo` int(8) DEFAULT NULL COMMENT 'Фото',
  `caption_before` varchar(255) DEFAULT NULL COMMENT 'Подпись перед ФИО',
  `caption_after` varchar(255) DEFAULT NULL COMMENT 'Подпись после ФИО',
  `short_info` text COMMENT 'Краткое описание',
  `full_info` text COMMENT 'Полное описание',
  PRIMARY KEY (`id_consultation_answerer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Отвечающий';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pr_consultation_answerer`
--

LOCK TABLES `pr_consultation_answerer` WRITE;
/*!40000 ALTER TABLE `pr_consultation_answerer` DISABLE KEYS */;
/*!40000 ALTER TABLE `pr_consultation_answerer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pr_consultation_answerer_specialization`
--

DROP TABLE IF EXISTS `pr_consultation_answerer_specialization`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pr_consultation_answerer_specialization` (
  `id_consultation_answerer` int(8) NOT NULL,
  `id_consultation_specialization` int(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pr_consultation_answerer_specialization`
--

LOCK TABLES `pr_consultation_answerer_specialization` WRITE;
/*!40000 ALTER TABLE `pr_consultation_answerer_specialization` DISABLE KEYS */;
/*!40000 ALTER TABLE `pr_consultation_answerer_specialization` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pr_consultation_ask`
--

DROP TABLE IF EXISTS `pr_consultation_ask`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pr_consultation_ask` (
  `id_consultation_ask` int(8) NOT NULL COMMENT 'id',
  `user_fio` varchar(255) NOT NULL COMMENT 'ФИО спрашивающего',
  `email` varchar(255) DEFAULT NULL COMMENT 'E-mail спрашивающего',
  `ask_date` int(10) unsigned DEFAULT NULL COMMENT 'Дата вопроса',
  `ask` text NOT NULL COMMENT 'Вопрос',
  `ip` varchar(255) DEFAULT NULL COMMENT 'IP спрашивающего',
  `is_visible` tinyint(1) DEFAULT NULL COMMENT 'Видимость',
  `attachment` int(8) DEFAULT NULL COMMENT 'Приложение',
  PRIMARY KEY (`id_consultation_ask`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Вопрос';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pr_consultation_ask`
--

LOCK TABLES `pr_consultation_ask` WRITE;
/*!40000 ALTER TABLE `pr_consultation_ask` DISABLE KEYS */;
/*!40000 ALTER TABLE `pr_consultation_ask` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pr_consultation_ask_specialization`
--

DROP TABLE IF EXISTS `pr_consultation_ask_specialization`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pr_consultation_ask_specialization` (
  `id_consultation_ask` int(8) NOT NULL,
  `id_consultation_specialization` int(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pr_consultation_ask_specialization`
--

LOCK TABLES `pr_consultation_ask_specialization` WRITE;
/*!40000 ALTER TABLE `pr_consultation_ask_specialization` DISABLE KEYS */;
/*!40000 ALTER TABLE `pr_consultation_ask_specialization` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pr_consultation_specialization`
--

DROP TABLE IF EXISTS `pr_consultation_specialization`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pr_consultation_specialization` (
  `id_consultation_specialization` int(8) NOT NULL COMMENT 'id',
  `specialization` varchar(255) NOT NULL COMMENT 'Специализация',
  `description` text COMMENT 'Описание',
  PRIMARY KEY (`id_consultation_specialization`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Специализация отвечающего';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pr_consultation_specialization`
--

LOCK TABLES `pr_consultation_specialization` WRITE;
/*!40000 ALTER TABLE `pr_consultation_specialization` DISABLE KEYS */;
/*!40000 ALTER TABLE `pr_consultation_specialization` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pr_feedback`
--

DROP TABLE IF EXISTS `pr_feedback`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pr_feedback` (
  `id_feedback` int(8) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `fio` varchar(255) NOT NULL COMMENT 'ФИО',
  `phone` varchar(255) DEFAULT NULL COMMENT 'Телефон',
  `mail` varchar(255) DEFAULT NULL COMMENT 'e-mail',
  `message` longtext NOT NULL COMMENT 'Сообщение',
  `date` int(10) NOT NULL COMMENT 'Дата сообщения',
  `ip` varchar(255) NOT NULL COMMENT 'ip',
  PRIMARY KEY (`id_feedback`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Обратная связь';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pr_feedback`
--

LOCK TABLES `pr_feedback` WRITE;
/*!40000 ALTER TABLE `pr_feedback` DISABLE KEYS */;
/*!40000 ALTER TABLE `pr_feedback` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pr_invoice`
--

DROP TABLE IF EXISTS `pr_invoice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pr_invoice` (
  `id_invoice` int(8) NOT NULL AUTO_INCREMENT,
  `create_date` int(10) unsigned NOT NULL COMMENT 'Дата создания',
  `pay_date` int(10) unsigned DEFAULT NULL COMMENT 'Дата оплаты',
  `amount` int(8) NOT NULL COMMENT 'Сумма',
  `id_offer` int(8) NOT NULL COMMENT 'Заказ',
  PRIMARY KEY (`id_invoice`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Счета';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pr_invoice`
--

LOCK TABLES `pr_invoice` WRITE;
/*!40000 ALTER TABLE `pr_invoice` DISABLE KEYS */;
/*!40000 ALTER TABLE `pr_invoice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pr_link_offer_product`
--

DROP TABLE IF EXISTS `pr_link_offer_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pr_link_offer_product` (
  `id_offer` int(8) NOT NULL,
  `id_product` int(8) NOT NULL,
  `amount` int(11) NOT NULL,
  PRIMARY KEY (`id_offer`,`id_product`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pr_link_offer_product`
--

LOCK TABLES `pr_link_offer_product` WRITE;
/*!40000 ALTER TABLE `pr_link_offer_product` DISABLE KEYS */;
/*!40000 ALTER TABLE `pr_link_offer_product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pr_news`
--

DROP TABLE IF EXISTS `pr_news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pr_news` (
  `id_news` int(8) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `title` varchar(255) NOT NULL COMMENT 'Заголовок',
  `date` int(10) unsigned NOT NULL COMMENT 'Дата',
  `id_news_category` int(8) DEFAULT NULL COMMENT 'Категория',
  `short` text COMMENT 'Краткое содержание',
  `content` text NOT NULL COMMENT 'Содержание',
  `photo` int(8) DEFAULT NULL COMMENT 'Картинка',
  `is_visible` tinyint(1) DEFAULT '1' COMMENT 'Видимость',
  PRIMARY KEY (`id_news`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='Новости';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pr_news`
--

LOCK TABLES `pr_news` WRITE;
/*!40000 ALTER TABLE `pr_news` DISABLE KEYS */;
INSERT INTO `pr_news` VALUES (1,'Первый раз вода в подарок',1416814932,2,'Вы у нас в первый раз? Мы Вам рады!','<table border=\"0\" style=\"border-collapse: collapse; color: #6a7f95;\">\r\n<tbody>\r\n<tr>\r\n<td align=\"center\" style=\"padding: 5px; border: none;\"><span style=\"color: #003366;\"><span style=\"color: #003366;\"><span style=\"color: #003366;\">Вы у нас в первый раз? Мы Вам рады!<br>Первый раз мы привезем Вам 19 л. чистой и полезной \"Краснозатонской\" <br><span style=\"color: #003366;\">В ПОДАРОК!</span></span><span style=\"color: #003366;\">*</span><br></span></span></td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p align=\"center\" style=\"color: #6a7f95;\"><span style=\"color: #003366;\"><br>*При покупке подлежащей обмену бутыли. Специальная поликарбонатная бутыль выкупается 1 раз. Затем мы будем привозить Вам свежую воду, и забирать бутыль на обмен.<br>Акция действует в городах Сыктывкар и Ухта.</span></p>',519,1),(2,'Вкусная и полезная \"Краснозатонская Серебряная\" с обновленной этикеткой!',1416814992,1,'Дорогие друзья! \r\n6 ноября мы запустили в продажу часть бутылок с водой 19 л \"Краснозатонская Серебряная\" с новыми этикетками. ','<p style=\"color: #6a7f95; text-align: center;\"><strong><span style=\"color: #003366;\" data-mce-mark=\"1\">Дорогие друзья! </span></strong></p>\r\n<p style=\"color: #6a7f95;\"><br><span style=\"color: #003366;\" data-mce-mark=\"1\">6 ноября мы запустили в продажу часть бутылок с водой 19 л \"Краснозатонская Серебряная\" с новыми этикетками. <br>Постепенно все старые этикетки будут заменены новыми, в том числе и на маленьких бутылках в ПЭТ-таре, которые вы сможете приобрести в любом магазине.<br><img alt=\"этикетка 4,8х15см\" src=\"http://ecoline-komi.ru/d/286213/d/%D1%8D%D1%82%D0%B8%D0%BA%D0%B5%D1%82%D0%BA%D0%B0_4,8%D1%8515%D1%81%D0%BC.jpg\" title=\"\" style=\"display: block; margin-left: auto; margin-right: auto;\"><br></span></p>\r\n<p style=\"color: #6a7f95; text-align: justify;\"><span style=\"color: #003366;\" data-mce-mark=\"1\">С новой этикеткой наш продукт выглядит <strong>более современным и ярким</strong>. А Вы легко отличите нашу </span><strong><span data-mce-mark=\"1\"><span style=\"color: #003366;\" data-mce-mark=\"1\"><a href=\"http://ecoline-komi.ru/choose_highest_quality_water\" style=\"color: #008acc; outline-style: none;\"><span style=\"color: #003366;\" data-mce-mark=\"1\">питьевую воду высшей категории</span></a></span> </span></strong><span style=\"color: #003366;\" data-mce-mark=\"1\">от других вод, большинство из которых более низкого качества - воды питьевые первой категории или воды питьевые столовые.<br></span></p>\r\n<p style=\"color: #6a7f95;\"><span style=\"color: #003366;\">Изменилась только этикетка, а в бутылках по-прежнему Ваша любимая вкусная и полезная вода «Краснозатонская Серебряная» производства Компании «Эколайн».<br></span></p>',526,1);
/*!40000 ALTER TABLE `pr_news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pr_news_category`
--

DROP TABLE IF EXISTS `pr_news_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pr_news_category` (
  `id_news_category` int(8) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(255) NOT NULL COMMENT 'Название',
  `seq` int(8) NOT NULL DEFAULT '1' COMMENT 'п/п',
  `is_visible` tinyint(1) DEFAULT '1' COMMENT 'Видимость',
  PRIMARY KEY (`id_news_category`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='Категории новостей';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pr_news_category`
--

LOCK TABLES `pr_news_category` WRITE;
/*!40000 ALTER TABLE `pr_news_category` DISABLE KEYS */;
INSERT INTO `pr_news_category` VALUES (1,'Новости',1,1),(2,'Акции',2,1);
/*!40000 ALTER TABLE `pr_news_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pr_offer`
--

DROP TABLE IF EXISTS `pr_offer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pr_offer` (
  `id_offer` int(8) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `fio` varchar(255) NOT NULL COMMENT 'ФИО',
  `phone` varchar(255) NOT NULL COMMENT 'Телефон',
  `mail` varchar(255) DEFAULT NULL COMMENT 'e-mail',
  `comment` longtext COMMENT 'Пожелания',
  `offer_text` longtext COMMENT 'Заказ',
  `create_date` int(10) NOT NULL COMMENT 'Дата заказа',
  `is_process` tinyint(1) DEFAULT NULL COMMENT 'Обработано',
  `ip` varchar(255) NOT NULL COMMENT 'ip',
  `is_send` tinyint(1) DEFAULT NULL COMMENT 'Отправлено ли уведомление',
  `status` int(8) NOT NULL COMMENT 'Статус',
  `amount` int(8) NOT NULL COMMENT 'Сумма',
  `id_invoice` int(8) DEFAULT NULL COMMENT 'Счет',
  `company` varchar(255) DEFAULT NULL COMMENT 'Компания',
  `address` varchar(255) DEFAULT NULL COMMENT 'Адрес',
  `client` varchar(255) DEFAULT NULL COMMENT 'Клиентский номер',
  `date_delivery` int(10) unsigned NOT NULL COMMENT 'Дата доставки',
  `first` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Впервые',
  PRIMARY KEY (`id_offer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Заказы пользователей';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pr_offer`
--

LOCK TABLES `pr_offer` WRITE;
/*!40000 ALTER TABLE `pr_offer` DISABLE KEYS */;
/*!40000 ALTER TABLE `pr_offer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pr_order`
--

DROP TABLE IF EXISTS `pr_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pr_order` (
  `id_order` int(8) NOT NULL AUTO_INCREMENT,
  `fio` varchar(255) DEFAULT NULL COMMENT 'ФИО',
  `phone` varchar(255) DEFAULT NULL COMMENT 'Телефон',
  `address` varchar(255) DEFAULT NULL COMMENT 'Адрес',
  `email` varchar(255) DEFAULT NULL COMMENT 'email',
  `convenient_time` varchar(255) DEFAULT NULL COMMENT 'Удобное время доставки',
  `subscriber_number` varchar(255) DEFAULT NULL COMMENT 'Абонентский номер',
  `description` longtext COMMENT 'Примечание',
  `company` varchar(255) DEFAULT NULL COMMENT 'Компания',
  `date` int(10) unsigned DEFAULT NULL COMMENT 'Дата',
  PRIMARY KEY (`id_order`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='Заказ';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pr_order`
--

LOCK TABLES `pr_order` WRITE;
/*!40000 ALTER TABLE `pr_order` DISABLE KEYS */;
INSERT INTO `pr_order` VALUES (2,'ewre','g34g3','43t23','4g34@eF.TU','cq35g','r24f','',NULL,1417784250),(3,'Александр','1231242351','wefv, 123','cranky4.89@gmail.com','sv23','1234','',NULL,1417788598),(4,'Александр','345345346','v3v','cranky4.89@gmail.com','d g','346345','',NULL,1417788685),(5,'Александр','r23r2','3r23r2','cranky4.89@gmail.com','r23r','2143r2','',NULL,1417788787),(6,'Александр','r24234523','234q2c3','cranky4.89@gmail.com','c23','1234','c2',NULL,1417789055),(7,'Александр','g34g3','4g34g3','cranky4.89@gmail.com','cq35g','1234','43',NULL,1417789305),(8,'Александр','23cr2','3c232','cranky4.89@gmail.com','2r23r','2rc3r','',NULL,1417789493),(9,'Александр23','c23c23c','42c3','cranky4.89@gmail.com','c234c','12c413c','2314c2',NULL,1417789612);
/*!40000 ALTER TABLE `pr_order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pr_photogallery`
--

DROP TABLE IF EXISTS `pr_photogallery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pr_photogallery` (
  `id_photogallery` int(8) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(255) NOT NULL COMMENT 'Название',
  `text_in_gallery` text COMMENT 'Текст в галерее',
  `sequence` int(8) DEFAULT NULL COMMENT 'п/п',
  `id_parent` int(8) DEFAULT NULL COMMENT 'Родительский раздел',
  PRIMARY KEY (`id_photogallery`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Фотогалереи';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pr_photogallery`
--

LOCK TABLES `pr_photogallery` WRITE;
/*!40000 ALTER TABLE `pr_photogallery` DISABLE KEYS */;
/*!40000 ALTER TABLE `pr_photogallery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pr_photogallery_photo`
--

DROP TABLE IF EXISTS `pr_photogallery_photo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pr_photogallery_photo` (
  `id_photogallery_photo` int(8) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(255) DEFAULT NULL COMMENT 'Название',
  `id_photogallery_object` varchar(255) NOT NULL COMMENT 'Объект',
  `id_photogallery_instance` int(8) NOT NULL COMMENT 'Экземпляр-фотогалерея',
  `file` int(8) NOT NULL COMMENT 'Файл',
  `sequence` int(8) DEFAULT NULL COMMENT 'п/п',
  PRIMARY KEY (`id_photogallery_photo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Фотографии';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pr_photogallery_photo`
--

LOCK TABLES `pr_photogallery_photo` WRITE;
/*!40000 ALTER TABLE `pr_photogallery_photo` DISABLE KEYS */;
/*!40000 ALTER TABLE `pr_photogallery_photo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pr_product`
--

DROP TABLE IF EXISTS `pr_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pr_product` (
  `id_product` int(8) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `id_product_category` int(8) NOT NULL COMMENT 'Каталог продукции',
  `code` varchar(255) DEFAULT NULL COMMENT 'Артикул',
  `name` varchar(255) NOT NULL COMMENT 'Название',
  `trade_price` decimal(8,2) NOT NULL COMMENT 'Оптовая цена',
  `sm_trade_price` decimal(8,2) NOT NULL COMMENT 'Мал. оптовая цена',
  `retail_price` decimal(8,2) NOT NULL COMMENT 'Розничная цена',
  `unit` varchar(255) DEFAULT NULL COMMENT 'Единица измерения',
  `quanList` varchar(255) DEFAULT NULL,
  `remain` varchar(255) DEFAULT NULL COMMENT 'Остаток',
  `description` longtext COMMENT 'Описание товара',
  `deleted` tinyint(1) NOT NULL COMMENT 'Удален',
  `image` int(8) DEFAULT NULL COMMENT 'Изображение',
  `properties` longtext COMMENT 'Характеристики',
  `additional_desc` longtext COMMENT 'Монтаж',
  `visible` tinyint(1) DEFAULT '1' COMMENT 'Видимость',
  `id_brand` int(8) DEFAULT NULL COMMENT 'Брэнд',
  `video` longtext COMMENT 'Видео',
  PRIMARY KEY (`id_product`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Продукция';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pr_product`
--

LOCK TABLES `pr_product` WRITE;
/*!40000 ALTER TABLE `pr_product` DISABLE KEYS */;
INSERT INTO `pr_product` VALUES (1,1,NULL,'Краснозатонская Серебряная',150.00,150.00,170.00,'шт',NULL,NULL,'Чистая питьевая артезианская вода высшей  категории. Вода высшей категории отвечает не только критериям безопасности, но и полезности, так как содержит сбалансированное количество основных биогенных элементов (таких как кальций, магний, калий и фтор) необходимых человеку.\r\n\r\n\"Краснозатонская Серебряная\"разливается в бутыли0,5, 1,5, 5, 10 и 19 литров. Вода в бутылях объемом 19 л доставляется на дом, в офис, в школы, детские сады, учреждения здравоохранения и т. д.\r\n\r\nводу в бутылках 10 л, 5л, 1,5 л, 0,5 л Вы найдете в продуктовых магазинах Сыктывкара, Ухты и близлежащих районах',0,517,'','',1,NULL,'');
/*!40000 ALTER TABLE `pr_product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pr_product_brand`
--

DROP TABLE IF EXISTS `pr_product_brand`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pr_product_brand` (
  `id_brand` int(8) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(255) NOT NULL COMMENT 'Название',
  `id_parent` int(8) DEFAULT NULL COMMENT 'Родительский брэнд',
  `image` int(8) DEFAULT NULL COMMENT 'Логотип брэнда',
  `sequence` int(8) DEFAULT NULL COMMENT 'п/п',
  PRIMARY KEY (`id_brand`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Брэнды';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pr_product_brand`
--

LOCK TABLES `pr_product_brand` WRITE;
/*!40000 ALTER TABLE `pr_product_brand` DISABLE KEYS */;
/*!40000 ALTER TABLE `pr_product_brand` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pr_product_category`
--

DROP TABLE IF EXISTS `pr_product_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pr_product_category` (
  `id_product_category` int(8) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(255) NOT NULL COMMENT 'Название',
  `id_parent` int(8) DEFAULT NULL COMMENT 'Родитель',
  `image` int(8) DEFAULT NULL COMMENT 'Изображение',
  `price_markup` int(8) NOT NULL DEFAULT '0' COMMENT 'Наценка',
  `sequence` int(8) DEFAULT NULL COMMENT 'п/п',
  `visible` tinyint(1) DEFAULT '1' COMMENT 'Видимость',
  PRIMARY KEY (`id_product_category`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Категории продукции';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pr_product_category`
--

LOCK TABLES `pr_product_category` WRITE;
/*!40000 ALTER TABLE `pr_product_category` DISABLE KEYS */;
INSERT INTO `pr_product_category` VALUES (1,'Вода',NULL,529,0,1,1);
/*!40000 ALTER TABLE `pr_product_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pr_question`
--

DROP TABLE IF EXISTS `pr_question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pr_question` (
  `id_question` int(8) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(255) NOT NULL COMMENT 'Спрашивающий',
  `email` varchar(255) DEFAULT NULL COMMENT 'E-mail',
  `ask_date` int(10) NOT NULL COMMENT 'Дата',
  `question` longtext NOT NULL COMMENT 'Вопрос',
  `answer` text COMMENT 'Ответ',
  `visible` tinyint(1) DEFAULT NULL COMMENT 'Видимость',
  `ip` varchar(100) NOT NULL,
  `category` int(8) NOT NULL DEFAULT '1' COMMENT 'Категория',
  `send` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Отправить ответ на email',
  PRIMARY KEY (`id_question`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='Вопрос-ответ';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pr_question`
--

LOCK TABLES `pr_question` WRITE;
/*!40000 ALTER TABLE `pr_question` DISABLE KEYS */;
INSERT INTO `pr_question` VALUES (2,'Пантелей','vthjrk@mail.ru',1417778988,'Сколько стоит 5 литров воды?',NULL,0,'192.168.0.173',1,0);
/*!40000 ALTER TABLE `pr_question` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pr_quiz`
--

DROP TABLE IF EXISTS `pr_quiz`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pr_quiz` (
  `id_quiz` int(8) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(255) NOT NULL COMMENT 'name',
  `description` text COMMENT 'description',
  `active` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'active',
  PRIMARY KEY (`id_quiz`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Викторины';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pr_quiz`
--

LOCK TABLES `pr_quiz` WRITE;
/*!40000 ALTER TABLE `pr_quiz` DISABLE KEYS */;
/*!40000 ALTER TABLE `pr_quiz` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pr_quiz_answer`
--

DROP TABLE IF EXISTS `pr_quiz_answer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pr_quiz_answer` (
  `id_quiz_answer` int(8) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `id_quiz_question` int(8) NOT NULL COMMENT 'Вопрос',
  `answer` varchar(255) NOT NULL COMMENT 'Текст ответа',
  `is_right` tinyint(1) DEFAULT NULL COMMENT 'Правильный ли ответ',
  `sequence` int(8) NOT NULL DEFAULT '1' COMMENT 'п/п',
  PRIMARY KEY (`id_quiz_answer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Варианты ответов';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pr_quiz_answer`
--

LOCK TABLES `pr_quiz_answer` WRITE;
/*!40000 ALTER TABLE `pr_quiz_answer` DISABLE KEYS */;
/*!40000 ALTER TABLE `pr_quiz_answer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pr_quiz_answer_user`
--

DROP TABLE IF EXISTS `pr_quiz_answer_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pr_quiz_answer_user` (
  `id_quiz_answer_user` int(8) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `id_quiz` int(8) NOT NULL COMMENT 'Викторина',
  `name` varchar(255) NOT NULL COMMENT 'ФИО',
  `mail` varchar(255) NOT NULL COMMENT 'mail',
  `library_card` varchar(255) DEFAULT NULL COMMENT 'Читательский билет',
  `contact` varchar(255) DEFAULT NULL COMMENT 'Контактная информация',
  `answer` text NOT NULL COMMENT 'Ответ',
  `create_date` int(10) unsigned NOT NULL COMMENT 'Дата',
  `ip` varchar(255) NOT NULL COMMENT 'ip',
  PRIMARY KEY (`id_quiz_answer_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Ответ пользователя';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pr_quiz_answer_user`
--

LOCK TABLES `pr_quiz_answer_user` WRITE;
/*!40000 ALTER TABLE `pr_quiz_answer_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `pr_quiz_answer_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pr_quiz_question`
--

DROP TABLE IF EXISTS `pr_quiz_question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pr_quiz_question` (
  `id_quiz_question` int(8) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `id_quiz` int(8) NOT NULL COMMENT 'Викторина',
  `question` text NOT NULL COMMENT 'Текст вопроса',
  `type` int(8) NOT NULL DEFAULT '1' COMMENT 'Тип ответов',
  `sequence` int(8) NOT NULL DEFAULT '1' COMMENT 'п/п',
  PRIMARY KEY (`id_quiz_question`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Вопросы викторины';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pr_quiz_question`
--

LOCK TABLES `pr_quiz_question` WRITE;
/*!40000 ALTER TABLE `pr_quiz_question` DISABLE KEYS */;
/*!40000 ALTER TABLE `pr_quiz_question` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pr_remain_status`
--

DROP TABLE IF EXISTS `pr_remain_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pr_remain_status` (
  `id_remain_status` int(8) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(255) NOT NULL COMMENT 'Название',
  `min_value` int(8) NOT NULL COMMENT 'Мин. значение по-умолчанию',
  `max_value` int(8) NOT NULL COMMENT 'Макс. значение по-умолчанию',
  `icon` varchar(255) DEFAULT NULL COMMENT 'Иконка',
  PRIMARY KEY (`id_remain_status`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='Статусы остатка продукции';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pr_remain_status`
--

LOCK TABLES `pr_remain_status` WRITE;
/*!40000 ALTER TABLE `pr_remain_status` DISABLE KEYS */;
INSERT INTO `pr_remain_status` VALUES (1,'под заказ',-999999999,0,NULL),(2,'последняя штука',0,1,'icon-red'),(3,'мало',1,5,'icon-yellow'),(4,'средне',5,10,NULL),(5,'много',10,999999999,'icon-green');
/*!40000 ALTER TABLE `pr_remain_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pr_vitrine`
--

DROP TABLE IF EXISTS `pr_vitrine`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pr_vitrine` (
  `id_vitrine` int(8) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `link` varchar(255) DEFAULT NULL COMMENT 'Ссылка на переход',
  `title` varchar(255) DEFAULT NULL COMMENT 'Заголовок',
  `text` longtext COMMENT 'Дополнительный текст',
  `image` int(8) DEFAULT NULL COMMENT 'Фото',
  `sequence` int(8) DEFAULT NULL COMMENT 'п/п',
  PRIMARY KEY (`id_vitrine`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='Витрина';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pr_vitrine`
--

LOCK TABLES `pr_vitrine` WRITE;
/*!40000 ALTER TABLE `pr_vitrine` DISABLE KEYS */;
INSERT INTO `pr_vitrine` VALUES (4,NULL,NULL,'',505,1),(7,NULL,NULL,NULL,511,2),(8,NULL,NULL,NULL,513,3),(9,NULL,NULL,NULL,515,4);
/*!40000 ALTER TABLE `pr_vitrine` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pr_voting`
--

DROP TABLE IF EXISTS `pr_voting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pr_voting` (
  `id_voting` int(8) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(60) NOT NULL COMMENT 'Название голосования',
  `create_date` int(10) NOT NULL DEFAULT '0' COMMENT 'Дата создания голосования',
  `is_active` int(1) NOT NULL DEFAULT '1' COMMENT 'Активное',
  `is_checkbox` int(1) NOT NULL DEFAULT '0' COMMENT 'Множество ответов',
  `in_module` int(1) NOT NULL DEFAULT '1' COMMENT 'Показать в модуле',
  PRIMARY KEY (`id_voting`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Голосование';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pr_voting`
--

LOCK TABLES `pr_voting` WRITE;
/*!40000 ALTER TABLE `pr_voting` DISABLE KEYS */;
/*!40000 ALTER TABLE `pr_voting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pr_voting_answer`
--

DROP TABLE IF EXISTS `pr_voting_answer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pr_voting_answer` (
  `id_voting_answer` int(8) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `id_voting` int(8) NOT NULL DEFAULT '0' COMMENT 'Голосование',
  `name` varchar(60) NOT NULL COMMENT 'Вариант ответа',
  `count` int(8) NOT NULL DEFAULT '0' COMMENT 'Количество голосов',
  PRIMARY KEY (`id_voting_answer`,`id_voting`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Ответы на голосование';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pr_voting_answer`
--

LOCK TABLES `pr_voting_answer` WRITE;
/*!40000 ALTER TABLE `pr_voting_answer` DISABLE KEYS */;
/*!40000 ALTER TABLE `pr_voting_answer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pr_water`
--

DROP TABLE IF EXISTS `pr_water`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pr_water` (
  `id_water` int(8) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL COMMENT 'Название',
  `short` longtext COMMENT 'Краткое описание',
  `photo` int(8) DEFAULT NULL COMMENT 'Фото',
  `price` varchar(255) DEFAULT NULL COMMENT 'Цена',
  `sequence` int(8) DEFAULT NULL COMMENT 'п/п',
  `visible` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Видимость',
  PRIMARY KEY (`id_water`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='Вид воды';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pr_water`
--

LOCK TABLES `pr_water` WRITE;
/*!40000 ALTER TABLE `pr_water` DISABLE KEYS */;
INSERT INTO `pr_water` VALUES (1,'Краснозатонская Серебряная','Чистая питьевая артезианская вода высшей  категории. Вода высшей категории отвечает не только критериям безопасности, но и полезности, так как содержит сбалансированное количество основных биогенных элементов (таких как кальций, магний, калий и фтор) необходимых человеку.\r\n\r\n\"Краснозатонская Серебряная\"разливается в бутыли0,5, 1,5, 5, 10 и 19 литров. Вода в бутылях объемом 19 л доставляется на дом, в офис, в школы, детские сады, учреждения здравоохранения и т. д.',604,'170 рублей',1,1),(2,'Краснозатонская Йодированная','Чистая питьевая артезианская вода высшей категории, обогащенная йодом. Йод в воде находится в виде ионов, поэтому практически полностью усваивается организмом человека и не разрушается при кипячении.\r\n\r\n\"Краснозатонская Йодированная разливается в бутыли 19 литров. Вода в бутылях объемом 19 л доставляется на дом, в офисы, в школы, детские сады, учреждения здравоохранения и т д.',606,'170 рублей',2,1),(3,'Сереговская Минеральная\"','Вода \"Сереговская Минеральная\" производится на основе рассола из скважины глубиной почти 500 метров курорта Серегово по рецептам Института курортологии (г.Москва). ',NULL,'1',3,0);
/*!40000 ALTER TABLE `pr_water` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pr_water_count`
--

DROP TABLE IF EXISTS `pr_water_count`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pr_water_count` (
  `id_water_count` int(8) NOT NULL AUTO_INCREMENT,
  `id_order` int(8) NOT NULL COMMENT 'Заказ',
  `count` int(8) NOT NULL DEFAULT '0' COMMENT 'Количество',
  `id_water` int(8) NOT NULL COMMENT 'Вода',
  PRIMARY KEY (`id_water_count`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COMMENT='Количество воды';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pr_water_count`
--

LOCK TABLES `pr_water_count` WRITE;
/*!40000 ALTER TABLE `pr_water_count` DISABLE KEYS */;
INSERT INTO `pr_water_count` VALUES (4,2,15,1),(5,2,17,2),(6,2,9,3),(7,3,5,1),(8,3,2,2),(9,3,15,3),(10,4,6,1),(11,5,4,2),(12,6,2,1),(13,7,4,2),(14,8,4,3),(15,9,4,1);
/*!40000 ALTER TABLE `pr_water_count` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-12-11 10:28:19
